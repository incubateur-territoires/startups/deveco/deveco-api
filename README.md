# deveco-api

## database

start database

```sh
yarn db:start
```

migrate database

```sh
yarn db:migrate
```

insert open data: `commune`, `epci`, `naf`

```sh
yarn db:seed
```

seed dev data

```sh
yarn db:seed-dev
```

seed SIRENE data (you need to have the deveco-sirene project somewhere)

```sh
cd /PATH/TO/deveco-sirene
docker-compose run sirene
```

## api

start api

```sh
yarn dev
```

## migration

generate a migration file

```sh
yarn typeorm migration:generate ./src/migration/{MIGRATION_NAME} -d ./src/data-source.ts
```

## test

```sh
curl -d '{"email":"deveco@ccd.fr"}' -H "Content-Type: application/json" -X POST http://localhost:4000/auth/login

curl -H "Content-Type: application/json" -X POST http://localhost:4000/auth/jwt/${accessKey}
```
