FROM node:16 AS base
ADD . /app
WORKDIR /app

FROM base as prod-install
WORKDIR /app
RUN yarn install --production --immutable --immutable-cache --check-cache

FROM base as prod-build
WORKDIR /app
COPY --from=prod-install /app/node_modules /app/node_modules
RUN yarn install --immutable --immutable-cache --check-cache
RUN yarn build

FROM gcr.io/distroless/nodejs16-debian11
WORKDIR /app
COPY --from=base /app /app
COPY --from=prod-install /app/node_modules /app/node_modules
COPY --from=prod-build /app/dist /app/dist

EXPOSE 3000

ENV API_HOST=0.0.0.0
ENV API_PORT=3000
ENV IS_BUILT=true

CMD [ "dist/index.js" ]
