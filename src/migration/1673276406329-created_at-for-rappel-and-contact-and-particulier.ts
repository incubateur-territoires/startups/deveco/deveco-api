import { MigrationInterface, QueryRunner } from "typeorm";

export class createdAtForRappelAndContactAndParticulier1673276406329 implements MigrationInterface {
    name = 'createdAtForRappelAndContactAndParticulier1673276406329'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "particulier" ADD "created_at" TIMESTAMP NOT NULL DEFAULT '1970-01-01'`);
        await queryRunner.query(`ALTER TABLE "contact" ADD "created_at" TIMESTAMP NOT NULL DEFAULT '1970-01-01'`);
        await queryRunner.query(`ALTER TABLE "rappel" ADD "created_at" TIMESTAMP NOT NULL DEFAULT '1970-01-01'`);
        await queryRunner.query(`UPDATE rappel SET created_at = fiche.created_at FROM fiche WHERE rappel.fiche_id = fiche.id`);
        await queryRunner.query(`UPDATE contact SET created_at = entite.created_at FROM entite WHERE contact.entite_id = entite.id`);
        await queryRunner.query(`UPDATE particulier SET created_at = entite.created_at FROM entite WHERE entite.particulier_id = particulier.id`);
        await queryRunner.query(`UPDATE particulier SET created_at = contact.created_at FROM contact WHERE contact.particulier_id = particulier.id`);
        await queryRunner.query(`ALTER TABLE "particulier" ALTER COLUMN "created_at" SET DEFAULT now()`);
        await queryRunner.query(`ALTER TABLE "contact" ALTER COLUMN "created_at" SET DEFAULT now()`);
        await queryRunner.query(`ALTER TABLE "rappel" ALTER COLUMN "created_at" SET DEFAULT now()`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "rappel" DROP COLUMN "created_at"`);
        await queryRunner.query(`ALTER TABLE "contact" DROP COLUMN "created_at"`);
        await queryRunner.query(`ALTER TABLE "particulier" DROP COLUMN "created_at"`);
    }

}
