import { MigrationInterface, QueryRunner } from "typeorm";

export class addAllTheIndexesInTER1673538367790 implements MigrationInterface {
    name = 'addAllTheIndexesInTER1673538367790'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE INDEX "IDX_e527e275d106ddfc5ef3c21748" ON "territory_etablissement_reference" ("etat_administratif") `);
        await queryRunner.query(`CREATE INDEX "IDX_0039cbad2853320b4fd583e4b5" ON "territory_etablissement_reference" ("code_naf") `);
        await queryRunner.query(`CREATE INDEX "IDX_3ea089a794b188f7e134ca8568" ON "territory_etablissement_reference" ("categorie_juridique_courante") `);
        await queryRunner.query(`CREATE INDEX "IDX_82c5ebc5d3502d186e1b067720" ON "territory_etablissement_reference" ("tranche_effectifs") `);
        await queryRunner.query(`CREATE INDEX "IDX_46450007babfb5469a8ba087fe" ON "territory_etablissement_reference" ("code_commune") `);
        await queryRunner.query(`CREATE INDEX "IDX_6380a9fbe51fbb28fd551dce9b" ON "territory_etablissement_reference" ("date_creation") `);
        await queryRunner.query(`CREATE INDEX "IDX_6b13c91aafba64ae7947743493" ON "territory_etablissement_reference" ("exogene") `);
        await queryRunner.query(`CREATE INDEX "IDX_2877b64700f95ecdb02c18659e" ON "territory_etablissement_reference" ("qpv_id") `);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`DROP INDEX "public"."IDX_2877b64700f95ecdb02c18659e"`);
        await queryRunner.query(`DROP INDEX "public"."IDX_6b13c91aafba64ae7947743493"`);
        await queryRunner.query(`DROP INDEX "public"."IDX_6380a9fbe51fbb28fd551dce9b"`);
        await queryRunner.query(`DROP INDEX "public"."IDX_46450007babfb5469a8ba087fe"`);
        await queryRunner.query(`DROP INDEX "public"."IDX_82c5ebc5d3502d186e1b067720"`);
        await queryRunner.query(`DROP INDEX "public"."IDX_3ea089a794b188f7e134ca8568"`);
        await queryRunner.query(`DROP INDEX "public"."IDX_0039cbad2853320b4fd583e4b5"`);
        await queryRunner.query(`DROP INDEX "public"."IDX_e527e275d106ddfc5ef3c21748"`);
    }

}
