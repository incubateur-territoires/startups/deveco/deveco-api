import { MigrationInterface, QueryRunner } from "typeorm";

export class epciCommune1664660088040 implements MigrationInterface {
    name = 'epciCommune1664660088040'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "commune" ADD "insee_epci" character varying`);
        await queryRunner.query(`ALTER TABLE "commune" ADD CONSTRAINT "FK_a092be262e98323ea1ac1c954f0" FOREIGN KEY ("insee_epci") REFERENCES "epci"("insee_epci") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`UPDATE commune SET insee_epci = epci_commune.insee_epci FROM epci_commune WHERE epci_commune.insee_com = commune.insee_com`);
        await queryRunner.query(`UPDATE commune SET insee_epci = epci_commune.insee_epci FROM epci_commune WHERE epci_commune.insee_com = commune.com_parent`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "commune" DROP CONSTRAINT "FK_a092be262e98323ea1ac1c954f0"`);
        await queryRunner.query(`ALTER TABLE "commune" DROP COLUMN "insee_epci"`);
    }

}
