import { MigrationInterface, QueryRunner } from "typeorm";

export class qpvInTer1672061000505 implements MigrationInterface {
    name = 'qpvInTer1672061000505'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "territory_etablissement_reference" ADD "qpv_id" integer`);
        await queryRunner.query(`ALTER TABLE "territory_etablissement_reference" ADD CONSTRAINT "FK_2877b64700f95ecdb02c18659e2" FOREIGN KEY ("qpv_id") REFERENCES "qp_metropoleoutremer_wgs84_epsg4326"("gid") ON DELETE NO ACTION ON UPDATE NO ACTION`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "territory_etablissement_reference" DROP CONSTRAINT "FK_2877b64700f95ecdb02c18659e2"`);
        await queryRunner.query(`ALTER TABLE "territory_etablissement_reference" DROP COLUMN "qpv_id"`);
    }

}
