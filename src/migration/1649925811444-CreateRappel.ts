import {MigrationInterface, QueryRunner} from "typeorm";

export class CreateRappel1649925811444 implements MigrationInterface {
    name = 'CreateRappel1649925811444'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE TABLE "rappel" ("id" SERIAL NOT NULL, "date_rappel" date NOT NULL, "titre" character varying NOT NULL, "fiche_id" integer, "createur_id" integer, CONSTRAINT "PK_27615be0e00385c0be0cb5610ea" PRIMARY KEY ("id"))`);
        await queryRunner.query(`ALTER TABLE "rappel" ADD CONSTRAINT "FK_0383d1ba9fe54849cc1a1db8083" FOREIGN KEY ("fiche_id") REFERENCES "fiche"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "rappel" ADD CONSTRAINT "FK_12d759b038600073e7fed16fb1f" FOREIGN KEY ("createur_id") REFERENCES "deveco"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "rappel" DROP CONSTRAINT "FK_12d759b038600073e7fed16fb1f"`);
        await queryRunner.query(`ALTER TABLE "rappel" DROP CONSTRAINT "FK_0383d1ba9fe54849cc1a1db8083"`);
        await queryRunner.query(`DROP TABLE "rappel"`);
    }

}
