import {MigrationInterface, QueryRunner} from "typeorm";

export class suiviConnexions1657196348928 implements MigrationInterface {
    name = 'suiviConnexions1657196348928'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE TABLE "connexion" ("id" SERIAL NOT NULL, "created_at" TIMESTAMP NOT NULL DEFAULT now(), "account_id" integer, CONSTRAINT "PK_828febbf31710f1778292f3465b" PRIMARY KEY ("id"))`);
        await queryRunner.query(`ALTER TABLE "connexion" ADD CONSTRAINT "FK_fb7a011c92438dd38ad957514a8" FOREIGN KEY ("account_id") REFERENCES "account"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "connexion" DROP CONSTRAINT "FK_fb7a011c92438dd38ad957514a8"`);
        await queryRunner.query(`DROP TABLE "connexion"`);
    }

}
