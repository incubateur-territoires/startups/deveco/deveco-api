import { MigrationInterface, QueryRunner } from "typeorm";

export class unaccentedTerritorySearch1682197621164 implements MigrationInterface {
    name = 'unaccentedTerritorySearch1682197621164'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE EXTENSION IF NOT EXISTS unaccent`);
        await queryRunner.query(`CREATE INDEX "IDX_commune_lib_com" ON "commune" ("lib_com")`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`DROP INDEX "public"."IDX_commune_lib_com"`);
        await queryRunner.query(`DROP EXTENSION IF EXISTS unaccent`);
    }

}
