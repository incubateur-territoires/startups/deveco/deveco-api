import { MigrationInterface, QueryRunner } from "typeorm";

export class UpdateEtablissementLight1659967012933 implements MigrationInterface {
    name = 'UpdateEtablissementLight1659967012933'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "etablissement_light" RENAME COLUMN "etat_administratif_etablissement" TO "etat_administratif"`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "etablissement_light" RENAME COLUMN "etat_administratif" TO "etat_administratif_etablissement"`);
    }

}
