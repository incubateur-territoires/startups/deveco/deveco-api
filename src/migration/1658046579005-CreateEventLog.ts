import { MigrationInterface, QueryRunner } from "typeorm";

export class CreateEventLog1658046579005 implements MigrationInterface {
    name = 'CreateEventLog1658046579005'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE TABLE "event_log" ("id" SERIAL NOT NULL, "date" TIMESTAMP NOT NULL DEFAULT now(), "deveco_id" integer, "fiche_id" integer, CONSTRAINT "PK_d8ccd9b5b44828ea378dd37e691" PRIMARY KEY ("id"))`);
        await queryRunner.query(`ALTER TABLE "deveco" DROP CONSTRAINT "FK_0750ad93125de72fcc1a2b10715"`);
        await queryRunner.query(`ALTER TABLE "deveco" DROP CONSTRAINT "FK_f8e5b14c60e822f91b0312f4688"`);
        await queryRunner.query(`ALTER TABLE "deveco" ALTER COLUMN "account_id" DROP NOT NULL`);
        await queryRunner.query(`ALTER TABLE "deveco" ALTER COLUMN "territory_id" DROP NOT NULL`);
        await queryRunner.query(`ALTER TABLE "deveco" ADD CONSTRAINT "FK_0750ad93125de72fcc1a2b10715" FOREIGN KEY ("account_id") REFERENCES "account"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "deveco" ADD CONSTRAINT "FK_f8e5b14c60e822f91b0312f4688" FOREIGN KEY ("territory_id") REFERENCES "territory"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "event_log" ADD CONSTRAINT "FK_42f72c4a61c08441bf089d5c56d" FOREIGN KEY ("deveco_id") REFERENCES "deveco"("id") ON DELETE CASCADE ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "event_log" ADD CONSTRAINT "FK_afe31166469098b0ab784863b00" FOREIGN KEY ("fiche_id") REFERENCES "fiche"("id") ON DELETE CASCADE ON UPDATE NO ACTION`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "event_log" DROP CONSTRAINT "FK_afe31166469098b0ab784863b00"`);
        await queryRunner.query(`ALTER TABLE "event_log" DROP CONSTRAINT "FK_42f72c4a61c08441bf089d5c56d"`);
        await queryRunner.query(`ALTER TABLE "deveco" DROP CONSTRAINT "FK_f8e5b14c60e822f91b0312f4688"`);
        await queryRunner.query(`ALTER TABLE "deveco" DROP CONSTRAINT "FK_0750ad93125de72fcc1a2b10715"`);
        await queryRunner.query(`ALTER TABLE "deveco" ALTER COLUMN "territory_id" SET NOT NULL`);
        await queryRunner.query(`ALTER TABLE "deveco" ALTER COLUMN "account_id" SET NOT NULL`);
        await queryRunner.query(`ALTER TABLE "deveco" ADD CONSTRAINT "FK_f8e5b14c60e822f91b0312f4688" FOREIGN KEY ("territory_id") REFERENCES "territory"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "deveco" ADD CONSTRAINT "FK_0750ad93125de72fcc1a2b10715" FOREIGN KEY ("account_id") REFERENCES "account"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`DROP TABLE "event_log"`);
    }

}
