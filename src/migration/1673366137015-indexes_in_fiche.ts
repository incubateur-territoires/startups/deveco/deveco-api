import { MigrationInterface, QueryRunner } from "typeorm";

export class indexesInFiche1673366137015 implements MigrationInterface {
    name = 'indexesInFiche1673366137015'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE INDEX "IDX_492ff1c33c8ad967f4bf15f484" ON "fiche" ("entite_id") `);
        await queryRunner.query(`CREATE INDEX "IDX_9dbd4085f66ab645960036b692" ON "fiche" ("territoire_id") `);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`DROP INDEX "public"."IDX_9dbd4085f66ab645960036b692"`);
        await queryRunner.query(`DROP INDEX "public"."IDX_492ff1c33c8ad967f4bf15f484"`);
    }

}
