import { MigrationInterface, QueryRunner } from "typeorm";

export class exogene1665689254948 implements MigrationInterface {
    name = 'exogene1665689254948'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "entite" ADD "exogene" boolean NOT NULL DEFAULT false`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "entite" DROP COLUMN "exogene"`);
    }

}
