import { MigrationInterface, QueryRunner } from "typeorm";

export class metropole1665732669666 implements MigrationInterface {
    name = 'metropole1665732669666'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE TABLE "metropole" ("insee_com" character varying NOT NULL, "lib_com" character varying NOT NULL, CONSTRAINT "PK_e366c7669cccc8a191ff342188b" PRIMARY KEY ("insee_com"))`);
        await queryRunner.query(`ALTER TABLE "territory" ADD "metropole_id" character varying`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "territory" DROP COLUMN "metropole_id"`);
        await queryRunner.query(`DROP TABLE "metropole"`);
    }

}
