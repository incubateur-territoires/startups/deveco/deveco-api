import {MigrationInterface, QueryRunner} from "typeorm";

export class UpdateEntreprise1654522603995 implements MigrationInterface {
    name = 'UpdateTerritory1654522603995'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "territory" ADD CONSTRAINT UNIQ_territory_epci_id UNIQUE (epci_id)`);
        await queryRunner.query(`ALTER TABLE "territory" ADD CONSTRAINT UNIQ_territory_commune_id UNIQUE (commune_id)`);
    }
    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "territory" DROP CONSTRAINT UNIQ_territory_epci_id`);
        await queryRunner.query(`ALTER TABLE "territory" DROP CONSTRAINT UNIQ_territory_commune_id`);
    }

}
