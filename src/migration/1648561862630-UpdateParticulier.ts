import {MigrationInterface, QueryRunner} from "typeorm";

export class UpdateParticulier1648561862630 implements MigrationInterface {
    name = 'UpdateParticulier1648561862630'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "particulier" ALTER COLUMN "adresse" DROP NOT NULL`);
        await queryRunner.query(`ALTER TABLE "particulier" ALTER COLUMN "code_postal" DROP NOT NULL`);
        await queryRunner.query(`ALTER TABLE "particulier" ALTER COLUMN "ville" DROP NOT NULL`);
        await queryRunner.query(`ALTER TABLE "particulier" ADD "email" character varying`);
        await queryRunner.query(`ALTER TABLE "particulier" ADD "telephone" character varying`);
        await queryRunner.query(`ALTER TABLE "particulier" ADD "geolocation" character varying`);
        await queryRunner.query(`ALTER TABLE "particulier" ADD "activite" character varying`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "particulier" ALTER COLUMN "ville" SET NOT NULL`);
        await queryRunner.query(`ALTER TABLE "particulier" ALTER COLUMN "code_postal" SET NOT NULL`);
        await queryRunner.query(`ALTER TABLE "particulier" ALTER COLUMN "adresse" SET NOT NULL`);
        await queryRunner.query(`ALTER TABLE "particulier" DROP COLUMN "activite"`);
        await queryRunner.query(`ALTER TABLE "particulier" DROP COLUMN "geolocation"`);
        await queryRunner.query(`ALTER TABLE "particulier" DROP COLUMN "telephone"`);
        await queryRunner.query(`ALTER TABLE "particulier" DROP COLUMN "email"`);
    }
}
