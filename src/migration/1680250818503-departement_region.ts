import { MigrationInterface, QueryRunner } from 'typeorm';

export class departementRegion1680250818503 implements MigrationInterface {
	name = 'departementRegion1680250818503';

	public async up(queryRunner: QueryRunner): Promise<void> {
		await queryRunner.query(
			`CREATE TABLE "departement" ("ctcd" character varying NOT NULL, "nom" character varying NOT NULL, CONSTRAINT "PK_230d561e7e46a6c70e5913dc1ad" PRIMARY KEY ("ctcd"))`
		);
		await queryRunner.query(
			`CREATE TABLE "region" ("insee_reg" character varying NOT NULL, "nom" character varying NOT NULL, CONSTRAINT "PK_43344db76cf48a488a955710e7d" PRIMARY KEY ("insee_reg"))`
		);
		await queryRunner.query(`ALTER TABLE "territory" ADD "departement_id" character varying`);
		await queryRunner.query(
			`ALTER TABLE "territory" ADD CONSTRAINT "UQ_f298691eaee560ee58f836f9089" UNIQUE ("departement_id")`
		);
		await queryRunner.query(`ALTER TABLE "territory" ADD "region_id" character varying`);
		await queryRunner.query(
			`ALTER TABLE "territory" ADD CONSTRAINT "UQ_fcbdc1c8e8a9a238a117e59d566" UNIQUE ("region_id")`
		);
	}

	public async down(queryRunner: QueryRunner): Promise<void> {
		await queryRunner.query(
			`ALTER TABLE "territory" DROP CONSTRAINT "UQ_fcbdc1c8e8a9a238a117e59d566"`
		);
		await queryRunner.query(`ALTER TABLE "territory" DROP COLUMN "region_id"`);
		await queryRunner.query(
			`ALTER TABLE "territory" DROP CONSTRAINT "UQ_f298691eaee560ee58f836f9089"`
		);
		await queryRunner.query(`ALTER TABLE "territory" DROP COLUMN "departement_id"`);
		await queryRunner.query(`DROP TABLE "region"`);
		await queryRunner.query(`DROP TABLE "departement"`);
	}
}
