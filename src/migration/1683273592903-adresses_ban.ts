import { MigrationInterface, QueryRunner } from "typeorm";

export class adressesBan1683273592903 implements MigrationInterface {
    name = 'adressesBan1683273592903'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "sirene_etablissement" ADD "ban_adresse" boolean DEFAULT false`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "sirene_etablissement" DROP COLUMN "ban_adresse"`);
    }

}
