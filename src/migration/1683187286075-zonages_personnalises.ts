import { MigrationInterface, QueryRunner } from "typeorm";

export class zonagesPersonnalises1683187286075 implements MigrationInterface {
    name = 'zonagesPersonnalises1683187286075'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE TABLE "zonage" ("id" SERIAL NOT NULL, "nom" character varying(254), "geometry" geometry(MultiPolygon,4326), "geometry_source" geometry(MultiPolygon,3949), "territoire_id" integer, CONSTRAINT "uniq_zonage_territory" UNIQUE ("nom", "territoire_id"), CONSTRAINT "PK_e95357e4bd946d58ea0dcaf43b7" PRIMARY KEY ("id"))`);
        await queryRunner.query(`CREATE INDEX "IDX_756290a7f7f8324cf7d5afee31" ON "zonage" ("territoire_id") `);
        await queryRunner.query(`CREATE INDEX "geometry" ON "zonage" USING GiST ("geometry") `);
        await queryRunner.query(`CREATE TABLE "territory_etablissement_reference_zonages_zonage" ("territory_etablissement_reference_id" character varying NOT NULL, "zonage_id" integer NOT NULL, CONSTRAINT "PK_85959d7ef6ebd0d8209ccf92b5a" PRIMARY KEY ("territory_etablissement_reference_id", "zonage_id"))`);
        await queryRunner.query(`CREATE INDEX "IDX_8e7e75da10cbc696e55f3d42c1" ON "territory_etablissement_reference_zonages_zonage" ("territory_etablissement_reference_id") `);
        await queryRunner.query(`CREATE INDEX "IDX_38332d934c55cccd21b9810d6b" ON "territory_etablissement_reference_zonages_zonage" ("zonage_id") `);
        await queryRunner.query(`ALTER TABLE "zonage" ADD CONSTRAINT "FK_756290a7f7f8324cf7d5afee318" FOREIGN KEY ("territoire_id") REFERENCES "territory"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "territory_etablissement_reference_zonages_zonage" ADD CONSTRAINT "FK_8e7e75da10cbc696e55f3d42c12" FOREIGN KEY ("territory_etablissement_reference_id") REFERENCES "territory_etablissement_reference"("id") ON DELETE CASCADE ON UPDATE CASCADE`);
        await queryRunner.query(`ALTER TABLE "territory_etablissement_reference_zonages_zonage" ADD CONSTRAINT "FK_38332d934c55cccd21b9810d6b0" FOREIGN KEY ("zonage_id") REFERENCES "zonage"("id") ON DELETE CASCADE ON UPDATE CASCADE`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "territory_etablissement_reference_zonages_zonage" DROP CONSTRAINT "FK_38332d934c55cccd21b9810d6b0"`);
        await queryRunner.query(`ALTER TABLE "territory_etablissement_reference_zonages_zonage" DROP CONSTRAINT "FK_8e7e75da10cbc696e55f3d42c12"`);
        await queryRunner.query(`ALTER TABLE "zonage" DROP CONSTRAINT "FK_756290a7f7f8324cf7d5afee318"`);
        await queryRunner.query(`DROP INDEX "public"."IDX_38332d934c55cccd21b9810d6b"`);
        await queryRunner.query(`DROP INDEX "public"."IDX_8e7e75da10cbc696e55f3d42c1"`);
        await queryRunner.query(`DROP TABLE "territory_etablissement_reference_zonages_zonage"`);
        await queryRunner.query(`DROP INDEX "public"."geometry"`);
        await queryRunner.query(`DROP INDEX "public"."IDX_756290a7f7f8324cf7d5afee31"`);
        await queryRunner.query(`DROP TABLE "zonage"`);
    }

}
