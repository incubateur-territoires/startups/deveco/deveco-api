import { MigrationInterface, QueryRunner } from "typeorm";

export class essDone1686641615927 implements MigrationInterface {
    name = 'essDone1686641615927'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "territory_import_status" ADD "ess" boolean NOT NULL DEFAULT false`);
        await queryRunner.query(`UPDATE "territory_import_status" SET ess = true WHERE ter`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "territory_import_status" DROP COLUMN "ess"`);
    }

}
