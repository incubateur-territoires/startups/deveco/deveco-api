import { MigrationInterface, QueryRunner } from "typeorm"

export class formesJuridiquesCourantesAvecEI1666254168719 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`UPDATE territory_etablissement_reference TER SET categorie_juridique_courante = CASE WHEN substring(CAST(EL.categorie_juridique AS VARCHAR) , 1, 2) IN ( '00', '21', '22', '27', '65', '71', '72', '73', '74', '81', '83', '84', '85', '91' ) THEN false ELSE true END FROM etablissement EL WHERE EL.siret = TER.siret;`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`UPDATE territory_etablissement_reference TER SET categorie_juridique_courante = CASE WHEN substring(CAST(EL.categorie_juridique AS VARCHAR) , 1, 2) IN ( '00', '10', '21', '22', '27', '65', '71', '72', '73', '74', '81', '83', '84', '85', '91' ) THEN false ELSE true END FROM etablissement EL WHERE EL.siret = TER.siret;`);
    }
}
