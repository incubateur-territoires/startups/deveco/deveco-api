import { MigrationInterface, QueryRunner } from "typeorm";

export class CreateEtablissementLight1659545337795 implements MigrationInterface {
    name = 'CreateEtablissementLight1659545337795'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE TYPE "public"."etablissement_light_etat_administratif_etablissement_enum" AS ENUM('A', 'F')`);
        await queryRunner.query(`CREATE TABLE "etablissement_light" ("siren" character varying(9) NOT NULL, "siret" character varying(14) NOT NULL, "nom_public" character varying, "nom_recherche" character varying, "tranche_effectifs" character varying(2), "annee_effectifs" character varying(9), "sigle" character varying, "code_naf" character varying, "libelle_naf" character varying, "etat_administratif_etablissement" "public"."etablissement_light_etat_administratif_etablissement_enum" NOT NULL DEFAULT 'A', "type_etablissement" character varying, CONSTRAINT "PK_fa6a26879acf033d46fc63a1cbb" PRIMARY KEY ("siret"))`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`DROP TABLE "etablissement_light"`);
        await queryRunner.query(`DROP TYPE "public"."etablissement_light_etat_administratif_etablissement_enum"`);
    }

}
