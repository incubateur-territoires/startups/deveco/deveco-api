import {MigrationInterface, QueryRunner} from "typeorm";

export class consultations1655415980302 implements MigrationInterface {
    name = 'consultations1655415980302'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE TABLE "consultation" ("id" SERIAL NOT NULL, "date" TIMESTAMP NOT NULL DEFAULT now(), "deveco_id" integer, "fiche_id" integer, CONSTRAINT "PK_5203569fac28a4a626c42abe70b" PRIMARY KEY ("id"))`);
        await queryRunner.query(`ALTER TABLE "consultation" ADD CONSTRAINT "FK_a6ad867ecdda7c4ac286b8f12b7" FOREIGN KEY ("deveco_id") REFERENCES "deveco"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "consultation" ADD CONSTRAINT "FK_28e5ea064bc941ff3fe682886f7" FOREIGN KEY ("fiche_id") REFERENCES "fiche"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "consultation" DROP CONSTRAINT "FK_28e5ea064bc941ff3fe682886f7"`);
        await queryRunner.query(`ALTER TABLE "consultation" DROP CONSTRAINT "FK_a6ad867ecdda7c4ac286b8f12b7"`);
        await queryRunner.query(`DROP TABLE "consultation"`);
    }

}
