import { MigrationInterface, QueryRunner } from 'typeorm';

export class entrepriseDateFermeture1675773155007 implements MigrationInterface {
	name = 'entrepriseDateFermeture1675773155007';

	public async up(queryRunner: QueryRunner): Promise<void> {
		// we use a SELECT NULL query because that specific migration may already have run in some environments
		await queryRunner.query(`SELECT NULL`);
	}

	public async down(queryRunner: QueryRunner): Promise<void> {
		await queryRunner.query(`SELECT NULL`);
	}
}
