import { MigrationInterface, QueryRunner } from "typeorm";

export class terExogene1669370163998 implements MigrationInterface {
	name = 'terExogene1669370163998'

	public async up(queryRunner: QueryRunner): Promise<void> {
		await queryRunner.query(`ALTER TABLE "territory_etablissement_reference" ADD "exogene" boolean NOT NULL DEFAULT false`);
		await queryRunner.query(`
      INSERT INTO territory_etablissement_reference(id, siret, territory_id, nom_recherche, etat_administratif, code_naf, libelle_naf, date_creation, categorie_juridique_courante, tranche_effectifs, code_commune, adresse_complete, exogene)
        SELECT
            CONCAT(entite.territoire_id, '-', EL.siret)
            , EL.siret
            , entite.territoire_id as territory_id
            , EL.nom_recherche
            , EL.etat_administratif
            , EL.code_naf
            , EL.libelle_naf
            , EL.date_creation
            , CASE WHEN substring(CAST(EL.categorie_juridique AS VARCHAR) , 1, 2) IN ( '00', '21', '22', '27', '65', '71', '72', '73', '74', '81', '83', '84', '85', '91' ) THEN
                false
              ELSE
                true
              END
            , EL.tranche_effectifs
            , EL.code_commune
            , EL.adresse_complete
            , entite.exogene
        FROM entite
        INNER JOIN etablissement EL ON EL.siret = entite.entreprise_id
        WHERE entite.exogene
      ON CONFLICT DO NOTHING
    `);
	}

	public async down(queryRunner: QueryRunner): Promise<void> {
		await queryRunner.query(`DELETE FROM "territory_etablissement_reference" WHERE exogene`);
		await queryRunner.query(`ALTER TABLE "territory_etablissement_reference" DROP COLUMN "exogene"`);
	}

}
