import {MigrationInterface, QueryRunner} from "typeorm";

export class CreateDemande1649511505530 implements MigrationInterface {
    name = 'CreateDemande1649511505530'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE TABLE "echange" ("id" SERIAL NOT NULL, "type_echange" character varying NOT NULL, "date_echange" date NOT NULL, "compte_rendu" character varying NOT NULL, "demandeTypes" text NOT NULL, CONSTRAINT "PK_c51fbb9327f5618d5000b59b98b" PRIMARY KEY ("id"))`);
        await queryRunner.query(`CREATE TABLE "demande" ("id" SERIAL NOT NULL, "type_demande" character varying NOT NULL, CONSTRAINT "PK_2102b7ec243910dfa64f15482a1" PRIMARY KEY ("id"))`);
        await queryRunner.query(`ALTER TABLE "echange" ADD "fiche_id" integer`);
        await queryRunner.query(`ALTER TABLE "echange" ADD CONSTRAINT "FK_45e6b16a9ece55a4d610dd11049" FOREIGN KEY ("fiche_id") REFERENCES "fiche"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`DROP TABLE "demande"`);
        await queryRunner.query(`DROP TABLE "echange"`);
    }

}
