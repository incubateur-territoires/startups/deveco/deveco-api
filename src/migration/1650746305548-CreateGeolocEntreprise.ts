import {MigrationInterface, QueryRunner} from "typeorm";

export class CreateGeolocEntreprise1650746305548 implements MigrationInterface {
    name = 'CreateGeolocEntreprise1650746305548'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`DROP TABLE IF EXISTS "geoloc_entreprise"`);
        await queryRunner.query(`CREATE TABLE "geoloc_entreprise" ("siret" character varying NOT NULL, "x" double precision, "y" double precision, "qualite_xy" character varying, "epsg" character varying, "plg_qp" character varying, "plg_iris" character varying, "plg_zus" character varying, "plg_zfu" character varying, "plg_qva" character varying, "plg_code_commune" character varying, "distance_precision" character varying, "qualite_qp" character varying, "qualite_iris" character varying, "qualite_zus" character varying, "qualite_zfu" character varying, "qualite_qva" character varying, "y_latitude" double precision, "x_longitude" double precision, CONSTRAINT "PK_94e552e5dbe3d06f725ca06961d" PRIMARY KEY ("siret"))`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`DROP TABLE "geoloc_entreprise"`);
    }

}
