import { MigrationInterface, QueryRunner } from "typeorm";

export class succession1681998422800 implements MigrationInterface {
    name = 'succession1681998422800'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE TABLE "lien_succession_temp" ("id" SERIAL NOT NULL, "siret_predecesseur" character varying(14), "siret_successeur" character varying(14), "date_succession" TIMESTAMP WITH TIME ZONE NOT NULL, "date_traitement" TIMESTAMP WITH TIME ZONE NOT NULL, "transfert_siege" boolean NOT NULL, "continuite_economique" boolean NOT NULL, CONSTRAINT "PK_3ea795471f96f0f409d77cbcaac" PRIMARY KEY ("id"))`);
        await queryRunner.query(`CREATE TABLE "lien_succession" ("id" SERIAL NOT NULL, "siret_predecesseur" character varying(14), "siret_successeur" character varying(14), "date_succession" TIMESTAMP WITH TIME ZONE NOT NULL, "date_traitement" TIMESTAMP WITH TIME ZONE NOT NULL, "transfert_siege" boolean NOT NULL, "continuite_economique" boolean NOT NULL, CONSTRAINT "PK_d8fc5b9edbd98b80f9d9fac2a39" PRIMARY KEY ("id"))`);
        await queryRunner.query(`CREATE INDEX "IDX_6e70969abcdba7fd315cd6f7c9" ON "lien_succession" ("siret_predecesseur") `);
        await queryRunner.query(`CREATE INDEX "IDX_c69ec1f2bc2b668bd159690e31" ON "lien_succession" ("siret_successeur") `);
        await queryRunner.query(`ALTER TABLE "lien_succession" ADD CONSTRAINT "uniq_predecesseur_successeur_date_lien" UNIQUE ("siret_predecesseur", "siret_successeur", "date_succession")`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`DROP INDEX "public"."IDX_c69ec1f2bc2b668bd159690e31"`);
        await queryRunner.query(`DROP INDEX "public"."IDX_6e70969abcdba7fd315cd6f7c9"`);
        await queryRunner.query(`ALTER TABLE "lien_succession" DROP CONSTRAINT "uniq_predecesseur_successeur_date_lien"`);
        await queryRunner.query(`DROP TABLE "lien_succession"`);
        await queryRunner.query(`DROP TABLE "lien_succession_temp"`);
    }

}
