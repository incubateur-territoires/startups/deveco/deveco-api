import {MigrationInterface, QueryRunner} from "typeorm";

export class InitialMigration1648036353813 implements MigrationInterface {
    name = 'InitialMigration1648036353813'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE TABLE "account" ("id" SERIAL NOT NULL, "email" character varying NOT NULL, "account_type" character varying NOT NULL DEFAULT 'deveco', "access_key" character varying, "access_key_date" TIMESTAMP WITH TIME ZONE, "last_login" TIMESTAMP WITH TIME ZONE, CONSTRAINT "UQ_4c8f96ccf523e9a3faefd5bdd4c" UNIQUE ("email"), CONSTRAINT "PK_54115ee388cdb6d86bb4bf5b2ea" PRIMARY KEY ("id"))`);
        await queryRunner.query(`CREATE TABLE "commune" ("typecom" character varying NOT NULL, "insee_com" character varying NOT NULL, "insee_dep" character varying NOT NULL, "insee_reg" character varying NOT NULL, "ctcd" character varying NOT NULL, "insee_arr" character varying NOT NULL, "tncc" character varying NOT NULL, "ncc" character varying NOT NULL, "nccenr" character varying NOT NULL, "lib_com" character varying NOT NULL, "insee_can" character varying NOT NULL, "com_parent" character varying NOT NULL, CONSTRAINT "PK_06be038b1cb3c3afa412695d7f1" PRIMARY KEY ("insee_com"))`);
        await queryRunner.query(`CREATE TABLE "epci" ("insee_epci" character varying NOT NULL, "lib_epci" character varying NOT NULL, "nature" character varying NOT NULL, CONSTRAINT "PK_eecabc0d20ec37959387f098900" PRIMARY KEY ("insee_epci"))`);
        await queryRunner.query(`CREATE TABLE "territory" ("id" SERIAL NOT NULL, "territory_type" character varying NOT NULL, "name" character varying NOT NULL, "epci_id" character varying, "commune_id" character varying, CONSTRAINT "REL_1d67e6ffafac89853948c44373" UNIQUE ("epci_id"), CONSTRAINT "REL_31acb7c820be64ca8470406fe3" UNIQUE ("commune_id"), CONSTRAINT "PK_2250448f958bc52a8d040b48f82" PRIMARY KEY ("id"))`);
        await queryRunner.query(`CREATE TABLE "deveco" ("id" SERIAL NOT NULL, "account_id" integer, "territory_id" integer, CONSTRAINT "REL_0750ad93125de72fcc1a2b1071" UNIQUE ("account_id"), CONSTRAINT "PK_7ce528d607d52d8c053e8946f3e" PRIMARY KEY ("id"))`);
        await queryRunner.query(`CREATE TABLE "entreprise" ("siret" character varying NOT NULL, "siren" character varying NOT NULL, "nic" character varying NOT NULL, "date_creation_etablissement" TIMESTAMP, "denomination_unite_legale" character varying, "activite_principale_unite_legale" character varying, "numero_voie_etablissement" character varying, "type_voie_etablissement" character varying, "libelle_voie_etablissement" character varying, "code_postal_etablissement" character varying, "libelle_commune_etablissement" character varying, "distribution_speciale_etablissement" character varying, "code_commune_etablissement" character varying, CONSTRAINT "PK_974ff6395741fd9fcfe2efdbf6e" PRIMARY KEY ("siret"))`);
        await queryRunner.query(`CREATE TABLE "particulier" ("id" SERIAL NOT NULL, "nom" character varying NOT NULL, "prenom" character varying NOT NULL, "adresse" character varying NOT NULL, "code_postal" character varying NOT NULL, "ville" character varying NOT NULL, CONSTRAINT "PK_c1e84d4ae25e9832c4cbda13e92" PRIMARY KEY ("id"))`);
        await queryRunner.query(`CREATE TABLE "fiche" ("id" SERIAL NOT NULL, "fiche_type" character varying NOT NULL, "territoire_id" integer, "deveco_id" integer, "entreprise_id" character varying, "particulier_id" integer, CONSTRAINT "REL_5304848ebec35665bcadac8d00" UNIQUE ("entreprise_id"), CONSTRAINT "REL_de7cf33e3a99922fbe58ad11de" UNIQUE ("particulier_id"), CONSTRAINT "PK_d7138e829814db35f5ab3e230c5" PRIMARY KEY ("id"))`);
        await queryRunner.query(`ALTER TABLE "territory" ADD CONSTRAINT "FK_1d67e6ffafac89853948c443730" FOREIGN KEY ("epci_id") REFERENCES "epci"("insee_epci") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "territory" ADD CONSTRAINT "FK_31acb7c820be64ca8470406fe32" FOREIGN KEY ("commune_id") REFERENCES "commune"("insee_com") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "deveco" ADD CONSTRAINT "FK_0750ad93125de72fcc1a2b10715" FOREIGN KEY ("account_id") REFERENCES "account"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "deveco" ADD CONSTRAINT "FK_f8e5b14c60e822f91b0312f4688" FOREIGN KEY ("territory_id") REFERENCES "territory"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "fiche" ADD CONSTRAINT "FK_9dbd4085f66ab645960036b692c" FOREIGN KEY ("territoire_id") REFERENCES "territory"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "fiche" ADD CONSTRAINT "FK_21952e18c17573e2497a973767e" FOREIGN KEY ("deveco_id") REFERENCES "deveco"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "fiche" ADD CONSTRAINT "FK_5304848ebec35665bcadac8d00a" FOREIGN KEY ("entreprise_id") REFERENCES "entreprise"("siret") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "fiche" ADD CONSTRAINT "FK_de7cf33e3a99922fbe58ad11ded" FOREIGN KEY ("particulier_id") REFERENCES "particulier"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "fiche" DROP CONSTRAINT "FK_de7cf33e3a99922fbe58ad11ded"`);
        await queryRunner.query(`ALTER TABLE "fiche" DROP CONSTRAINT "FK_5304848ebec35665bcadac8d00a"`);
        await queryRunner.query(`ALTER TABLE "fiche" DROP CONSTRAINT "FK_21952e18c17573e2497a973767e"`);
        await queryRunner.query(`ALTER TABLE "fiche" DROP CONSTRAINT "FK_9dbd4085f66ab645960036b692c"`);
        await queryRunner.query(`ALTER TABLE "deveco" DROP CONSTRAINT "FK_f8e5b14c60e822f91b0312f4688"`);
        await queryRunner.query(`ALTER TABLE "deveco" DROP CONSTRAINT "FK_0750ad93125de72fcc1a2b10715"`);
        await queryRunner.query(`ALTER TABLE "territory" DROP CONSTRAINT "FK_31acb7c820be64ca8470406fe32"`);
        await queryRunner.query(`ALTER TABLE "territory" DROP CONSTRAINT "FK_1d67e6ffafac89853948c443730"`);
        await queryRunner.query(`DROP TABLE "fiche"`);
        await queryRunner.query(`DROP TABLE "particulier"`);
        await queryRunner.query(`DROP TABLE "entreprise"`);
        await queryRunner.query(`DROP TABLE "deveco"`);
        await queryRunner.query(`DROP TABLE "territory"`);
        await queryRunner.query(`DROP TABLE "epci"`);
        await queryRunner.query(`DROP TABLE "commune"`);
        await queryRunner.query(`DROP TABLE "account"`);
    }

}
