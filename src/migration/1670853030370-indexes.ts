import { MigrationInterface, QueryRunner } from "typeorm";

export class indexes1670853030370 implements MigrationInterface {
    name = 'indexes1670853030370'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE INDEX "IDX_d2339e8225f83ad5e8c5c04cb0" ON "territory_etablissement_reference" ("territory_id") `);
        await queryRunner.query(`CREATE INDEX "IDX_500ed29404ba8324a98b15930f" ON "etablissement" ("code_commune") `);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`DROP INDEX "public"."IDX_500ed29404ba8324a98b15930f"`);
				await queryRunner.query(`DROP INDEX "public"."IDX_d2339e8225f83ad5e8c5c04cb0"`);
    }

}
