import { MigrationInterface, QueryRunner } from "typeorm";

export class petr1678356038975 implements MigrationInterface {
    name = 'petr1678356038975'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE TABLE "petr" ("code_petr" character varying NOT NULL, "lib_petr" character varying NOT NULL, CONSTRAINT "PK_3bd271747cbbd34f0232e057dee" PRIMARY KEY ("code_petr"))`);
        await queryRunner.query(`ALTER TABLE "epci" ADD "code_petr" character varying`);
        await queryRunner.query(`ALTER TABLE "territory" ADD "petr_id" character varying`);
        await queryRunner.query(`ALTER TABLE "territory" ADD CONSTRAINT "UQ_1186a7d0783cc916c6e1eb9f154" UNIQUE ("petr_id")`);
        await queryRunner.query(`ALTER TABLE "territory" ADD CONSTRAINT "UQ_a65fe9307faf97650d7372e5a32" UNIQUE ("metropole_id")`);
        await queryRunner.query(`CREATE INDEX "IDX_1fc956ab448bd83cb46133628a" ON "epci" ("code_petr") `);
        await queryRunner.query(`ALTER TABLE "epci" ADD CONSTRAINT "FK_1fc956ab448bd83cb46133628ab" FOREIGN KEY ("code_petr") REFERENCES "petr"("code_petr") ON DELETE NO ACTION ON UPDATE NO ACTION`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "epci" DROP CONSTRAINT "FK_1fc956ab448bd83cb46133628ab"`);
        await queryRunner.query(`DROP INDEX "public"."IDX_1fc956ab448bd83cb46133628a"`);
        await queryRunner.query(`ALTER TABLE "territory" DROP CONSTRAINT "UQ_a65fe9307faf97650d7372e5a32"`);
        await queryRunner.query(`ALTER TABLE "territory" DROP CONSTRAINT "UQ_1186a7d0783cc916c6e1eb9f154"`);
        await queryRunner.query(`ALTER TABLE "territory" DROP COLUMN "petr_id"`);
        await queryRunner.query(`ALTER TABLE "epci" DROP COLUMN "code_petr"`);
        await queryRunner.query(`DROP TABLE "petr"`);
    }

}
