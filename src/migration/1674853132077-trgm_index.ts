import { MigrationInterface, QueryRunner } from "typeorm";

export class trgmIndex1674853132077 implements MigrationInterface {
    name = 'trgmIndex1674853132077'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE INDEX IF NOT EXISTS index_nom_trig ON territory_etablissement_reference USING gin (nom_recherche gin_trgm_ops);`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`DROP INDEX "public"."index_nom_trig"`);
    }

}
