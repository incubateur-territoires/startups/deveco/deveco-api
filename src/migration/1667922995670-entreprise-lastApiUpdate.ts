import { MigrationInterface, QueryRunner } from "typeorm";

export class entrepriseLastApiUpdate1667922995670 implements MigrationInterface {
    name = 'entrepriseLastApiUpdate1667922995670'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "entreprise" ADD "last_api_update" TIMESTAMP WITH TIME ZONE`);
        await queryRunner.query(`UPDATE entreprise SET last_api_update = entite.created_at FROM entite where last_api_update is null and entite.entreprise_id = entreprise.siret;`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "entreprise" DROP COLUMN "last_api_update"`);
    }

}
