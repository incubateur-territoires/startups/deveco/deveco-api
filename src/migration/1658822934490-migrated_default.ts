import { MigrationInterface, QueryRunner } from "typeorm";

export class migratedDefault1658822934490 implements MigrationInterface {
    name = 'migratedDefault1658822934490'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "entreprise" ALTER COLUMN "migrated" SET DEFAULT false`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "entreprise" ALTER COLUMN "migrated" DROP DEFAULT`);
    }

}
