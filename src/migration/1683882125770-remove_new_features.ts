import { MigrationInterface, QueryRunner } from "typeorm";

export class removeNewFeatures1683882125770 implements MigrationInterface {
    name = 'removeNewFeatures1683882125770'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "account" DROP COLUMN "new_features"`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "account" ADD "new_features" character varying`);
    }

}
