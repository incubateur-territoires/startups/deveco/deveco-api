import {MigrationInterface, QueryRunner} from "typeorm";

export class nomPrenom1651741111140 implements MigrationInterface {
    name = 'nomPrenom1651741111140'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "account" ADD "nom" character varying`);
        await queryRunner.query(`ALTER TABLE "account" ADD "prenom" character varying`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "account" DROP COLUMN "prenom"`);
        await queryRunner.query(`ALTER TABLE "account" DROP COLUMN "nom"`);
    }

}
