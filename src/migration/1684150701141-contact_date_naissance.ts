import { MigrationInterface, QueryRunner } from "typeorm";

export class contactDateNaissance1684150701141 implements MigrationInterface {
    name = 'contactDateNaissance1684150701141'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "particulier" ADD "date_de_naissance" date`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "particulier" DROP COLUMN "date_de_naissance"`);
    }

}
