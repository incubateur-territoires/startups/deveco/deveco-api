import { MigrationInterface, QueryRunner } from 'typeorm';

export class welcomeEmail1679997350248 implements MigrationInterface {
	name = 'welcomeEmail1679997350248';

	public async up(queryRunner: QueryRunner): Promise<void> {
		await queryRunner.query(
			`ALTER TABLE "territory" ADD "import_in_progress" boolean NOT NULL DEFAULT true`
		);
		await queryRunner.query(`UPDATE "territory" set "import_in_progress" = false`);
		await queryRunner.query(
			`ALTER TABLE "deveco" ADD "welcome_email_sent" boolean NOT NULL DEFAULT false`
		);
		await queryRunner.query(`UPDATE "deveco" set "welcome_email_sent" = true`);
	}

	public async down(queryRunner: QueryRunner): Promise<void> {
		await queryRunner.query(`ALTER TABLE "deveco" DROP COLUMN "welcome_email_sent"`);
		await queryRunner.query(`ALTER TABLE "territory" DROP COLUMN "import_in_progress"`);
	}
}
