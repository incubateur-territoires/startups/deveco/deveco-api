import { MigrationInterface, QueryRunner } from "typeorm";

export class sireneHash1675712110860 implements MigrationInterface {
    name = 'sireneHash1675712110860'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "sirene_etablissement" ADD "hash" character varying(32)`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "sirene_etablissement" DROP COLUMN "hash"`);
    }

}
