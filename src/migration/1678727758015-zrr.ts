import { MigrationInterface, QueryRunner } from "typeorm";

export class zrr1678727758015 implements MigrationInterface {
    name = 'zrr1678727758015'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "commune" ADD "zrr" character varying NOT NULL DEFAULT 'N'`);
        await queryRunner.query(`ALTER TABLE "territory_etablissement_reference" ADD "zrr" character varying NOT NULL DEFAULT 'N'`);
        await queryRunner.query(`CREATE INDEX "IDX_913bfa9775ae9208d532634cfc" ON "territory_etablissement_reference" ("zrr") `);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`DROP INDEX "public"."IDX_913bfa9775ae9208d532634cfc"`);
        await queryRunner.query(`ALTER TABLE "territory_etablissement_reference" DROP COLUMN "zrr"`);
        await queryRunner.query(`ALTER TABLE "commune" DROP COLUMN "zrr"`);
    }

}
