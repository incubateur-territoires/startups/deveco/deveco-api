import { MigrationInterface, QueryRunner } from "typeorm";

export class territoryPerimetre1686143511224 implements MigrationInterface {
    name = 'territoryPerimetre1686143511224'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE TABLE "perimetre" ("id" SERIAL NOT NULL, "level" character varying NOT NULL, CONSTRAINT "PK_f8fe7e17d3a816e1d5ce77ff4dd" PRIMARY KEY ("id"))`);
        await queryRunner.query(`CREATE TABLE "perimetre_communes_commune" ("perimetreId" integer NOT NULL, "communeInseeCom" character varying NOT NULL, CONSTRAINT "PK_d4f9f1c457995b35218ccb51f47" PRIMARY KEY ("perimetreId", "communeInseeCom"))`);
        await queryRunner.query(`CREATE TABLE "perimetre_epcis_epci" ("perimetreId" integer NOT NULL, "epciInseeEpci" character varying NOT NULL, CONSTRAINT "PK_630e6dfe33223613e8d23c03bf5" PRIMARY KEY ("perimetreId", "epciInseeEpci"))`);
        await queryRunner.query(`CREATE TABLE "perimetre_departements_departement" ("perimetreId" integer NOT NULL, "departementCtcd" character varying NOT NULL, CONSTRAINT "PK_f275087ccad49ab162f5484c862" PRIMARY KEY ("perimetreId", "departementCtcd"))`);
        await queryRunner.query(`ALTER TABLE "territory" ADD "perimetre_id" integer`);
        await queryRunner.query(`ALTER TABLE "perimetre_communes_commune" ADD CONSTRAINT "FK_e83554ba262ca7e5236c6e646d3" FOREIGN KEY ("perimetreId") REFERENCES "perimetre"("id") ON DELETE CASCADE ON UPDATE CASCADE`);
        await queryRunner.query(`ALTER TABLE "perimetre_communes_commune" ADD CONSTRAINT "FK_a423764efa7adfb3e86c53c7376" FOREIGN KEY ("communeInseeCom") REFERENCES "commune"("insee_com") ON DELETE CASCADE ON UPDATE CASCADE`);
        await queryRunner.query(`ALTER TABLE "perimetre_epcis_epci" ADD CONSTRAINT "FK_e7fd8cb51f1ff4976612166393a" FOREIGN KEY ("perimetreId") REFERENCES "perimetre"("id") ON DELETE CASCADE ON UPDATE CASCADE`);
        await queryRunner.query(`ALTER TABLE "perimetre_epcis_epci" ADD CONSTRAINT "FK_92d24464de3c6bbdec9935c4f7c" FOREIGN KEY ("epciInseeEpci") REFERENCES "epci"("insee_epci") ON DELETE CASCADE ON UPDATE CASCADE`);
        await queryRunner.query(`ALTER TABLE "perimetre_departements_departement" ADD CONSTRAINT "FK_7b21ca152fbd37bad15367dc848" FOREIGN KEY ("perimetreId") REFERENCES "perimetre"("id") ON DELETE CASCADE ON UPDATE CASCADE`);
        await queryRunner.query(`ALTER TABLE "perimetre_departements_departement" ADD CONSTRAINT "FK_ee24e6c0bdd1fee17af8dab1344" FOREIGN KEY ("departementCtcd") REFERENCES "departement"("ctcd") ON DELETE CASCADE ON UPDATE CASCADE`);
        await queryRunner.query(`CREATE INDEX "IDX_e83554ba262ca7e5236c6e646d" ON "perimetre_communes_commune" ("perimetreId") `);
        await queryRunner.query(`CREATE INDEX "IDX_a423764efa7adfb3e86c53c737" ON "perimetre_communes_commune" ("communeInseeCom") `);
        await queryRunner.query(`CREATE INDEX "IDX_e7fd8cb51f1ff4976612166393" ON "perimetre_epcis_epci" ("perimetreId") `);
        await queryRunner.query(`CREATE INDEX "IDX_92d24464de3c6bbdec9935c4f7" ON "perimetre_epcis_epci" ("epciInseeEpci") `);
        await queryRunner.query(`CREATE INDEX "IDX_7b21ca152fbd37bad15367dc84" ON "perimetre_departements_departement" ("perimetreId") `);
        await queryRunner.query(`CREATE INDEX "IDX_ee24e6c0bdd1fee17af8dab134" ON "perimetre_departements_departement" ("departementCtcd") `);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`DROP INDEX "public"."IDX_ee24e6c0bdd1fee17af8dab134"`);
        await queryRunner.query(`DROP INDEX "public"."IDX_7b21ca152fbd37bad15367dc84"`);
        await queryRunner.query(`DROP INDEX "public"."IDX_92d24464de3c6bbdec9935c4f7"`);
        await queryRunner.query(`DROP INDEX "public"."IDX_e7fd8cb51f1ff4976612166393"`);
        await queryRunner.query(`DROP INDEX "public"."IDX_a423764efa7adfb3e86c53c737"`);
        await queryRunner.query(`DROP INDEX "public"."IDX_e83554ba262ca7e5236c6e646d"`);
        await queryRunner.query(`ALTER TABLE "perimetre_departements_departement" DROP CONSTRAINT "FK_ee24e6c0bdd1fee17af8dab1344"`);
        await queryRunner.query(`ALTER TABLE "perimetre_departements_departement" DROP CONSTRAINT "FK_7b21ca152fbd37bad15367dc848"`);
        await queryRunner.query(`ALTER TABLE "perimetre_epcis_epci" DROP CONSTRAINT "FK_92d24464de3c6bbdec9935c4f7c"`);
        await queryRunner.query(`ALTER TABLE "perimetre_epcis_epci" DROP CONSTRAINT "FK_e7fd8cb51f1ff4976612166393a"`);
        await queryRunner.query(`ALTER TABLE "perimetre_communes_commune" DROP CONSTRAINT "FK_a423764efa7adfb3e86c53c7376"`);
        await queryRunner.query(`ALTER TABLE "perimetre_communes_commune" DROP CONSTRAINT "FK_e83554ba262ca7e5236c6e646d3"`);
        await queryRunner.query(`ALTER TABLE "territory" DROP COLUMN "perimetre_id"`);
        await queryRunner.query(`DROP TABLE "perimetre_departements_departement"`);
        await queryRunner.query(`DROP TABLE "perimetre_epcis_epci"`);
        await queryRunner.query(`DROP TABLE "perimetre_communes_commune"`);
        await queryRunner.query(`DROP TABLE "perimetre"`);
    }

}
