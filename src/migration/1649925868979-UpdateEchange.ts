import {MigrationInterface, QueryRunner} from "typeorm";

export class UpdateEchange1649925868979 implements MigrationInterface {
    name = 'UpdateEchange1649925868979'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "echange" ADD "titre" character varying`);
        await queryRunner.query(`ALTER TABLE "echange" ADD "createur_id" integer`);
        await queryRunner.query(`ALTER TABLE "echange" ADD CONSTRAINT "FK_de5cbe7c883710bdd017fddbb6c" FOREIGN KEY ("createur_id") REFERENCES "deveco"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "echange" DROP CONSTRAINT "FK_de5cbe7c883710bdd017fddbb6c"`);
        await queryRunner.query(`ALTER TABLE "echange" DROP COLUMN "createur_id"`);
        await queryRunner.query(`ALTER TABLE "echange" DROP COLUMN "titre"`);
    }

}
