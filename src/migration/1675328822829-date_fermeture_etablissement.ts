import { MigrationInterface, QueryRunner } from "typeorm";

export class dateFermetureEtablissement1675328822829 implements MigrationInterface {
    name = 'dateFermetureEtablissement1675328822829'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "entreprise" ADD "date_fermeture" date`);
        await queryRunner.query(`ALTER TABLE "etablissement" ADD "date_fermeture" date`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "etablissement" DROP COLUMN "date_fermeture"`);
        await queryRunner.query(`ALTER TABLE "entreprise" DROP COLUMN "date_fermeture"`);
    }

}
