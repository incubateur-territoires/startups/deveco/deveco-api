import { MigrationInterface, QueryRunner } from 'typeorm';

export class territoryImportStarted1686174019417 implements MigrationInterface {
	name = 'territoryImportStarted1686174019417';

	public async up(queryRunner: QueryRunner): Promise<void> {
		await queryRunner.query(
			`ALTER TABLE "territory_import_status" ADD "started" boolean NOT NULL DEFAULT false`
		);
		await queryRunner.query(
			`ALTER TABLE "territory_import_status" ADD "done" boolean NOT NULL DEFAULT false`
		);
		await queryRunner.query(
			`UPDATE "territory_import_status" SET started = geolocation OR ter OR qpv OR stats`
		);
		await queryRunner.query(
			`UPDATE "territory_import_status" SET done = geolocation AND ter AND qpv AND stats`
		);
	}

	public async down(queryRunner: QueryRunner): Promise<void> {
		await queryRunner.query(`ALTER TABLE "territory_import_status" DROP COLUMN "started"`);
		await queryRunner.query(`ALTER TABLE "territory_import_status" DROP COLUMN "done"`);
	}
}
