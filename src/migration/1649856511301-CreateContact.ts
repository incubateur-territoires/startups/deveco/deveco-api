import {MigrationInterface, QueryRunner} from "typeorm";

export class CreateContact1649856511301 implements MigrationInterface {
    name = 'CreateContact1649856511301'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "contact" ADD "fiche_id" integer`);
        await queryRunner.query(`ALTER TABLE "contact" ADD CONSTRAINT "FK_3fe9e8fc8add9973e5745beda77" FOREIGN KEY ("fiche_id") REFERENCES "fiche"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "contact" DROP CONSTRAINT "FK_3fe9e8fc8add9973e5745beda77"`);
        await queryRunner.query(`ALTER TABLE "contact" DROP COLUMN "fiche_id"`);
    }

}
