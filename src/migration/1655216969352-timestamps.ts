import {MigrationInterface, QueryRunner} from "typeorm";

export class timestamps1655216969352 implements MigrationInterface {
    name = 'timestamps1655216969352'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "territory" ADD "created_at" TIMESTAMP NOT NULL DEFAULT now()`);
        await queryRunner.query(`ALTER TABLE "territory" ADD "updated_at" TIMESTAMP NOT NULL DEFAULT now()`);
        await queryRunner.query(`ALTER TABLE "deveco" ADD "created_at" TIMESTAMP NOT NULL DEFAULT now()`);
        await queryRunner.query(`ALTER TABLE "deveco" ADD "updated_at" TIMESTAMP NOT NULL DEFAULT now()`);
        await queryRunner.query(`ALTER TABLE "echange" ADD "created_at" TIMESTAMP NOT NULL DEFAULT now()`);
        await queryRunner.query(`ALTER TABLE "echange" ADD "updated_at" TIMESTAMP NOT NULL DEFAULT now()`);
        await queryRunner.query(`ALTER TABLE "fiche" ADD "created_at" TIMESTAMP NOT NULL DEFAULT now()`);
        await queryRunner.query(`ALTER TABLE "fiche" ADD "updated_at" TIMESTAMP NOT NULL DEFAULT now()`);
        await queryRunner.query(`ALTER TABLE "demande" ADD "created_at" TIMESTAMP NOT NULL DEFAULT now()`);
        await queryRunner.query(`ALTER TABLE "demande" ADD "updated_at" TIMESTAMP NOT NULL DEFAULT now()`);
        await queryRunner.query(`UPDATE "territory" SET "created_at" = now() - interval '7 day'`);
        await queryRunner.query(`UPDATE "territory" SET "updated_at" = now() - interval '7 day'`);
        await queryRunner.query(`UPDATE "deveco" SET "created_at" = now() - interval '7 day'`);
        await queryRunner.query(`UPDATE "deveco" SET "updated_at" = now() - interval '7 day'`);
        await queryRunner.query(`UPDATE "echange" SET "created_at" = now() - interval '7 day'`);
        await queryRunner.query(`UPDATE "echange" SET "updated_at" = now() - interval '7 day'`);
        await queryRunner.query(`UPDATE "fiche" SET "created_at" = now() - interval '7 day'`);
        await queryRunner.query(`UPDATE "fiche" SET "updated_at" = now() - interval '7 day'`);
        await queryRunner.query(`UPDATE "demande" SET "created_at" = now() - interval '7 day'`);
        await queryRunner.query(`UPDATE "demande" SET "updated_at" = now() - interval '7 day'`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "demande" DROP COLUMN "updated_at"`);
        await queryRunner.query(`ALTER TABLE "demande" DROP COLUMN "created_at"`);
        await queryRunner.query(`ALTER TABLE "fiche" DROP COLUMN "updated_at"`);
        await queryRunner.query(`ALTER TABLE "fiche" DROP COLUMN "created_at"`);
        await queryRunner.query(`ALTER TABLE "echange" DROP COLUMN "updated_at"`);
        await queryRunner.query(`ALTER TABLE "echange" DROP COLUMN "created_at"`);
        await queryRunner.query(`ALTER TABLE "deveco" DROP COLUMN "updated_at"`);
        await queryRunner.query(`ALTER TABLE "deveco" DROP COLUMN "created_at"`);
        await queryRunner.query(`ALTER TABLE "territory" DROP COLUMN "updated_at"`);
        await queryRunner.query(`ALTER TABLE "territory" DROP COLUMN "created_at"`);
    }

}
