import { MigrationInterface, QueryRunner } from "typeorm";

export class ess1680207631830 implements MigrationInterface {
    name = 'ess1680207631830'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "territory_etablissement_reference" ADD "ess" boolean NOT NULL DEFAULT false`);
        await queryRunner.query(`CREATE INDEX "IDX_b59d5de888cb07f6b92e1fa43a" ON "territory_etablissement_reference" ("ess") `);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`DROP INDEX "public"."IDX_b59d5de888cb07f6b92e1fa43a"`);
        await queryRunner.query(`ALTER TABLE "territory_etablissement_reference" DROP COLUMN "ess"`);
    }

}
