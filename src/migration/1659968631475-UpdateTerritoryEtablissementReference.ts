import { MigrationInterface, QueryRunner } from "typeorm";

export class UpdateTerritoryEtablissementReference1659968631475 implements MigrationInterface {
    name = 'UpdateTerritoryEtablissementReference1659968631475'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "territory_etablissement_reference" ADD "territory_id" integer`);
        await queryRunner.query(`ALTER TABLE "territory_etablissement_reference" ADD "siret" character varying(14) NOT NULL`);
        await queryRunner.query(`ALTER TABLE "territory_etablissement_reference" ADD CONSTRAINT "FK_11e7290ea493d892783abb1a5bf" FOREIGN KEY ("territory_id") REFERENCES "territory"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "territory_etablissement_reference" ADD CONSTRAINT "FK_71e7291ea493d892783abb1a5bf" FOREIGN KEY ("siret") REFERENCES "sirene_etablissement"("siret") ON DELETE NO ACTION ON UPDATE NO ACTION`);

    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "territory_etablissement_reference" DROP CONSTRAINT "FK_11e7290ea493d892783abb1a5bf"`);
        await queryRunner.query(`ALTER TABLE "territory_etablissement_reference" DROP CONSTRAINT "FK_71e7291ea493d892783abb1a5bf"`);
        await queryRunner.query(`ALTER TABLE "territory_etablissement_reference" DROP COLUMN "territory_id"`);
        await queryRunner.query(`ALTER TABLE "territory_etablissement_reference" DROP COLUMN "siret"`);

    }

}
