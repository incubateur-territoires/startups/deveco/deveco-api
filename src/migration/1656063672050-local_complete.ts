import {MigrationInterface, QueryRunner} from "typeorm";

export class localComplete1656063672050 implements MigrationInterface {
    name = 'localComplete1656063672050'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "local" ADD "titre" character varying NOT NULL`);
        await queryRunner.query(`ALTER TABLE "local" DROP CONSTRAINT "PK_0fb290786865912848b7a60dd90"`);
        await queryRunner.query(`ALTER TABLE "local" ADD CONSTRAINT "PK_82c35a26f26984f8f22fa5b4633" PRIMARY KEY ("id", "titre")`);
        await queryRunner.query(`ALTER TABLE "local" ADD "adresse" character varying NOT NULL`);
        await queryRunner.query(`ALTER TABLE "local" DROP CONSTRAINT "PK_82c35a26f26984f8f22fa5b4633"`);
        await queryRunner.query(`ALTER TABLE "local" ADD CONSTRAINT "PK_a3ed97b4eb569451a652fc8778b" PRIMARY KEY ("id", "titre", "adresse")`);
        await queryRunner.query(`ALTER TABLE "local" ADD "ville" character varying`);
        await queryRunner.query(`ALTER TABLE "local" ADD "code_postal" character varying`);
        await queryRunner.query(`ALTER TABLE "local" ADD "geolocation" character varying`);
        await queryRunner.query(`ALTER TABLE "local" ADD "local_type" character varying NOT NULL`);
        await queryRunner.query(`ALTER TABLE "local" ADD "local_statut" character varying NOT NULL`);
        await queryRunner.query(`ALTER TABLE "local" ADD "surface" character varying`);
        await queryRunner.query(`ALTER TABLE "local" ADD "loyer" character varying`);
        await queryRunner.query(`ALTER TABLE "local" ADD "localisations" text`);
        await queryRunner.query(`ALTER TABLE "local" ADD "commentaire" character varying`);
        await queryRunner.query(`ALTER TABLE "local" ADD "created_at" TIMESTAMP NOT NULL DEFAULT now()`);
        await queryRunner.query(`ALTER TABLE "local" ADD "updated_at" TIMESTAMP NOT NULL DEFAULT now()`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "local" DROP COLUMN "updated_at"`);
        await queryRunner.query(`ALTER TABLE "local" DROP COLUMN "created_at"`);
        await queryRunner.query(`ALTER TABLE "local" DROP COLUMN "commentaire"`);
        await queryRunner.query(`ALTER TABLE "local" DROP COLUMN "localisations"`);
        await queryRunner.query(`ALTER TABLE "local" DROP COLUMN "loyer"`);
        await queryRunner.query(`ALTER TABLE "local" DROP COLUMN "surface"`);
        await queryRunner.query(`ALTER TABLE "local" DROP COLUMN "local_statut"`);
        await queryRunner.query(`ALTER TABLE "local" DROP COLUMN "local_type"`);
        await queryRunner.query(`ALTER TABLE "local" DROP COLUMN "geolocation"`);
        await queryRunner.query(`ALTER TABLE "local" DROP COLUMN "code_postal"`);
        await queryRunner.query(`ALTER TABLE "local" DROP COLUMN "ville"`);
        await queryRunner.query(`ALTER TABLE "local" DROP CONSTRAINT "PK_a3ed97b4eb569451a652fc8778b"`);
        await queryRunner.query(`ALTER TABLE "local" ADD CONSTRAINT "PK_82c35a26f26984f8f22fa5b4633" PRIMARY KEY ("id", "titre")`);
        await queryRunner.query(`ALTER TABLE "local" DROP COLUMN "adresse"`);
        await queryRunner.query(`ALTER TABLE "local" DROP CONSTRAINT "PK_82c35a26f26984f8f22fa5b4633"`);
        await queryRunner.query(`ALTER TABLE "local" ADD CONSTRAINT "PK_0fb290786865912848b7a60dd90" PRIMARY KEY ("id")`);
        await queryRunner.query(`ALTER TABLE "local" DROP COLUMN "titre"`);
    }

}
