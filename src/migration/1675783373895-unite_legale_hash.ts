import { MigrationInterface, QueryRunner } from 'typeorm';

export class uniteLegaleSireneHash1675783373895 implements MigrationInterface {
	name = 'uniteLegaleSireneHash1675783373895';

	public async up(queryRunner: QueryRunner): Promise<void> {
		await queryRunner.query(`ALTER TABLE "sirene_unite_legale" ADD "hash" character varying(32)`);
	}

	public async down(queryRunner: QueryRunner): Promise<void> {
		await queryRunner.query(`ALTER TABLE "sirene_unite_legale" DROP COLUMN "hash"`);
	}
}
