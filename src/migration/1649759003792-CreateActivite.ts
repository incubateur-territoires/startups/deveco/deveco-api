import {MigrationInterface, QueryRunner} from "typeorm";

export class CreateActivite1649759003792 implements MigrationInterface {
    name = 'CreateActivite1649759003792'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE TABLE "activite_entreprise" ("id" SERIAL NOT NULL, "activite" character varying NOT NULL, "territory_id" integer, CONSTRAINT "PK_e7af206ac0d63b49be55712d77e" PRIMARY KEY ("id"))`);
        await queryRunner.query(`ALTER TABLE "fiche" ADD "entreprise_activites" text`);
        await queryRunner.query(`ALTER TABLE "fiche" ADD "entreprise_activite_autre" character varying`);
        await queryRunner.query(`ALTER TABLE "activite_entreprise" ADD CONSTRAINT "FK_771587c7e49d31bccfc3154cae4" FOREIGN KEY ("territory_id") REFERENCES "territory"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "activite_entreprise" DROP CONSTRAINT "FK_771587c7e49d31bccfc3154cae4"`);
        await queryRunner.query(`ALTER TABLE "fiche" DROP COLUMN "entreprise_activite_autre"`);
        await queryRunner.query(`ALTER TABLE "fiche" DROP COLUMN "entreprise_activites"`);
        await queryRunner.query(`DROP TABLE "activite_entreprise"`);
    }

}
