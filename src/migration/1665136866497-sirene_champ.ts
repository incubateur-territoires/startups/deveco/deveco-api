import { MigrationInterface, QueryRunner } from "typeorm";

export class epciCommune1665136866497 implements MigrationInterface {
    name = 'epciCommune1665136866497'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE TYPE "public"."sirene_unite_legale_societe_mission_unite_legale_enum" AS ENUM('O', 'N')`);
        await queryRunner.query(`ALTER TABLE "sirene_unite_legale" ADD "societe_mission_unite_legale" "public"."sirene_unite_legale_societe_mission_unite_legale_enum"`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "sirene_unite_legale" DROP COLUMN "societe_mission_unite_legale"`);
        await queryRunner.query(`DROP TYPE "public"."sirene_unite_legale_societe_mission_unite_legale_enum"`);
    }

}
