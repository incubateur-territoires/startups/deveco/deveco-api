import { MigrationInterface, QueryRunner } from "typeorm";

export class disableAccount1676319657418 implements MigrationInterface {
    name = 'disableAccount1676319657418'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "account" ADD "enabled" boolean NOT NULL DEFAULT true`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "account" DROP COLUMN "enabled"`);
    }

}
