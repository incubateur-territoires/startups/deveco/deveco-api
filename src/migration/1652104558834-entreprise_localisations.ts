import {MigrationInterface, QueryRunner} from "typeorm";

export class entrepriseLocalisations1652104558834 implements MigrationInterface {
    name = 'entrepriseLocalisations1652104558834'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "fiche" ADD "entreprise_localisations" text`);
        await queryRunner.query(`CREATE TABLE "localisation_entreprise" ("id" SERIAL NOT NULL, "localisation" character varying NOT NULL, "territory_id" integer, CONSTRAINT "PK_1d97981f3faac0ad3401f0745dc" PRIMARY KEY ("id"))`);
        await queryRunner.query(`ALTER TABLE "localisation_entreprise" ADD CONSTRAINT "FK_ae48a48c093acb601917bfbb31a" FOREIGN KEY ("territory_id") REFERENCES "territory"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "fiche" DROP COLUMN "entreprise_localisations"`);
        await queryRunner.query(`ALTER TABLE "localisation_entreprise" DROP CONSTRAINT "FK_ae48a48c093acb601917bfbb31a"`);
        await queryRunner.query(`DROP TABLE "localisation_entreprise"`);
    }

}
