import { MigrationInterface, QueryRunner } from "typeorm";

export class geoJsonToken1666444246298 implements MigrationInterface {
    name = 'geoJsonToken1666444246298'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE TABLE "geo_token" ("id" SERIAL NOT NULL, "token" character varying NOT NULL, "active" boolean NOT NULL DEFAULT true, "deveco_id" integer, CONSTRAINT "PK_a22102ccd6036b34c92c675e7f9" PRIMARY KEY ("id"))`);
        await queryRunner.query(`CREATE TABLE "geo_token_request" ("id" SERIAL NOT NULL, "user_agent" character varying, "url" character varying NOT NULL, "created_at" TIMESTAMP NOT NULL DEFAULT now(), "geo_token_id" integer, CONSTRAINT "PK_d5fe856933f6def106e8af74b5b" PRIMARY KEY ("id"))`);
        await queryRunner.query(`ALTER TABLE "geo_token" ADD CONSTRAINT "FK_e686de08d0bc02c0f9f68997ca4" FOREIGN KEY ("deveco_id") REFERENCES "deveco"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "geo_token_request" ADD CONSTRAINT "FK_aeef7771b7cd26f7030c3c83bef" FOREIGN KEY ("geo_token_id") REFERENCES "geo_token"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "geo_token_request" DROP CONSTRAINT "FK_aeef7771b7cd26f7030c3c83bef"`);
        await queryRunner.query(`ALTER TABLE "geo_token" DROP CONSTRAINT "FK_e686de08d0bc02c0f9f68997ca4"`);
        await queryRunner.query(`DROP TABLE "geo_token_request"`);
        await queryRunner.query(`DROP TABLE "geo_token"`);
    }
}
