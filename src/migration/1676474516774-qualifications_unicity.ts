import { MigrationInterface, QueryRunner } from "typeorm";

export class qualificationsUnicity1676474516774 implements MigrationInterface {
    name = 'qualificationsUnicity1676474516774'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`DELETE FROM activite_entreprise a USING activite_entreprise b WHERE a.id > b.id AND a.activite = b.activite AND a.territory_id = b.territory_id`);
        await queryRunner.query(`DELETE FROM localisation_entreprise a USING localisation_entreprise b WHERE a.id > b.id AND a.localisation = b.localisation AND a.territory_id = b.territory_id`);
        await queryRunner.query(`DELETE FROM mot_cle a USING mot_cle b WHERE a.id > b.id AND a.mot_cle = b.mot_cle AND a.territory_id = b.territory_id`);
        await queryRunner.query(`ALTER TABLE "activite_entreprise" ADD CONSTRAINT "UNIQ_activite_entreprise_for_territoire" UNIQUE ("territory_id", "activite")`);
        await queryRunner.query(`ALTER TABLE "localisation_entreprise" ADD CONSTRAINT "UNIQ_localisation_entreprise_for_territoire" UNIQUE ("territory_id", "localisation")`);
        await queryRunner.query(`ALTER TABLE "mot_cle" ADD CONSTRAINT "UNIQ_mot_cle_for_territoire" UNIQUE ("territory_id", "mot_cle")`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "mot_cle" DROP CONSTRAINT "UNIQ_mot_cle_for_territoire"`);
        await queryRunner.query(`ALTER TABLE "localisation_entreprise" DROP CONSTRAINT "UNIQ_localisation_entreprise_for_territoire"`);
        await queryRunner.query(`ALTER TABLE "activite_entreprise" DROP CONSTRAINT "UNIQ_activite_entreprise_for_territoire"`);
    }

}
