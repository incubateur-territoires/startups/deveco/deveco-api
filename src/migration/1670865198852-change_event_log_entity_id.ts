import { MigrationInterface, QueryRunner } from 'typeorm';

export class changeEventLogEntityId1670865198852 implements MigrationInterface {
	public async up(queryRunner: QueryRunner): Promise<void> {
		await queryRunner.query(`
            DELETE FROM event_log
            WHERE NOT EXISTS (
                SELECT fiche.entite_id
                FROM fiche
                WHERE event_log.entity_id = fiche.id
            )
        `);
		await queryRunner.query(`
            UPDATE event_log
            SET entity_id = (
                SELECT fiche.entite_id
                FROM fiche
                WHERE event_log.entity_id = fiche.id
            )
            WHERE entity_type IN ('FICHE_PP', 'FICHE_PM')
            AND EXISTS (
                SELECT fiche.entite_id
                FROM fiche
                WHERE event_log.entity_id = fiche.id
            )
        `);
	}

	public async down(queryRunner: QueryRunner): Promise<void> {
		await queryRunner.query(`
            UPDATE event_log
            SET entity_id = (
                SELECT fiche.id
                FROM fiche
                WHERE event_log.entity_id = fiche.entite_id
            )
            WHERE entity_type IN ('FICHE_PP', 'FICHE_PM')
        `);
	}
}
