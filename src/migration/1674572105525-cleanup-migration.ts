import { MigrationInterface, QueryRunner } from "typeorm";

export class cleanupMigration1674568452253 implements MigrationInterface {
    name = 'cleanupMigration1674568452253'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "commune" ALTER COLUMN "com_parent" DROP NOT NULL`);

        await queryRunner.query(`ALTER TABLE "entreprise" ADD CONSTRAINT "UQ_974ff6395741fd9fcfe2efdbf6e" UNIQUE ("siret")`);

        await queryRunner.query(`ALTER TABLE "entreprise" ADD CONSTRAINT "FK_974ff6395741fd9fcfe2efdbf6e" FOREIGN KEY ("siret") REFERENCES "etablissement"("siret") ON DELETE NO ACTION ON UPDATE NO ACTION`);

        await queryRunner.query(`ALTER TABLE "territory_etablissement_reference" ALTER COLUMN "etat_administratif" SET NOT NULL`);

        await queryRunner.query(`ALTER TABLE "sirene_unite_legale" RENAME COLUMN "economie_sociale_solidaire_unite_legale" TO "eco_soc_sol_unite_legale"`);
        await queryRunner.query(`ALTER TYPE "public"."sirene_unite_legale_economie_sociale_solidaire_unite_legale_enu" RENAME TO "sirene_unite_legale_eco_soc_sol_unite_legale_enum"`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "territory_etablissement_reference" ALTER COLUMN "etat_administratif" DROP NOT NULL`);

        await queryRunner.query(`ALTER TABLE "entreprise" DROP CONSTRAINT "FK_974ff6395741fd9fcfe2efdbf6e"`);

        await queryRunner.query(`ALTER TABLE "entreprise" DROP CONSTRAINT "UQ_974ff6395741fd9fcfe2efdbf6e"`);

        await queryRunner.query(`ALTER TABLE "commune" ALTER COLUMN "com_parent" SET NOT NULL`);

        await queryRunner.query(`ALTER TYPE "public"."sirene_unite_legale_eco_soc_sol_unite_legale_enum" RENAME TO "sirene_unite_legale_economie_sociale_solidaire_unite_legale_enu"`);
        await queryRunner.query(`ALTER TABLE "sirene_unite_legale" RENAME COLUMN "eco_soc_sol_unite_legale" TO "economie_sociale_solidaire_unite_legale"`);
    }

}
