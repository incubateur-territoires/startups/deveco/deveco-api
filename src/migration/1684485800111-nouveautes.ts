import { MigrationInterface, QueryRunner } from "typeorm";

export class nouveautes1684485800111 implements MigrationInterface {
    name = 'nouveautes1684485800111'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE TABLE "nouveautes" ("id" SERIAL NOT NULL, "amount" integer NOT NULL, "created_at" TIMESTAMP NOT NULL DEFAULT now(), CONSTRAINT "PK_31d64ed71eccdac9ee6846bde38" PRIMARY KEY ("id"))`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`DROP TABLE "nouveautes"`);
    }

}
