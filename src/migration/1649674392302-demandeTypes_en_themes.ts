import {MigrationInterface, QueryRunner} from "typeorm";

export class demandeTypesEnThemes1649674392302 implements MigrationInterface {
    name = 'demandeTypesEnThemes1649674392302'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "echange" RENAME COLUMN "demandeTypes" TO "themes"`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "echange" RENAME COLUMN "themes" TO "demandeTypes"`);
    }

}
