import { MigrationInterface, QueryRunner } from "typeorm";

export class cleanup1683896565817 implements MigrationInterface {
    name = 'cleanup1683896565817'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "login" DROP CONSTRAINT "FK_fb7a011c92438dd38ad957514a8"`);
				await queryRunner.query(`ALTER TABLE "login" ADD CONSTRAINT "FK_ccb21910a98c45deb13e4a9c219" FOREIGN KEY ("account_id") REFERENCES "account"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);

        await queryRunner.query(`DROP INDEX "public"."IDX_c69ec1f2bc2b668bd159690e31"`);
        await queryRunner.query(`DROP INDEX "public"."IDX_6e70969abcdba7fd315cd6f7c9"`);

        await queryRunner.query(`CREATE INDEX "IDX_08f86ed1791de4e0863e1b8ccf" ON "lien_succession" ("siret_predecesseur") `);
        await queryRunner.query(`CREATE INDEX "IDX_70329533adf03ec170bb587323" ON "lien_succession" ("siret_successeur") `);
        await queryRunner.query(`CREATE INDEX "IDX_f28a85ff64863615bc700c7fac" ON "lien_succession_temp" ("siret_predecesseur") `);
        await queryRunner.query(`CREATE INDEX "IDX_96649822a475c38721aca20bb7" ON "lien_succession_temp" ("siret_successeur") `);

        await queryRunner.query(`ALTER TABLE "commune" ADD CONSTRAINT "FK_2e2a0a6b155cd875656ddd86b7c" FOREIGN KEY ("insee_reg") REFERENCES "region"("insee_reg") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "commune" ADD CONSTRAINT "FK_02cf54e48cfe56631252e643261" FOREIGN KEY ("ctcd") REFERENCES "departement"("ctcd") ON DELETE NO ACTION ON UPDATE NO ACTION`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "login" DROP CONSTRAINT "FK_ccb21910a98c45deb13e4a9c219"`);
        await queryRunner.query(`ALTER TABLE "commune" DROP CONSTRAINT "FK_02cf54e48cfe56631252e643261"`);
        await queryRunner.query(`ALTER TABLE "commune" DROP CONSTRAINT "FK_2e2a0a6b155cd875656ddd86b7c"`);
        await queryRunner.query(`DROP INDEX "public"."IDX_96649822a475c38721aca20bb7"`);
        await queryRunner.query(`DROP INDEX "public"."IDX_f28a85ff64863615bc700c7fac"`);
        await queryRunner.query(`DROP INDEX "public"."IDX_70329533adf03ec170bb587323"`);
        await queryRunner.query(`DROP INDEX "public"."IDX_08f86ed1791de4e0863e1b8ccf"`);
        await queryRunner.query(`CREATE INDEX "IDX_6e70969abcdba7fd315cd6f7c9" ON "lien_succession" ("siret_predecesseur") `);
        await queryRunner.query(`CREATE INDEX "IDX_c69ec1f2bc2b668bd159690e31" ON "lien_succession" ("siret_successeur") `);
        await queryRunner.query(`ALTER TABLE "login" ADD CONSTRAINT "FK_fb7a011c92438dd38ad957514a8" FOREIGN KEY ("account_id") REFERENCES "account"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
    }

}
