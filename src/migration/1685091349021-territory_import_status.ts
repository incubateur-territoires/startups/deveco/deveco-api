import { MigrationInterface, QueryRunner } from "typeorm";

export class territoryImportStatus1685091349021 implements MigrationInterface {
    name = 'territoryImportStatus1685091349021'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE TABLE "territory_import_status" ("id" SERIAL NOT NULL, "geolocation" boolean NOT NULL DEFAULT false, "ter" boolean NOT NULL DEFAULT false, "qpv" boolean NOT NULL DEFAULT false, "stats" boolean NOT NULL DEFAULT false, "created_at" TIMESTAMP NOT NULL DEFAULT now(), "updated_at" TIMESTAMP NOT NULL DEFAULT now(), "territoire_id" integer NOT NULL, CONSTRAINT "uniq_territory" UNIQUE ("territoire_id"), CONSTRAINT "REL_3660db63e1b58b8412a7071e0d" UNIQUE ("territoire_id"), CONSTRAINT "PK_0a5e37ff6b09e5a72ddf87efb26" PRIMARY KEY ("id"))`);
        await queryRunner.query(`ALTER TABLE "territory" DROP COLUMN "import_in_progress"`);
        await queryRunner.query(`ALTER TABLE "territory_import_status" ADD CONSTRAINT "FK_3660db63e1b58b8412a7071e0d0" FOREIGN KEY ("territoire_id") REFERENCES "territory"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`INSERT INTO "territory_import_status" ("geolocation", "ter", "qpv", "stats", "territoire_id") SELECT true, true, true, true, id FROM territory`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "territory_import_status" DROP CONSTRAINT "FK_3660db63e1b58b8412a7071e0d0"`);
        await queryRunner.query(`ALTER TABLE "territory" ADD "import_in_progress" boolean NOT NULL DEFAULT true`);
        await queryRunner.query(`DROP TABLE "territory_import_status"`);
    }

}
