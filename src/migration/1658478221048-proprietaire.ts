import { MigrationInterface, QueryRunner } from "typeorm";

export class proprietaire1658478221048 implements MigrationInterface {
    name = 'proprietaire1658478221048'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE TABLE "proprietaire" ("id" SERIAL NOT NULL, "proprietaire_type" character varying NOT NULL, "created_at" TIMESTAMP NOT NULL DEFAULT now(), "updated_at" TIMESTAMP NOT NULL DEFAULT now(), "local_id" integer, "entite_id" integer, "territoire_id" integer, "auteur_modification_id" integer, "deveco_id" integer, CONSTRAINT "UNIQ_proprietaire_local_entite" UNIQUE ("local_id", "entite_id"), CONSTRAINT "REL_2f90b8ab0d3d0b5b393821f4f6" UNIQUE ("local_id"), CONSTRAINT "REL_ed9e03d6df42f75e0312eaeed4" UNIQUE ("entite_id"), CONSTRAINT "PK_9c03e1f16979eb4d7462f7513e9" PRIMARY KEY ("id"))`);
        await queryRunner.query(`ALTER TABLE "proprietaire" ADD CONSTRAINT "FK_2f90b8ab0d3d0b5b393821f4f6b" FOREIGN KEY ("local_id") REFERENCES "local"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "proprietaire" ADD CONSTRAINT "FK_ed9e03d6df42f75e0312eaeed4a" FOREIGN KEY ("entite_id") REFERENCES "entite"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "proprietaire" ADD CONSTRAINT "FK_d0def1da187f4e97d238fba755d" FOREIGN KEY ("territoire_id") REFERENCES "territory"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "proprietaire" ADD CONSTRAINT "FK_f91180c1e7e37fb2b1e4536be8c" FOREIGN KEY ("auteur_modification_id") REFERENCES "deveco"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "proprietaire" ADD CONSTRAINT "FK_ef2fba811dd09ea45d86800ddac" FOREIGN KEY ("deveco_id") REFERENCES "deveco"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "contact" ADD CONSTRAINT "FK_5d74910608cb76beb22cfa47766" FOREIGN KEY ("particulier_id") REFERENCES "particulier"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "contact" DROP CONSTRAINT "FK_5d74910608cb76beb22cfa47766"`);
        await queryRunner.query(`ALTER TABLE "proprietaire" DROP CONSTRAINT "FK_ef2fba811dd09ea45d86800ddac"`);
        await queryRunner.query(`ALTER TABLE "proprietaire" DROP CONSTRAINT "FK_f91180c1e7e37fb2b1e4536be8c"`);
        await queryRunner.query(`ALTER TABLE "proprietaire" DROP CONSTRAINT "FK_d0def1da187f4e97d238fba755d"`);
        await queryRunner.query(`ALTER TABLE "proprietaire" DROP CONSTRAINT "FK_ed9e03d6df42f75e0312eaeed4a"`);
        await queryRunner.query(`ALTER TABLE "proprietaire" DROP CONSTRAINT "FK_2f90b8ab0d3d0b5b393821f4f6b"`);
        await queryRunner.query(`DROP TABLE "proprietaire"`);
    }

}
