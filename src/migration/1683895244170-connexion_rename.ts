import { MigrationInterface, QueryRunner } from "typeorm";

export class connexionRename1683895244170 implements MigrationInterface {
    name = 'connexionRename1683895244170'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE connexion RENAME TO login`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE login RENAME TO connexion`);
    }

}
