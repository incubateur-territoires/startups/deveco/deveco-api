import { MigrationInterface, QueryRunner } from "typeorm";

export class enseigne1663855115860 implements MigrationInterface {
    name = 'enseigne1663855115860'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "entreprise" ADD "enseigne" character varying`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "entreprise" DROP COLUMN "enseigne"`);
    }

}
