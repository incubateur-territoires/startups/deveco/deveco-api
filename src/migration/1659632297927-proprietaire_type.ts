import { MigrationInterface, QueryRunner } from "typeorm";

export class proprietaireType1659632297927 implements MigrationInterface {
    name = 'proprietaireType1659632297927'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "proprietaire" DROP COLUMN "proprietaire_type"`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "proprietaire" ADD "proprietaire_type" character varying NOT NULL`);
    }

}
