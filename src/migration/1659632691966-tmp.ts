import { MigrationInterface, QueryRunner } from "typeorm";

export class tmp1659632691966 implements MigrationInterface {
    name = 'tmp1659632691966'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "proprietaire" DROP CONSTRAINT "FK_2f90b8ab0d3d0b5b393821f4f6b"`);
        await queryRunner.query(`ALTER TABLE "proprietaire" DROP CONSTRAINT "FK_ed9e03d6df42f75e0312eaeed4a"`);
        await queryRunner.query(`ALTER TABLE "proprietaire" DROP CONSTRAINT "UNIQ_proprietaire_local_entite"`);
        await queryRunner.query(`ALTER TABLE "proprietaire" DROP CONSTRAINT "REL_2f90b8ab0d3d0b5b393821f4f6"`);
        await queryRunner.query(`ALTER TABLE "proprietaire" DROP CONSTRAINT "REL_ed9e03d6df42f75e0312eaeed4"`);
        await queryRunner.query(`ALTER TABLE "proprietaire" ADD CONSTRAINT "UNIQ_proprietaire_local_entite" UNIQUE ("local_id", "entite_id")`);
        await queryRunner.query(`ALTER TABLE "proprietaire" ADD CONSTRAINT "FK_2f90b8ab0d3d0b5b393821f4f6b" FOREIGN KEY ("local_id") REFERENCES "local"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "proprietaire" ADD CONSTRAINT "FK_ed9e03d6df42f75e0312eaeed4a" FOREIGN KEY ("entite_id") REFERENCES "entite"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "proprietaire" DROP CONSTRAINT "FK_ed9e03d6df42f75e0312eaeed4a"`);
        await queryRunner.query(`ALTER TABLE "proprietaire" DROP CONSTRAINT "FK_2f90b8ab0d3d0b5b393821f4f6b"`);
        await queryRunner.query(`ALTER TABLE "proprietaire" DROP CONSTRAINT "UNIQ_proprietaire_local_entite"`);
        await queryRunner.query(`ALTER TABLE "proprietaire" ADD CONSTRAINT "REL_ed9e03d6df42f75e0312eaeed4" UNIQUE ("entite_id")`);
        await queryRunner.query(`ALTER TABLE "proprietaire" ADD CONSTRAINT "REL_2f90b8ab0d3d0b5b393821f4f6" UNIQUE ("local_id")`);
        await queryRunner.query(`ALTER TABLE "proprietaire" ADD CONSTRAINT "UNIQ_proprietaire_local_entite" UNIQUE ("local_id", "entite_id")`);
        await queryRunner.query(`ALTER TABLE "proprietaire" ADD CONSTRAINT "FK_ed9e03d6df42f75e0312eaeed4a" FOREIGN KEY ("entite_id") REFERENCES "entite"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "proprietaire" ADD CONSTRAINT "FK_2f90b8ab0d3d0b5b393821f4f6b" FOREIGN KEY ("local_id") REFERENCES "local"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
    }

}
