import { MigrationInterface, QueryRunner } from "typeorm";

export class sirene1664866860764 implements MigrationInterface {
    name = 'sirene1664866860764'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE TABLE IF NOT EXISTS "etablissement" ("siren" character varying(9) NOT NULL, "siret" character varying(14) NOT NULL, "nom_public" character varying, "nom_recherche" character varying, "enseigne" character varying, "adresse_complete" character varying, "code_commune" character varying, "longitude" double precision, "latitude" double precision, "tranche_effectifs" character varying(2), "annee_effectifs" character varying(9), "sigle" character varying, "code_naf" character varying, "libelle_naf" character varying, "libelle_categorie_naf" character varying, "date_creation" date, "date_creation_entreprise" date, "etat_administratif" character varying NOT NULL, "categorie_juridique" character varying NOT NULL, "type_etablissement" character varying, CONSTRAINT "PK_1fc916141e368b8f59c2ec6753e" PRIMARY KEY ("siret"))`);

        await queryRunner.query(`CREATE TABLE IF NOT EXISTS "geoloc_entreprise" ("siret" character varying NOT NULL, "x" double precision, "y" double precision, "qualite_xy" character varying, "epsg" character varying, "plg_qp" character varying, "plg_iris" character varying, "plg_zus" character varying, "plg_zfu" character varying, "plg_qva" character varying, "plg_code_commune" character varying, "distance_precision" character varying, "qualite_qp" character varying, "qualite_iris" character varying, "qualite_zus" character varying, "qualite_zfu" character varying, "qualite_qva" character varying, "y_latitude" double precision, "x_longitude" double precision, CONSTRAINT "PK_94e552e5dbe3d06f725ca06961d" PRIMARY KEY ("siret"))`);
        await queryRunner.query(`CREATE INDEX IF NOT EXISTS "qp_metropoleoutremer_wgs84_epsg4326_geom_idx" ON "qp_metropoleoutremer_wgs84_epsg4326" USING GiST ("geom") `);

        await queryRunner.query(`INSERT INTO "typeorm_metadata"("database", "schema", "table", "type", "name", "value") VALUES ($1, $2, $3, $4, $5, $6)`, ["deveco","public","territory_etablissement_reference","GENERATED_COLUMN","tsv","to_tsvector('french', nom_recherche)"]);
        await queryRunner.query(`CREATE TABLE IF NOT EXISTS "territory_etablissement_reference" ("id" character varying NOT NULL, "siret" character varying(14) NOT NULL, "nom_recherche" character varying NOT NULL, "etat_administratif" character varying, "libelle_naf" character varying, "code_naf" character varying, "categorie_juridique_courante" boolean NOT NULL, "tranche_effectifs" character varying, "code_commune" character varying NOT NULL, "tsv" tsvector GENERATED ALWAYS AS (to_tsvector('french', nom_recherche)) STORED NOT NULL, "date_creation" date, "adresse_complete" character varying, "territory_id" integer NOT NULL, CONSTRAINT "PK_territory_etablissement_reference" PRIMARY KEY ("id"))`);
        await queryRunner.query(`ALTER TABLE "territory_etablissement_reference" DROP CONSTRAINT IF EXISTS "FK_ter_eta_ref_territory_id"`);
        await queryRunner.query(`ALTER TABLE "territory_etablissement_reference" ADD CONSTRAINT "FK_ter_eta_ref_territory_id" FOREIGN KEY ("territory_id") REFERENCES "territory"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`CREATE INDEX IF NOT EXISTS IDX_ter_eta_ref_tsv ON territory_etablissement_reference USING GIN (tsv)`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`DROP INDEX "public"."IDX_ter_eta_ref_tsv"`);
        await queryRunner.query(`ALTER TABLE "territory_etablissement_reference" DROP CONSTRAINT "FK_ter_eta_ref_territory_id"`);
        await queryRunner.query(`DROP TABLE "territory_etablissement_reference"`);
        await queryRunner.query(`DELETE FROM "typeorm_metadata" WHERE "type" = $1 AND "name" = $2 AND "database" = $3 AND "schema" = $4 AND "table" = $5`, ["GENERATED_COLUMN","tsv","deveco","public","territory_etablissement_reference"]);
        await queryRunner.query(`DROP INDEX "public"."qp_metropoleoutremer_wgs84_epsg4326_geom_idx"`);
        await queryRunner.query(`DROP TABLE "geoloc_entreprise"`);

        await queryRunner.query(`DROP TABLE "etablissement"`);
    }

}
