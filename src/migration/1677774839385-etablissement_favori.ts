import { MigrationInterface, QueryRunner } from "typeorm";

export class etablissementFavori1677774839385 implements MigrationInterface {
    name = 'etablissementFavori1677774839385'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE TABLE "etablissement_favori" ("id" SERIAL NOT NULL, "favori" boolean NOT NULL, "created_at" TIMESTAMP NOT NULL DEFAULT now(), "updated_at" TIMESTAMP NOT NULL DEFAULT now(), "deveco_id" integer, "territory_siret" character varying, CONSTRAINT "UNIQ_etablissement_favori" UNIQUE ("deveco_id", "territory_siret"), CONSTRAINT "PK_94340b87651b05ca2ea79e7d0b2" PRIMARY KEY ("id"))`);
        await queryRunner.query(`ALTER TABLE "etablissement_favori" ADD CONSTRAINT "FK_efcf90ca3a2027222bde0c87550" FOREIGN KEY ("deveco_id") REFERENCES "deveco"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "etablissement_favori" ADD CONSTRAINT "FK_8569e6063f7ebabf5f3dcd5ee86" FOREIGN KEY ("territory_siret") REFERENCES "territory_etablissement_reference"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "etablissement_favori" DROP CONSTRAINT "FK_8569e6063f7ebabf5f3dcd5ee86"`);
        await queryRunner.query(`ALTER TABLE "etablissement_favori" DROP CONSTRAINT "FK_efcf90ca3a2027222bde0c87550"`);
        await queryRunner.query(`DROP TABLE "etablissement_favori"`);
    }

}
