import { MigrationInterface, QueryRunner } from 'typeorm';

export class eventLogIndices1680250818504 implements MigrationInterface {
	name = 'eventLogIndices1680250818504';

	public async up(queryRunner: QueryRunner): Promise<void> {
		await queryRunner.query(
			`CREATE INDEX "IDX_42f72c4a61c08441bf089d5c56" ON "event_log" ("deveco_id")`
		);
		await queryRunner.query(
			`CREATE INDEX "IDX_f65e19735d8706dbfda56ed35a" ON "event_log" ("entity_type")`
		);
		await queryRunner.query(
			`CREATE INDEX "IDX_3f5435c8b7735cae90fdfd9e5e" ON "event_log" ("entity_id")`
		);
		await queryRunner.query(
			`CREATE INDEX "IDX_3d248fa0a2294a3ea3b8f03763" ON "event_log" ("action")`
		);
		await queryRunner.query(
			`CREATE INDEX "IDX_2e2a0a6b155cd875656ddd86b7" ON "commune" ("insee_reg")`
		);
		await queryRunner.query(`CREATE INDEX "IDX_02cf54e48cfe56631252e64326" ON "commune" ("ctcd")`);
		await queryRunner.query(
			`CREATE INDEX "IDX_771587c7e49d31bccfc3154cae" ON "activite_entreprise" ("territory_id")`
		);
		await queryRunner.query(
			`CREATE INDEX "IDX_44d8b9c849f601cc29e05fbd88" ON "entite" ("entite_type")`
		);
		await queryRunner.query(
			`CREATE INDEX "IDX_ae48a48c093acb601917bfbb31" ON "localisation_entreprise" ("territory_id")`
		);
		await queryRunner.query(
			`CREATE INDEX "IDX_b5215ebfa132885a10d6c63d6a" ON "mot_cle" ("territory_id")`
		);
	}

	public async down(queryRunner: QueryRunner): Promise<void> {
		await queryRunner.query(`DROP INDEX "public"."IDX_3d248fa0a2294a3ea3b8f03763"`);
		await queryRunner.query(`DROP INDEX "public"."IDX_3f5435c8b7735cae90fdfd9e5e"`);
		await queryRunner.query(`DROP INDEX "public"."IDX_f65e19735d8706dbfda56ed35a"`);
		await queryRunner.query(`DROP INDEX "public"."IDX_42f72c4a61c08441bf089d5c56"`);
		await queryRunner.query(`DROP INDEX "public"."IDX_b5215ebfa132885a10d6c63d6a"`);
		await queryRunner.query(`DROP INDEX "public"."IDX_ae48a48c093acb601917bfbb31"`);
		await queryRunner.query(`DROP INDEX "public"."IDX_44d8b9c849f601cc29e05fbd88"`);
		await queryRunner.query(`DROP INDEX "public"."IDX_771587c7e49d31bccfc3154cae"`);
		await queryRunner.query(`DROP INDEX "public"."IDX_02cf54e48cfe56631252e64326"`);
		await queryRunner.query(`DROP INDEX "public"."IDX_2e2a0a6b155cd875656ddd86b7"`);
	}
}
