import { MigrationInterface, QueryRunner } from "typeorm";

export class removeQpvFromEtablissement1674853554304 implements MigrationInterface {
    name = 'removeQpvFromEtablissement1674853554304'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "etablissement" DROP CONSTRAINT "FK_212e2591a0e941e039f22947a4e"`);
        await queryRunner.query(`ALTER TABLE "etablissement" DROP COLUMN "qpv_id"`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "etablissement" ADD "qpv_id" integer`);
        await queryRunner.query(`ALTER TABLE "etablissement" ADD CONSTRAINT "FK_212e2591a0e941e039f22947a4e" FOREIGN KEY ("qpv_id") REFERENCES "qp_metropoleoutremer_wgs84_epsg4326"("gid") ON DELETE NO ACTION ON UPDATE NO ACTION`);
    }

}
