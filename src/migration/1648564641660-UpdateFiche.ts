import {MigrationInterface, QueryRunner} from "typeorm";

export class UpdateFiche1648564641660 implements MigrationInterface {
    name = 'UpdateFiche1648564641660'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "fiche" ADD "extra_info" character varying`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "fiche" DROP COLUMN "extra_info"`);
    }

}
