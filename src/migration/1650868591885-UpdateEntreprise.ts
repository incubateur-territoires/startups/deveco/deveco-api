import {MigrationInterface, QueryRunner} from "typeorm";

export class UpdateEntreprise1650868591885 implements MigrationInterface {
    name = 'UpdateEntreprise1650868591885'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "entreprise" ADD "longitude" double precision`);
        await queryRunner.query(`ALTER TABLE "entreprise" ADD "latitude" double precision`);
        await queryRunner.query(`ALTER TABLE "entreprise" ADD "etat_administratif" character varying`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "entreprise" DROP COLUMN "latitude"`);
        await queryRunner.query(`ALTER TABLE "entreprise" DROP COLUMN "longitude"`);
        await queryRunner.query(`ALTER TABLE "entreprise" DROP COLUMN "etat_administratif"`);
    }

}
