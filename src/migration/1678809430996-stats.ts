import { MigrationInterface, QueryRunner } from "typeorm";

export class stats1678809430996 implements MigrationInterface {
    name = 'stats1678809430996'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE TABLE "stats" ("id" SERIAL NOT NULL, "code_commune" character varying NOT NULL, "date" date NOT NULL, "content" jsonb NOT NULL, CONSTRAINT "PK_c76e93dfef28ba9b6942f578ab1" PRIMARY KEY ("id"))`);
        await queryRunner.query(`ALTER TABLE "stats" ADD CONSTRAINT "UNIQ_stats_for_commune_for_day" UNIQUE ("code_commune", "date")`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`DROP TABLE "stats"`);
    }

}
