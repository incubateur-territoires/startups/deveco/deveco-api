import {MigrationInterface, QueryRunner} from "typeorm";

export class UpdateFKs1650717886613 implements MigrationInterface {
    name = 'UpdateFKs1650717886613'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "territory" DROP CONSTRAINT "FK_1d67e6ffafac89853948c443730"`);
        await queryRunner.query(`ALTER TABLE "territory" DROP CONSTRAINT "FK_31acb7c820be64ca8470406fe32"`);
        await queryRunner.query(`ALTER TABLE "echange" DROP CONSTRAINT "FK_de5cbe7c883710bdd017fddbb6c"`);
        await queryRunner.query(`ALTER TABLE "rappel" DROP CONSTRAINT "FK_12d759b038600073e7fed16fb1f"`);
        await queryRunner.query(`ALTER TABLE "territory" DROP CONSTRAINT "REL_1d67e6ffafac89853948c44373"`);
        await queryRunner.query(`ALTER TABLE "territory" DROP CONSTRAINT "REL_31acb7c820be64ca8470406fe3"`);
        await queryRunner.query(`ALTER TABLE "echange" ADD CONSTRAINT "FK_06ac2a109b2855ae757c7ef0d4c" FOREIGN KEY ("createur_id") REFERENCES "deveco"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "rappel" ADD CONSTRAINT "FK_6f2af008d71796feae0f6653352" FOREIGN KEY ("createur_id") REFERENCES "deveco"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "entreprise" DROP CONSTRAINT "FK_49d8450adf6a1fe903fb53eb99a"`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "rappel" DROP CONSTRAINT "FK_6f2af008d71796feae0f6653352"`);
        await queryRunner.query(`ALTER TABLE "echange" DROP CONSTRAINT "FK_06ac2a109b2855ae757c7ef0d4c"`);
        await queryRunner.query(`ALTER TABLE "territory" ADD CONSTRAINT "REL_31acb7c820be64ca8470406fe3" UNIQUE ("commune_id")`);
        await queryRunner.query(`ALTER TABLE "territory" ADD CONSTRAINT "REL_1d67e6ffafac89853948c44373" UNIQUE ("epci_id")`);
        await queryRunner.query(`ALTER TABLE "rappel" ADD CONSTRAINT "FK_12d759b038600073e7fed16fb1f" FOREIGN KEY ("createur_id") REFERENCES "deveco"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "echange" ADD CONSTRAINT "FK_de5cbe7c883710bdd017fddbb6c" FOREIGN KEY ("createur_id") REFERENCES "deveco"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "territory" ADD CONSTRAINT "FK_31acb7c820be64ca8470406fe32" FOREIGN KEY ("commune_id") REFERENCES "commune"("insee_com") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "territory" ADD CONSTRAINT "FK_1d67e6ffafac89853948c443730" FOREIGN KEY ("epci_id") REFERENCES "epci"("insee_epci") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "entreprise" ADD CONSTRAINT "FK_49d8450adf6a1fe903fb53eb99a" FOREIGN KEY ("activite_principale_unite_legale") REFERENCES "naf"("id_5") ON DELETE NO ACTION ON UPDATE NO ACTION`);
    }

}
