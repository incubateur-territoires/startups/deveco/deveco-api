import { MigrationInterface, QueryRunner } from "typeorm";

export class territoryNames1682427972374 implements MigrationInterface {
    name = 'territoryNames1682427972374'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "territory" ADD CONSTRAINT "UQ_524248a6d2f2bd303f59f6dda50" UNIQUE ("name")`);
        await queryRunner.query(`ALTER TABLE "territory" DROP CONSTRAINT "uniq_territory_epci_id"`);
        await queryRunner.query(`ALTER TABLE "territory" DROP CONSTRAINT "UQ_a65fe9307faf97650d7372e5a32"`);
        await queryRunner.query(`ALTER TABLE "territory" DROP CONSTRAINT "UQ_1186a7d0783cc916c6e1eb9f154"`);
        await queryRunner.query(`ALTER TABLE "territory" DROP CONSTRAINT "UQ_f298691eaee560ee58f836f9089"`);
        await queryRunner.query(`ALTER TABLE "territory" DROP CONSTRAINT "UQ_fcbdc1c8e8a9a238a117e59d566"`);
        await queryRunner.query(`ALTER TABLE "territory" DROP CONSTRAINT "uniq_territory_commune_id"`);
        await queryRunner.query(`ALTER TABLE "territory" ADD CONSTRAINT "uniq_territory_commune_id" UNIQUE ("commune_id", "name")`);
        await queryRunner.query(`ALTER TABLE "territory" ADD CONSTRAINT "uniq_territory_region_id" UNIQUE ("region_id", "name")`);
        await queryRunner.query(`ALTER TABLE "territory" ADD CONSTRAINT "uniq_territory_departement_id" UNIQUE ("departement_id", "name")`);
        await queryRunner.query(`ALTER TABLE "territory" ADD CONSTRAINT "uniq_territory_petr_id" UNIQUE ("petr_id", "name")`);
        await queryRunner.query(`ALTER TABLE "territory" ADD CONSTRAINT "uniq_territory_metropole_id" UNIQUE ("metropole_id", "name")`);
        await queryRunner.query(`ALTER TABLE "territory" ADD CONSTRAINT "uniq_territory_epci_id" UNIQUE ("epci_id", "name")`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "territory" DROP CONSTRAINT "uniq_territory_epci_id"`);
        await queryRunner.query(`ALTER TABLE "territory" DROP CONSTRAINT "uniq_territory_metropole_id"`);
        await queryRunner.query(`ALTER TABLE "territory" DROP CONSTRAINT "uniq_territory_petr_id"`);
        await queryRunner.query(`ALTER TABLE "territory" DROP CONSTRAINT "uniq_territory_departement_id"`);
        await queryRunner.query(`ALTER TABLE "territory" DROP CONSTRAINT "uniq_territory_region_id"`);
        await queryRunner.query(`ALTER TABLE "territory" DROP CONSTRAINT "uniq_territory_commune_id"`);
        await queryRunner.query(`ALTER TABLE "territory" ADD CONSTRAINT "uniq_territory_commune_id" UNIQUE ("commune_id")`);
        await queryRunner.query(`ALTER TABLE "territory" ADD CONSTRAINT "UQ_fcbdc1c8e8a9a238a117e59d566" UNIQUE ("region_id")`);
        await queryRunner.query(`ALTER TABLE "territory" ADD CONSTRAINT "UQ_f298691eaee560ee58f836f9089" UNIQUE ("departement_id")`);
        await queryRunner.query(`ALTER TABLE "territory" ADD CONSTRAINT "UQ_1186a7d0783cc916c6e1eb9f154" UNIQUE ("petr_id")`);
        await queryRunner.query(`ALTER TABLE "territory" ADD CONSTRAINT "UQ_a65fe9307faf97650d7372e5a32" UNIQUE ("metropole_id")`);
        await queryRunner.query(`ALTER TABLE "territory" ADD CONSTRAINT "uniq_territory_epci_id" UNIQUE ("epci_id")`);
        await queryRunner.query(`ALTER TABLE "territory" DROP CONSTRAINT "UQ_524248a6d2f2bd303f59f6dda50"`);
    }

}
