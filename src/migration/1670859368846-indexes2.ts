import { MigrationInterface, QueryRunner } from "typeorm";

export class indexes21670859368846 implements MigrationInterface {
    name = 'indexes21670859368846'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE INDEX "IDX_447fd3fcc7588fd0b092a487f1" ON "entite" ("territoire_id") `);
        await queryRunner.query(`CREATE INDEX "IDX_5c18958aaa72818d592552105b" ON "entite" ("entreprise_id") `);
        await queryRunner.query(`CREATE INDEX "IDX_02b30026b8b3456e61997d7be9" ON "territory_etablissement_reference" ("siret") `);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`DROP INDEX "public"."IDX_02b30026b8b3456e61997d7be9"`);
        await queryRunner.query(`DROP INDEX "public"."IDX_5c18958aaa72818d592552105b"`);
        await queryRunner.query(`DROP INDEX "public"."IDX_447fd3fcc7588fd0b092a487f1"`);
    }

}
