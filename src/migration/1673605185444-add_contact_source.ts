import { MigrationInterface, QueryRunner } from 'typeorm';

export class addContactSource1673605185444 implements MigrationInterface {
	name = 'addContactSource1673605185444';

	public async up(queryRunner: QueryRunner): Promise<void> {
		await queryRunner.query(`ALTER TABLE "contact" ADD "source" character varying`);
	}

	public async down(queryRunner: QueryRunner): Promise<void> {
		await queryRunner.query(`ALTER TABLE "contact" DROP COLUMN "source"`);
	}
}
