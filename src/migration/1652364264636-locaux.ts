import {MigrationInterface, QueryRunner} from "typeorm";

export class locaux1652364264636 implements MigrationInterface {
    name = 'locaux1652364264636'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE TABLE "local" ("id" SERIAL NOT NULL, "date_modification" TIMESTAMP WITH TIME ZONE NOT NULL, "territoire_id" integer, "auteur_modification_id" integer, "deveco_id" integer, CONSTRAINT "PK_0fb290786865912848b7a60dd90" PRIMARY KEY ("id"))`);
        await queryRunner.query(`ALTER TABLE "local" ADD CONSTRAINT "FK_41b0e57764d8772a7e6f06f0f57" FOREIGN KEY ("territoire_id") REFERENCES "territory"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "local" ADD CONSTRAINT "FK_2bfd4509b9f14784639e99a0ad6" FOREIGN KEY ("auteur_modification_id") REFERENCES "deveco"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "local" ADD CONSTRAINT "FK_4a1f9b4283d8e702a5d4fbc72d9" FOREIGN KEY ("deveco_id") REFERENCES "deveco"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "local" DROP CONSTRAINT "FK_4a1f9b4283d8e702a5d4fbc72d9"`);
        await queryRunner.query(`ALTER TABLE "local" DROP CONSTRAINT "FK_2bfd4509b9f14784639e99a0ad6"`);
        await queryRunner.query(`ALTER TABLE "local" DROP CONSTRAINT "FK_41b0e57764d8772a7e6f06f0f57"`);
        await queryRunner.query(`DROP TABLE "local"`);
    }

}
