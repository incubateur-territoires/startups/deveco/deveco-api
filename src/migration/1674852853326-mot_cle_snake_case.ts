import { MigrationInterface, QueryRunner } from "typeorm";

export class motCleSnakeCase1674852853326 implements MigrationInterface {
    name = 'motCleSnakeCase1674852853326'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "mot_cle" RENAME COLUMN "motCle" TO "mot_cle"`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "mot_cle" RENAME COLUMN "mot_cle" TO "motCle"`);
    }

}
