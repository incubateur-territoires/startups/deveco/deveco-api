import { MigrationInterface, QueryRunner } from "typeorm";

export class UpdateEtablissement1661413803993 implements MigrationInterface {
    name = 'UpdateEtablissement1661413803993'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`DROP TABLE etablissement_light`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
    }

}
