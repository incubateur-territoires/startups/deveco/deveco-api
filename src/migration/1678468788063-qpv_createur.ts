import { MigrationInterface, QueryRunner } from "typeorm";

export class qpvCreateur1678468788063 implements MigrationInterface {
    name = 'qpvCreateur1678468788063'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "particulier" ADD "qpv_id" integer`);
        await queryRunner.query(`CREATE INDEX "IDX_6bbdddae8ee5faaa9614a4ca41" ON "particulier" ("qpv_id") `);
        await queryRunner.query(`ALTER TABLE "particulier" ADD CONSTRAINT "FK_6bbdddae8ee5faaa9614a4ca41d" FOREIGN KEY ("qpv_id") REFERENCES "qp_metropoleoutremer_wgs84_epsg4326"("gid") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`UPDATE particulier SET geolocation = REPLACE(REPLACE(ST_AsText(ST_Transform(ST_GeomFromText('POINT' || REPLACE(geolocation, ',', ' ')), 'EPSG:2154', 'WGS 84')), ' ', ','), 'POINT', '') WHERE geolocation IS NOT NULL`);
        await queryRunner.query(`UPDATE particulier SET qpv_id = (SELECT gid FROM qp_metropoleoutremer_wgs84_epsg4326 WHERE ST_Contains(geom, ST_GeomFromText('POINT' || replace(geolocation, ',', ' '))))`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "particulier" DROP CONSTRAINT "FK_6bbdddae8ee5faaa9614a4ca41d"`);
        await queryRunner.query(`DROP INDEX "public"."IDX_6bbdddae8ee5faaa9614a4ca41"`);
        await queryRunner.query(`ALTER TABLE "particulier" DROP COLUMN "qpv_id"`);
        await queryRunner.query(`UPDATE particulier SET geolocation = REPLACE(REPLACE(ST_AsText(ST_Transform(ST_GeomFromText('POINT' || REPLACE(geolocation, ',', ' ')), 'WGS 84', 'EPSG:2154')), ' ', ','), 'POINT', '') WHERE geolocation IS NOT NULL`);
    }

}
