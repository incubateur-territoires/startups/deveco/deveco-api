import { MigrationInterface, QueryRunner } from "typeorm";

export class searchRequest1669942683619 implements MigrationInterface {
    name = 'searchRequest1669942683619'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE TABLE "search_request" ("id" SERIAL NOT NULL, "query" character varying NOT NULL, "type" character varying NOT NULL, "created_at" TIMESTAMP NOT NULL DEFAULT now(), "deveco_id" integer, CONSTRAINT "PK_d01dd084a9e155f976ca2e55d47" PRIMARY KEY ("id"))`);
        await queryRunner.query(`ALTER TABLE "search_request" ADD CONSTRAINT "FK_1c22acefb8ac347d31d987e0539" FOREIGN KEY ("deveco_id") REFERENCES "deveco"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "search_request" DROP CONSTRAINT "FK_1c22acefb8ac347d31d987e0539"`);
        await queryRunner.query(`DROP TABLE "search_request"`);
    }

}
