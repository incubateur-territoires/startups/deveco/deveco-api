import { MigrationInterface, QueryRunner } from "typeorm";

export class futureEnseigne1663656368678 implements MigrationInterface {
    name = 'futureEnseigne1663656368678'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "entite" ADD "future_enseigne" character varying`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "entite" DROP COLUMN "future_enseigne"`);
    }

}
