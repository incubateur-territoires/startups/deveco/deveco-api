import { MigrationInterface, QueryRunner } from "typeorm";

export class updateSireneEtablissement1658851975372 implements MigrationInterface {
    name = 'updateSireneEtablissement1658851975372'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "sirene_etablissement" ADD "longitude" double precision`);
        await queryRunner.query(`ALTER TABLE "sirene_etablissement" ADD "latitude" double precision`);
        await queryRunner.query(`ALTER TABLE "sirene_etablissement" ADD "geo_score" double precision`);
        await queryRunner.query(`ALTER TABLE "sirene_etablissement" ADD "geo_type" character varying`);
        await queryRunner.query(`ALTER TABLE "sirene_etablissement" ADD "geo_adresse" character varying`);
        await queryRunner.query(`ALTER TABLE "sirene_etablissement" ADD "geo_id" character varying`);
        await queryRunner.query(`ALTER TABLE "sirene_etablissement" ADD "geo_ligne" character varying`);
        await queryRunner.query(`ALTER TABLE "sirene_etablissement" ADD "geo_l4" character varying`);
        await queryRunner.query(`ALTER TABLE "sirene_etablissement" ADD "geo_l5" character varying`);
        await queryRunner.query(`ALTER TABLE "sirene_etablissement" DROP COLUMN "complement_adresse_etablissement"`);
        await queryRunner.query(`ALTER TABLE "sirene_etablissement" ADD "complement_adresse_etablissement" character varying(100)`);
        await queryRunner.query(`ALTER TABLE "sirene_etablissement" DROP COLUMN "indice_repetition_etablissement"`);
        await queryRunner.query(`ALTER TABLE "sirene_etablissement" ADD "indice_repetition_etablissement" character varying(10)`);
        await queryRunner.query(`ALTER TABLE "sirene_etablissement" DROP COLUMN "distribution_speciale_etablissement"`);
        await queryRunner.query(`ALTER TABLE "sirene_etablissement" ADD "distribution_speciale_etablissement" character varying(100)`);
        await queryRunner.query(`ALTER TABLE "sirene_etablissement" DROP COLUMN "denomination_usuelle_etablissement"`);
        await queryRunner.query(`ALTER TABLE "sirene_etablissement" ADD "denomination_usuelle_etablissement" character varying(500)`);
        await queryRunner.query(`ALTER TABLE "sirene_etablissement" DROP COLUMN "enseigne1_etablissement"`);
        await queryRunner.query(`ALTER TABLE "sirene_etablissement" ADD "enseigne1_etablissement" character varying(100)`);
        await queryRunner.query(`ALTER TABLE "sirene_etablissement" DROP COLUMN "numero_voie_etablissement"`);
        await queryRunner.query(`ALTER TABLE "sirene_etablissement" ADD "numero_voie_etablissement" character varying(10)`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "sirene_etablissement" DROP COLUMN "numero_voie_etablissement"`);
        await queryRunner.query(`ALTER TABLE "sirene_etablissement" ADD "numero_voie_etablissement" character varying(4)`);
        await queryRunner.query(`ALTER TABLE "sirene_etablissement" DROP COLUMN "enseigne1_etablissement"`);
        await queryRunner.query(`ALTER TABLE "sirene_etablissement" ADD "enseigne1_etablissement" character varying(50)`);
        await queryRunner.query(`ALTER TABLE "sirene_etablissement" DROP COLUMN "denomination_usuelle_etablissement"`);
        await queryRunner.query(`ALTER TABLE "sirene_etablissement" ADD "denomination_usuelle_etablissement" character varying(100)`);
        await queryRunner.query(`ALTER TABLE "sirene_etablissement" DROP COLUMN "distribution_speciale_etablissement"`);
        await queryRunner.query(`ALTER TABLE "sirene_etablissement" ADD "distribution_speciale_etablissement" character varying(26)`);
        await queryRunner.query(`ALTER TABLE "sirene_etablissement" DROP COLUMN "complement_adresse_etablissement"`);
        await queryRunner.query(`ALTER TABLE "sirene_etablissement" ADD "complement_adresse_etablissement" character varying(38)`);
        await queryRunner.query(`ALTER TABLE "sirene_etablissement" DROP COLUMN "geo_l5"`);
        await queryRunner.query(`ALTER TABLE "sirene_etablissement" DROP COLUMN "geo_l4"`);
        await queryRunner.query(`ALTER TABLE "sirene_etablissement" DROP COLUMN "geo_ligne"`);
        await queryRunner.query(`ALTER TABLE "sirene_etablissement" DROP COLUMN "geo_id"`);
        await queryRunner.query(`ALTER TABLE "sirene_etablissement" DROP COLUMN "geo_adresse"`);
        await queryRunner.query(`ALTER TABLE "sirene_etablissement" DROP COLUMN "geo_type"`);
        await queryRunner.query(`ALTER TABLE "sirene_etablissement" DROP COLUMN "geo_score"`);
        await queryRunner.query(`ALTER TABLE "sirene_etablissement" DROP COLUMN "latitude"`);
        await queryRunner.query(`ALTER TABLE "sirene_etablissement" DROP COLUMN "longitude"`);
        await queryRunner.query(`ALTER TABLE "sirene_etablissement" DROP COLUMN "indice_repetition_etablissement"`);
        await queryRunner.query(`ALTER TABLE "sirene_etablissement" ADD "indice_repetition_etablissement" character varying(1)`);
    }

}
