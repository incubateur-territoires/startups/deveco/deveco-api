import { MigrationInterface, QueryRunner } from "typeorm";

export class indexesEverywhere1673367159721 implements MigrationInterface {
    name = 'indexesEverywhere1673367159721'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE INDEX "IDX_a15b3bf3f0f25886412f1a5913" ON "demande" ("fiche_id") `);
        await queryRunner.query(`CREATE INDEX "IDX_45e6b16a9ece55a4d610dd1104" ON "echange" ("fiche_id") `);
        await queryRunner.query(`CREATE INDEX "IDX_0383d1ba9fe54849cc1a1db808" ON "rappel" ("fiche_id") `);
        await queryRunner.query(`CREATE INDEX "IDX_ef26bc1487b0f1073e2ed7f4cc" ON "entite" ("particulier_id") `);
        await queryRunner.query(`CREATE INDEX "IDX_8c5bd0ee6928cbe0ffa8e9a402" ON "contact" ("entite_id") `);
        await queryRunner.query(`CREATE INDEX "IDX_5d74910608cb76beb22cfa4776" ON "contact" ("particulier_id") `);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`DROP INDEX "public"."IDX_5d74910608cb76beb22cfa4776"`);
        await queryRunner.query(`DROP INDEX "public"."IDX_8c5bd0ee6928cbe0ffa8e9a402"`);
        await queryRunner.query(`DROP INDEX "public"."IDX_ef26bc1487b0f1073e2ed7f4cc"`);
        await queryRunner.query(`DROP INDEX "public"."IDX_0383d1ba9fe54849cc1a1db808"`);
        await queryRunner.query(`DROP INDEX "public"."IDX_45e6b16a9ece55a4d610dd1104"`);
        await queryRunner.query(`DROP INDEX "public"."IDX_a15b3bf3f0f25886412f1a5913"`);
    }

}
