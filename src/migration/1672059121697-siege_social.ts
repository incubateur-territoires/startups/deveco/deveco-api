import { MigrationInterface, QueryRunner } from "typeorm";

export class siegeSocial1672059121697 implements MigrationInterface {
    name = 'siegeSocial1672059121697'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "etablissement" ADD "siege_social" boolean NOT NULL DEFAULT false`);
        await queryRunner.query(`ALTER TABLE "entreprise" ADD "siege_social" boolean NOT NULL DEFAULT false`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "entreprise" DROP COLUMN "siege_social"`);
        await queryRunner.query(`ALTER TABLE "etablissement" DROP COLUMN "siege_social"`);
    }

}
