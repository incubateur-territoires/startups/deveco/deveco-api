import {MigrationInterface, QueryRunner} from "typeorm";

export class dateCloture1651573054531 implements MigrationInterface {
    name = 'dateCloture1651573054531'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "rappel" ADD "auteur_cloture_id" integer`);
        await queryRunner.query(`ALTER TABLE "rappel" ADD CONSTRAINT "FK_e292fe6d329b8b9573fcd8227bb" FOREIGN KEY ("auteur_cloture_id") REFERENCES "deveco"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "rappel" ADD "date_cloture" DATE`);
        await queryRunner.query(`UPDATE "rappel" SET date_cloture=NULL where cloture IS NULL`);
        await queryRunner.query(`UPDATE "rappel" SET date_cloture=NULL where cloture=FALSE`);
        await queryRunner.query(`UPDATE "rappel" SET date_cloture=now() where cloture=TRUE`);

        await queryRunner.query(`ALTER TABLE "rappel" DROP COLUMN "cloture"`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "rappel" ADD "cloture" boolean`);
        await queryRunner.query(`UPDATE "rappel" SET cloture=TRUE where date_cloture IS NOT NULL`);
        await queryRunner.query(`UPDATE "rappel" SET cloture=FALSE where date_cloture IS NULL`);

        await queryRunner.query(`ALTER TABLE "rappel" DROP CONSTRAINT "FK_e292fe6d329b8b9573fcd8227bb"`);
        await queryRunner.query(`ALTER TABLE "rappel" DROP COLUMN "date_cloture"`);
        await queryRunner.query(`ALTER TABLE "rappel" DROP COLUMN "auteur_cloture_id"`);
    }

}
