import {MigrationInterface, QueryRunner} from "typeorm";

export class localTypes1656423633810 implements MigrationInterface {
    name = 'localTypes1656423633810'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "local" ALTER "local_type" drop default;`);
        await queryRunner.query(`ALTER TABLE "local" ALTER "local_type" type text;`);
        await queryRunner.query(`ALTER TABLE "local" ALTER "local_type" set default NULL;`);
        await queryRunner.query(`ALTER TABLE "local" RENAME COLUMN "local_type" TO "local_types"`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "local" ADD COLUMN "local_type" character varying;`);
        await queryRunner.query(`UPDATE local SET local_type = split_part(local.local_types, ',', 1);`);
        await queryRunner.query(`ALTER TABLE "local" DROP COLUMN "local_types";`);
    }

}
