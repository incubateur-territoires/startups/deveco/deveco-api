import {MigrationInterface, QueryRunner} from "typeorm";

export class ficheArchive1655736091829 implements MigrationInterface {
    name = 'ficheArchive1655736091829'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE TABLE "fiche_archive" ("id" SERIAL NOT NULL, "content" jsonb NOT NULL, "created_at" TIMESTAMP NOT NULL DEFAULT now(), CONSTRAINT "PK_b4ad70c660a554464c2b99b0b22" PRIMARY KEY ("id"))`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`DROP TABLE "fiche_archive"`);
    }

}
