import { MigrationInterface, QueryRunner } from "typeorm";

export class nonDiffusible1679997815792 implements MigrationInterface {
    name = 'nonDiffusible1679997815792'

    public async up(queryRunner: QueryRunner): Promise<void> {
				await queryRunner.query(`ALTER TYPE "public"."sirene_etablissement_statut_diffusion_etablissement_enum" ADD VALUE 'P'`);
				await queryRunner.query(`ALTER TYPE "public"."sirene_unite_legale_statut_diffusion_unite_legale_enum" ADD VALUE 'P'`);
				await queryRunner.query(`ALTER TYPE "public"."sirene_unite_legale_sexe_unite_legale_enum" ADD VALUE '[ND]'`);
    }

    public async down(_queryRunner: QueryRunner): Promise<void> {
			// empty
    }

}
