import { MigrationInterface, QueryRunner } from "typeorm";

export class proprietaireRemoveTerritory1659619580888 implements MigrationInterface {
    name = 'proprietaireRemoveTerritory1659619580888'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "proprietaire" DROP CONSTRAINT "FK_d0def1da187f4e97d238fba755d"`);
        await queryRunner.query(`ALTER TABLE "proprietaire" DROP COLUMN "territoire_id"`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "proprietaire" ADD "territoire_id" integer`);
        await queryRunner.query(`ALTER TABLE "proprietaire" ADD CONSTRAINT "FK_d0def1da187f4e97d238fba755d" FOREIGN KEY ("territoire_id") REFERENCES "territory"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
    }

}
