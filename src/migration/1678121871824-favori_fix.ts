import { MigrationInterface, QueryRunner } from "typeorm";

export class favoriFix1678121871824 implements MigrationInterface {
    name = 'favoriFix1678121871824'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "etablissement_favori" DROP CONSTRAINT "FK_8569e6063f7ebabf5f3dcd5ee86"`);
        await queryRunner.query(`ALTER TABLE "etablissement_favori" DROP CONSTRAINT "UNIQ_etablissement_favori"`);
        await queryRunner.query(`ALTER TABLE "etablissement_favori" DROP COLUMN "territory_siret"`);
        await queryRunner.query(`ALTER TABLE "etablissement_favori" ADD "territory_id" integer`);
        await queryRunner.query(`ALTER TABLE "etablissement_favori" ADD "siret" character varying(14)`);
        await queryRunner.query(`CREATE INDEX "IDX_efcf90ca3a2027222bde0c8755" ON "etablissement_favori" ("deveco_id") `);
        await queryRunner.query(`ALTER TABLE "territory_etablissement_reference" ADD CONSTRAINT "UNIQ_siret_for_territory" UNIQUE ("territory_id", "siret")`);
        await queryRunner.query(`ALTER TABLE "etablissement_favori" ADD CONSTRAINT "UNIQ_deveco_etablissement_favori" UNIQUE ("deveco_id", "territory_id", "siret")`);
        await queryRunner.query(`ALTER TABLE "etablissement_favori" ADD CONSTRAINT "FK_b8aac59f2e15537bc9259c5acbd" FOREIGN KEY ("territory_id", "siret") REFERENCES "territory_etablissement_reference"("territory_id","siret") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "etablissement_favori" ADD CONSTRAINT "FK_30f8e7c4666aef5ab261d367ca1" FOREIGN KEY ("territory_id") REFERENCES "territory"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "etablissement_favori" ADD CONSTRAINT "FK_5bd9e96ed73b93b4505e6f7ed63" FOREIGN KEY ("siret") REFERENCES "etablissement"("siret") ON DELETE NO ACTION ON UPDATE NO ACTION`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "etablissement_favori" DROP CONSTRAINT "FK_5bd9e96ed73b93b4505e6f7ed63"`);
        await queryRunner.query(`ALTER TABLE "etablissement_favori" DROP CONSTRAINT "FK_30f8e7c4666aef5ab261d367ca1"`);
        await queryRunner.query(`ALTER TABLE "etablissement_favori" DROP CONSTRAINT "FK_b8aac59f2e15537bc9259c5acbd"`);
        await queryRunner.query(`ALTER TABLE "etablissement_favori" DROP CONSTRAINT "UNIQ_deveco_etablissement_favori"`);
        await queryRunner.query(`ALTER TABLE "territory_etablissement_reference" DROP CONSTRAINT "UNIQ_siret_for_territory"`);
        await queryRunner.query(`DROP INDEX "public"."IDX_efcf90ca3a2027222bde0c8755"`);
        await queryRunner.query(`ALTER TABLE "etablissement_favori" DROP COLUMN "siret"`);
        await queryRunner.query(`ALTER TABLE "etablissement_favori" DROP COLUMN "territory_id"`);
        await queryRunner.query(`ALTER TABLE "etablissement_favori" ADD "territory_siret" character varying`);
        await queryRunner.query(`ALTER TABLE "etablissement_favori" ADD CONSTRAINT "UNIQ_etablissement_favori" UNIQUE ("deveco_id", "territory_siret")`);
        await queryRunner.query(`ALTER TABLE "etablissement_favori" ADD CONSTRAINT "FK_8569e6063f7ebabf5f3dcd5ee86" FOREIGN KEY ("territory_siret") REFERENCES "territory_etablissement_reference"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
    }

}
