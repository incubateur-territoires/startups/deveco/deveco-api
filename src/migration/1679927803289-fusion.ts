import { MigrationInterface, QueryRunner } from "typeorm";

export class fusion1679927803289 implements MigrationInterface {
    name = 'fusion1679927803289'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "entite" ADD "enseigne_id" character varying`);
        await queryRunner.query(`CREATE INDEX "IDX_ffdba34a9aafc0b084a0064687" ON "entite" ("enseigne_id") `);
        await queryRunner.query(`ALTER TABLE "entite" ADD CONSTRAINT "FK_ffdba34a9aafc0b084a00646878" FOREIGN KEY ("enseigne_id") REFERENCES "entreprise"("siret") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "entite" ADD "createur_id" integer`);
        await queryRunner.query(`CREATE INDEX "IDX_ce2514e217fccb569b2b40d696" ON "entite" ("createur_id") `);
        await queryRunner.query(`ALTER TABLE "entite" ADD CONSTRAINT "FK_ce2514e217fccb569b2b40d696c" FOREIGN KEY ("createur_id") REFERENCES "particulier"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "entite" DROP CONSTRAINT "FK_ffdba34a9aafc0b084a00646878"`);
        await queryRunner.query(`DROP INDEX "public"."IDX_ffdba34a9aafc0b084a0064687"`);
        await queryRunner.query(`ALTER TABLE "entite" DROP COLUMN "enseigne_id"`);
        await queryRunner.query(`ALTER TABLE "entite" DROP CONSTRAINT "FK_ce2514e217fccb569b2b40d696c"`);
        await queryRunner.query(`DROP INDEX "public"."IDX_ce2514e217fccb569b2b40d696"`);
        await queryRunner.query(`ALTER TABLE "entite" DROP COLUMN "createur_id"`);
    }

}
