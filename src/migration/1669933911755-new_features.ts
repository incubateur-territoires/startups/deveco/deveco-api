import { MigrationInterface, QueryRunner } from "typeorm";

export class newFeatures1669933911755 implements MigrationInterface {
    name = 'newFeatures1669933911755'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "account" ADD "new_features" character varying`);
        await queryRunner.query(`UPDATE "account" SET "new_features" = 'fusion'`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "account" DROP COLUMN "new_features"`);
    }

}
