import app from './server';
import startApp from './app';
import testController from './controller/TestController';

app.use(testController.routes()).use(testController.allowedMethods());

startApp(app);
