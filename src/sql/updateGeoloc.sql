WITH
  geo (siret, longitude, latitude) AS (
    SELECT
      GE.siret,
      GE.x_longitude,
      GE.y_latitude
    FROM
      geoloc_entreprise AS GE
    WHERE
      GE.siret = ANY ($1)
  )
UPDATE etablissement EL
SET
  longitude = geo.longitude,
  latitude = geo.latitude
FROM
  geo
WHERE
  geo.siret = EL.siret;
