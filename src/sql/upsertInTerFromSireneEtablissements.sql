INSERT INTO
  territory_etablissement_reference (
    id,
    siret,
    territory_id,
    nom_recherche,
    etat_administratif,
    code_naf,
    libelle_naf,
    date_creation,
    categorie_juridique_courante,
    tranche_effectifs,
    code_commune,
    adresse_complete
  )
SELECT
  CONCAT(T.id, '-', EL.siret),
  EL.siret,
  T.id,
  EL.nom_recherche,
  EL.etat_administratif,
  EL.code_naf,
  EL.libelle_naf,
  EL.date_creation,
  CASE
    WHEN substring(CAST(EL.categorie_juridique AS VARCHAR), 1, 2) IN (
      '00',
      '21',
      '22',
      '27',
      '65',
      '71',
      '72',
      '73',
      '74',
      '81',
      '83',
      '84',
      '85',
      '91'
    ) THEN false
    ELSE true
  END,
  EL.tranche_effectifs,
  EL.code_commune,
  EL.adresse_complete
FROM
  temporary_territory_communes T
  INNER JOIN etablissement EL ON EL.code_commune = ANY (T.communes)
WHERE
  EL.siret = ANY ($1) ON CONFLICT (id)
DO
UPDATE
SET
  nom_recherche = EXCLUDED.nom_recherche,
  etat_administratif = EXCLUDED.etat_administratif,
  code_naf = EXCLUDED.code_naf,
  libelle_naf = EXCLUDED.libelle_naf,
  date_creation = EXCLUDED.date_creation,
  categorie_juridique_courante = EXCLUDED.categorie_juridique_courante,
  tranche_effectifs = EXCLUDED.tranche_effectifs,
  code_commune = EXCLUDED.code_commune,
  adresse_complete = EXCLUDED.adresse_complete;
