INSERT INTO
  etablissement (
    siren,
    siret,
    nom_public,
    nom_recherche,
    enseigne,
    adresse_complete,
    code_commune,
    tranche_effectifs,
    annee_effectifs,
    sigle,
    code_naf,
    libelle_naf,
    libelle_categorie_naf,
    date_creation,
    date_creation_entreprise,
    etat_administratif,
    categorie_juridique,
    type_etablissement,
    longitude,
    latitude,
    siege_social,
    date_fermeture
  )
SELECT
  U.siren as siren,
  SE.siret as siret,
  CASE
    WHEN U.categorie_juridique_unite_legale = '1000' THEN CASE
      WHEN U.sigle_unite_legale IS NOT NULL THEN COALESCE(LOWER(U.prenom1_unite_legale), '') || COALESCE(' ' || LOWER(U.nom_unite_legale), '') || COALESCE(' (' || LOWER(U.sigle_unite_legale) || ')', '')
      ELSE COALESCE(LOWER(U.prenom1_unite_legale), '') || COALESCE(' ' || LOWER(U.nom_unite_legale), '')
    END
    ELSE CASE
      WHEN U.sigle_unite_legale IS NOT NULL THEN COALESCE(LOWER(U.denomination_unite_legale), '') || COALESCE(LOWER(' (' || U.sigle_unite_legale || ')'), '')
      ELSE COALESCE(LOWER(U.denomination_unite_legale), '')
    END
  END as nom_public,
  COALESCE(LOWER(U.prenom1_unite_legale), '') || COALESCE(' ' || LOWER(U.nom_unite_legale), '') || COALESCE(' ' || LOWER(U.nom_usage_unite_legale), '') || COALESCE(' ' || LOWER(U.sigle_unite_legale), '') || COALESCE(' ' || LOWER(U.denomination_unite_legale), '') || COALESCE(
    ' ' || LOWER(U.denomination_usuelle1_unite_legale),
    ''
  ) || COALESCE(
    ' ' || LOWER(U.denomination_usuelle2_unite_legale),
    ''
  ) || COALESCE(
    ' ' || LOWER(U.denomination_usuelle3_unite_legale),
    ''
  ) || COALESCE(' ' || LOWER(SE.enseigne1_etablissement), '') || COALESCE(' ' || LOWER(SE.enseigne2_etablissement), '') || COALESCE(' ' || LOWER(SE.enseigne3_etablissement), '') || COALESCE(
    ' ' || LOWER(SE.denomination_usuelle_etablissement),
    ''
  ) as nom_recherche,
  COALESCE(
    SE.enseigne1_etablissement,
    SE.enseigne2_etablissement,
    SE.enseigne3_etablissement,
    SE.denomination_usuelle_etablissement,
    ''
  ) as enseigne,
  CASE
    WHEN SE.geo_score IS NOT NULL
    AND SE.geo_adresse IS NOT NULL
    AND SE.geo_score > 0.6
    AND SE.geo_adresse ILIKE '%' || SE.code_postal_etablissement || '%' THEN SE.geo_adresse
    ELSE COALESCE(LOWER(SE.numero_voie_etablissement), '') || COALESCE(' ' || LOWER(SE.type_voie_etablissement), '') || COALESCE(' ' || LOWER(SE.libelle_voie_etablissement), '') || COALESCE(
      ' ' || LOWER(SE.libelle_commune_etablissement),
      ''
    ) || COALESCE(' ' || LOWER(SE.code_postal_etablissement), '') || COALESCE(
      ' ' || LOWER(SE.code_pays_etranger_etablissement),
      ''
    )
  END as adresse_complete,
  SE.code_commune_etablissement,
  SE.tranche_effectifs_etablissement,
  SE.annee_effectifs_etablissement,
  U.sigle_unite_legale,
  SE.activite_principale_etablissement,
  N.label_5,
  N.label_2,
  SE.date_creation_etablissement,
  U.date_creation_unite_legale,
  SE.etat_administratif_etablissement,
  U.categorie_juridique_unite_legale,
  CASE
    WHEN U.categorie_juridique_unite_legale = '1000' THEN 'personne_physique'
    ELSE 'personne_morale'
  END,
  COALESCE(GEO.x_longitude, SE.longitude),
  COALESCE(GEO.y_latitude, SE.latitude),
  SE.etablissement_siege,
  CASE SE.etat_administratif_etablissement
    WHEN 'F' THEN SE.date_debut
    ELSE NULL
  END
FROM
  sirene_etablissement SE
  INNER JOIN sirene_unite_legale U ON U.siren = SE.siren
  LEFT JOIN naf N ON N.id_5 = SE.activite_principale_etablissement
  LEFT JOIN geoloc_entreprise GEO ON GEO.siret = SE.siret
WHERE
  SE.code_commune_etablissement is not NULL
  AND SE.siret = ANY ($1) ON CONFLICT (siret)
DO
UPDATE
SET
  siren = EXCLUDED.siren,
  nom_public = EXCLUDED.nom_public,
  nom_recherche = EXCLUDED.nom_recherche,
  enseigne = EXCLUDED.enseigne,
  adresse_complete = EXCLUDED.adresse_complete,
  code_commune = EXCLUDED.code_commune,
  tranche_effectifs = EXCLUDED.tranche_effectifs,
  annee_effectifs = EXCLUDED.annee_effectifs,
  sigle = EXCLUDED.sigle,
  code_naf = EXCLUDED.code_naf,
  libelle_naf = EXCLUDED.libelle_naf,
  libelle_categorie_naf = EXCLUDED.libelle_categorie_naf,
  date_creation = EXCLUDED.date_creation,
  date_creation_entreprise = EXCLUDED.date_creation_entreprise,
  etat_administratif = EXCLUDED.etat_administratif,
  categorie_juridique = EXCLUDED.categorie_juridique,
  type_etablissement = EXCLUDED.type_etablissement,
  longitude = EXCLUDED.longitude,
  latitude = EXCLUDED.latitude,
  siege_social = EXCLUDED.siege_social;
