UPDATE territory_etablissement_reference as TER
SET
  qpv_id = QPV.gid
FROM
  etablissement as E
  INNER JOIN qp_metropoleoutremer_wgs84_epsg4326 as QPV ON ST_Contains (geom, ST_POINT (E.longitude, E.latitude))
WHERE
  E.siret = TER.siret
  AND E.siret = ANY ($1);
