CREATE UNLOGGED TABLE IF NOT EXISTS temporary_territory_communes (id, communes) AS (
  SELECT
    T.id,
    ARRAY (
      SELECT
        insee_com
      FROM
        commune as C
      WHERE
        C.insee_com = T.commune_id
        OR (
          T.metropole_id IS NOT NULL
          AND C.com_parent = T.metropole_id
        )
        OR (
          T.departement_id IS NOT NULL
          AND C.ctcd = T.departement_id
        )
        OR (
          T.region_id IS NOT NULL
          AND C.insee_reg = T.metropole_id
        )
        OR (
          T.petr_id IS NOT NULL
          AND C.insee_epci IN (
            SELECT
              insee_epci
            FROM
              epci
            WHERE
              epci.code_petr = T.petr_id
          )
        )
        OR (
          T.epci_id IS NOT NULL
          AND C.insee_epci = T.epci_id
        )
        OR (
          T.perimetre_id IS NOT NULL
          AND (
            (
              perimetre.level = 'commune'
              AND C.insee_com IN (
                SELECT
                  "communeInseeCom"
                FROM
                  perimetre_communes_commune PCC
                WHERE
                  PCC."perimetreId" = T.perimetre_id
              )
              OR (
                perimetre.level = 'epci'
                AND C.insee_epci IN (
                  SELECT
                    "epciInseeEpci"
                  FROM
                    perimetre_epcis_epci PEE
                  WHERE
                    PEE."perimetreId" = T.perimetre_id
                )
              )
              OR (
                perimetre.level = 'departement'
                AND C.ctcd IN (
                  SELECT
                    "departementCtcd"
                  FROM
                    perimetre_departements_departement PDD
                  WHERE
                    PDD."perimetreId" = T.perimetre_id
                )
              )
            )
          )
        )
    )
  FROM
    territory T
    JOIN perimetre ON perimetre.id = T.perimetre_id
);

CREATE INDEX ON temporary_territory_communes (id);