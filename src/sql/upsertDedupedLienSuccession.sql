INSERT INTO
  lien_succession (
    siret_predecesseur,
    siret_successeur,
    date_succession,
    date_traitement,
    transfert_siege,
    continuite_economique
  )
SELECT DISTINCT
  ON (LST.siret_predecesseur, LST.siret_successeur) LST.siret_predecesseur,
  LST.siret_successeur,
  LST.date_succession,
  LST.date_traitement,
  LST.transfert_siege,
  LST.continuite_economique
FROM
  lien_succession_temp LST
ORDER BY
  LST.siret_predecesseur,
  LST.siret_successeur,
  LST.date_traitement ON CONFLICT (
    siret_predecesseur,
    siret_successeur,
    date_succession
  )
DO
UPDATE
SET
  date_succession = EXCLUDED.date_succession,
  date_traitement = EXCLUDED.date_traitement,
  transfert_siege = EXCLUDED.transfert_siege,
  continuite_economique = EXCLUDED.continuite_economique
WHERE
  EXCLUDED.date_traitement > lien_succession.date_traitement
