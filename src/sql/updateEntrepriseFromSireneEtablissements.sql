UPDATE entreprise
SET
  longitude = E.longitude,
  latitude = E.latitude,
  date_creation = E.date_creation,
  date_fermeture = E.date_fermeture,
  etat_administratif = E.etat_administratif,
  nom = UPPER(COALESCE(E.nom_public, '')),
  enseigne = UPPER(COALESCE(E.enseigne, '')),
  adresse = UPPER(COALESCE(E.adresse_complete, '')),
  code_commune = COALESCE(E.code_commune, ''),
  code_naf = E.code_naf,
  libelle_naf = E.libelle_naf,
  libelle_categorie_naf = E.libelle_categorie_naf,
  effectifs = CASE E.tranche_effectifs
    WHEN '00' THEN '0'
    WHEN '01' THEN '1 ou 2'
    WHEN '02' THEN '3 à 5'
    WHEN '03' THEN '6 à 9'
    WHEN '11' THEN '10 à 19'
    WHEN '12' THEN '20 à 49'
    WHEN '21' THEN '50 à 99'
    WHEN '22' THEN '100 à 199'
    WHEN '31' THEN '200 à 249'
    WHEN '32' THEN '250 à 499'
    WHEN '41' THEN '500 à 999'
    WHEN '42' THEN '1 000 à 1 999'
    WHEN '51' THEN '2 000 à 4 999'
    WHEN '52' THEN '5 000 à 9 999'
    WHEN '53' THEN '10 000 et plus'
    WHEN 'NN' THEN '?'
    ELSE '?'
  END,
  date_effectifs = CASE
    WHEN CAST(E.annee_effectifs AS INTEGER) < 2000 THEN NULL
    WHEN CAST(E.annee_effectifs AS INTEGER) > 2050 THEN NULL
    ELSE CAST(E.annee_effectifs || '-12-25' AS DATE)
  END,
  forme_juridique = case E.categorie_juridique
    WHEN '00' THEN 'Organisme de placement collectif en valeurs mobilières sans personnalité morale'
    WHEN '10' THEN 'Entrepreneur individuel'
    WHEN '21' THEN 'Indivision'
    WHEN '22' THEN 'Société créée de fait'
    WHEN '23' THEN 'Société en participation'
    WHEN '24' THEN 'Fiducie'
    WHEN '27' THEN 'Paroisse hors zone concordataire'
    WHEN '28' THEN 'Assujetti unique à la TVA'
    WHEN '29' THEN 'Autre groupement de droit privé non doté de la personnalité morale'
    WHEN '31' THEN 'Personne morale de droit étranger immatriculée au RCS (registre du commerce et des sociétés)'
    WHEN '32' THEN 'Personne morale de droit étranger non immatriculée au RCS'
    WHEN '41' THEN 'Etablissement public ou régie à caractère industriel ou commercial'
    WHEN '51' THEN 'Société coopérative commerciale particulière'
    WHEN '52' THEN 'Société en nom collectif'
    WHEN '53' THEN 'Société en commandite'
    WHEN '54' THEN 'Société à responsabilité limitée (SARL)'
    WHEN '55' THEN 'Société anonyme à conseil d''administration'
    WHEN '56' THEN 'Société anonyme à directoire'
    WHEN '57' THEN 'Société par actions simplifiée'
    WHEN '58' THEN 'Société européenne '
    WHEN '61' THEN 'Caisse d''épargne et de prévoyance'
    WHEN '62' THEN 'Groupement d''intérêt économique'
    WHEN '63' THEN 'Société coopérative agricole'
    WHEN '64' THEN 'Société d''assurance mutuelle'
    WHEN '65' THEN 'Société civile'
    WHEN '69' THEN 'Autre personne morale de droit privé inscrite au registre du commerce et des sociétés'
    WHEN '71' THEN 'Administration de l''état'
    WHEN '72' THEN 'Collectivité territoriale'
    WHEN '73' THEN 'Etablissement public administratif'
    WHEN '74' THEN 'Autre personne morale de droit public administratif'
    WHEN '81' THEN 'Organisme gérant un régime de protection sociale à adhésion obligatoire'
    WHEN '82' THEN 'Organisme mutualiste'
    WHEN '83' THEN 'Comité d''entreprise'
    WHEN '84' THEN 'Organisme professionnel'
    WHEN '85' THEN 'Organisme de retraite à adhésion non obligatoire'
    WHEN '91' THEN 'Syndicat de propriétaires'
    WHEN '92' THEN 'Association loi 1901 ou assimilé'
    WHEN '93' THEN 'Fondation'
    WHEN '99' THEN 'Autre personne morale de droit privé'
    ELSE ''
  END,
  siege_social = E.siege_social
FROM
  etablissement E
WHERE
  E.siret = entreprise.siret
  AND E.siret = ANY ($1);
