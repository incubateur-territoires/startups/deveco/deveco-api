INSERT INTO
  stats (code_commune, date, content)
SELECT
  code_commune,
  NOW(),
  jsonb_object_agg(
    type,
    results
  ) as content
FROM
  (
    SELECT
      code_commune,
      'etablissements' as
    type,
    COALESCE(jsonb_agg(data.*), '[]') as results
    FROM
      (
        SELECT
          TER.code_commune,
          C.lib_com as label,
          COUNT(DISTINCT TER.siret)
        FROM
          territory_etablissement_reference TER
          JOIN commune C ON C.insee_com = TER.code_commune
        WHERE
          TER.etat_administratif = 'A'
          AND TER.categorie_juridique_courante
          AND TER.code_commune = ANY ($1)
        GROUP BY
          TER.code_commune,
          C.lib_com
      ) data
    GROUP BY
      code_commune
    UNION
    SELECT
      code_commune,
      'effectifs' as
    type,
    COALESCE(jsonb_agg(data.*), '[]') as results
    FROM
      (
        SELECT
          TER.code_commune,
          COALESCE(TER.tranche_effectifs, 'NC') as label,
          COUNT(DISTINCT TER.siret)
        FROM
          territory_etablissement_reference TER
        WHERE
          TER.etat_administratif = 'A'
          AND TER.categorie_juridique_courante
          AND TER.code_commune = ANY ($1)
        GROUP BY
          TER.code_commune,
          COALESCE(TER.tranche_effectifs, 'NC')
        ORDER BY
          count DESC
      ) data
    GROUP BY
      code_commune
    UNION
    SELECT
      code_commune,
      'nafs' as
    type,
    COALESCE(jsonb_agg(data.*), '[]') as results
    FROM
      (
        WITH
          cat_naf (code, label) AS (
            SELECT DISTINCT
              id_2,
              label_2
            FROM
              naf
          )
        SELECT
          TER.code_commune,
          NAF.label,
          COUNT(DISTINCT TER.siret)
        FROM
          territory_etablissement_reference TER
          JOIN cat_naf NAF ON SUBSTRING(TER.code_naf, 0, 3) = NAF.code
        WHERE
          TER.etat_administratif = 'A'
          AND TER.categorie_juridique_courante
          AND TER.code_commune = ANY ($1)
        GROUP BY
          TER.code_commune,
          SUBSTRING(TER.code_naf, 0, 3),
          NAF.label
        ORDER BY
          count DESC
      ) data
    GROUP BY
      code_commune
    UNION
    SELECT
      code_commune,
      'categories_juridiques' as
    type,
    COALESCE(jsonb_agg(data.*), '[]') as results
    FROM
      (
        SELECT
          E.code_commune,
          SUBSTRING(CAST(E.categorie_juridique AS VARCHAR), 0, 3) as label,
          COUNT(DISTINCT E.siret)
        FROM
          etablissement E
        WHERE
          E.etat_administratif = 'A'
          AND SUBSTRING(CAST(E.categorie_juridique AS VARCHAR), 1, 2) NOT IN (
            '00',
            '21',
            '22',
            '27',
            '65',
            '71',
            '72',
            '73',
            '74',
            '81',
            '83',
            '84',
            '85',
            '91'
          )
          AND E.code_commune IN (
            SELECT DISTINCT
              TER.code_commune
            FROM
              territory_etablissement_reference TER
          )
          AND E.code_commune = ANY ($1)
        GROUP BY
          E.code_commune,
          SUBSTRING(CAST(E.categorie_juridique AS VARCHAR), 0, 3)
        ORDER BY
          count DESC
      ) data
    GROUP BY
      code_commune
    UNION
    SELECT
      code_commune,
      'qpvs' as
    type,
    COALESCE(jsonb_agg(data.*), '[]') as results
    FROM
      (
        SELECT
          TER.code_commune,
          QPV.nom_qp as label,
          COUNT(DISTINCT TER.siret)
        FROM
          territory_etablissement_reference TER
          JOIN qp_metropoleoutremer_wgs84_epsg4326 QPV ON TER.qpv_id = QPV.gid
        WHERE
          TER.etat_administratif = 'A'
          AND TER.categorie_juridique_courante
          AND TER.qpv_id IS NOT NULL
          AND TER.code_commune = ANY ($1)
        GROUP BY
          TER.code_commune,
          QPV.nom_qp
        ORDER BY
          count DESC
      ) data
    GROUP BY
      code_commune
  ) stats_data
GROUP BY
  code_commune ON CONFLICT ("code_commune", "date")
DO
UPDATE
SET
  content = EXCLUDED.content;
