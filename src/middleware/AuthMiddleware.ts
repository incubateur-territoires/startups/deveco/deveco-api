import Koa from 'koa';
import jwt from 'jsonwebtoken';
import { StatusCodes } from 'http-status-codes';

import AppDataSource from '../data-source';
import { Account } from '../entity/Account';
import { JwtPayload } from '../lib/getJwt';
import devecoService from '../service/DevEcoService';
import { DevEco } from '../entity/DevEco';
import { logger } from '../lib/timestamp-logger';

export type DevecoContext = Koa.ParameterizedContext<{
	account: Account;
	deveco?: DevEco;
	reqId?: string;
}>;

export const AuthMiddleware = async (
	ctx: DevecoContext,
	next: (ctx: DevecoContext) => Promise<unknown>
) => {
	const cookies = ctx.cookie;
	if (!cookies) {
		ctx.status = StatusCodes.UNAUTHORIZED;
		return;
	}

	const token = cookies.jwt;
	if (!token) {
		ctx.status = StatusCodes.UNAUTHORIZED;
		return;
	}

	const { id } = jwt.verify(token, process.env.JWT_KEY || '') as JwtPayload;
	const account = await AppDataSource.getRepository(Account).findOneBy({ id });
	if (!account) {
		ctx.status = StatusCodes.UNAUTHORIZED;
		return;
	}

	if (!account.enabled) {
		ctx.throw(401);
	}

	const lastAuth = new Date();
	await AppDataSource.getRepository(Account).update(id, { lastAuth });

	ctx.state.account = { ...account, lastAuth };
	if (account.accountType === 'deveco') {
		ctx.state.deveco = await devecoService.getDevEcoByAccountWithTerritory(account);

		if (!ctx.state.deveco) {
			ctx.status = StatusCodes.UNAUTHORIZED;
			return;
		}

		if (ctx.state.reqId) {
			logger(ctx, 'AUTH');
		}
	}

	return next(ctx);
};

export default AuthMiddleware;
