import Koa from 'koa';
import { StatusCodes } from 'http-status-codes';

import { DevEco } from '../entity/DevEco';

const TokenCheckMiddleware =
	(referenceToken: string | undefined, referenceTokenName: string) =>
	async (
		token: string,
		ctx: Koa.ParameterizedContext<{ deveco?: DevEco }>,
		next: (ctx: Koa.ParameterizedContext<{ deveco?: DevEco }>) => Promise<unknown>
	) => {
		if (!referenceToken || token !== referenceToken) {
			ctx.throw(
				StatusCodes.FORBIDDEN,
				`Jeton ${referenceTokenName} invalide : ${ctx.params.token}`
			);
		}

		return next(ctx);
	};

export default TokenCheckMiddleware;
