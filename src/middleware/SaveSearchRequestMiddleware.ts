import Koa from 'koa';

import AppDataSource from '../data-source';
import { DevEco } from '../entity/DevEco';
import { SearchContextEntity, SearchRequest } from '../entity/SearchRequest';
import { DevecoContext } from '../middleware/AuthMiddleware';

const SaveSearchRequestMiddleware =
	(type: SearchContextEntity) =>
	async (ctx: DevecoContext, next: (ctx: Koa.Context) => Promise<unknown>) => {
		const deveco = ctx.state.deveco as DevEco;

		if (!deveco) {
			return next(ctx);
		}

		const query = ctx.request.querystring;
		const format = ctx.query.format;

		AppDataSource.getRepository(SearchRequest).insert({
			deveco,
			query,
			type: `${format === 'xlsx' ? 'export-' : ''}${type}`,
		});

		return next(ctx);
	};

export default SaveSearchRequestMiddleware;
