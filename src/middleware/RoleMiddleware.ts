import Koa from 'koa';
import { StatusCodes } from 'http-status-codes';

import { DevecoContext } from '../middleware/AuthMiddleware';

const RoleMiddleware =
	(role: string) => async (ctx: DevecoContext, next: (ctx: Koa.Context) => Promise<unknown>) => {
		const account = ctx.state.account;
		if (!account) {
			ctx.throw(StatusCodes.UNAUTHORIZED);
		}

		if (role !== account.accountType) {
			ctx.throw(StatusCodes.UNAUTHORIZED);
		}

		return next(ctx);
	};

export default RoleMiddleware;
