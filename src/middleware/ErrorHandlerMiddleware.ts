import { StatusCodes } from 'http-status-codes';
import Koa from 'koa';

import { DevecoContext } from '../middleware/AuthMiddleware';

const ErrorHandlerMiddleware = async (ctx: DevecoContext, next: Koa.Next) => {
	try {
		await next();
	} catch (error: any) {
		ctx.status = error.statusCode ?? error.status ?? StatusCodes.INTERNAL_SERVER_ERROR;
		error.status = ctx.status;
		if (!ctx.body) {
			ctx.body = { error };
		}
		if (error.status === StatusCodes.INTERNAL_SERVER_ERROR) {
			ctx.app.emit('error', error, ctx);
		}
	}
};

export default ErrorHandlerMiddleware;
