import Koa from 'koa';

import 'dotenv/config';
import { launchCron } from './cron';
import AppDataSource from './data-source';
import metrics from './prometheus';

const startApp = (app: Koa<Koa.DefaultState, Koa.DefaultContext>) =>
	AppDataSource.initialize()
		.then(async () => {
			const host = process.env.API_HOST ?? '127.0.0.1';

			const port = process.env.API_PORT ? parseInt(process.env.API_PORT, 10) : 4000;

			app.listen(port, host, () => {
				console.info(`Koa Ready on port ${port}`);
			});

			launchCron();

			const monitoringPort = process.env.MONITORING_PORT
				? parseInt(process.env.MONITORING_PORT, 10)
				: 9102;

			metrics.listen(monitoringPort, host, () => {
				console.info(
					`Monitoring server listening to ${monitoringPort}, metrics exposed on the /metrics endpoint`
				);
			});
		})
		.catch((error) => console.error(error));

export default startApp;
