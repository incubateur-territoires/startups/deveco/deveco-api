import * as Koa from 'koa';
import Router from 'koa-router';
import { EntityTarget } from 'typeorm';

import { deleteTerritoryTags, getTerritoryTags } from './AdminController';

import AppDataSource from '../data-source';
import { Commune } from '../entity/Commune';
import { Epci } from '../entity/Epci';
import { Metropole } from '../entity/Metropole';
import AuthMiddleware, { DevecoContext } from '../middleware/AuthMiddleware';
import RoleMiddleware from '../middleware/RoleMiddleware';
import territoireService from '../service/TerritoireService';
import { Petr } from '../entity/Petr';
import { Departement } from '../entity/Departement';
import { Region } from '../entity/Region';
import { PLM_CODES } from '../service/CommuneService';

const prefix = '/territoires';
const routerOpts: Router.IRouterOptions = {
	prefix,
};

const routes = {
	search: '/search',
	communes: '/communes',
	tags: '/tags',
};

type TerritoryFormatted = {
	id: string;
	territoryType: string;
	name: string;
};

const router: Router = new Router(routerOpts);
router.use(AuthMiddleware);

export const formatCommune = (commune: Commune): TerritoryFormatted => ({
	id: commune.id,
	territoryType: 'commune',
	name: `${commune.libCom} (${commune.inseeDep})`,
});

export const formatEpci = <T extends Epci>(epci: T): TerritoryFormatted => ({
	id: epci.id,
	territoryType: 'epci',
	name: epci.libEpci,
});

export const formatMetropole = (metropole: Metropole): TerritoryFormatted => ({
	id: metropole.id,
	territoryType: 'metropole',
	name: metropole.libCom,
});

export const formatPetr = (petr: Petr): TerritoryFormatted => ({
	id: petr.id,
	territoryType: 'petr',
	name: petr.libPetr,
});

export const formatDepartement = (departement: Departement): TerritoryFormatted => ({
	id: departement.id,
	territoryType: 'departement',
	name: departement.nom,
});

export const formatRegion = (region: Region): TerritoryFormatted => ({
	id: region.id,
	territoryType: 'region',
	name: region.nom,
});

const getUnaccentedMatchOn =
	<T extends Epci | Metropole | Petr | Departement | Region | Commune>({
		classe,
		column,
	}: {
		classe: EntityTarget<T>;
		column: string;
	}) =>
	(search: string) =>
		AppDataSource.getRepository(classe)
			.createQueryBuilder()
			.where(`unaccent(${column}) ILIKE '%' || unaccent('${search.replace("'", "''")}') || '%'`)
			.getMany();

router.get(routes.search, async (ctx: Koa.Context) => {
	const {
		search,
		type,
	}: {
		search: string;
		type: string;
	} = ctx.query as any;

	let classe: EntityTarget<Epci | Metropole | Petr | Departement | Region | Commune>;
	let column: string;
	let formatter: <T extends Epci | Metropole | Petr | Departement | Region | Commune>(
		arg: T
	) => TerritoryFormatted;

	switch (type) {
		case 'epci':
			classe = Epci;
			column = 'lib_epci';
			formatter = formatEpci as unknown as typeof formatter;
			break;
		case 'metropole':
			classe = Metropole;
			column = 'lib_com';
			formatter = formatMetropole as unknown as typeof formatter;
			break;
		case 'petr':
			classe = Petr;
			column = 'lib_petr';
			formatter = formatPetr as unknown as typeof formatter;
			break;
		case 'departement':
			classe = Departement;
			column = 'nom';
			formatter = formatDepartement as unknown as typeof formatter;
			break;
		case 'region':
			classe = Region;
			column = 'nom';
			formatter = formatRegion as unknown as typeof formatter;
			break;
		case 'commune':
			classe = Commune;
			column = 'lib_com';
			formatter = formatCommune as unknown as typeof formatter;
			break;
		default:
			ctx.body = { error: `Type de territoire invalide : ${type}` };
			return;
	}

	let results = await getUnaccentedMatchOn({ classe, column })(search);

	if (type === 'commune') {
		// NOTE: filter out communes which are also metropoles (PLM)
		// to avoid adding them as commune instead of metropole
		results = results.filter(({ id }) => !PLM_CODES.includes(id));
	}

	ctx.body = results
		.map(formatter)
		.sort((a: TerritoryFormatted, b: TerritoryFormatted) => a.name.localeCompare(b.name));
});

router.get(routes.communes, RoleMiddleware('deveco'), async (ctx: DevecoContext) => {
	const deveco = ctx.state.deveco;
	if (!deveco) {
		ctx.throw('Deveco not found');
	}

	const territoire = deveco.territory;

	ctx.body = territoireService
		.getCommunesForTerritoire(territoire)
		.map(({ id, libCom, inseeDep }) => ({
			id,
			label: libCom,
			departement: inseeDep,
		}))
		.sort((a, b) => a.label.localeCompare(b.label, 'fr', { ignorePunctuation: true }));
});

router.get(routes.tags, async (ctx: Koa.Context) => {
	const deveco = ctx.state.deveco;

	if (!deveco) {
		ctx.throw('Deveco not found');
	}

	const territoryId = deveco.territory.id;

	ctx.body = await getTerritoryTags(territoryId);
});

router.delete(routes.tags, RoleMiddleware('deveco'), async (ctx: DevecoContext) => {
	const deveco = ctx.state.deveco;

	if (!deveco) {
		ctx.throw('Deveco not found');
	}

	const territoryId = deveco.territory.id;

	await deleteTerritoryTags(territoryId, ctx);

	ctx.body = await getTerritoryTags(territoryId);
});

export default router;
