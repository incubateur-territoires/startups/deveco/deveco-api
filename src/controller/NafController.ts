import Router from 'koa-router';
import { ILike } from 'typeorm';

import AuthMiddleware, { DevecoContext } from '../middleware/AuthMiddleware';
import AppDataSource from '../data-source';
import { Naf } from '../entity/Naf';

const routerOpts: Router.IRouterOptions = {
	prefix: '/naf',
};

const router: Router = new Router(routerOpts);
router.use(AuthMiddleware);

router.get('/', async (ctx: DevecoContext) => {
	const query = ctx.query;
	const search = query.search as string;

	const codesNaf = await AppDataSource.getRepository(Naf).find({
		order: { id5: 'ASC' },
		where: [
			{
				id5: ILike(`%${search}%`),
			},
			{
				label5: ILike(`%${search}%`),
			},
		],
		take: 10,
	});

	ctx.body = codesNaf.map(({ id5, label5 }) => ({ id: id5, label: label5 }));
});

export default router;
