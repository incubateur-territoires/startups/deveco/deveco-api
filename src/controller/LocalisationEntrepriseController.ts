import Router from 'koa-router';
import { ILike } from 'typeorm';

import { LocalisationEntreprise } from '../entity/LocalisationEntreprise';
import { DevEco } from '../entity/DevEco';
import AuthMiddleware, { DevecoContext } from '../middleware/AuthMiddleware';
import RoleMiddleware from '../middleware/RoleMiddleware';
import AppDataSource from '../data-source';

const routerOpts: Router.IRouterOptions = {
	prefix: '/localisations',
};

const router: Router = new Router(routerOpts);
router.use(AuthMiddleware);
router.use(RoleMiddleware('deveco'));

router.get('/', async (ctx: DevecoContext) => {
	const { territory } = ctx.state.deveco as DevEco;
	const { search }: { search: string } = ctx.query as any;

	const localisations = await AppDataSource.getRepository(LocalisationEntreprise).find({
		order: { localisation: 'ASC' },
		where: {
			territory: { id: territory.id },
			localisation: ILike(`%${search}%`),
		},
		take: 5,
	});

	ctx.body = localisations;
});

export default router;
