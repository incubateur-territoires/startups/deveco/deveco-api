import { StatusCodes } from 'http-status-codes';
import * as Koa from 'koa';
import Router from 'koa-router';
import { In } from 'typeorm';

import { ReadStream } from 'fs';

import AppDataSource from '../data-source';
import { DevEco } from '../entity/DevEco';
import { Entreprise } from '../entity/Entreprise';
import { Entite } from '../entity/Entite';
import { isSiret } from '../lib/regexp';
import AuthMiddleware, { DevecoContext } from '../middleware/AuthMiddleware';
import RoleMiddleware from '../middleware/RoleMiddleware';
import SaveSearchRequestMiddleware from '../middleware/SaveSearchRequestMiddleware';
import communeService from '../service/CommuneService';
import entrepriseService from '../service/EntrepriseService';
import inseeService, {
	Criteria,
	EntityReadStream,
	Payload,
	relationsForToView,
} from '../service/InseeService';
import entiteService, { BatchResults } from '../service/EntiteService';
import eventLogService from '../service/EventLogService';
import { EventLogActionType } from '../entity/EventLog';
import { getLogEntityType } from '../lib/utils';
import excelService from '../service/ExcelService';
import { streamEtablissementsGeoJson } from '../controller/GeoJsonController';
import inseeUpdateService from '../service/InseeUpdateService';
import { extraPropsMap } from '../service/GeoJsonService';
import { Contact } from '../entity/Contact';
import { Particulier } from '../entity/Particulier';
import TokenCheckMiddleware from '../middleware/TokenCheckMiddleware';

const routerOpts: Router.IRouterOptions = {
	prefix: '/insee',
};

const router: Router = new Router(routerOpts);

const InseeUpdateAuthMiddleware = TokenCheckMiddleware(
	process.env.INSEE_UPDATE_TOKEN,
	'mise à jour INSEE'
);

router.param('token', InseeUpdateAuthMiddleware);

const sireneUpdate = (ctx: Koa.Context) => {
	let { startDate, endDate, cursor, loop } = ctx.query;
	const { extraParams } = ctx.query;

	if (Array.isArray(cursor)) {
		cursor = cursor[0];
	}

	if (Array.isArray(loop)) {
		loop = loop[0];
	}

	const loopNumber = loop ? Number(loop) : 0;
	if (isNaN(loopNumber)) {
		ctx.status = StatusCodes.BAD_REQUEST;
		ctx.body = { error: `Provided value for loop is not a number: ${loop}` };
		return;
	}

	if (!startDate) {
		ctx.status = StatusCodes.BAD_REQUEST;
		ctx.body = { error: 'No startDate in query.' };
		return;
	}

	if (Array.isArray(startDate)) {
		startDate = startDate[0];
	}

	const sDate = new Date(startDate);

	if (sDate.toString() === 'Invalid Date') {
		ctx.status = StatusCodes.BAD_REQUEST;
		ctx.body = { error: `Invalid startDate ${startDate}` };
		return;
	}

	if (!endDate) {
		ctx.status = StatusCodes.BAD_REQUEST;
		ctx.body = { error: 'No endDate in query.' };
		return;
	}

	if (Array.isArray(endDate)) {
		endDate = endDate[0];
	}

	const eDate = new Date(endDate);

	if (eDate.toString() === 'Invalid Date') {
		ctx.status = StatusCodes.BAD_REQUEST;
		ctx.body = { error: `Invalid endDate ${endDate}` };
		return;
	}

	const start = sDate.toISOString().slice(0, 10);
	const end = eDate.toISOString().slice(0, 10);

	inseeUpdateService.getSireneUpdateForDates({
		start,
		end,
		cursor,
		loop: loopNumber,
		extraParams: typeof extraParams === 'string' ? [extraParams] : extraParams,
	});
};

const successionLiensUpdate = (ctx: Koa.Context) => {
	let { startDate, endDate, cursor, loop } = ctx.query;
	const { extraParams } = ctx.query;

	if (Array.isArray(cursor)) {
		cursor = cursor[0];
	}

	if (Array.isArray(loop)) {
		loop = loop[0];
	}

	const loopNumber = loop ? Number(loop) : 0;
	if (isNaN(loopNumber)) {
		ctx.status = StatusCodes.BAD_REQUEST;
		ctx.body = { error: `Provided value for loop is not a number: ${loop}` };
		return;
	}

	if (!startDate) {
		ctx.status = StatusCodes.BAD_REQUEST;
		ctx.body = { error: 'No startDate in query.' };
		return;
	}

	if (Array.isArray(startDate)) {
		startDate = startDate[0];
	}

	const sDate = new Date(startDate);

	if (sDate.toString() === 'Invalid Date') {
		ctx.status = StatusCodes.BAD_REQUEST;
		ctx.body = { error: `Invalid startDate ${startDate}` };
		return;
	}

	if (!endDate) {
		ctx.status = StatusCodes.BAD_REQUEST;
		ctx.body = { error: 'No endDate in query.' };
		return;
	}

	if (Array.isArray(endDate)) {
		endDate = endDate[0];
	}

	const eDate = new Date(endDate);

	if (eDate.toString() === 'Invalid Date') {
		ctx.status = StatusCodes.BAD_REQUEST;
		ctx.body = { error: `Invalid endDate ${endDate}` };
		return;
	}

	const start = sDate.toISOString().slice(0, 10);
	const end = eDate.toISOString().slice(0, 10);

	inseeUpdateService.getSireneSuccessionUpdateForDates({
		start,
		end,
		cursor,
		loop: loopNumber,
		extraParams: typeof extraParams === 'string' ? [extraParams] : extraParams,
	});
};

const dateDeNaissanceUpdate = async () => {
	type MandataireTemp = {
		hash: string;
		dateDeNaissance: string | undefined;
		siret: string;
	};

	console.info(
		'Will try updating birthdates of existing Contacts/Particuliers using saved Mandataires Sociaux from API Entreprise.'
	);

	const existingMandataires = (await AppDataSource.getRepository(Entreprise).find()).reduce(
		(result: Array<MandataireTemp>, e: Entreprise) => [
			...result,
			...e.mandatairesSociaux.map((ms) => ({
				hash: `${e.siret}-${ms.nom}-${ms.prenom}`,
				dateDeNaissance: ms.date_naissance,
				siret: e.siret,
			})),
		],
		[]
	);

	const existingSirets = existingMandataires.map((ms: MandataireTemp) => ms.siret);

	const dateDeNaissanceMap = existingMandataires.reduce(
		(result: Record<string, string>, mt: MandataireTemp) => ({
			...result,
			...(mt.dateDeNaissance ? { [mt.hash]: mt.dateDeNaissance } : {}),
		}),
		{}
	);

	const relevantContacts: Array<{ particulierId: Particulier['id']; hash: string }> = (
		await AppDataSource.getRepository(Contact).find({
			where: {
				entite: {
					entreprise: {
						siret: In(existingSirets),
					},
				},
			},
			relations: {
				entite: {
					entreprise: true,
				},
				particulier: true,
			},
		})
	).map((contact) => ({
		particulierId: contact.particulier.id,
		hash: `${contact.entite.entreprise?.siret}-${contact.particulier.nom}-${contact.particulier.prenom}`,
	}));

	let updates = 0;

	for (const contact of relevantContacts) {
		const particulierId = contact.particulierId;

		if (!particulierId) {
			continue;
		}

		updates += 1;

		const dateDeNaissance = dateDeNaissanceMap[contact.hash];
		if (dateDeNaissance) {
			await AppDataSource.getRepository(Particulier).update(particulierId, {
				dateDeNaissance: dateDeNaissance,
			});
		}
	}

	console.info(`${updates} Particuliers have had their birthdate updated.`);
};

router.get('/update/:token/:updateType?', async (ctx: DevecoContext) => {
	const updateType = ctx.params.updateType;

	switch (updateType) {
		case 'dateFermeture':
			inseeUpdateService.updateDateFermeture();
			break;
		case 'geolocalisation':
			inseeUpdateService.updateGeolocalisation();
			break;
		case 'ess':
			inseeUpdateService.updateESS();
			break;
		case 'ess_territory': {
			const { territoryId } = ctx.query;
			if (!territoryId || Array.isArray(territoryId) || Number.isNaN(Number(territoryId))) {
				ctx.status = StatusCodes.BAD_REQUEST;
				ctx.body = { error: `Invalid territoryId ${territoryId}` };
				return;
			}
			inseeUpdateService.updateESSTerritory(Number(territoryId));
			break;
		}
		case 'adresses':
			inseeUpdateService.adressesToBAN();
			break;
		case 'liens':
			successionLiensUpdate(ctx);
			break;
		case 'dateDeNaissance':
			dateDeNaissanceUpdate();
			break;
		case 'sirene':
		default:
			sireneUpdate(ctx);
			break;
	}

	ctx.status = StatusCodes.OK;
	ctx.body = {};
});

router.use(AuthMiddleware);
router.use(RoleMiddleware('deveco'));

const routes = {
	getAll: '/',
	batch: '/batch',
	searchBySiret: '/siret',
	getOne: '/:siret',
	modifier: '/:siret/modifier',
	creer: '/creer',
	toggleFavorite: '/favoris',
};

const resultsPerPage = 20;

type EntreprisePayload =
	| {
			entreprise: (Entreprise & { exogene: boolean }) | null;
			existing: boolean;
	  }
	| { error: string };

export const extractSearchCriteria = async (ctx: DevecoContext): Promise<Criteria> => {
	const deveco = ctx.state.deveco as DevEco;

	const recherche = ((ctx.query.recherche as string) || '').replaceAll("'", "''");
	const etatType = (ctx.query.etat as string) || '';
	const suiviRaw = (ctx.query.suivi as string) || '';
	const codesNaf = (ctx.query.codesNaf as string[]) || [];
	const communes = (ctx.query.communes as string[]) || [];
	const epcis = (ctx.query.epcis as string[]) || [];
	const tranchesEffectifs = (ctx.query.effectifs as string[]) || [];
	const formes = (ctx.query.formes as string) || '';
	const cp = (ctx.query.cp as string) || '';
	const rue = (ctx.query.rue as string) || '';
	const activites = (ctx.query.activites as string[]) || [];
	const zones = (ctx.query.zones as string[]) || [];
	const demandes = (ctx.query.demandes as string[]) || [];
	const mots = (ctx.query.mots as string[]) || [];
	const territorialiteRaw = (ctx.query.territorialite as string) ?? '';
	const qpvs = (ctx.query.qpvs as string[]) || [];
	const zrrs = (ctx.query.zrrs as string[]) || [];
	const ess = (ctx.query.ess as string) || '';
	const zonages = (ctx.query.zonages as string[]) || [];

	const codesCommunes = await communeService.expandCommunesForMetropoles(communes);

	const suivi = inseeService.parseSuivi(suiviRaw);

	const territorialite = inseeService.parseTerritorialite(territorialiteRaw);

	const criteria: Criteria = {
		recherche,
		etatType,
		codesNaf,
		codesCommunes,
		epcis,
		tranchesEffectifs,
		formes,
		territoire: deveco.territory.id,
		cp,
		rue,
		suivi,
		activites,
		zones,
		demandes,
		mots,
		territorialite,
		qpvs,
		zrrs,
		ess,
		zonages,
	};

	return criteria;
};

router.post(routes.toggleFavorite, async (ctx: DevecoContext) => {
	const deveco = ctx.state.deveco as DevEco;
	const territory = deveco.territory;
	const siret = ctx.request.body?.siret as string;
	const favori = !!ctx.request.body?.favori;

	await AppDataSource.manager.query(
		`
		INSERT INTO etablissement_favori(territory_id, siret, deveco_id, favori)
		VALUES ($1, $2, $3, $4)
		ON CONFLICT ("deveco_id", "territory_id", "siret")
		DO
		UPDATE
		SET
		favori = EXCLUDED.favori
		`,
		[territory.id, siret, deveco.id, favori]
	);
	ctx.body = {};
});

router.get(
	routes.searchBySiret,
	async (
		ctx: Koa.ParameterizedContext<Koa.DefaultState, Koa.DefaultContext, EntreprisePayload>
	) => {
		const query = ctx.query;
		const search = (query.search || query.recherche) as string;

		if (!search) {
			ctx.status = StatusCodes.BAD_REQUEST;
			ctx.body = { error: 'Cannot search for empty string.' };
			return;
		}

		if (!isSiret(search)) {
			ctx.status = StatusCodes.BAD_REQUEST;
			ctx.body = { error: `Invalid SIRET: ${search}` };
			return;
		}

		const deveco = ctx.state.deveco as DevEco;
		const territoire = deveco.territory;

		const entite = await AppDataSource.getRepository(Entite).findOne({
			where: {
				territoire: {
					id: territoire.id,
				},
				entreprise: { siret: search },
			},
			relations: { entreprise: true },
		});

		// Entreprise already has an Entite
		if (entite?.entreprise) {
			ctx.body = {
				existing: true,
				entreprise: { ...entite.entreprise, exogene: entite.exogene },
			};
			return;
		}

		// Look for entreprise inside Etablissement table
		const etablissement = await inseeService.searchBySiret(search);

		const entreprise = etablissement
			? entrepriseService.etablissementToEntreprise(etablissement)
			: null;

		ctx.body = {
			existing: false,
			entreprise: entreprise
				? {
						...entreprise,
						exogene: !entrepriseService.isCodeCommuneInTerritory({
							codeCommune: etablissement?.codeCommune || '',
							territoire,
						}),
				  }
				: null,
		};
	}
);

router.post(routes.batch, async (ctx: DevecoContext) => {
	const deveco = ctx.state.deveco as DevEco;

	const activitesReelles = (ctx.request.body?.activites as string[]) ?? [];
	const entrepriseLocalisations = (ctx.request.body?.localisations as string[]) ?? [];
	const motsCles = (ctx.request.body?.mots as string[]) ?? [];

	let sirets = (ctx.request.body?.sirets as string[]) ?? [];

	const entiteIds: Entite['id'][] = [];

	if (!sirets.length) {
		sirets = await doSiretsSearchAll(ctx);
	}

	let batchResults: BatchResults = {
		total: 0,
		successes: 0,
		errors: [],
		errorEntites: [],
	};

	if (sirets.length > 0) {
		for (const siret of sirets) {
			const entite = await entrepriseService.getOrCreateEntrepriseAndEntite({
				createur: deveco,
				siret,
				skipApiEntreprise: true,
			});
			entiteIds.push(entite.id);
		}

		batchResults = await entiteService.addQualificationsToEntites(entiteIds, deveco, {
			activitesReelles,
			entrepriseLocalisations,
			motsCles,
		});

		batchResults.errors = (
			await AppDataSource.getRepository(Entite).find({
				where: { id: In(batchResults.errorEntites ?? []) },
				select: { entreprise: { nom: true } },
			})
		)
			.map((e) => e.entreprise?.nom || '')
			.filter(Boolean);
	}

	const { page, pages, results, sirets: siretsResults } = await doSiretsSearch(ctx);
	const data = await inseeService.siretsToListPayload(deveco, siretsResults);
	ctx.body = { page, pages, results, data, batchResults };
});

router.get(
	routes.getOne,
	async (ctx: Koa.ParameterizedContext<Koa.DefaultState, Koa.DefaultContext, Payload>) => {
		const deveco = ctx.state.deveco as DevEco;

		const siret = ctx.params.siret;

		if (!siret) {
			ctx.throw(StatusCodes.BAD_REQUEST, 'Empty search');
		}

		if (!isSiret(siret)) {
			ctx.throw(StatusCodes.BAD_REQUEST, `Invalid siret ${siret}`);
		}

		const entite = await entrepriseService.getOrCreateEntrepriseAndEntite({
			createur: deveco,
			siret,
			skipApiEntreprise: false,
		});

		await eventLogService.addEventLog({
			action: EventLogActionType.VIEW,
			deveco,
			entityType: getLogEntityType(entite),
			entityId: entite.id,
		});

		const withFreres = true;
		const withHistory = true;

		ctx.body = await inseeService.augmentedEntrepriseFromEntite({
			ctx,
			deveco,
			entite,
			withFreres,
			withHistory,
		});
	}
);

router.post(routes.creer, async (ctx: DevecoContext) => {
	const {
		siret,
	}: {
		siret: string;
	} = ctx.request.body;

	const createur = ctx.state.deveco;

	if (!createur) {
		ctx.throw('Deveco not found');
	}

	const entite = await entiteService.createEntiteForSiret({ createur, fuzzy: siret });

	ctx.body = { siret: entite.entreprise?.siret };
});

router.post(
	routes.modifier,
	async (ctx: Koa.ParameterizedContext<Koa.DefaultState, Koa.DefaultContext, Payload>) => {
		const deveco = ctx.state.deveco as DevEco;

		const siret = ctx.params.siret;

		if (!siret) {
			ctx.throw(StatusCodes.BAD_REQUEST, 'Empty search');
		}

		if (!isSiret(siret)) {
			ctx.throw(StatusCodes.BAD_REQUEST, `Invalid siret ${siret}`);
		}

		let entite = await entrepriseService.getOrCreateEntrepriseAndEntite({
			createur: deveco,
			siret,
			skipApiEntreprise: false,
		});

		const activitesReelles = (ctx.request.body?.activites as string[]) ?? [];
		const entrepriseLocalisations = (ctx.request.body?.localisations as string[]) ?? [];
		const motsCles = (ctx.request.body?.mots as string[]) ?? [];
		const activiteAutre = (ctx.request.body?.autre as string) ?? '';

		await entiteService.updateQualificationsForEntite(entite.id, deveco, {
			activitesReelles,
			entrepriseLocalisations,
			motsCles,
			activiteAutre,
		});

		entite = await AppDataSource.getRepository(Entite).findOneOrFail({
			where: {
				id: entite.id,
			},
			relations: relationsForToView,
		});

		const withFreres = true;
		const withHistory = true;

		ctx.body = await inseeService.augmentedEntrepriseFromEntite({
			ctx,
			deveco,
			entite,
			withFreres,
			withHistory,
		});
	}
);

const doSiretsSearch = async (
	ctx: DevecoContext
): Promise<{
	page: number;
	pages: number;
	results: number;
	sirets: string[];
}> => {
	const deveco = ctx.state.deveco as DevEco;
	const rawPage = ctx.query.page as unknown;
	let page = Number.isNaN(Number(rawPage)) ? 1 : Number(rawPage);

	const orderBy = (ctx.query.o as string) ?? undefined;
	const orderDirection = (ctx.query.d as string) ?? undefined;

	const criteria = await extractSearchCriteria(ctx);

	let [sirets, totalResults] = await inseeService.search({
		criteria,
		page,
		resultsPerPage,
		orderBy,
		orderDirection,
		deveco,
	});

	// if no sirets have been returned, return last page
	if (sirets.length == 0 && totalResults > 0) {
		page = Math.floor(totalResults / resultsPerPage) + 1;
		[sirets, totalResults] = await inseeService.search({
			criteria,
			page,
			resultsPerPage,
			orderBy,
			orderDirection,
			deveco,
		});
	}

	const fullPages = Math.floor(totalResults / resultsPerPage);
	const pages = fullPages + (totalResults - fullPages * resultsPerPage > 0 ? 1 : 0);
	return {
		page,
		pages,
		results: totalResults,
		sirets,
	};
};

const doSiretsSearchAll = async (ctx: DevecoContext): Promise<string[]> => {
	const deveco = ctx.state.deveco as DevEco;
	const orderBy = (ctx.query.o as string) ?? undefined;
	const orderDirection = (ctx.query.d as string) ?? undefined;

	const criteria = await extractSearchCriteria(ctx);

	const [sirets] = await inseeService.search({
		criteria,
		orderBy,
		orderDirection,
		deveco,
	});

	return sirets;
};

const xlsxExport = async (
	ctx: DevecoContext,
	{
		deveco,
		sirets,
		criteria,
		orderBy,
		orderDirection,
	}: {
		deveco: DevEco;
		sirets: string[];
		criteria: Criteria;
		orderBy?: string;
		orderDirection?: string;
	}
) => {
	const timestamp = Date.now();
	const filename = `deveco-export-entreprises-${timestamp}.xlsx`;

	ctx.set('Content-disposition', `attachment; filename=${filename}`);
	ctx.set('Content-type', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
	ctx.status = StatusCodes.OK;

	const queryRunner = AppDataSource.createQueryRunner();
	await queryRunner.connect();

	let siretsStream;
	if (sirets.length) {
		siretsStream = ReadStream.from(sirets.map((siret) => ({ siret }))) as EntityReadStream<{
			siret: string;
		}>;
	} else {
		siretsStream = await inseeService.search({
			criteria,
			orderBy,
			orderDirection,
			deveco,
			queryRunner,
		});
	}

	try {
		await excelService.makeWorkbookForEtablissements({
			deveco,
			criteria,
			input: siretsStream,
			output: ctx.res,
		});
	} catch (e) {
		console.error(e);
	}
};

// Search for Établissements
router.all(
	routes.getAll,
	SaveSearchRequestMiddleware('etablissements'),
	async (ctx: DevecoContext) => {
		const deveco = ctx.state.deveco as DevEco;

		const format = ctx.query.format as string | null;

		if (format) {
			const criteria = await extractSearchCriteria(ctx);

			const orderBy = (ctx.query.o as string) ?? undefined;
			const orderDirection = (ctx.query.d as string) ?? undefined;

			let sirets = (ctx.request.body?.ids as string)?.split(',').filter(Boolean);
			const extraProps = (ctx.request.body?.props as string)
				?.split(',')
				.filter(Boolean)
				.filter((s): s is keyof typeof extraPropsMap => ['dc', 'df'].includes(s));

			if (format === 'geojson' || format === 'ndgeojson') {
				if (!Array.isArray(sirets) || sirets.length === 0) {
					[sirets] = await inseeService.search({ criteria, orderBy, orderDirection, deveco });
				}

				await streamEtablissementsGeoJson(ctx, {
					territoire: deveco.territory,
					sirets,
					extraProps,
					format,
				});

				return;
			}

			await xlsxExport(ctx, {
				deveco,
				sirets,
				criteria,
			});

			return;
		}

		const { page, pages, results, sirets } = await doSiretsSearch(ctx);

		ctx.status = StatusCodes.OK;
		ctx.body = {
			page,
			pages,
			results,
			data: sirets.length ? await inseeService.siretsToListPayload(deveco, sirets) : [],
		};
	}
);

export default router;
