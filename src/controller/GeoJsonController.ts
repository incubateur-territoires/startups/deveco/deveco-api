import Router from 'koa-router';
import { StatusCodes } from 'http-status-codes';

import { Readable, Transform } from 'stream';

import AppDataSource from '../data-source';
import { GeoToken } from '../entity/GeoToken';
import { GeoTokenRequest } from '../entity/GeoTokenRequest';
import {
	EtablissementGeoJsonData,
	propsMap,
	extraPropsMap,
	toFeat,
} from '../service/GeoJsonService';
import inseeService from '../service/InseeService';
import { Territory } from '../entity/Territory';
import { DevecoContext } from '../middleware/AuthMiddleware';

const prefix = '/geojson';
const routerOpts: Router.IRouterOptions = {
	prefix,
};

const router: Router = new Router(routerOpts);

const getGeoToken = (token: string) =>
	AppDataSource.getRepository(GeoToken).findOne({
		where: { token },
		relations: {
			deveco: {
				territory: true,
			},
		},
	});

export const streamEtablissementsGeoJson = async (
	ctx: DevecoContext,
	{
		territoire,
		sirets,
		extraProps,
		headersOnly,
		format,
	}: {
		territoire: Territory;
		sirets?: string[];
		extraProps?: Array<keyof typeof extraPropsMap>;
		headersOnly?: boolean;
		format: 'geojson' | 'ndgeojson';
	}
) => {
	const queryRunner = AppDataSource.createQueryRunner();
	await queryRunner.connect();

	const propsMapKeys = Object.keys(propsMap) as Array<keyof typeof propsMap>;
	const extraPropsKeys = extraProps ?? [];

	const geoJsonStream = await inseeService.createEtablissementStream(
		queryRunner,
		territoire,
		[...propsMapKeys, ...extraPropsKeys],
		sirets
	);

	const transform = async (
		geoJson: EtablissementGeoJsonData,
		_encoding: unknown,
		callback: (error: Error | null, arg: string) => void
	) => {
		const output = toFeat(geoJson);
		callback(null, output);
	};

	const dataStream = new Transform({
		transform,
		readableObjectMode: true,
		writableObjectMode: true,
	});

	geoJsonStream.pipe(dataStream);

	const streamOpts =
		format === 'geojson'
			? {
					separator: ',',
					dataPrefix: `{ "type": "FeatureCollection", "features": [`,
					dataSuffix: '] }',
					cleanup: () => queryRunner.release(),
			  }
			: {
					separator: '\n',
					cleanup: () => queryRunner.release(),
			  };
	const timestamp = Date.now();
	const extension = format === 'geojson' ? 'geojson' : 'ndjson';
	const filename = `deveco-export-entreprises-${timestamp}.${extension}`;

	const fileOpts = {
		filename,
		mimetype: 'application/x-ndjson',
	};

	return streamData({
		ctx,
		dataStream,
		streamOpts,
		fileOpts,
		headersOnly,
	});
};

export const streamData = ({
	ctx,
	dataStream,
	streamOpts,
	fileOpts,
	headersOnly,
}: {
	ctx: DevecoContext;
	dataStream: Readable;
	streamOpts?: {
		separator?: string;
		dataPrefix?: string;
		dataSuffix?: string | null;
		cleanup?: () => void;
	};
	fileOpts?: {
		filename: string;
		mimetype: string;
	};
	headersOnly?: boolean;
}): void => {
	ctx.status = StatusCodes.OK;
	ctx.type = fileOpts?.mimetype ?? 'application/octet-stream';
	ctx.set('Content-disposition', `attachment; filename=${fileOpts?.filename ?? 'data'}`);

	if (headersOnly) {
		ctx.body = '';
		return;
	}

	const responseStream = new Readable({
		read() {
			return;
		},
	});
	ctx.body = responseStream;

	ctx.req.on('close', () => {
		dataStream.destroy();
		streamOpts?.cleanup?.();
	});

	dataStream.on('end', () => {
		if (typeof streamOpts?.dataSuffix !== 'undefined') {
			responseStream.push(streamOpts.dataSuffix);
		}
		responseStream.push(null);
		responseStream.emit('end');
		streamOpts?.cleanup?.();
		ctx.res.end();
	});

	let i = 0;
	dataStream.on('data', async (data: string) => {
		if (streamOpts?.separator && i > 0) {
			responseStream.push(streamOpts.separator);
		}

		dataStream.pause();
		responseStream.push(data);
		dataStream.resume();
		i++;
	});

	if (streamOpts?.dataPrefix) {
		responseStream.push(streamOpts.dataPrefix);
	}

	return;
};

const createGeoTokenRequestAndGetId = (ctx: DevecoContext) => {
	const url = ctx.url;
	const userAgent = ctx.header['user-agent'] ?? null;
	const geoTokenRequest = AppDataSource.getRepository(GeoTokenRequest).create({
		url,
		userAgent,
	});

	return AppDataSource.getRepository(GeoTokenRequest).save(geoTokenRequest);
};

const updateGeoTokenRequestWithGeoToken = (geoTokenRequestId: number, geoToken: GeoToken) =>
	AppDataSource.getRepository(GeoTokenRequest).update(geoTokenRequestId, {
		geoToken,
	});

router.all('/:token', async (ctx: DevecoContext) => {
	if (!['GET', 'HEAD'].includes(ctx.method)) {
		ctx.throw(405);
	}

	const token = ctx.params.token;

	const { id: geoTokenRequestId } = await createGeoTokenRequestAndGetId(ctx);

	if (!token) {
		ctx.throw(StatusCodes.BAD_REQUEST, 'Le jeton ne peut pas être vide.');
	}

	const geoToken = await getGeoToken(token);

	if (!geoToken) {
		ctx.throw(StatusCodes.BAD_REQUEST, "Le jeton n'a pas été trouvé.");
	}

	if (!geoToken.active) {
		ctx.throw(StatusCodes.BAD_REQUEST, "Le jeton n'est pas actif.");
	}

	await updateGeoTokenRequestWithGeoToken(geoTokenRequestId, geoToken);

	const territoire = geoToken.deveco.territory;

	return streamEtablissementsGeoJson(ctx, {
		territoire,
		headersOnly: ctx.method === 'HEAD',
		format: 'ndgeojson',
	});
});

export default router;
