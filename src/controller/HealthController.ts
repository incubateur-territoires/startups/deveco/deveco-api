import { StatusCodes } from 'http-status-codes';
import Router from 'koa-router';

import AppDataSource from '../data-source';
import { Territory } from '../entity/Territory';
import { DevecoContext } from '../middleware/AuthMiddleware';

export const healthRoutePrefix = '/health';

const routerOpts: Router.IRouterOptions = {
	prefix: healthRoutePrefix,
};

const router: Router = new Router(routerOpts);

const routes = {
	healthcheck: '/',
};

router.get(routes.healthcheck, async (ctx: DevecoContext) => {
	// check that the DB connection is working and fast
	const territoryCount = await AppDataSource.getRepository(Territory).count();

	if (!territoryCount) {
		ctx.throw(StatusCodes.SERVICE_UNAVAILABLE);
	}

	ctx.status = StatusCodes.OK;
	ctx.body = {
		status: 'OK',
	};
});

export default router;
