import { StatusCodes } from 'http-status-codes';
import * as Koa from 'koa';
import Router from 'koa-router';
import { In } from 'typeorm';

import { Fiche } from '../entity/Fiche';
import { Entite } from '../entity/Entite';
import { Particulier } from '../entity/Particulier';
import { Territory } from '../entity/Territory';
import AuthMiddleware, { DevecoContext } from '../middleware/AuthMiddleware';
import RoleMiddleware from '../middleware/RoleMiddleware';
import ficheService, { Criteria } from '../service/FicheService';
import { toView } from '../view/FicheView';
import { FicheArchive } from '../entity/FicheArchive';
import { Zonage } from '../entity/Zonage';
import AppDataSource from '../data-source';
import eventLogService from '../service/EventLogService';
import { DevEco } from '../entity/DevEco';
import { EventLogActionType } from '../entity/EventLog';
import { displayFicheName, getLogEntityType } from '../lib/utils';
import entiteService, { BatchResults } from '../service/EntiteService';
import SaveSearchRequestMiddleware from '../middleware/SaveSearchRequestMiddleware';
import createurService from '../service/CreateurService';
import { Qpv } from '../entity/Qpv';
import excelService from '../service/ExcelService';
import territoireService from '../service/TerritoireService';

const routerOpts: Router.IRouterOptions = {
	prefix: '/fiches',
};

const router: Router = new Router(routerOpts);

const routes = {
	batch: '/batch',
	getAll: '/',
	modifier: '/:id/modifier',
	creer: '/creer',
	getOne: '/:id',
	lierEntreprise: '/:id/lierEntreprise/:siret',
	supprimerLienEntreprise: '/:id/supprimerLienEntreprise',
	filtersSource: '/filtersSource/:context?',
	getType: '/type/:id',
};

router.use(AuthMiddleware);
router.use(RoleMiddleware('deveco'));

const resultsPerPage = 20;

router.post(routes.batch, async (ctx: DevecoContext) => {
	const entiteIds = (ctx.request.body?.entiteIds as number[]) ?? [];

	// if no ficheIds were specified,
	if (!entiteIds.length) {
		const [fiches] = await doSearch(ctx);
		fiches.forEach((fiche) => {
			if (fiche.entite.id) {
				entiteIds.push(fiche.entite.id);
			}
		});
	}

	let batchResults: BatchResults = {
		total: 0,
		successes: 0,
		errors: [],
		errorEntites: [],
	};

	if (entiteIds.length > 0) {
		const deveco = ctx.state.deveco as DevEco;

		const activitesReelles = (ctx.request.body?.activites as string[]) ?? [];
		const entrepriseLocalisations = (ctx.request.body?.localisations as string[]) ?? [];
		const motsCles = (ctx.request.body?.mots as string[]) ?? [];

		batchResults = await entiteService.addQualificationsToEntites(entiteIds, deveco, {
			activitesReelles,
			entrepriseLocalisations,
			motsCles,
		});

		for (const entiteId of batchResults.errorEntites ?? []) {
			const light = true;
			const fiche = await ficheService.loadFiche({ entite: { id: entiteId } }, light);
			batchResults.errors.push(displayFicheName(fiche.entite));
		}

		delete batchResults.errorEntites;
	}

	const searchResults = await getFichesForPaginatedView(ctx);

	ctx.body = { ...searchResults, batchResults };
});

export const extractSearchCriteria = async (ctx: DevecoContext): Promise<Criteria> => {
	const recherche = ((ctx.query.recherche as string) || '').replace(`'`, `''`);
	const activites = (ctx.query.activites as string[]) || [];
	const zones = (ctx.query.zones as string[]) || [];
	const demandes = (ctx.query.demandes as string[]) || [];
	const rawLocal = ctx.query.local as string;
	const local = Number.isNaN(Number(rawLocal)) ? 0 : Number(rawLocal);
	const mots = (ctx.query.mots as string[]) || [];
	const cp = (ctx.query.cp as string) || '';
	const rue = (ctx.query.rue as string) || '';
	const qpvs = (ctx.query.qpvs as string[]) || [];
	const suiviRaw = (ctx.query.suivi as string) || '';

	const ids = (ctx.request.body?.ids as string)
		?.split(',')
		.filter(Boolean)
		.map(Number)
		.filter((n) => !isNaN(n));

	const deveco = ctx.state.deveco as DevEco;

	const suivi = ficheService.parseSuivi(suiviRaw);

	const criteria: Criteria = {
		recherche,
		activites,
		zones,
		demandes,
		local,
		territoire: deveco.territory.id,
		mots,
		cp,
		rue,
		qpvs,
		suivi,
		ids,
	};

	return criteria;
};

const getFichesForPaginatedView = async (ctx: DevecoContext) => {
	const [fiches, totalResults] = await doSearch(ctx);

	const filledPages = Math.floor(totalResults / resultsPerPage);
	const totalPages = filledPages + (totalResults - filledPages * resultsPerPage > 0 ? 1 : 0);

	const rawPage = Number(ctx.query.page);
	const page = Number.isNaN(rawPage) ? 0 : rawPage;

	return {
		page,
		pages: totalPages,
		results: totalResults,
		data: fiches.map((fiche) => toView(fiche.entite, fiche)),
	};
};

const xlsxExport = async (ctx: DevecoContext) => {
	const timestamp = Date.now();
	const filename = `deveco-export-createurs-${timestamp}.xlsx`;

	ctx.set('Content-disposition', `attachment; filename=${filename}`);
	ctx.set('Content-type', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
	ctx.status = StatusCodes.OK;

	const [fiches] = await doSearch(ctx);

	if (fiches.length > 1000) {
		ctx.throw(
			StatusCodes.BAD_REQUEST,
			`Cannot export more than 1000 (${fiches?.length} requested)`
		);
	}

	const criteria = await extractSearchCriteria(ctx);

	try {
		await excelService.makeWorkbookForCreateurs({
			fiches,
			criteria,
			output: ctx.res,
		});
	} catch (e) {
		console.error(e);
	}
};

const doSearch = async (ctx: DevecoContext): Promise<[Fiche[], number]> => {
	const orderBy = (ctx.query.o as string) ?? undefined;
	const orderDirection = (ctx.query.d as string) ?? undefined;

	const criteria = await extractSearchCriteria(ctx);

	const rawPage = Number(ctx.query.page);
	const page = Number.isNaN(rawPage) ? 1 : rawPage;

	const pagination = page ? { page, resultsPerPage } : {};

	let entiteIds: Entite['id'][] = criteria.ids || [];
	let count = entiteIds?.length;
	if (!count) {
		[entiteIds, count] = await ficheService.search({
			criteria,
			orderBy,
			orderDirection,
			...pagination,
		});
	}

	const fiches = await AppDataSource.getRepository(Fiche).find({
		where: { entite: { id: In(entiteIds) } },
		relations: {
			entite: {
				contacts: true,
				particulier: true,
				proprietes: {
					local: true,
				},
				etablissementCree: true,
			},
			demandes: true,
			echanges: true,
		},
	});

	fiches.sort((f1, f2) => entiteIds.indexOf(f1.entite.id) - entiteIds.indexOf(f2.entite.id));

	return [fiches, count];
};

// Search for Fiches
router.all(routes.getAll, SaveSearchRequestMiddleware('createurs'), async (ctx: DevecoContext) => {
	const format = ctx.query.format;

	if (format === 'xlsx') {
		await xlsxExport(ctx);
		return;
	}

	ctx.body = await getFichesForPaginatedView(ctx);
});

// Update Fiche
router.post(routes.modifier, async (ctx: DevecoContext) => {
	const deveco = ctx.state.deveco as DevEco;

	const id = ctx.params.id;

	const { futureEnseigne, ...particulier } = ctx.request.body
		.particulier as Partial<Particulier> & { futureEnseigne?: string };

	const activitesReelles = (ctx.request.body.activites as string[]) ?? [];
	const entrepriseLocalisations = (ctx.request.body.localisations as string[]) ?? [];
	const motsCles = (ctx.request.body.mots as string[]) ?? [];
	const activiteAutre = (ctx.request.body.autre as string) ?? '';

	const fiche = await AppDataSource.getRepository(Fiche).findOneOrFail({
		relations: {
			entite: {
				particulier: true,
				entreprise: true,
			},
		},
		where: { id },
	});

	if (fiche.entite.etablissementCree) {
		ctx.throw(
			StatusCodes.BAD_REQUEST,
			'Impossible de modifier un Créateur qui a créé un Établissement'
		);
	}

	if (fiche.entite.entreprise) {
		ctx.throw(StatusCodes.BAD_REQUEST, 'Réservé aux Créateurs seulement');
	}

	const particulierId = fiche.entite.particulier?.id;
	if (!particulierId) {
		ctx.throw(StatusCodes.BAD_REQUEST, 'Réservé aux Créateurs seulement');
	}

	if (Object.keys(particulier).length > 1) {
		if (particulier.geolocation) {
			const [x, y] = particulier.geolocation.replace('(', '').replace(')', '').split(',');
			const qpv = await AppDataSource.getRepository(Qpv)
				.createQueryBuilder()
				.where('ST_Contains (geom, ST_POINT (:x, :y))', { x, y })
				.getOne();
			if (qpv) {
				particulier.qpv = qpv;
			}
		}
		await AppDataSource.getRepository(Particulier).update(particulierId, particulier);
	}

	// TODO find a better way to do both at the same time while still doing the special stuff required for activites and all
	await entiteService.updateQualificationsForEntite(fiche.entite.id, deveco, {
		activitesReelles,
		activiteAutre,
		entrepriseLocalisations,
		motsCles,
	});

	const entite = {} as Partial<Entite>;
	if (futureEnseigne) {
		entite.futureEnseigne = futureEnseigne;
	}

	await entiteService.updateEntite(fiche.entite.id, deveco, entite, EventLogActionType.UPDATE);

	const ficheView = await ficheService.ficheToView({ id });
	ctx.body = ficheView;
});

const getQpvForCreateurs = async (territoire: Territory) => {
	const sql = `
	SELECT distinct particulier.qpv_id
	FROM particulier
	INNER JOIN entite ON entite.particulier_id = particulier.id
	WHERE entite.territoire_id = $1 AND particulier.qpv_id IS NOT NULL`;

	const qpvIds = (
		(await AppDataSource.manager.query(sql, [territoire.id])) as { qpv_id: string }[]
	).map(({ qpv_id }) => qpv_id);

	const qpvs = await AppDataSource.getRepository(Qpv).find({ where: { id: In(qpvIds) } });

	return qpvs.reduce(
		(acc, qpv) => [...acc, { id: qpv.id, nom: qpv.nomQp }],
		<{ id: string; nom: string }[]>[]
	);
};

const getQpvForTerritoryEtablissementReference = async (territoire: Territory) => {
	const sql = `
	SELECT distinct qpv_id
	FROM territory_etablissement_reference
	WHERE territory_id = $1 AND qpv_id IS NOT NULL AND NOT exogene`;

	const qpvIds = (
		(await AppDataSource.manager.query(sql, [territoire.id])) as { qpv_id: string }[]
	).map(({ qpv_id }) => qpv_id);

	const qpvs = await AppDataSource.getRepository(Qpv).find({ where: { id: In(qpvIds) } });

	return qpvs.reduce(
		(acc, qpv) => [...acc, { id: qpv.id, nom: qpv.nomQp }],
		<{ id: string; nom: string }[]>[]
	);
};

const formatZonage = ({ id, nom }: Zonage) => ({
	id,
	nom,
});

router.get(routes.filtersSource, async (ctx: DevecoContext) => {
	const deveco = ctx.state.deveco as DevEco;
	const context = ctx.params.context;
	const territoire = deveco.territory;

	const [demandes, qualifications, qpvs, zonages] = await Promise.all([
		ficheService.getExistingDemandes(territoire),
		entiteService.getExistingQualifications(territoire),
		context === 'createurs'
			? getQpvForCreateurs(territoire)
			: getQpvForTerritoryEtablissementReference(territoire),
		territoireService.getZonagesForTerritoire(territoire),
	]);

	ctx.body = {
		...qualifications,
		demandes,
		qpvs,
		zonages: zonages.map(formatZonage),
	};
});

// Create Fiche
router.post(routes.creer, async (ctx: DevecoContext) => {
	const {
		particulier: particulierData,
		echange: echangeData,
		activites: activitesReelles,
		localisations,
		mots,
		autre,
	}: {
		particulier: {
			nom: string;
			prenom: string;
			email: string;
			telephone: string;
			adresse: string;
			ville: string;
			codePostal: string;
			geolocation: string;
			description: string;
			futureEnseigne: string;
		};
		echange: {
			compteRendu: string;
			date: Date;
			type: string;
			themes: string[];
			titre: string;
		};
		activites: string[];
		localisations: string[];
		mots: string[];
		autre: string;
	} = ctx.request.body;

	const createur = ctx.state.deveco as DevEco;
	const territoire = createur.territory;

	const ficheId = await createurService.createFicheFor({
		createur,
		particulierData,
		echangeData,
		activitesReelles,
		localisations,
		mots,
		autre,
	});

	const [ficheView, fiche] = await ficheService.ficheToViewWithFiche({
		id: ficheId,
		territoire: { id: territoire.id },
	});

	await eventLogService.addEventLog({
		action: EventLogActionType.CREATE,
		deveco: createur,
		entityType: getLogEntityType(fiche.entite),
		entityId: fiche.entite.id,
	});

	ctx.body = ficheView;
});

// Viewing Fiche
router.get(routes.getOne, async (ctx: DevecoContext) => {
	const id = ctx.params.id;
	const deveco = ctx.state.deveco as DevEco;
	const territoire = deveco.territory;

	const [ficheView, fiche] = await ficheService.ficheToViewWithFiche({
		id,
		territoire: { id: territoire.id },
	});

	await eventLogService.addEventLog({
		action: EventLogActionType.VIEW,
		deveco,
		entityType: getLogEntityType(fiche.entite),
		entityId: fiche.entite.id,
	});

	ctx.body = ficheView;
});

// Remove Fiche
router.del(routes.getOne, async (ctx: DevecoContext) => {
	const id = ctx.params.id;
	const deveco = ctx.state.deveco as DevEco;
	const territoire = deveco.territory;

	const fiche = await AppDataSource.getRepository(Fiche).findOneOrFail({
		// NOTE: territory id is used to confirm Fiche is indeed on the same territory
		where: { id, territoire: { id: territoire.id } },
		relations: {
			entite: { contacts: true },
			demandes: true,
			echanges: true,
			rappels: true,
		},
	});

	await AppDataSource.getRepository(FicheArchive).insert({ content: JSON.stringify(fiche) });
	await AppDataSource.getRepository(Fiche).delete({ id, territoire });

	await eventLogService.addEventLog({
		action: EventLogActionType.DELETE,
		deveco,
		entityType: getLogEntityType(fiche.entite),
		entityId: fiche.entite.id,
	});

	ctx.body = {};
});

router.get(
	routes.getType,
	async (
		ctx: Koa.ParameterizedContext<
			Koa.DefaultState,
			Koa.DefaultContext,
			{ type: 'createur'; ficheId: Fiche['id'] } | { type: 'etablissement'; siret: string }
		>
	) => {
		const ficheId = ctx.params.id;

		const fiche = await AppDataSource.getRepository(Fiche).findOneOrFail({
			where: {
				id: ficheId,
			},
			relations: {
				entite: {
					entreprise: true,
					particulier: true,
				},
			},
		});

		if (fiche.entite.entreprise) {
			ctx.body = { type: 'etablissement', siret: fiche.entite.entreprise.siret };
			return;
		}

		if (fiche.entite.particulier) {
			ctx.body = { type: 'createur', ficheId: fiche.id };
			return;
		}

		ctx.status = StatusCodes.NOT_FOUND;
		return;
	}
);

router.post(routes.lierEntreprise, async (ctx: DevecoContext) => {
	const id = ctx.params.id;
	const siret = ctx.params.siret as string;
	const deveco = ctx.state.deveco as DevEco;

	const fiche = await createurService.linkEntreprise({ id, deveco, siret });

	await eventLogService.addEventLog({
		action: EventLogActionType.CREATE_ENTREPRISE,
		deveco,
		entityType: getLogEntityType(fiche.entite),
		entityId: fiche.entite.id,
	});

	const [ficheView] = await ficheService.ficheToViewWithFiche({
		id,
		territoire: { id: deveco.territory.id },
	});

	ctx.body = ficheView;
});

router.post(routes.supprimerLienEntreprise, async (ctx: DevecoContext) => {
	const id = ctx.params.id;
	const deveco = ctx.state.deveco as DevEco;

	const fiche = await createurService.unlinkEntreprise({ id, deveco });

	await eventLogService.addEventLog({
		action: EventLogActionType.UNLINK_ENTREPRISE,
		deveco,
		entityType: getLogEntityType(fiche.entite),
		entityId: fiche.entite.id,
	});

	const [ficheView] = await ficheService.ficheToViewWithFiche({
		id,
		territoire: { id: deveco.territory.id },
	});

	ctx.body = ficheView;
});

export default router;
