import { StatusCodes } from 'http-status-codes';
import * as Koa from 'koa';
import Router from 'koa-router';
import { createObjectCsvStringifier } from 'csv-writer';
import { v4 as uuidv4 } from 'uuid';
import { In } from 'typeorm';

import { parse } from 'node:querystring';

import { Account } from '../entity/Account';
import { Commune } from '../entity/Commune';
import { DevEco } from '../entity/DevEco';
import { SearchRequest } from '../entity/SearchRequest';
import { Epci } from '../entity/Epci';
import { Password } from '../entity/Password';
import { Territory } from '../entity/Territory';
import { Metropole } from '../entity/Metropole';
import AuthMiddleware, { DevecoContext } from '../middleware/AuthMiddleware';
import RoleMiddleware from '../middleware/RoleMiddleware';
import AppDataSource from '../data-source';
import { Petr } from '../entity/Petr';
import { GeoToken } from '../entity/GeoToken';
import { Region } from '../entity/Region';
import { Departement } from '../entity/Departement';
import devecoService from '../service/DevEcoService';
import territoireService, { GeographyTarget, GeographyType } from '../service/TerritoireService';
import TokenCheckMiddleware from '../middleware/TokenCheckMiddleware';
import { Nouveautes } from '../entity/Nouveautes';

const routerOpts: Router.IRouterOptions = {
	prefix: '/admin',
};

const router: Router = new Router(routerOpts);

const routes = {
	territoires: '/territoires',
	tags: '/territoires/:territoryId/tags',
	utilisateurs: '/utilisateurs',
	utilisateursCsv: '/utilisateurs/csv',
	recherchesCsv: '/recherches/csv',
	stats: '/stats',
	refreshTerritoriesStats: '/refreshTerritoriesStats',
	password: '/password',
	toggleUser: '/toggleUser',
	nouveautes: '/nouveautes/:token/:amount',
	retryImport: '/territoires/:territoryId/import',
};

const UpdateNouveautesMiddleware = TokenCheckMiddleware(
	process.env.NOUVEAUTES_UPDATE_TOKEN,
	'mise à jour du nombre de nouveautés'
);

router.param('token', UpdateNouveautesMiddleware);

router.get(
	routes.nouveautes,
	async (
		ctx: Koa.ParameterizedContext<Koa.DefaultState, Koa.DefaultContext, Record<never, never>>
	) => {
		const amount = Number(ctx.params.amount);

		if (Number.isNaN(amount)) {
			ctx.throw(`Wrong amount: ${amount}`);
		}

		await AppDataSource.getRepository(Nouveautes).insert({
			amount,
		});

		ctx.status = StatusCodes.OK;
	}
);

router.get(
	routes.retryImport,
	async (
		ctx: Koa.ParameterizedContext<Koa.DefaultState, Koa.DefaultContext, Record<never, never>>
	) => {
		const territoireId = Number(ctx.params.territoryId);

		territoireService.doImportStepsForTerritory(territoireId);

		ctx.body = {};
	}
);

router.use(AuthMiddleware);
router.use(RoleMiddleware('superadmin'));

const returnAllDevecos = async () => {
	const devecoRepository = AppDataSource.getRepository(DevEco);
	const devecos = await devecoRepository.find({
		relations: {
			account: true,
			territory: {
				territoryImportStatus: true,
			},
		},
	});

	const appUrl = process.env.APP_URL || '';
	const res = devecos.map((user) => ({
		...user.account,
		hasPassword: false,
		territoire: {
			id: user.territory.id,
			name: user.territory.name,
			territoryType: user.territory.territoryType,
			importInProgress: territoireService.isImportInProgress(user.territory.territoryImportStatus),
		},
		accessUrl: user.account.accessKey ? `${appUrl}/auth/jwt/${user.account.accessKey}` : '',
		welcomeEmailSent: user.welcomeEmailSent,
	}));

	return res;
};

const returnAllSearchRequests = async (): Promise<SearchRequest[]> => {
	const searchRequests = await AppDataSource.getRepository(SearchRequest).find({
		relations: {
			deveco: {
				account: true,
				territory: true,
			},
		},
	});

	return searchRequests;
};

type DevecoCsv = {
	email: string;
	nom?: string;
	prenom?: string;
	territoire: string;
	lastAuth: Date | null | undefined;
	lastLogin: Date | null | undefined;
};

router.get(routes.utilisateurs, async (ctx: DevecoContext) => {
	ctx.body = await returnAllDevecos();
});

router.get(routes.utilisateursCsv, async (ctx: DevecoContext) => {
	const devecos = await returnAllDevecos();

	const csvDevecos: DevecoCsv[] = devecos.map((deveco) => ({
		email: deveco.email,
		nom: deveco.nom,
		prenom: deveco.prenom,
		territoire: deveco.territoire.name,
		lastLogin: deveco.lastLogin,
		lastAuth: deveco.lastAuth,
	}));

	const header: { id: keyof DevecoCsv; title: string }[] = [
		{ id: 'email', title: 'Email' },
		{ id: 'nom', title: 'Nom' },
		{ id: 'prenom', title: 'Prénom' },
		{ id: 'territoire', title: 'Territoire' },
		{ id: 'lastAuth', title: 'Dernière action' },
		{ id: 'lastLogin', title: 'Dernière demande de login' },
	];
	const csvStringifier = createObjectCsvStringifier({ header });

	ctx.type = 'text/csv';
	ctx.body = csvStringifier.getHeaderString() + csvStringifier.stringifyRecords(csvDevecos);
});

router.get(routes.recherchesCsv, async (ctx: DevecoContext) => {
	const searchRequests = await returnAllSearchRequests();

	const queryParams: Set<string> = new Set();

	const csvSearchRequests = searchRequests.map((searchRequest) => {
		const query = searchRequest.query.replaceAll('%5B', '').replaceAll('%5D', '');

		const params = parse(query);

		Object.keys(params).forEach((key) => {
			queryParams.add(key);
		});

		return {
			...searchRequest,
			query: JSON.stringify(params),
			type: /createurs/.test(searchRequest.type) ? 'createurs' : 'etablissements',
			exported: /export/.test(searchRequest.type),
			date: searchRequest.createdAt,
			deveco: searchRequest.deveco.account.email,
			territoire: searchRequest.deveco.territory.name,
			...params,
		};
	});

	const header: { id: string; title: string }[] = [
		{ id: 'date', title: 'Date' },
		{ id: 'deveco', title: 'Deveco' },
		{ id: 'territoire', title: 'Territoire' },
		{ id: 'type', title: 'Type' },
		{ id: 'exported', title: 'Exporté' },
		...Array.from(queryParams).map((id) => ({ id, title: `Param. ${id}` })),
	];
	const csvStringifier = createObjectCsvStringifier({ header });

	const date = new Date();
	const filename = `deveco_recherches${date.getTime()}`;

	ctx.set('Content-disposition', `attachment; filename=${filename}.csv`);
	ctx.type = 'text/csv';
	ctx.body = csvStringifier.getHeaderString() + csvStringifier.stringifyRecords(csvSearchRequests);
});

const isCorrectTypeTerritoire = (s?: string): s is GeographyType => {
	return !!s && ['epci', 'commune', 'metropole', 'petr', 'departement', 'region'].includes(s);
};

const formatTerritory = (territoire: Territory) => {
	const type = territoireService.getGeographyType(territoire);

	return {
		id: territoire.id,
		name: territoire.name,
		territoryType: type,
		importInProgress: territoireService.isImportInProgress(territoire.territoryImportStatus),
		geographyId: `${territoire[type].id}`,
		geographyType: territoire.territoryType,
		geographyLibelle: territoireService.formatGeographyLibelle(territoire),
	};
};

router.get(routes.territoires, async (ctx: DevecoContext) => {
	const territoires = (
		await AppDataSource.getRepository(Territory).find({
			relations: {
				territoryImportStatus: true,
				commune: true,
				epci: true,
				metropole: true,
				petr: true,
				departement: true,
				region: true,
				perimetre: {
					communes: true,
					epcis: true,
					departements: true,
				},
			},
		})
	)
		.map(formatTerritory)
		.sort((a, b) => a.name.localeCompare(b.name));

	ctx.body = territoires;
});

router.post(routes.territoires, async (ctx: DevecoContext) => {
	const {
		type,
		name: nameRaw,
		geographyIds,
	}: {
		type: string;
		name: string;
		geographyIds: string[];
	} = ctx.request.body as any;

	const name = nameRaw?.trim();

	if (!name) {
		ctx.body = { error: `empty name: "${nameRaw}"` };
		return;
	}

	if (!isCorrectTypeTerritoire(type)) {
		ctx.body = { error: `bad territory type: ${type}` };
		return;
	}

	const territory = await AppDataSource.getRepository(Territory).findOne({
		where: {
			name,
		},
	});

	if (territory) {
		ctx.body = { error: 'Ce territoire existe déjà' };
		return;
	}

	let geographyTargets: GeographyTarget[] = [];

	if (type === 'epci') {
		geographyTargets = await AppDataSource.getRepository(Epci).find({
			where: {
				id: In(geographyIds),
			},
		});
	} else if (type === 'metropole') {
		geographyTargets = await AppDataSource.getRepository(Metropole).find({
			where: { id: In(geographyIds) },
		});
	} else if (type === 'region') {
		geographyTargets = await AppDataSource.getRepository(Region).find({
			where: { id: In(geographyIds) },
		});
	} else if (type === 'departement') {
		geographyTargets = await AppDataSource.getRepository(Departement).find({
			where: { id: In(geographyIds) },
		});
	} else if (type === 'petr') {
		geographyTargets = await AppDataSource.getRepository(Petr).find({
			where: { id: In(geographyIds) },
		});
	} else if (type === 'commune') {
		geographyTargets = await AppDataSource.getRepository(Commune).find({
			where: { id: In(geographyIds) },
		});
	}

	if (geographyTargets.length !== geographyIds.length) {
		ctx.body = { error: `Type de périmètre inconnu : ${type}` };
		return;
	}

	const territoryNew = await territoireService.createTerritoryForTerritoryTargets({
		name,
		geographyType: type,
		geographyTargets,
	});

	if (!territoryNew) {
		ctx.body = { error: 'Impossible de créer ce territoire' };
		return;
	}

	ctx.body = formatTerritory(territoryNew);
});

router.post(routes.utilisateurs, async (ctx: DevecoContext) => {
	const {
		nom,
		prenom,
		email,
		idTerritoire,
	}: {
		nom: string;
		prenom: string;
		email: string;
		idTerritoire: number;
	} = ctx.request.body as any;

	const existingAccount = await AppDataSource.getRepository(Account).findOne({ where: { email } });
	if (existingAccount) {
		ctx.body = { error: 'email exists' };
		return;
	}

	const territory = await AppDataSource.getRepository(Territory).findOne({
		where: {
			id: idTerritoire,
		},
		relations: {
			territoryImportStatus: true,
		},
	});

	if (!territory) {
		ctx.body = { error: "Ce territoire n'existe pas" };
		return;
	}

	const accessKey = uuidv4();

	// create Account and Deveco in database
	const account = await AppDataSource.getRepository(Account).save({
		email,
		nom,
		prenom,
		accountType: 'deveco',
		accessKey,
		accessKeyDate: new Date(),
	});

	const { identifiers } = await AppDataSource.getRepository(DevEco).insert({
		territory,
		account,
	});
	const token = uuidv4();

	const devecoId = identifiers[0]?.id;

	await AppDataSource.getRepository(GeoToken).insert({
		token,
		deveco: { id: devecoId },
	});

	const importInProgress = territoireService.isImportInProgress(territory.territoryImportStatus);

	if (!importInProgress) {
		const deveco = await AppDataSource.getRepository(DevEco).findOne({
			where: {
				id: devecoId,
			},
			relations: {
				account: true,
			},
		});

		if (!deveco) {
			return;
		}

		await devecoService.sendWelcomeEmailToDeveco(deveco);
	}

	ctx.body = await returnAllDevecos();
});

router.post(routes.password, async (ctx: DevecoContext) => {
	const { id, password }: { id: number; password: string } = (ctx.request.body as any) ?? {};

	if (!id || !password) {
		ctx.status = StatusCodes.BAD_REQUEST;
		ctx.body = {};
		return;
	}

	const account = await AppDataSource.getRepository(Account).findOneOrFail({ where: { id } });
	const currentPassword = await AppDataSource.getRepository(Password).findOneBy({
		account: { id: account.id },
	});

	if (currentPassword) {
		await AppDataSource.getRepository(Password).update(
			{ account: { id: account.id } },
			{ password }
		);
	} else {
		await AppDataSource.getRepository(Password).insert({ account, password });
	}

	ctx.body = {};
});

router.post(routes.toggleUser, async (ctx: DevecoContext) => {
	const { id, enabled }: { id: number; enabled: boolean } = (ctx.request.body as any) ?? {};

	if (!id || typeof enabled === 'undefined') {
		ctx.status = StatusCodes.BAD_REQUEST;
		ctx.body = {};
		return;
	}

	await AppDataSource.getRepository(Account).update(id, { enabled });

	ctx.body = await returnAllDevecos();
});

type TagsPayload = {
	activites: Tag[];
	zones: Tag[];
	mots: Tag[];
};

type Tag = {
	id: number;
	label: string;
	usages: number;
};

type EntiteTagType = 'activites_reelles' | 'entreprise_localisations' | 'mots_cles';
type EntiteTagTable = 'activite_entreprise' | 'localisation_entreprise' | 'mot_cle';

const tableForTag = (tag: EntiteTagType): EntiteTagTable => {
	switch (tag) {
		case 'activites_reelles':
			return 'activite_entreprise';
		case 'entreprise_localisations':
			return 'localisation_entreprise';
		case 'mots_cles':
			return 'mot_cle';
	}
};

export const getTerritoryTags = async (territoryId: number) => {
	const buildTagsQuery = (entiteField: EntiteTagType, tagTable: string, tagField: string) =>
		`
WITH id_tag(id, tag) AS (
	SELECT
		id,
		unnest(string_to_array(${entiteField}, ',')) AS tag
	FROM
		entite
	WHERE
		territoire_id = $1
)
SELECT
	s.id,
	s.${tagField} AS label,
	count(it.id)::INTEGER AS usages
FROM
	${tagTable} s
LEFT JOIN
	id_tag it ON s.${tagField} = it.tag
WHERE
	territory_id = $1
GROUP BY
	s.id, s.${tagField}
`;

	const activites: Tag[] = await AppDataSource.manager.query(
		buildTagsQuery('activites_reelles', 'activite_entreprise', 'activite'),
		[territoryId]
	);

	const zones: Tag[] = await AppDataSource.manager.query(
		buildTagsQuery('entreprise_localisations', 'localisation_entreprise', 'localisation'),
		[territoryId]
	);

	const mots: Tag[] = await AppDataSource.manager.query(
		buildTagsQuery('mots_cles', 'mot_cle', 'mot_cle'),
		[territoryId]
	);

	return { activites, zones, mots };
};

export const deleteTerritoryTags = async (
	territoryId: number,
	ctx: DevecoContext
): Promise<void> => {
	// TODO: escape tagLabel to handle SQL injection (ouch)
	const { tagType, tagLabel, id }: { tagType: unknown; tagLabel: string; id: string } =
		ctx.request.body;

	if (!tagLabel || typeof tagLabel !== 'string') {
		ctx.throw(StatusCodes.BAD_REQUEST, `Bad tag label ${tagLabel}`);
	}

	if (!validTagType(tagType)) {
		ctx.throw(StatusCodes.BAD_REQUEST, `Bad tag type ${tagType}`);
	}

	// delete from the "central" repo
	await AppDataSource.manager.query(
		`
				DELETE
				FROM
					${tableForTag(tagType)}
				WHERE
					territory_id = $1
				AND
					id = ${id}
			`,
		[territoryId]
	);

	// delete from each Entite
	await AppDataSource.manager.query(
		`
				UPDATE
					entite
				SET
					${tagType} = array_to_string(
						array_remove(
							string_to_array(entite."${tagType}", ','),
							$2
						),
						','
					)
				WHERE
					territoire_id = $1
			`,
		[territoryId, tagLabel]
	);
};

router.get(
	routes.tags,
	async (ctx: Koa.ParameterizedContext<Koa.DefaultState, Koa.DefaultContext, TagsPayload>) => {
		const territoryId = Number(ctx.params.territoryId);

		ctx.body = await getTerritoryTags(territoryId);
	}
);

router.post(
	routes.refreshTerritoriesStats,
	async (
		ctx: Koa.ParameterizedContext<Koa.DefaultState, Koa.DefaultContext, Record<never, never>>
	) => {
		territoireService.insertStats();

		ctx.body = {};
	}
);

const validTagType = (tagType: unknown): tagType is EntiteTagType => {
	if (typeof tagType !== 'string') {
		return false;
	}
	return ['activites_reelles', 'entreprise_localisations', 'mots_cles'].includes(tagType);
};

router.delete(
	routes.tags,
	async (ctx: Koa.ParameterizedContext<Koa.DefaultState, Koa.DefaultContext, TagsPayload>) => {
		const territoryId = Number(ctx.params.territoryId);

		await deleteTerritoryTags(territoryId, ctx);

		ctx.body = await getTerritoryTags(territoryId);
	}
);

export default router;
