import Router from 'koa-router';
import Excel, { Cell, Row, Worksheet } from 'exceljs';
import { StatusCodes } from 'http-status-codes';

import AuthMiddleware, { DevecoContext } from '../middleware/AuthMiddleware';
import AppDataSource from '../data-source';
import { DevEco } from '../entity/DevEco';
import { EventLog } from '../entity/EventLog';
import { Territory } from '../entity/Territory';
import { formatJSDateToPostgresDate, getMonthAgo, getNWeeksAgo } from '../lib/utils';

const routerOpts: Router.IRouterOptions = {
	prefix: '/export',
};

const router: Router = new Router(routerOpts);
router.use(AuthMiddleware);

const routes = {
	logs: '/logs',
};

enum DEVECO_TYPE {
	INACTIF = 'inactif',
	DEMISSIONAIRE = 'démissionnaire',
	TESTEUR = 'testeur',
	REGULIER = 'régulier',
}

type DEVECO_STATS = {
	devecoId: number;
	nom: string;
	email: string;
	territory: string;
	territory_id: number;
	w1: number;
	w2: number;
	w3: number;
	w4: number;
	m1: number;
	total: number;
	type_utilisateur: DEVECO_TYPE;
};

type TERRITORY_STATS = {
	territory: string;
	total_nb_deveco: number;
	nb_inactif: number;
	nb_demissionaire: number;
	nb_testeur: number;
	nb_regulier: number;
	total_actions: number;
	m1: number;
	w1: number;
	actions_PP?: number;
	actions_PM?: number;
	actions_LOCAL?: number;
};

type DevecoActivityRaw = {
	total_stats: string;
	m1: string;
	nom: string;
	email: string;
	deveco_id: number;
	territory: string;
	territory_id: number;
	w1: string;
	w2: string;
	w3: string;
	w4: string;
};

async function getDevecoActivity(
	today: string,
	w1: string,
	w2: string,
	w3: string,
	w4: string,
	m1: string
): Promise<DevecoActivityRaw[]> {
	return await AppDataSource.getRepository(DevEco)
		.createQueryBuilder('D')
		.select('D.id', 'deveco_id')
		.addSelect('min(territory.id)', 'territory_id')
		.addSelect('min(territory.name)', 'territory')
		.addSelect("COALESCE((min(account.prenom)),'') ||' ' || COALESCE((min(account.nom)),'')", 'nom')
		.addSelect('COUNT(logs.action)', 'total_stats')
		.addSelect("COALESCE((min(account.email)),'')", 'email')
		.addSelect((subQuery) => {
			return subQuery
				.select('count(L.action)')
				.from(EventLog, 'L')
				.where(`L.date >= '${w1}' and L.date < '${today}' and L.deveco_id = D.id`)
				.groupBy('L.deveco_id');
		}, 'w1')
		.addSelect((subQuery) => {
			return subQuery
				.select('count(L.action)')
				.from(EventLog, 'L')
				.where(`L.date >= '${w2}' and L.date < '${w1}' and L.deveco_id = D.id`)
				.groupBy('L.deveco_id');
		}, 'w2')
		.addSelect((subQuery) => {
			return subQuery
				.select('count(L.action)')
				.from(EventLog, 'L')
				.where(`L.date >= '${w3}' and L.date < '${w2}' and L.deveco_id = D.id`)
				.groupBy('L.deveco_id');
		}, 'w3')
		.addSelect((subQuery) => {
			return subQuery
				.select('count(L.action)')
				.from(EventLog, 'L')
				.where(`L.date >= '${w4}' and L.date < '${w3}' and L.deveco_id = D.id`)
				.groupBy('L.deveco_id');
		}, 'w4')
		.addSelect((subQuery) => {
			return subQuery
				.select('count(L.action)')
				.from(EventLog, 'L')
				.where(`L.date >= '${m1}' and L.date < '${today}' and L.deveco_id = D.id`)
				.groupBy('L.deveco_id');
		}, 'm1')
		.innerJoin('D.account', 'account')
		.leftJoin('D.territory', 'territory')
		.leftJoin('event_log', 'logs', 'D.id=logs.deveco_id')
		.groupBy('D.id')
		.orderBy('D.id', 'ASC')
		.getRawMany();
}

const activeForTheWeek = (weeklyActivity: number): boolean => weeklyActivity > 5;

const activeForFourWeeks = ({
	w1,
	w2,
	w3,
	w4,
}: {
	w1: number;
	w2: number;
	w3: number;
	w4: number;
}): boolean => {
	const [a1, a2, a3, a4] = [w1, w2, w3, w4].map(activeForTheWeek);

	return (a1 && !a2 && !a3 && !a4) || (a1 && a2 && !a3 && !a4) || (a1 && a2 && a3);
};

function getTypeUtilisateur({
	total,
	m1,
	w1,
	w2,
	w3,
	w4,
}: {
	total?: number;
	m1?: number;
	w1: number;
	w2: number;
	w3: number;
	w4: number;
}) {
	if (total && total > 2) {
		if (activeForFourWeeks({ w1, w2, w3, w4 })) {
			return DEVECO_TYPE.REGULIER;
		}
		if (total >= 2 && (m1 === undefined || m1 === 0)) {
			return DEVECO_TYPE.DEMISSIONAIRE;
		}
		return DEVECO_TYPE.TESTEUR;
	}
	return DEVECO_TYPE.INACTIF;
}

async function combineDevecoStats(): Promise<DEVECO_STATS[]> {
	const now = new Date();
	const oneWeekAgo = formatJSDateToPostgresDate(getNWeeksAgo(now, 1));
	const twoWeeksAgo = formatJSDateToPostgresDate(getNWeeksAgo(now, 2));
	const threeWeeksAgo = formatJSDateToPostgresDate(getNWeeksAgo(now, 3));
	const fourWeeksAgo = formatJSDateToPostgresDate(getNWeeksAgo(now, 4));
	const monthAgo = formatJSDateToPostgresDate(getMonthAgo(now));
	const today = formatJSDateToPostgresDate(now);

	const devecoActivity = await getDevecoActivity(
		today,
		oneWeekAgo,
		twoWeeksAgo,
		threeWeeksAgo,
		fourWeeksAgo,
		monthAgo
	);

	const devecoStats = devecoActivity.map((item: DevecoActivityRaw): DEVECO_STATS => {
		const total = item.total_stats ? parseInt(item.total_stats) : 0;
		const m1 = item.m1 ? parseInt(item.m1) : 0;
		const email = item.email;
		const nom = item.nom && item.nom.replace(/ /g, '') !== '' ? item.nom : '-';
		const w1 = item.w1 ? parseInt(item.w1) : 0;
		const w2 = item.w2 ? parseInt(item.w2) : 0;
		const w3 = item.w3 ? parseInt(item.w3) : 0;
		const w4 = item.w4 ? parseInt(item.w4) : 0;

		return {
			devecoId: item.deveco_id,
			email,
			nom,
			territory: item.territory,
			territory_id: item.territory_id,
			w1,
			w2,
			w3,
			w4,
			m1,
			total,
			type_utilisateur: getTypeUtilisateur({ total, m1, w1, w2, w3, w4 }),
		};
	});
	return devecoStats;
}

function combineTerritoryStats(
	deveco_page: DEVECO_STATS[],
	territoryPPPMLOCALstats: TerritoryStatsRaw[]
) {
	const result1: Record<number, TERRITORY_STATS> = {};
	deveco_page.forEach((item: DEVECO_STATS) => {
		if (Object.prototype.hasOwnProperty.call(result1, item.territory_id)) {
			const t = { ...result1[item.territory_id] };
			result1[item.territory_id] = {
				territory: t.territory,
				total_nb_deveco: t.total_nb_deveco + 1,
				nb_inactif: item.type_utilisateur === DEVECO_TYPE.INACTIF ? t.nb_inactif + 1 : t.nb_inactif,
				nb_demissionaire:
					item.type_utilisateur === DEVECO_TYPE.DEMISSIONAIRE
						? t.nb_demissionaire + 1
						: t.nb_demissionaire,
				nb_testeur: item.type_utilisateur === DEVECO_TYPE.TESTEUR ? t.nb_testeur + 1 : t.nb_testeur,
				nb_regulier:
					item.type_utilisateur === DEVECO_TYPE.REGULIER ? t.nb_regulier + 1 : t.nb_regulier,
				total_actions: t.total_actions + item.total,
				m1: t.m1 + item.m1,
				w1: t.w1 + item.w1,
			};
		} else {
			result1[item.territory_id] = {
				territory: item.territory,
				total_nb_deveco: 1,
				nb_inactif: item.type_utilisateur === DEVECO_TYPE.INACTIF ? 1 : 0,
				nb_demissionaire: item.type_utilisateur === DEVECO_TYPE.DEMISSIONAIRE ? 1 : 0,
				nb_testeur: item.type_utilisateur === DEVECO_TYPE.TESTEUR ? 1 : 0,
				nb_regulier: item.type_utilisateur === DEVECO_TYPE.REGULIER ? 1 : 0,
				total_actions: item.total,
				m1: item.m1,
				w1: item.w1,
			};
		}
	});

	const result: TERRITORY_STATS[] = [];

	territoryPPPMLOCALstats.forEach((item) => {
		const t = result1[item.tid];
		if (t) {
			result.push({
				...t,
				actions_PP: item.actions_PP ? item.actions_PP : 0,
				actions_PM: item.actions_PM ? item.actions_PM : 0,
				actions_LOCAL: item.actions_LOCAL ? item.actions_LOCAL : 0,
			});
		} else {
			result.push({
				territory: item.tname,
				total_nb_deveco: 0,
				nb_inactif: 0,
				nb_demissionaire: 0,
				nb_testeur: 0,
				nb_regulier: 0,
				total_actions: 0,
				m1: 0,
				w1: 0,
				actions_PP: 0,
				actions_PM: 0,
				actions_LOCAL: 0,
			});
		}
	});

	return result;
}

type TerritoryStatsRaw = {
	tid: number;
	tname: string;
	actions_PM: number;
	actions_PP: number;
	actions_LOCAL: number;
};

async function getTerritoryPPPMLOCALstats(): Promise<TerritoryStatsRaw[]> {
	return await AppDataSource.getRepository(Territory)
		.createQueryBuilder('T')
		.select('T.id', 'tid')
		.addSelect('T.name', 'tname')
		.addSelect((subQuery) => {
			return subQuery
				.select('count(L.entity_type)')
				.from(EventLog, 'L')
				.leftJoin('L.deveco', 'D')
				.where("L.entity_type='FICHE_PM' and D.territory_id = T.id")
				.groupBy('D.territory_id');
		}, 'actions_PM')
		.addSelect((subQuery) => {
			return subQuery
				.select('count(L.entity_type)')
				.from(EventLog, 'L')
				.leftJoin('L.deveco', 'D')
				.where("L.entity_type='FICHE_PP' and D.territory_id = T.id")
				.groupBy('D.territory_id');
		}, 'actions_PP')
		.addSelect((subQuery) => {
			return subQuery
				.select('count(L.entity_type)')
				.from(EventLog, 'L')
				.leftJoin('L.deveco', 'D')
				.where("L.entity_type='LOCAL' and D.territory_id = T.id")
				.groupBy('D.territory_id');
		}, 'actions_LOCAL')
		.getRawMany();
}

function applyStylesToWorksheet(w: Worksheet) {
	w.eachRow(function (Row: Row, rowNum: number) {
		Row.eachCell(function (Cell: Cell, cellNum: number) {
			if (rowNum === 1) {
				Cell.alignment = {
					vertical: 'middle',
					horizontal: 'center',
				};
			} else if (cellNum === 1) {
				Cell.alignment = {
					vertical: 'middle',
					horizontal: 'left',
				};
			} else {
				Cell.alignment = {
					vertical: 'middle',
					horizontal: 'right',
				};
			}
		});
	});
}

function createExcelFile(deveco_page: DEVECO_STATS[], territory_page: TERRITORY_STATS[]) {
	const workbook = new Excel.Workbook();
	const wsDeveco = workbook.addWorksheet('Deveco');
	const wsTerritory = workbook.addWorksheet('Territory');

	wsDeveco.columns = [
		{ header: 'Id', key: 'id', width: 5 },
		{ header: 'Email', key: 'email', width: 25 },
		{ header: 'Nom', key: 'nom', width: 25 },
		{ header: 'Territoire', key: 'territory', width: 25 },
		{ header: 'Nb actions S-4', key: 'w4', width: 15 },
		{ header: 'Nb actions S-3', key: 'w3', width: 15 },
		{ header: 'Nb actions S-2', key: 'w2', width: 15 },
		{ header: 'Nb actions S-1', key: 'w1', width: 15 },
		{ header: 'Total Nb action M-1', key: 'm1', width: 15 },
		{ header: 'Type utilisateur', key: 'type_utilisateur', width: 15 },
	];

	wsTerritory.columns = [
		{ header: 'Territoire', key: 'territory', width: 25 },
		{ header: 'Nb développeurs:', key: 'total_nb_deveco', width: 15 },
		{ header: 'Nb inactif', key: 'nb_inactif', width: 15 },
		{ header: 'Nb testeur', key: 'nb_testeur', width: 15 },
		{ header: 'Nb démissionnaire', key: 'nb_demissionaire', width: 15 },
		{ header: 'Nb réguliers', key: 'nb_regulier', width: 15 },
		{ header: 'Total Nb actions depuis début', key: 'total_actions', width: 15 },
		{ header: 'Total Nb actions M-1', key: 'm1', width: 15 },
		{ header: 'Total Nb actions S-1', key: 'w1', width: 15 },
		{ header: 'Nb actions fiches PM depuis le début', key: 'actions_PM', width: 15 },
		{ header: 'Nb actions fiches PP depuis le début', key: 'actions_PP', width: 15 },
		{ header: 'Nb actions fiches locaux depuis le début', key: 'actions_LOCAL', width: 15 },
	];

	wsDeveco.addRows(deveco_page);
	wsTerritory.addRows(territory_page);

	applyStylesToWorksheet(wsDeveco);
	applyStylesToWorksheet(wsTerritory);

	return workbook;
}

/* Si tu as fait plus de 5 actions par semaine pendant 4 semaines sur deveco, tu es actif */
export async function exportActivityLogs() {
	const deveco_page = await combineDevecoStats();
	const territoryPPPMLOCALstats = await getTerritoryPPPMLOCALstats();

	const territory_page = combineTerritoryStats(deveco_page, territoryPPPMLOCALstats);
	return createExcelFile(deveco_page, territory_page);
}

router.get(routes.logs, async (ctx: DevecoContext) => {
	const workbook = await exportActivityLogs();
	const date = new Date();
	const filename = `deveco_activity_export_${date.getTime()}`;
	ctx.set('Content-disposition', 'attachment; filename=' + filename + '.xlsx');
	ctx.set('Content-type', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
	ctx.status = StatusCodes.OK;
	await workbook.xlsx.write(ctx.res);
	ctx.res.end();
});

export default router;
