import { StatusCodes } from 'http-status-codes';
import { startOfDay, subDays } from 'date-fns';
import Router from 'koa-router';
import { IsNull, MoreThanOrEqual, Not } from 'typeorm';

import AppDataSource from '../data-source';
import { Account } from '../entity/Account';
import { EventLog, EventLogActionType, EventLogEntityType } from '../entity/EventLog';
import { DevEco } from '../entity/DevEco';
import { GeoToken } from '../entity/GeoToken';
import { Password } from '../entity/Password';
import { respondWithAccountCookie } from '../lib/cookie';
import AuthMiddleware, { DevecoContext } from '../middleware/AuthMiddleware';
import RoleMiddleware from '../middleware/RoleMiddleware';
import territoireService from '../service/TerritoireService';
import { Contact } from '../entity/Contact';
import { Entite } from '../entity/Entite';
import { Demande } from '../entity/Demande';
import { Fiche } from '../entity/Fiche';
import { Rappel } from '../entity/Rappel';
import { TerritoryEtablissementReference } from '../entity/TerritoryEtablissementRef';
import { Entreprise } from '../entity/Entreprise';
import { Nouveautes } from '../entity/Nouveautes';
import { Echange } from '../entity/Echange';
import { Territory } from '../entity/Territory';
import { displayFirstName } from '../lib/utils';
import { Etablissement } from '../entity/Etablissement';

const routerOpts: Router.IRouterOptions = {
	prefix: '/compte',
};

const router: Router = new Router(routerOpts);
router.use(AuthMiddleware);

const routes = {
	zonages: '/zonages',
	zonage: '/zonages/:zonageId',
	collaborateurs: '/collaborateurs',
	password: '/password',
	geotokens: '/geotokens',
	stats: '/stats',
	activite: '/stats/activite/:categorie',
};

router.post('/', async (ctx: DevecoContext) => {
	const {
		nom,
		prenom,
	}: {
		nom: string;
		prenom: string;
	} = ctx.request.body;

	const accountRepository = AppDataSource.getRepository(Account);

	await accountRepository.update(ctx.state.account.id, {
		nom,
		prenom,
	});

	const account = await accountRepository.findOneOrFail({ where: { id: ctx.state.account.id } });

	await respondWithAccountCookie({ account, ctx });
});

router.post(routes.password, async (ctx: DevecoContext) => {
	const account: Account = ctx.state.account as Account;
	const {
		password,
	}: {
		password: string;
	} = ctx.request.body ?? {};

	if (!password) {
		ctx.status = StatusCodes.BAD_REQUEST;
		ctx.body = {};
		return;
	}

	const currentPassword = await AppDataSource.getRepository(Password).findOneBy({
		account: { id: account.id },
	});

	if (currentPassword) {
		await AppDataSource.getRepository(Password).update(
			{ account: { id: account.id } },
			{ password }
		);
	} else {
		await AppDataSource.getRepository(Password).insert({ account, password });
	}

	await respondWithAccountCookie({ account, ctx });
});

router.use(RoleMiddleware('deveco'));

const getRappels =
	(type: 'particulier' | 'entreprise') =>
	(t: Territory['id'], d: Date, n: Date): Promise<[number, number, number]> => {
		const query = `
				SELECT sum(CASE WHEN rappel.created_at >= $2 THEN 1 ELSE 0 END)::int as nouveaux,
				sum(CASE WHEN rappel.date_rappel BETWEEN $2 AND $3 THEN 1 ELSE 0 END)::int as ouverts,
				sum(CASE WHEN rappel.date_cloture BETWEEN $2 AND $3 THEN 1 ELSE 0 END)::int as clotures
				FROM rappel
				INNER JOIN fiche ON fiche.id = rappel.fiche_id
				INNER JOIN entite ON entite.id = fiche.entite_id
				WHERE fiche.territoire_id = $1
				AND entite.${type}_id IS NOT NULL
				`;

		return AppDataSource.createQueryRunner()
			.query(query, [t, d, n])
			.then(
				(results) =>
					[results[0].nouveaux ?? 0, results[0].ouverts ?? 0, results[0].clotures ?? 0] as [
						number,
						number,
						number
					]
			);
	};

const getDemandes =
	(type: 'particulier' | 'entreprise') =>
	(t: Territory['id'], d: Date, n: Date): Promise<[number, { type: string }[], number]> => {
		const query = `
				SELECT sum(CASE WHEN demande.created_at >= $2 THEN 1 ELSE 0 END)::int as nouvelles,
				array_remove(array_agg(case when (demande.date_cloture IS NULL OR demande.date_cloture < $2) then demande.type_demande end), NULL) as ouvertes,
				sum(CASE WHEN demande.date_cloture BETWEEN $2 AND $3 THEN 1 ELSE 0 END)::int as cloturees
				FROM demande
				INNER JOIN fiche ON fiche.id = demande.fiche_id
				INNER JOIN entite ON entite.id = fiche.entite_id
				WHERE fiche.territoire_id = $1
				AND entite.${type}_id IS NOT NULL
				`;

		return AppDataSource.createQueryRunner()
			.query(query, [t, d, n])
			.then(
				(results) =>
					[
						results[0].nouvelles ?? 0,
						(results[0].ouvertes ?? []).map((t: string) => ({ type: t })),
						results[0].cloturees ?? 0,
					] as [number, { type: string }[], number]
			);
	};

router.get(routes.collaborateurs, async (ctx: DevecoContext) => {
	const account = ctx.state.account as Account;
	const { territory } = ctx.state.deveco as DevEco;

	const devecoRepository = AppDataSource.getRepository(DevEco);

	const devecos = await devecoRepository.find({
		where: {
			territory: {
				id: territory.id,
			},
		},
		relations: {
			account: true,
			territory: {
				territoryImportStatus: true,
			},
		},
	});

	const res = devecos
		.map((user) => ({
			...user.account,
			hasPassword: false,
			territoire: {
				...user.territory,
				importInProgress: territoireService.isImportInProgress(
					user.territory.territoryImportStatus
				),
			},
			welcomeEmailSent: user.welcomeEmailSent,
		}))
		.filter(({ email }) => email !== account.email);

	ctx.body = res;
});

type EtablissementsStats = {
	nouveaux: number;
	qpvs: number;
	exogenes: number;
};

type CreateursStats = {
	nouveaux: number;
	qpvs: number;
	transformes: number;
};

type RappelsStats = {
	nouveaux: number;
	expires: number;
	clotures: number;
};

type EchangesStats = {
	auteur: string;
	type: string;
};

type DemandesStats = {
	nouvelles: number;
	ouvertes: Array<{ type: string }>;
	cloturees: number;
};

type ActionsStats = {
	vues: number;
	qualifications: number;
	etiquetages: number;
};

type CreateursStatsPayload = {
	createurs: CreateursStats;
	rappels: RappelsStats;
	echanges: Array<EchangesStats>;
	demandes: DemandesStats;
	actions: ActionsStats;
};

const getCreateursStats = async ({
	territory,
	now,
	date,
}: {
	territory: Territory;
	now: Date;
	date: Date;
}): Promise<CreateursStatsPayload> => {
	const otherPromises: [Promise<Entite[]>, Promise<Echange[]>, Promise<number>, Promise<number>] = [
		AppDataSource.getRepository(Entite).find({
			where: {
				territoire: {
					id: territory.id,
				},
				createdAt: MoreThanOrEqual(date),
				particulier: Not(IsNull()),
			},
			relations: {
				particulier: {
					qpv: true,
				},
				etablissementCree: true,
			},
			select: {
				particulier: {
					qpv: {
						id: true,
					},
				},
				etablissementCree: {
					siret: true,
				},
			},
		}),

		AppDataSource.getRepository(Echange).find({
			where: {
				fiche: {
					entite: {
						particulier: Not(IsNull()),
					},
					territoire: {
						id: territory.id,
					},
				},
				dateEchange: MoreThanOrEqual(date),
			},
			relations: {
				createur: {
					account: true,
				},
			},
			select: {
				createur: {
					account: {
						prenom: true,
						nom: true,
					},
				},
				typeEchange: true,
			},
		}),

		AppDataSource.getRepository(EventLog).count({
			where: {
				action: EventLogActionType.VIEW,
				entityType: EventLogEntityType.PERSONNE_PHYSIQUE,
				date: MoreThanOrEqual(date),
				deveco: {
					territory: {
						id: territory.id,
					},
				},
			},
		}),

		AppDataSource.getRepository(EventLog).count({
			where: {
				action: EventLogActionType.UPDATE_QUALIFICATION,
				entityType: EventLogEntityType.PERSONNE_PHYSIQUE,
				date: MoreThanOrEqual(date),
				deveco: {
					territory: {
						id: territory.id,
					},
				},
			},
		}),
	];

	const [
		[nouveauxRappels, expiresRappels, cloturesRappels],
		[nouvellesDemandes, ouvertesDemandesTypes, clotureesDemandes],
		[nouveauxCreateurs, nouveauxEchanges, vues, etiquetages],
	] = await Promise.all([
		getRappels('particulier')(territory.id, date, now),
		getDemandes('particulier')(territory.id, date, now),
		Promise.all(otherPromises),
	]);

	const createurs = {
		nouveaux: nouveauxCreateurs.length,
		qpvs: nouveauxCreateurs.filter((e) => !!e.particulier?.qpv?.id).length,
		transformes: nouveauxCreateurs.filter((e) => !!e.etablissementCree?.siret).length,
	};

	const rappels = {
		nouveaux: nouveauxRappels,
		expires: expiresRappels,
		clotures: cloturesRappels,
	};

	const echanges = nouveauxEchanges.map((e) => ({
		auteur: displayFirstName(e.createur?.account),
		type: e.typeEchange,
	}));

	const demandes = {
		nouvelles: nouvellesDemandes,
		ouvertes: ouvertesDemandesTypes,
		cloturees: clotureesDemandes,
	};

	const actions = {
		vues,
		qualifications: 0,
		etiquetages,
	};

	return {
		createurs,
		rappels,
		echanges,
		demandes,
		actions,
	};
};

type EtablissementsStatsPayload = {
	etablissements: EtablissementsStats;
	rappels: RappelsStats;
	echanges: Array<EchangesStats>;
	demandes: DemandesStats;
	actions: ActionsStats;
};

const getEtablissementsStats = async ({
	territory,
	now,
	date,
}: {
	territory: Territory;
	now: Date;
	date: Date;
}): Promise<EtablissementsStatsPayload> => {
	const otherPromises: [
		Promise<TerritoryEtablissementReference[]>,
		Promise<Echange[]>,
		Promise<number>,
		Promise<number>,
		Promise<number>
	] = [
		AppDataSource.getRepository(TerritoryEtablissementReference).find({
			where: {
				territory: {
					id: territory.id,
				},
				dateCreation: MoreThanOrEqual(date),
			},
			relations: {
				qpv: true,
			},
			select: {
				qpv: {
					id: true,
				},
				exogene: true,
			},
		}),

		AppDataSource.getRepository(Echange).find({
			where: {
				fiche: {
					entite: {
						entreprise: Not(IsNull()),
					},
					territoire: {
						id: territory.id,
					},
				},
				dateEchange: MoreThanOrEqual(date),
			},
			relations: {
				createur: {
					account: true,
				},
			},
			select: {
				createur: {
					account: {
						prenom: true,
						nom: true,
					},
				},
				typeEchange: true,
			},
		}),

		AppDataSource.getRepository(EventLog).count({
			where: {
				action: EventLogActionType.VIEW,
				entityType: EventLogEntityType.PERSONNE_MORALE,
				date: MoreThanOrEqual(date),
				deveco: {
					territory: {
						id: territory.id,
					},
				},
			},
		}),

		AppDataSource.manager
			.query(
				`
			SELECT count(entite.id)::int
			FROM entite
			WHERE entite.territoire_id = $1
			AND entite.entreprise_id IS NOT NULL
			AND (
				EXISTS (
					SELECT event_log.id
					FROM event_log
					WHERE entity_id = entite.id
					AND event_log.action IN ('UPDATE_QUALIFICATION', 'CONTACT_QUALIFICATION')
					AND event_log.date > $2
					LIMIT 1
				)
				OR EXISTS (
					SELECT id
					FROM contact
					WHERE contact.entite_id = entite.id
					AND (
						contact.source IS NULL
						OR contact.source <> 'API'
					)
					AND contact.created_at >= $2
					LIMIT 1
				)
			)
			`,
				[territory.id, date]
			)
			.then((result) => result[0].count),

		AppDataSource.getRepository(EventLog).count({
			where: {
				action: EventLogActionType.UPDATE_QUALIFICATION,
				entityType: EventLogEntityType.PERSONNE_MORALE,
				date: MoreThanOrEqual(date),
				deveco: {
					territory: {
						id: territory.id,
					},
				},
			},
		}),
	];

	const [
		[nouveauxRappels, expiresRappels, cloturesRappels],
		[nouvellesDemandes, ouvertesDemandesTypes, clotureesDemandes],
		[nouveauxEtablissements, nouveauxEchanges, vues, qualifications, etiquetages],
	] = await Promise.all([
		getRappels('entreprise')(territory.id, date, now),
		getDemandes('entreprise')(territory.id, date, now),
		Promise.all(otherPromises),
	]);

	const etablissements = {
		nouveaux: nouveauxEtablissements.length,
		qpvs: nouveauxEtablissements.filter((e) => !!e.qpv?.id).length,
		exogenes: nouveauxEtablissements.filter((e) => e.exogene).length,
	};

	const rappels = {
		nouveaux: nouveauxRappels,
		expires: expiresRappels,
		clotures: cloturesRappels,
	};

	const echanges = nouveauxEchanges.map((e) => ({
		auteur: displayFirstName(e.createur?.account),
		type: e.typeEchange,
	}));

	const demandes = {
		nouvelles: nouvellesDemandes,
		ouvertes: ouvertesDemandesTypes,
		cloturees: clotureesDemandes,
	};

	const actions = {
		vues,
		qualifications,
		etiquetages,
	};

	return {
		etablissements,
		rappels,
		echanges,
		demandes,
		actions,
	};
};

router.get(routes.activite, async (ctx: DevecoContext) => {
	const categorie = ctx.params.categorie;

	const deveco = ctx.state.deveco as DevEco;

	const territory = deveco.territory;

	const now = new Date();
	const lastWeek = subDays(startOfDay(now), 7);

	if (categorie === 'createurs') {
		const createurs = await getCreateursStats({ territory, now, date: lastWeek });

		ctx.body = createurs;
		return;
	}

	const etablissements = await getEtablissementsStats({ territory, now, date: lastWeek });

	ctx.body = etablissements;
});

type EtablissementOuvertFerme = {
	siret: Etablissement['siret'];
	nomPublic: Etablissement['nomPublic'];
	enseigne: Etablissement['enseigne'];
	trancheEffectifs: Etablissement['trancheEffectifs'];
	ouvert: boolean;
	ferme: boolean;
};

const getEtablissementsOuvertsFermes = async (
	t: Territory['id'],
	d: Date
): Promise<[EtablissementOuvertFerme[], EtablissementOuvertFerme[]]> => {
	const query = `
		SELECT
			EL.siret,
			EL.nom_public as "nomPublic",
			EL.enseigne,
			EL.tranche_effectifs as "trancheEffectifs",
			EL.date_creation > $2 as ouvert,
			EL.date_fermeture > $2 as ferme
		FROM
			etablissement EL
		WHERE
		EL.siret IN (
			SELECT
				siret
			FROM
				territory_etablissement_reference TER
			WHERE
				TER.territory_id = $1
		)
		AND (
			EL.date_creation > $2
			OR EL.date_fermeture > $2
		)
	`;

	const results = await AppDataSource.createQueryRunner().query(query, [t, d]);

	return results.reduce(
		(
			[ouverts, fermes]: [EtablissementOuvertFerme[], EtablissementOuvertFerme[]],
			etablissement: EtablissementOuvertFerme
		) => {
			if (etablissement.ouvert) {
				ouverts.push(etablissement);
			}
			if (etablissement.ferme) {
				fermes.push(etablissement);
			}

			return [ouverts, fermes];
		},
		[[], []]
	);
};

router.get(routes.stats, async (ctx: DevecoContext) => {
	const deveco = ctx.state.deveco as DevEco;

	const territoire = deveco.territory;

	const now = new Date();
	const date = subDays(startOfDay(now), 5);

	const [
		nouveautes,
		[etablissementsOuverts, etablissementsFermes],
		[rappelsCrees, etablissementsEtiquetes, contacts, demandes],
	] = await Promise.all([
		AppDataSource.getRepository(Nouveautes)
			.find({
				order: {
					createdAt: {
						direction: 'DESC',
						nulls: 'LAST',
					},
				},
				take: 1,
			})
			.then((nouveautes) => nouveautes[0])
			.then((nouveaute) => nouveaute?.amount ?? 0),

		getEtablissementsOuvertsFermes(territoire.id, date),

		Promise.resolve().then(async () => {
			const rappelsCrees = await AppDataSource.getRepository(Rappel)
				.createQueryBuilder('rappel')
				// we need to cast to varchar because otherwise, the 'date' object ('YYYY-MM-DD') is selected as a UTC Date, which means the day will change depending on the client's timezone, which is not what we want.
				.select(
					'rappel.titre, rappel.date_rappel::varchar as "dateRappel", entreprise.siret, entreprise.nom'
				)
				.innerJoin(
					Fiche,
					'fiche',
					'rappel.fiche_id = fiche.id AND fiche.territoire_id = :territoryId',
					{ territoryId: territoire.id }
				)
				.innerJoin(Entite, 'entite', 'fiche.entite_id = entite.id')
				.innerJoin(Entreprise, 'entreprise', 'entreprise.siret = entite.entreprise_id')
				.where('rappel.created_at > :date', { date })
				.getRawMany()
				.then((results) => results);

			const etablissementsEtiquetes = await AppDataSource.getRepository(EventLog)
				.createQueryBuilder('event')
				.select('count(event.id)::int', 'count')
				.innerJoin(
					Entite,
					'entite',
					`event.entity_id = entite.id AND event.entity_type <> 'LOCAL' AND entite.territoire_id = :territoryId`,
					{ territoryId: territoire.id }
				)
				.where(`event.action = 'UPDATE_QUALIFICATION'`)
				.andWhere('event.date > :date', { date })
				.groupBy('event.entityId')
				.getRawMany()
				.then((results) => results.length);

			const contacts = await AppDataSource.getRepository(Contact)
				.createQueryBuilder('contact')
				.select('count(contact.id)::int', 'count')
				.innerJoin(
					Entite,
					'entite',
					'contact.entite_id = entite.id AND entite.territoire_id = :territoryId',
					{ territoryId: territoire.id }
				)
				.where(`contact.created_at > '${date.toISOString()}'`) // we use template strings because passing the Date does not seem to work, go figure
				.getRawOne()
				.then((results) => results.count);

			const demandes = await AppDataSource.getRepository(Demande)
				.createQueryBuilder('demande')
				.select('demande.id, demande.type_demande as "typeDemande"')
				.innerJoin(
					Fiche,
					'fiche',
					'demande.fiche_id = fiche.id AND fiche.territoire_id = :territoryId',
					{ territoryId: territoire.id }
				)
				.where('demande.created_at > :date', { date })
				.getRawMany();

			return [rappelsCrees, etablissementsEtiquetes, contacts, demandes];
		}),
	]);

	const res = {
		nouveautes,
		etablissementsOuverts,
		etablissementsFermes,
		rappelsCrees,
		etablissementsEtiquetes,
		contacts,
		demandes,
	};

	ctx.body = res;
});

router.get(routes.geotokens, async (ctx: DevecoContext) => {
	const deveco = ctx.state.deveco as DevEco;

	const tokens = await AppDataSource.getRepository(GeoToken).find({
		where: {
			deveco: {
				id: deveco.id,
			},
		},
	});

	ctx.body = tokens;
});

router.get(routes.zonages, async (ctx: DevecoContext) => {
	const deveco = ctx.state.deveco as DevEco;
	const territoire = deveco.territory;

	const zonages = await territoireService.getZonagesForTerritoire(territoire);

	ctx.body = zonages;
});

router.post(routes.zonages, async (ctx: DevecoContext) => {
	const deveco = ctx.state.deveco as DevEco;
	const territoire = deveco.territory;

	const {
		nom,
		contenu,
	}: {
		nom: string;
		contenu: string;
	} = ctx.request.body ?? {};

	if (!nom) {
		ctx.throw(StatusCodes.BAD_REQUEST, `nom est nécessaire`);
	}

	if (!contenu) {
		ctx.throw(StatusCodes.BAD_REQUEST, `contenu est nécessaire`);
	}

	let geometrySource;
	try {
		geometrySource = JSON.parse(contenu);
	} catch (e) {
		ctx.status = 400;
		ctx.body = { error: 'Invalid JSON' };
		return;
	}

	const feature = geometrySource.features[0];
	const polygon = feature.geometry.rings[0];
	const postgis = `MULTIPOLYGON(((${polygon
		.map(([lat, lon]: Array<string>) => `${lat} ${lon}`)
		.join(',')})))`;

	await AppDataSource.manager.query(
		`INSERT INTO zonage(nom, territoire_id, geometry_source) VALUES('${nom}', ${territoire.id}, '${postgis}');`
	);

	await AppDataSource.manager.query(
		`UPDATE zonage SET geometry = ST_Transform(zonage.geometry_source, 'EPSG:3949', 'EPSG:4326') WHERE geometry IS NULL;`
	);

	const zonages = await territoireService.getZonagesForTerritoire(territoire);

	ctx.body = zonages;

	AppDataSource.manager.query(
		`
		${territoireService.insertIntoTERZonage()}
		WHERE TER.territory_id = $1 AND Z.nom = $2;
`,
		[territoire.id, nom]
	);
});

router.delete(routes.zonage, async (ctx: DevecoContext) => {
	const deveco = ctx.state.deveco as DevEco;
	const territoire = deveco.territory;

	const zonageId = Number(ctx.params.zonageId);

	await territoireService.deleteZonageForTerritoire(territoire, zonageId);

	const zonages = await territoireService.getZonagesForTerritoire(territoire);

	ctx.body = zonages;
});

export default router;
