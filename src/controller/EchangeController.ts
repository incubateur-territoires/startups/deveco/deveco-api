import { StatusCodes } from 'http-status-codes';
import Router from 'koa-router';

import AuthMiddleware, { DevecoContext } from '../middleware/AuthMiddleware';
import RoleMiddleware from '../middleware/RoleMiddleware';
import { Echange } from '../entity/Echange';
import ficheService from '../service/FicheService';
import AppDataSource from '../data-source';
import eventLogService from '../service/EventLogService';
import { DevEco } from '../entity/DevEco';
import { EventLogActionType } from '../entity/EventLog';
import { getLogEntityType } from '../lib/utils';
import inseeService, { relationsForToView } from '../service/InseeService';
import { Entite } from '../entity/Entite';

const routerOpts: Router.IRouterOptions = {
	prefix: '/echanges',
};

const router: Router = new Router(routerOpts);
router.use(AuthMiddleware);
router.use(RoleMiddleware('deveco'));

const routes = {
	getOne: '/:id',
	modifier: '/:id/modifier',
	creer: '/creer',
};

// Create Echange
router.post(routes.creer, async (ctx: DevecoContext) => {
	const deveco = ctx.state.deveco as DevEco;

	const fiche = await ficheService.getOrCreateFicheFromRequest(ctx);

	if (fiche.entite.etablissementCree) {
		ctx.throw(
			StatusCodes.BAD_REQUEST,
			'Impossible de modifier un Créateur qui a créé un Établissement'
		);
	}

	const ficheId = fiche.id;

	const {
		echange: payload,
		from,
	}: {
		echange: {
			compteRendu: string;
			typeEchange: string;
			dateEchange: Date;
			themes: string[];
			titre: string;
		};
		from: string;
	} = ctx.request.body;
	const { compteRendu, dateEchange, typeEchange, themes, titre } = payload;

	const echange = {
		createur: deveco,
		titre,
		typeEchange,
		dateEchange,
		compteRendu,
		themes: themes || [],
		fiche,
		updatedAt: null,
	};
	await AppDataSource.getRepository(Echange).insert(echange);

	await ficheService.touchFiche({
		ficheId,
		auteur: deveco,
	});

	await eventLogService.addEventLog({
		action: EventLogActionType.UPDATE,
		deveco: deveco,
		entityType: getLogEntityType(fiche.entite),
		entityId: fiche.entite.id,
	});

	await ficheService.createDemandesIfNecessary(themes, ficheId);
	await ficheService.removeDemandesIfNecessary(ficheId);

	switch (from) {
		case 'entreprise': {
			const entite = await AppDataSource.getRepository(Entite).findOneOrFail({
				where: {
					fiche: { id: ficheId },
					territoire: { id: deveco.territory.id },
				},
				relations: relationsForToView,
			});

			const withFreres = true;
			const withHistory = true;

			ctx.body = await inseeService.augmentedEntrepriseFromEntite({
				ctx,
				deveco,
				entite,
				withFreres,
				withHistory,
			});

			break;
		}
		case 'createur':
			ctx.body = await ficheService.ficheToView({ id: ficheId });

			break;
		default:
			ctx.throw(StatusCodes.BAD_REQUEST, `Invalid context ${from}`);
	}
});

router.post(routes.modifier, async (ctx: DevecoContext) => {
	const deveco = ctx.state.deveco as DevEco;

	const id = parseInt(ctx.params.id);
	if (!id) {
		ctx.throw(StatusCodes.BAD_REQUEST, 'id is required');
	}

	const echangeRepository = AppDataSource.getRepository(Echange);
	const echange = await echangeRepository.findOne({
		where: {
			id,
			fiche: {
				territoire: { id: deveco.territory.id },
			},
		},
		relations: {
			fiche: {
				entite: true,
			},
		},
	});

	if (!echange) {
		ctx.throw(StatusCodes.BAD_REQUEST, `Échange #${id} not found`);
	}

	if (!echange.fiche) {
		ctx.throw(StatusCodes.BAD_REQUEST, `Échange #${id} without Fiche`);
	}

	if (echange.fiche.entite.etablissementCree) {
		ctx.throw(
			StatusCodes.BAD_REQUEST,
			'Impossible de modifier un Créateur qui a créé un Établissement'
		);
	}

	const {
		echange: echangePayload,
		from,
	}: {
		echange: Partial<Echange>;
		from: string;
	} = ctx.request.body;

	const ficheId = echange.fiche.id;

	const now = new Date();
	await echangeRepository.update(id, {
		...echangePayload,
		updatedAt: now,
		auteurModification: deveco,
	});

	if (echange.themes) {
		await ficheService.createDemandesIfNecessary(echange.themes, echange.fiche.id);
	}
	await ficheService.removeDemandesIfNecessary(ficheId);
	await ficheService.touchFiche({
		ficheId,
		auteur: deveco,
	});

	switch (from) {
		case 'entreprise': {
			const entite = await AppDataSource.getRepository(Entite).findOneOrFail({
				where: {
					fiche: { id: ficheId },
					territoire: { id: deveco.territory.id },
				},
				relations: relationsForToView,
			});

			const withFreres = true;
			const withHistory = true;

			ctx.body = await inseeService.augmentedEntrepriseFromEntite({
				ctx,
				deveco,
				entite,
				withFreres,
				withHistory,
			});

			break;
		}
		case 'createur':
			ctx.body = await ficheService.ficheToView({ id: ficheId });

			break;
		default:
			ctx.throw(StatusCodes.BAD_REQUEST, `Invalid context ${from}`);
	}

	await eventLogService.addEventLog({
		action: EventLogActionType.UPDATE,
		deveco,
		entityType: getLogEntityType(echange.fiche.entite),
		entityId: echange.fiche.entite.id,
	});
});

export default router;
