import { StatusCodes } from 'http-status-codes';
import Router from 'koa-router';

import AppDataSource from '../data-source';
import { Contact } from '../entity/Contact';
import { DevEco } from '../entity/DevEco';
import { EventLogActionType } from '../entity/EventLog';
import { Particulier } from '../entity/Particulier';
import { getLogEntityType } from '../lib/utils';
import AuthMiddleware, { DevecoContext } from '../middleware/AuthMiddleware';
import RoleMiddleware from '../middleware/RoleMiddleware';
import eventLogService from '../service/EventLogService';
import ficheService from '../service/FicheService';
import contactService from '../service/ContactService';
import entiteService from '../service/EntiteService';
import inseeService, { relationsForToView } from '../service/InseeService';
import { Entite } from '../entity/Entite';

const routerOpts: Router.IRouterOptions = {
	prefix: '/contacts',
};

const router: Router = new Router(routerOpts);
router.use(AuthMiddleware);
router.use(RoleMiddleware('deveco'));

const routes = {
	getOne: '/:id',
	creer: '/creer',
	modifier: '/:id/modifier',
};

router.post(routes.creer, async (ctx: DevecoContext) => {
	const deveco = ctx.state.deveco as DevEco;

	let entite = await entiteService.getOrCreateEntite(ctx);

	const {
		nom,
		prenom,
		fonction,
		telephone,
		email,
		dateDeNaissance,
	}: {
		nom: string;
		prenom: string;
		fonction: string;
		telephone: string;
		email: string;
		dateDeNaissance: string;
	} = ctx.request.body;

	await contactService.createContactFromParticulier({
		nom,
		prenom,
		email,
		telephone,
		dateDeNaissance,
		fonction,
		deveco,
		entite,
		source: null,
	});

	entite = await AppDataSource.getRepository(Entite).findOneOrFail({
		where: { id: entite.id },
		relations: relationsForToView,
	});

	await eventLogService.addEventLog({
		action: EventLogActionType.UPDATE,
		deveco,
		entityType: getLogEntityType(entite),
		entityId: entite.id,
	});

	ctx.status = StatusCodes.OK;

	const withFreres = true;
	const withHistory = true;

	ctx.body = await inseeService.augmentedEntrepriseFromEntite({
		ctx,
		deveco,
		entite,
		withFreres,
		withHistory,
	});
});

router.del(routes.getOne, async (ctx: DevecoContext) => {
	const deveco = ctx.state.deveco as DevEco;

	const id = parseInt(ctx.params.id);
	if (!id) {
		ctx.throw(StatusCodes.BAD_REQUEST, 'id is required');
	}

	const contact = await AppDataSource.getRepository(Contact).findOne({
		where: {
			id: id,
			entite: { territoire: { id: deveco.territory.id } },
		},
		relations: {
			particulier: {
				entite: true,
			},
			entite: {
				fiche: true,
				contacts: { particulier: true },
				entreprise: true,
			},
		},
	});

	if (!contact) {
		ctx.throw(StatusCodes.BAD_REQUEST, 'Contact not found');
	}

	const particulier = contact.particulier;
	let entite = contact.entite;

	await AppDataSource.getRepository(Contact).delete({
		id,
	});

	// if, after deleting the Contact, the Particulier will have no link to any Entite,
	// then it has no link to anything else and should be removed
	// TODO this will change if one Particulier can be linked to several Contacts
	if (!particulier.entite) {
		await AppDataSource.getRepository(Particulier).delete({
			id: particulier.id,
		});
	}

	entite = await AppDataSource.getRepository(Entite).findOneOrFail({
		where: { id: entite.id },
		relations: relationsForToView,
	});

	await eventLogService.addEventLog({
		action: EventLogActionType.UPDATE,
		deveco,
		entityType: getLogEntityType(entite),
		entityId: entite.id,
	});

	const fiche = contact.entite.fiche;
	if (fiche) {
		const ficheId = fiche.id;

		await ficheService.touchFiche({
			ficheId,
			auteur: deveco,
		});
	}

	ctx.status = StatusCodes.OK;

	const withFreres = true;
	const withHistory = true;

	ctx.body = await inseeService.augmentedEntrepriseFromEntite({
		ctx,
		deveco,
		entite,
		withFreres,
		withHistory,
	});
});

router.post(routes.modifier, async (ctx: DevecoContext) => {
	const {
		contact: contactPayload,
	}: {
		contact: {
			fonction: string;
			prenom: string;
			nom: string;
			email: string;
			telephone: string;
			dateDeNaissance: string | null;
		};
	} = ctx.request.body;
	const deveco = ctx.state.deveco as DevEco;
	const territoireId = deveco.territory.id;

	const id = parseInt(ctx.params.id);
	if (!id) {
		ctx.throw(StatusCodes.BAD_REQUEST, 'id is required');
	}

	const contact = await AppDataSource.getRepository(Contact).findOne({
		where: {
			id,
			entite: {
				territoire: {
					id: territoireId,
				},
			},
		},
		relations: {
			particulier: true,
			entite: {
				fiche: true,
			},
		},
	});

	if (!contact) {
		ctx.throw(StatusCodes.NOT_FOUND, `Pas de contact ${id} trouvé pour le territoire`);
	}

	const { fonction, ...particulier } = contactPayload;
	await AppDataSource.getRepository(Contact).update(id, { fonction, source: null });

	/* for some reason, updating using an object like `{ contact: { id } }` yields an error, so we need to retrieve the Particulier entity beforehand and use its id */
	const part = contact.particulier;
	if (part?.id) {
		await AppDataSource.getRepository(Particulier).update(part.id, particulier);
	}

	const entite = await AppDataSource.getRepository(Entite).findOneOrFail({
		where: {
			id: contact.entite.id,
		},
		relations: relationsForToView,
	});

	if (particulier.email !== part.email || particulier.telephone !== part.telephone) {
		await eventLogService.addEventLog({
			action: EventLogActionType.CONTACT_QUALIFICATION,
			deveco,
			entityType: getLogEntityType(entite),
			entityId: entite.id,
		});
	}

	await eventLogService.addEventLog({
		action: EventLogActionType.UPDATE,
		deveco,
		entityType: getLogEntityType(entite),
		entityId: entite.id,
	});

	const fiche = entite.fiche;
	if (fiche) {
		const ficheId = fiche.id;
		await ficheService.touchFiche({
			ficheId,
			auteur: deveco,
		});
	}

	ctx.status = StatusCodes.OK;

	const withFreres = true;
	const withHistory = true;

	ctx.body = await inseeService.augmentedEntrepriseFromEntite({
		ctx,
		deveco,
		entite,
		withFreres,
		withHistory,
	});
});

export default router;
