import { StatusCodes } from 'http-status-codes';
import Router from 'koa-router';

import AuthMiddleware, { DevecoContext } from '../middleware/AuthMiddleware';
import RoleMiddleware from '../middleware/RoleMiddleware';
import { Demande } from '../entity/Demande';
import ficheService from '../service/FicheService';
import AppDataSource from '../data-source';
import eventLogService from '../service/EventLogService';
import { DevEco } from '../entity/DevEco';
import { EventLogActionType } from '../entity/EventLog';
import { getLogEntityType } from '../lib/utils';
import { Entite } from '../entity/Entite';
import inseeService, { relationsForToView } from '../service/InseeService';

const routerOpts: Router.IRouterOptions = {
	prefix: '/demandes',
};

const router: Router = new Router(routerOpts);
router.use(AuthMiddleware);
router.use(RoleMiddleware('deveco'));

const routes = {
	cloturer: '/cloturer',
};

router.post(routes.cloturer, async (ctx: DevecoContext) => {
	const fiche = await ficheService.getOrCreateFicheFromRequest(ctx);

	if (fiche.entite.etablissementCree) {
		ctx.throw(
			StatusCodes.BAD_REQUEST,
			'Impossible de modifier un Créateur qui a créé un Établissement'
		);
	}

	const ficheId = fiche.id;

	const deveco = ctx.state.deveco as DevEco;

	const {
		demandeId,
		motif,
		from,
	}: {
		demandeId: number;
		motif: string;
		from: string;
	} = ctx.request.body;

	const demandeRepository = AppDataSource.getRepository(Demande);

	await demandeRepository.findOneOrFail({
		where: {
			id: demandeId,
			fiche: {
				id: ficheId,
			},
		},
	});

	await demandeRepository.update(demandeId, {
		motif,
		cloture: true,
		dateCloture: new Date().toISOString().slice(0, 10),
	});

	await ficheService.touchFiche({
		ficheId,
		auteur: deveco,
	});

	switch (from) {
		case 'entreprise': {
			const entite = await AppDataSource.getRepository(Entite).findOneOrFail({
				where: {
					fiche: { id: ficheId },
					territoire: { id: deveco.territory.id },
				},
				relations: relationsForToView,
			});

			const withFreres = true;
			const withHistory = true;

			ctx.body = await inseeService.augmentedEntrepriseFromEntite({
				ctx,
				deveco,
				entite,
				withFreres,
				withHistory,
			});

			break;
		}
		case 'createur':
			ctx.body = await ficheService.ficheToView({ id: ficheId });

			break;
		default:
			ctx.throw(StatusCodes.BAD_REQUEST, `Invalid context ${from}`);
	}

	// TODO: remove await?
	// same for Rappel / Echange / etc.
	await eventLogService.addEventLog({
		action: EventLogActionType.UPDATE,
		deveco,
		entityType: getLogEntityType(fiche.entite),
		entityId: fiche.entite.id,
	});
});

export default router;
