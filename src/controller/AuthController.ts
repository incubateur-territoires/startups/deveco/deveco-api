import { compareSync } from 'bcrypt';
import { StatusCodes } from 'http-status-codes';
import Router from 'koa-router';
import { ILike } from 'typeorm';
import { v4 as uuidv4 } from 'uuid';
import { intlFormat } from 'date-fns';

import AppDataSource from '../data-source';
import { Account } from '../entity/Account';
import { Login } from '../entity/Login';
import { Password } from '../entity/Password';
import { cookieName, respondWithAccountCookie, respondWithDeletedCookie } from '../lib/cookie';
import { send, isSmtp } from '../lib/emailing';
import { verifyJwt } from '../lib/getJwt';
import { DevecoContext } from '../middleware/AuthMiddleware';

const routerOpts: Router.IRouterOptions = {
	prefix: '/auth',
};

const router: Router = new Router(routerOpts);

router.post('/jwt/:accessKey', async (ctx: DevecoContext) => {
	const body = ctx.request.body as any;
	const redirectUrl = body?.redirectUrl ?? undefined;
	const accessKey = ctx.params.accessKey;
	const accountRepository = AppDataSource.getRepository(Account);

	const account = await accountRepository.findOneBy({
		accessKey,
	});

	if (!account) {
		ctx.status = StatusCodes.NOT_FOUND;
		ctx.body = {
			error: `no account for accessKey ${accessKey}`,
		};
		return;
	}

	if (!account.enabled) {
		ctx.status = StatusCodes.UNAUTHORIZED;
		ctx.body = {
			error: `account disabled for accessKey ${accessKey}`,
		};
		return;
	}

	const showTutorial = !account.lastLogin;

	accountRepository.update(account.id, {
		accessKey: null,
		accessKeyDate: null,
		lastLogin: new Date(),
	});
	await AppDataSource.getRepository(Login).insert({ account });

	await respondWithAccountCookie({ account, ctx, redirectUrl }, showTutorial);
});

router.get('/status', async (ctx: DevecoContext) => {
	const token = ctx.cookies.get(cookieName);
	if (token !== 'deleted') {
		const decoded = verifyJwt(token);
		if (decoded && typeof decoded !== 'string') {
			const accountId = decoded.id;

			const account = await AppDataSource.getRepository(Account).findOneOrFail({
				where: { id: accountId },
			});

			if (!account.enabled) {
				ctx.throw(401);
			}

			ctx.status = StatusCodes.OK;
			ctx.body = {
				token,
				user: decoded,
			};
		} else {
			ctx.status = StatusCodes.UNAUTHORIZED;
		}
	} else {
		ctx.status = StatusCodes.NOT_FOUND;
	}
});

router.post('/logout', async (ctx: DevecoContext) => {
	await respondWithDeletedCookie({ ctx });
});

router.post('/login', async (ctx: DevecoContext) => {
	const body = ctx.request.body as any;
	const email = (body?.email ?? '') as string;
	const redirectUrl = body?.redirectUrl ?? (undefined as string | undefined);

	if (!email) {
		ctx.status = StatusCodes.BAD_REQUEST;
		ctx.body = {
			error: 'email is required',
		};
		return;
	}

	const accountRepository = AppDataSource.getRepository(Account);

	const account = await accountRepository.findOne({
		where: {
			email: ILike(`%${email}%`),
		},
	});

	if (!account) {
		ctx.status = StatusCodes.NOT_FOUND;
		ctx.body = {
			error: `no account for email ${email}`,
		};
		return;
	}

	if (!account.enabled) {
		ctx.status = StatusCodes.UNAUTHORIZED;
		ctx.body = {
			error: `account has been disabled for email ${email}`,
		};
		return;
	}

	const password = body.password as string;

	if (password) {
		const { password: currentEncryptedPassword } =
			(await AppDataSource.getRepository(Password).findOneBy({
				account: { id: account.id },
			})) ?? {};

		if (!currentEncryptedPassword) {
			ctx.status = StatusCodes.UNAUTHORIZED;
			return;
		}

		if (!compareSync(password, currentEncryptedPassword)) {
			ctx.status = StatusCodes.UNAUTHORIZED;
			return;
		}

		ctx.status = StatusCodes.OK;
		await AppDataSource.getRepository(Login).insert({ account });

		await respondWithAccountCookie({ account, ctx, redirectUrl });
	} else {
		const accessKey = uuidv4();

		await accountRepository.update(account.id, {
			accessKey,
			accessKeyDate: new Date(),
		});

		const appUrl = process.env.APP_URL || '';

		const date = intlFormat(
			new Date(),
			{
				day: 'numeric',
				month: 'long',
				year: 'numeric',
				hour: 'numeric',
				minute: 'numeric',
			},
			{
				locale: 'fr-FR',
			}
		);
		const subject = `Accédez à votre espace Deveco [${date}]`;

		// send email
		if (isSmtp()) {
			send({
				options: {
					to: email,
					subject,
				},
				template: 'loginRequest',
				params: {
					url: {
						accessKey,
						appUrl,
						redirectUrl,
					},
					account,
				},
			}).catch((emailError) => {
				console.error(emailError);
			});
		} else {
			console.info('No SMTP config found');
		}

		const accessUrl = process.env.SANDBOX_LOGIN
			? `/auth/jwt/${accessKey}${redirectUrl ? `?url=${redirectUrl}` : ''}`
			: '';
		ctx.status = StatusCodes.OK;
		ctx.body = {
			email,
			accessUrl,
		};
	}
});

export default router;
