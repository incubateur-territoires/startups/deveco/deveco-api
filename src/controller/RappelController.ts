import { StatusCodes } from 'http-status-codes';
import Router from 'koa-router';
import { DeepPartial, In } from 'typeorm';

import AuthMiddleware, { DevecoContext } from '../middleware/AuthMiddleware';
import RoleMiddleware from '../middleware/RoleMiddleware';
import { DevEco } from '../entity/DevEco';
import { Rappel } from '../entity/Rappel';
import { Fiche } from '../entity/Fiche';
import ficheService from '../service/FicheService';
import eventLogService from '../service/EventLogService';
import AppDataSource from '../data-source';
import { getLogEntityType } from '../lib/utils';
import { EventLogActionType } from '../entity/EventLog';
import inseeService, { relationsForToView } from '../service/InseeService';
import { Entite } from '../entity/Entite';

const routerOpts: Router.IRouterOptions = {
	prefix: '/rappels',
};

const router: Router = new Router(routerOpts);
router.use(AuthMiddleware);
router.use(RoleMiddleware('deveco'));

const routes = {
	getAll: '/',
	getOne: '/:id',
	modifier: '/:id/modifier',
	creer: '/creer',
};

async function getRappelsForDeveco(deveco: DevEco): Promise<{ rappel: Rappel; fiche: Fiche }[]> {
	const territoire = deveco.territory;

	const entityManager = AppDataSource.manager;
	const result: { fiche_id: number; rappel_id: number }[] = await entityManager.query(
		`SELECT rappel.id as rappel_id, fiche.id as fiche_id FROM "rappel" JOIN "fiche" ON fiche.id = rappel.fiche_id WHERE fiche.territoire_id = $1`,
		[territoire.id]
	);

	const ficheIds = result.map(({ fiche_id }) => fiche_id);
	const fiches = await AppDataSource.getRepository(Fiche).find({
		where: { id: In(ficheIds) },
		relations: {
			entite: {
				particulier: true,
				entreprise: true,
			},
		},
	});
	const fichesById = fiches.reduce(
		(acc, fiche) => ({ ...acc, [fiche.id || '']: fiche }),
		<Record<number, Fiche>>{}
	);
	const rappelIds = result.map(({ rappel_id }) => rappel_id);
	const rappels = await AppDataSource.getRepository(Rappel).findBy({
		id: In(rappelIds),
	});
	const rappelsById = rappels.reduce(
		(acc, rappel) => ({ ...acc, [rappel.id || '']: rappel }),
		<Record<number, Rappel>>{}
	);

	const response = result.map(({ rappel_id, fiche_id }) => ({
		rappel: rappelsById[rappel_id],
		fiche: fichesById[fiche_id],
	}));

	return response;
}

async function ensureRappelIsInDevecoTerritory({
	rappelId,
	deveco,
	ctx,
}: {
	rappelId: number;
	deveco: DevEco;
	ctx: DevecoContext;
}): Promise<Rappel> {
	const rappelRepository = await AppDataSource.getRepository(Rappel);

	const rap = await rappelRepository.findOneOrFail({
		where: { id: rappelId },
		relations: {
			fiche: {
				territoire: true,
				entite: {
					entreprise: true,
					particulier: true,
				},
			},
		},
	});

	if (rap.fiche && rap.fiche.territoire.id !== deveco.territory.id) {
		ctx.throw(StatusCodes.BAD_REQUEST, 'fiche is not in your territory');
	}

	return rap;
}

router.get(routes.getAll, async (ctx: DevecoContext) => {
	const deveco = ctx.state.deveco as DevEco;
	ctx.body = await getRappelsForDeveco(deveco);
});

router.post(routes.creer, async (ctx: DevecoContext) => {
	const fiche = await ficheService.getOrCreateFicheFromRequest(ctx);

	if (fiche.entite.etablissementCree) {
		ctx.throw(
			StatusCodes.BAD_REQUEST,
			'Impossible de modifier un Créateur qui a créé un Établissement'
		);
	}

	const ficheId = fiche.id;

	const deveco = ctx.state.deveco as DevEco;
	const {
		rappel,
		from,
	}: {
		rappel: {
			dateRappel: Date;
			titre: string;
		};
		from: string;
	} = ctx.request.body;

	const { dateRappel, titre } = rappel;

	const rap: Rappel = AppDataSource.getRepository(Rappel).create({
		createur: deveco,
		titre,
		dateRappel,
		fiche,
	});

	await AppDataSource.getRepository(Rappel).save(rap);

	await ficheService.touchFiche({
		ficheId,
		auteur: deveco,
	});

	switch (from) {
		case 'entreprise': {
			const entite = await AppDataSource.getRepository(Entite).findOneOrFail({
				where: {
					fiche: { id: ficheId },
					territoire: { id: deveco.territory.id },
				},
				relations: relationsForToView,
			});

			const withFreres = true;
			const withHistory = true;

			ctx.body = await inseeService.augmentedEntrepriseFromEntite({
				ctx,
				deveco,
				entite,
				withFreres,
				withHistory,
			});

			break;
		}
		case 'createur':
			ctx.body = await ficheService.ficheToView({ id: ficheId });

			break;
		case 'actions':
			ctx.body = await getRappelsForDeveco(deveco);

			break;
		default:
			ctx.throw(StatusCodes.BAD_REQUEST, `Invalid context ${from}`);
	}

	await eventLogService.addEventLog({
		action: EventLogActionType.UPDATE,
		deveco: deveco,
		entityType: getLogEntityType(fiche.entite),
		entityId: fiche.entite.id,
	});
});

router.post(routes.modifier, async (ctx: DevecoContext) => {
	const deveco = ctx.state.deveco as DevEco;

	const id = parseInt(ctx.params.id);
	if (!id) {
		ctx.throw(StatusCodes.BAD_REQUEST, 'id is required');
	}

	const {
		rappel,
		from,
	}: {
		rappel: DeepPartial<Rappel>;
		from: string;
	} = ctx.request.body;

	if (rappel.dateCloture) {
		rappel.auteurCloture = deveco;
	} else {
		rappel.auteurCloture = undefined;
	}

	const rap = await ensureRappelIsInDevecoTerritory({ rappelId: id, deveco, ctx });

	const fiche = rap.fiche;

	if (!fiche) {
		ctx.throw(StatusCodes.NOT_FOUND, `No Fiche for Rappel ${id}`);
	}

	if (fiche.entite.etablissementCree) {
		ctx.throw(
			StatusCodes.BAD_REQUEST,
			'Impossible de modifier un Créateur qui a créé un Établissement'
		);
	}

	const ficheId = fiche.id;

	await AppDataSource.getRepository(Rappel).update(id, rappel);

	await ficheService.touchFiche({
		ficheId,
		auteur: deveco,
	});

	switch (from) {
		case 'entreprise': {
			const entite = await AppDataSource.getRepository(Entite).findOneOrFail({
				where: {
					fiche: { id: ficheId },
					territoire: { id: deveco.territory.id },
				},
				relations: relationsForToView,
			});

			const withFreres = true;
			const withHistory = true;

			ctx.body = await inseeService.augmentedEntrepriseFromEntite({
				ctx,
				deveco,
				entite,
				withFreres,
				withHistory,
			});

			break;
		}
		case 'createur':
			ctx.body = await ficheService.ficheToView({ id: ficheId });

			break;
		case 'actions':
			ctx.body = await getRappelsForDeveco(deveco);

			break;
		default:
			ctx.throw(StatusCodes.BAD_REQUEST, `Invalid context ${from}`);
	}

	await eventLogService.addEventLog({
		action: EventLogActionType.UPDATE,
		deveco,
		entityType: getLogEntityType(fiche.entite),
		entityId: fiche.entite.id,
	});
});

export default router;
