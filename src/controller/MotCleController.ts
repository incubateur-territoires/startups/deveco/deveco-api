import Router from 'koa-router';
import { ILike } from 'typeorm';

import { MotCle } from '../entity/MotCle';
import { DevEco } from '../entity/DevEco';
import AuthMiddleware, { DevecoContext } from '../middleware/AuthMiddleware';
import RoleMiddleware from '../middleware/RoleMiddleware';
import AppDataSource from '../data-source';

const routerOpts: Router.IRouterOptions = {
	prefix: '/mots',
};

const router: Router = new Router(routerOpts);
router.use(AuthMiddleware);
router.use(RoleMiddleware('deveco'));

router.get('/', async (ctx: DevecoContext) => {
	const { territory } = ctx.state.deveco as DevEco;
	const { search }: { search: string } = ctx.query as any;

	const motsCles = await AppDataSource.getRepository(MotCle).find({
		order: { motCle: 'ASC' },
		where: {
			territory: { id: territory.id },
			motCle: ILike(`%${search}%`),
		},
		take: 5,
	});

	ctx.body = motsCles;
});

export default router;
