import Router from 'koa-router';
import { ObjectLiteral, SelectQueryBuilder } from 'typeorm';

import { getPeriodStart } from '../lib/utils';
import AppDataSource from '../data-source';
import { Account } from '../entity/Account';
import { EventLog } from '../entity/EventLog';
import { Demande } from '../entity/Demande';
import { DevEco } from '../entity/DevEco';
import { Echange } from '../entity/Echange';
import { Fiche } from '../entity/Fiche';
import { Territory } from '../entity/Territory';
import AuthMiddleware, { DevecoContext } from '../middleware/AuthMiddleware';
import territoireService from '../service/TerritoireService';

const prefix = '/stats';
const routerOpts: Router.IRouterOptions = {
	prefix,
};

const router: Router = new Router(routerOpts);

type StatItem = { label: string; count: number };

const aggregateStats = async (
	codesCommunes: string[]
): Promise<{ key: string; values: StatItem[] }[]> => {
	let where = '';

	if (codesCommunes.length) {
		const codesCommunesValues = codesCommunes.map((c) => `'${c}'`).join(', ');

		where = `WHERE stats.code_commune IN (${codesCommunesValues})`;
	}

	return AppDataSource.manager.query(`
		SELECT
			key,
			json_agg(
					json_build_object('label', label, 'count', count)
					ORDER BY count desc
			) AS values
		FROM
			(
				SELECT
					key,
					elements ->> 'label' AS label,
					sum((elements ->> 'count') :: int) AS count
				FROM
					(
						SELECT
							code_commune,
							key,
							jsonb_array_elements(value) AS elements
						FROM
							(
								SELECT
									DISTINCT ON (stats.code_commune, communes_keys_values.key)
									stats.code_commune,
									communes_keys_values.*
								FROM
									stats,
									jsonb_each(stats.content) AS communes_keys_values
								${where}
								ORDER BY
									stats.code_commune, communes_keys_values.key, stats.date DESC
							) communes_keys_values
					) communes_keys_elements
				GROUP BY
					key,
					label
			) keys_labels_counts
		GROUP BY key;
	`);
};

const routes = {
	getAll: '/',
	analyse: '/analyse',
};

router.get(routes.analyse, AuthMiddleware, async (ctx: DevecoContext) => {
	const deveco = ctx.state.deveco as DevEco;

	let codesCommunes = (ctx.query.communes as string[]) ?? [];

	const territory = deveco.territory;

	const codesCommunesTerritoire = (
		await territoireService.fetchCommunesForTerritoire(territory)
	).map(({ id }) => id);

	if (!codesCommunes.length) {
		codesCommunes = codesCommunesTerritoire;
	} else {
		// filter out Communes which are not in the Deveco's Territory
		codesCommunes = codesCommunes.filter((codeCommune) =>
			codesCommunesTerritoire.includes(codeCommune)
		);
	}

	// aggregate results by key (nafs, etablissements, ...)
	const results = await aggregateStats(codesCommunes);

	ctx.body = ['etablissements', 'nafs', 'qpvs', 'effectifs', 'categories_juridiques'].reduce(
		(acc: Record<string, StatItem[]>, key) => {
			const found = results.find(({ key: k }) => k === key);

			acc[key] = found ? found.values : [];

			return acc;
		},
		{}
	);
});

router.get(routes.getAll, async (ctx: DevecoContext) => {
	const { periode, type }: { periode: string; type: string } = ctx.query as any;

	const periodStart = getPeriodStart(periode);

	// https://github.com/typeorm/typeorm/pull/9357
	const periodFilter =
		(key: string) =>
		<T extends ObjectLiteral>(query: SelectQueryBuilder<T>) =>
			query.andWhere(`${key} >= :periodStart`, { key, periodStart });

	let territoireFilter = <T extends ObjectLiteral>(query: SelectQueryBuilder<T>) => query;
	if (type && !Array.isArray(type)) {
		if (type === 'epci') {
			territoireFilter = <T extends ObjectLiteral>(query: SelectQueryBuilder<T>) =>
				query.andWhere('territoire.epci_id IS NOT NULL');
		}
		if (type === 'commune') {
			territoireFilter = <T extends ObjectLiteral>(query: SelectQueryBuilder<T>) =>
				query.andWhere('territoire.commune_id IS NOT NULL');
		}
	}

	/***************************/
	/* Requêtes sur l'ensemble */
	/***************************/
	const baseTerritoryQuery = AppDataSource.getRepository(Territory)
		.createQueryBuilder('territoire')
		.innerJoin(DevEco, 'deveco', 'deveco.territory_id = territoire.id')
		.innerJoin(Account, 'account', 'deveco.account_id = account.id')
		.where('account.lastLogin IS NOT NULL');
	const territoiresTotal = await baseTerritoryQuery.clone().getCount();

	const territoiresNouveauxQuery = baseTerritoryQuery.clone();
	periodFilter('territoire.createdAt')(territoiresNouveauxQuery);
	const territoiresNouveaux = await territoiresNouveauxQuery.getCount();

	const baseDevecoQuery = AppDataSource.getRepository(DevEco)
		.createQueryBuilder('deveco')
		.leftJoin('deveco.account', 'account')
		.where('account.lastLogin IS NOT NULL');

	const devecosTotal = await baseDevecoQuery.clone().getCount();

	const devecosNouveauxQuery = baseDevecoQuery.clone();
	periodFilter('deveco.createdAt')(devecosNouveauxQuery);
	const devecosNouveaux = await devecosNouveauxQuery.getCount();

	const devecosActifsQuery = baseDevecoQuery
		.clone()
		.leftJoinAndSelect('deveco.territory', 'territoire')
		.leftJoin(EventLog, 'eventLog', 'eventLog.deveco_id = deveco.id');
	periodFilter('eventLog.date')(devecosActifsQuery);
	const devecosActifs = await devecosActifsQuery.clone().getCount();

	const territoiresActifsArray = (await devecosActifsQuery.clone().getMany())
		.map((d) => {
			return d.territory.id;
		})
		.reduce(
			(acc, territoryId) => ({
				...acc,
				[territoryId]: '',
			}),
			{}
		);
	const territoiresActifs = Object.keys(territoiresActifsArray).length;

	const devecos = { total: devecosTotal, nouveaux: devecosNouveaux, actifs: devecosActifs };
	const territoires = {
		total: territoiresTotal,
		nouveaux: territoiresNouveaux,
		actifs: territoiresActifs,
	};

	/**************************/
	/* Requêtes sur les zones */
	/**************************/

	const devecosTotalZoneQuery = baseDevecoQuery.clone().leftJoin('deveco.territory', 'territoire');
	territoireFilter(devecosTotalZoneQuery);
	const devecosTotalZone = await devecosTotalZoneQuery.getCount();

	const devecosNouveauxZoneQuery = baseDevecoQuery
		.clone()
		.leftJoin('deveco.territory', 'territoire');
	periodFilter('deveco.createdAt')(devecosNouveauxZoneQuery);
	territoireFilter(devecosNouveauxZoneQuery);
	const devecosNouveauxZone = await devecosNouveauxZoneQuery.getCount();

	const devecosActifsZoneQuery = baseDevecoQuery
		.clone()
		.leftJoin('deveco.territory', 'territoire')
		.leftJoin(EventLog, 'eventLog', 'eventLog.deveco_id = deveco.id');
	territoireFilter(devecosActifsZoneQuery);
	periodFilter('eventLog.date')(devecosActifsZoneQuery);
	periodFilter('account.lastLogin')(devecosActifsZoneQuery);
	const devecosActifsZone = await devecosActifsZoneQuery.getCount();

	const fichesTotalZoneQuery = AppDataSource.getRepository(Fiche)
		.createQueryBuilder('fiche')
		.leftJoin('fiche.territoire', 'territoire');
	territoireFilter(fichesTotalZoneQuery);
	const fichesTotalZone = await fichesTotalZoneQuery.getCount();

	const fichesNouvellesZoneQuery = AppDataSource.getRepository(Fiche)
		.createQueryBuilder('fiche')
		.leftJoin('fiche.territoire', 'territoire');
	territoireFilter(fichesNouvellesZoneQuery);
	periodFilter('fiche.createdAt')(fichesNouvellesZoneQuery);
	const fichesNouvellesZone = await fichesNouvellesZoneQuery.getCount();

	const fichesModifieesZoneQuery = AppDataSource.getRepository(Fiche)
		.createQueryBuilder('fiche')
		.andWhere("fiche.updated_at >= (fiche.created_at + '1 second'::interval)")
		.leftJoin('fiche.territoire', 'territoire');
	territoireFilter(fichesModifieesZoneQuery);
	periodFilter('fiche.updated_at')(fichesModifieesZoneQuery);
	const fichesModifieesZone = await fichesModifieesZoneQuery.getCount();

	const echangesTotalZoneQuery = AppDataSource.getRepository(Echange)
		.createQueryBuilder('echange')
		.leftJoin('echange.fiche', 'fiche')
		.leftJoin('fiche.territoire', 'territoire');
	territoireFilter(echangesTotalZoneQuery);
	const echangesTotalZone = await echangesTotalZoneQuery.getCount();

	const echangesNouveauxZoneQuery = AppDataSource.getRepository(Echange)
		.createQueryBuilder('echange')
		.leftJoin('echange.fiche', 'fiche')
		.leftJoin('fiche.territoire', 'territoire');
	territoireFilter(echangesNouveauxZoneQuery);
	periodFilter('echange.createdAt')(echangesNouveauxZoneQuery);
	const echangesNouveauxZone = await echangesNouveauxZoneQuery.getCount();

	const demandesTotalZoneQuery = AppDataSource.getRepository(Demande)
		.createQueryBuilder('demande')
		.leftJoin('demande.fiche', 'fiche')
		.leftJoin('fiche.territoire', 'territoire');
	territoireFilter(demandesTotalZoneQuery);
	const demandesTotalZone = await demandesTotalZoneQuery.getCount();

	const demandesNouvellesZoneQuery = AppDataSource.getRepository(Demande)
		.createQueryBuilder('demande')
		.leftJoin('demande.fiche', 'fiche')
		.leftJoin('fiche.territoire', 'territoire');
	territoireFilter(demandesNouvellesZoneQuery);
	periodFilter('demande.createdAt')(demandesNouvellesZoneQuery);
	const demandesNouvellesZone = await demandesNouvellesZoneQuery.getCount();

	const demandesFermeesZoneQuery = AppDataSource.getRepository(Demande)
		.createQueryBuilder('demande')
		.leftJoin('demande.fiche', 'fiche')
		.leftJoin('fiche.territoire', 'territoire');
	territoireFilter(demandesFermeesZoneQuery);
	periodFilter('demande.dateCloture')(demandesFermeesZoneQuery);
	const demandesFermeesZone = await demandesFermeesZoneQuery.getCount();

	const zone = {
		devecos: { total: devecosTotalZone, nouveaux: devecosNouveauxZone, actifs: devecosActifsZone },
		fiches: {
			total: fichesTotalZone,
			nouvelles: fichesNouvellesZone,
			modifiees: fichesModifieesZone,
		},
		echanges: { total: echangesTotalZone, nouveaux: echangesNouveauxZone },
		demandes: {
			total: demandesTotalZone,
			nouvelles: demandesNouvellesZone,
			fermees: demandesFermeesZone,
		},
	};
	ctx.body = { territoires, devecos, zone };
});

export default router;
