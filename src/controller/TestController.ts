import Router from 'koa-router';
import * as Koa from 'koa';
import { Repository } from 'typeorm';
import { StatusCodes } from 'http-status-codes';

import * as path from 'path';
import { ParsedUrlQuery } from 'querystring';
import { readFileSync } from 'fs';

import { Fiche } from '../entity/Fiche';
import { Account } from '../entity/Account';
import { Particulier } from '../entity/Particulier';
import { Entreprise } from '../entity/Entreprise';
import { DevEco } from '../entity/DevEco';
import { Territory } from '../entity/Territory';
import { Password } from '../entity/Password';
import AppDataSource from '../data-source';
import { DevecoContext } from '../middleware/AuthMiddleware';

const preSeedSql = '../../data/seed/pre-init.sql';
const seedSql = '../../data/seed/init.sql';

const routerOpts: Router.IRouterOptions = {
	prefix: '/test',
};

const TestMiddleware = async (ctx: DevecoContext, next: (ctx: Koa.Context) => Promise<unknown>) => {
	if (process.env.NODE_ENV !== 'test') {
		ctx.throw(StatusCodes.UNAUTHORIZED);
	}

	return next(ctx);
};

const router: Router = new Router(routerOpts);
router.use(TestMiddleware);

const entityMap = {
	account: Account,
	deveco: DevEco,
	entreprise: Entreprise,
	particulier: Particulier,
	fiche: Fiche,
	superadmin: Account,
} as const;

const createDevecoAccount = async (
	payload: Partial<Account>,
	query: ParsedUrlQuery
): Promise<Account> => {
	const territoryRepository = AppDataSource.getRepository(Territory);
	const territory = await territoryRepository.findOneOrFail({
		where: { epci: { id: '200057990' } },
	});

	const accountRepository = AppDataSource.getRepository(Account);
	const account = await accountRepository.save({ lastLogin: new Date(), ...payload });

	const devecoRepository = AppDataSource.getRepository(DevEco);
	await devecoRepository.insert({ territory, account });

	const password = query.password;
	if (password && typeof password === 'string') {
		console.info('inserting', { password });
		await AppDataSource.getRepository(Password).insert({ account, password });
	}

	return account;
};

const createSuperAdminAccount = async (
	payload: Partial<Account>,
	query: ParsedUrlQuery
): Promise<Account> => {
	const accountRepository = AppDataSource.getRepository(Account);
	const account = await accountRepository.save({
		lastLogin: new Date(),
		...payload,
		accountType: 'superadmin',
	});

	const password = query.password;
	if (password && typeof password === 'string') {
		await AppDataSource.getRepository(Password).insert({ account, password });
	}

	return account;
};

const upsert = async <K extends keyof typeof entityMap>(
	entityName: K,
	entityId: string | null,
	payload: Record<string, unknown>,
	query: ParsedUrlQuery
): Promise<any> => {
	if (Object.keys(entityMap).includes(entityName)) {
		const entity = entityMap[entityName];
		const repository = AppDataSource.getRepository(entity) as Repository<any>;
		if (entityId) {
			await repository.update(Number(entityId), payload as any);
			return await repository.findOneBy({ id: entityId });
		} else if (entityName === 'account') {
			if (payload.email && typeof payload.email === 'string') {
				const account = await AppDataSource.getRepository(Account).findOne({
					where: { email: payload.email },
				});
				if (account) {
					await AppDataSource.getRepository(Account).update(account.id, payload);
					return account;
				}
			}
			return await createDevecoAccount(payload, query);
		} else if (entityName === 'superadmin') {
			return await createSuperAdminAccount(payload, query);
		} else {
			return await repository.save(payload);
		}
	}
	throw new Error(`Unsupported entity name ${entityName}`);
};

const resetTestDb = async () => {
	const preSeedSqlQuery = readFileSync(path.join(__dirname, ...preSeedSql.split('/')), {
		encoding: 'utf8',
	});
	await AppDataSource.manager.query(preSeedSqlQuery);

	const seedSqlQuery = readFileSync(path.join(__dirname, ...seedSql.split('/')), {
		encoding: 'utf8',
	});
	await AppDataSource.manager.query(seedSqlQuery);
};

router.get('/resetAndSeed', async (ctx: DevecoContext) => {
	await resetTestDb();

	return (ctx.status = StatusCodes.OK);
});

router.post('/upsert/:entity/:id?', async (ctx: DevecoContext) => {
	const entity = ctx.params.entity;
	const id = ctx.params.id;
	const {
		body: payload,
		query,
	}: {
		body: Record<string, unknown>;
		query: ParsedUrlQuery;
	} = ctx.request as any;
	ctx.body = await upsert(entity, id, payload, query);
});

export default router;
