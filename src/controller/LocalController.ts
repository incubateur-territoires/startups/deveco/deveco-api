import * as Koa from 'koa';
import Router from 'koa-router';
import { StatusCodes } from 'http-status-codes';

import AppDataSource from '../data-source';
import { DevEco } from '../entity/DevEco';
import { Entite } from '../entity/Entite';
import { EventLogActionType, EventLogEntityType } from '../entity/EventLog';
import { Fiche } from '../entity/Fiche';
import { Local, LocalStatut, LocalType } from '../entity/Local';
import { LocalArchive } from '../entity/LocalArchive';
import { Proprietaire } from '../entity/Proprietaire';
import { LocalisationEntreprise } from '../entity/LocalisationEntreprise';
import eventLogService from '../service/EventLogService';
import entrepriseService from '../service/EntrepriseService';
import particulierService from '../service/ParticulierService';
import entiteService from '../service/EntiteService';
import { loadView, LocalView, toView } from '../view/LocalView';
import AuthMiddleware from '../middleware/AuthMiddleware';
import RoleMiddleware from '../middleware/RoleMiddleware';

const routerOpts: Router.IRouterOptions = {
	prefix: '/locaux',
};

const router: Router = new Router(routerOpts);
router.use(AuthMiddleware);
router.use(RoleMiddleware('deveco'));

const routes = {
	getAll: '/',
	getOne: '/:id',
	creer: '/creer',
	modifier: '/:id/modifier',
	remove: '/:id/proprietaire/remove',
	addProprietaire: '/:id/proprietaire',
};

const resultsPerPage = 20;

router.get(
	routes.getAll,
	async (
		ctx: Koa.ParameterizedContext<
			Koa.DefaultState,
			Koa.DefaultContext,
			{ page: number; pages: number; results: number; data: LocalView[] }
		>
	) => {
		const rawPage = ctx.query.page as unknown;
		let page = Number.isNaN(Number(rawPage)) ? 1 : Number(rawPage);
		const recherche = ((ctx.query.recherche as string) || '').replace(`'`, `''`);
		const statut = (ctx.query.statut as LocalStatut) || '';
		const types = (ctx.query.types as LocalType[]) || [];
		const deveco = ctx.state.deveco as DevEco;
		const conditions = [];

		conditions.push(`local.territoire_id = ${deveco.territory.id}`);

		if (recherche) {
			conditions.push(`titre ILIKE '%${recherche}%'`);
		}

		if (statut === 'occupe') {
			conditions.push(`local_statut = 'occupe'`);
		} else if (statut === 'vacant') {
			conditions.push(`local_statut = 'vacant'`);
		}

		if (types?.length) {
			const or = [];
			for (const t of types) {
				or.push(`local_types ilike '%${t}%'`);
			}
			conditions.push('( ' + or.join(' OR ') + ' )');
		}

		let [data, totalResults] = await search(conditions, page);

		if (data.length == 0 && totalResults > 0) {
			page = 1;
			[data, totalResults] = await search(conditions, page);
		}

		const fullPages = Math.floor(totalResults / resultsPerPage);
		const pages = fullPages + (totalResults - fullPages * resultsPerPage > 0 ? 1 : 0);

		const locaux = data.map((local) => toView(local));

		ctx.body = { page, pages, results: totalResults, data: locaux };
	}
);

async function search(conditions: string[], page: number): Promise<[Local[], number]> {
	const skip = (page - 1) * resultsPerPage;
	return AppDataSource.getRepository(Local)
		.createQueryBuilder('local')
		.leftJoinAndSelect('local.auteurModification', 'auteurModification')
		.leftJoinAndSelect('auteurModification.account', 'auteurModificationAccount')
		.where(conditions.join(' AND '))
		.skip(skip)
		.take(resultsPerPage)
		.getManyAndCount();
}

router.post(
	routes.modifier,
	async (ctx: Koa.ParameterizedContext<Koa.DefaultState, Koa.DefaultContext, LocalView>) => {
		const { id } = ctx.params;
		const {
			local,
		}: {
			local: {
				localisations: string[];
			};
		} = ctx.request.body;

		const createur = ctx.state.deveco as DevEco;
		const territoire = createur.territory;

		await entiteService.createIfNecessary(
			LocalisationEntreprise,
			local.localisations ?? [],
			territoire
		);

		const now = new Date();
		const localRepository = AppDataSource.getRepository(Local);

		await localRepository.update(id, {
			...local,
			dateModification: now,
			auteurModification: createur,
			createdAt: now,
			updatedAt: now,
		});

		const updated = await loadView(id);

		await eventLogService.addEventLog({
			action: EventLogActionType.UPDATE,
			deveco: createur,
			entityType: EventLogEntityType.LOCAL,
			entityId: id,
		});

		ctx.body = toView(updated);
	}
);

type NewProprietaireType = 'manual' | 'siret';

function isCorrectType(s: string): s is NewProprietaireType {
	return true;
}
router.post(
	routes.remove,
	async (
		ctx: Koa.ParameterizedContext<
			Koa.DefaultState,
			Koa.DefaultContext,
			LocalView | { error: string }
		>
	) => {
		const { id } = ctx.params;
		const {
			proprietaireId,
		}: {
			proprietaireId: number;
		} = ctx.request.body;

		const proprietaire = await AppDataSource.getRepository(Proprietaire).findOne({
			where: { id: proprietaireId, local: { id } },
		});
		if (!proprietaire) {
			ctx.throw(StatusCodes.NOT_FOUND, `Pas de propriétaire avec l'id #${id}`);
		}
		await AppDataSource.getRepository(Proprietaire).remove(proprietaire);

		const viewLocal = await loadView(id);
		ctx.body = viewLocal;
		return;
	}
);

router.post(
	routes.addProprietaire,
	async (
		ctx: Koa.ParameterizedContext<
			Koa.DefaultState,
			Koa.DefaultContext,
			LocalView | { error: string }
		>
	) => {
		const { id } = ctx.params;
		const {
			type,
		}: {
			type: string;
		} = ctx.request.body;

		if (!isCorrectType(type)) {
			ctx.throw(StatusCodes.BAD_REQUEST, 'Bad owner type');
		}

		const local = await AppDataSource.getRepository(Local).findOne({ where: { id } });

		if (!local) {
			ctx.throw(StatusCodes.BAD_REQUEST, 'Local not found');
		}

		const createur = ctx.state.deveco as DevEco;
		const territoire = createur.territory;
		const now = new Date();

		let entiteId: Entite['id'];

		switch (type) {
			case 'siret': {
				const {
					siret,
				}: {
					siret: string;
				} = ctx.request.body;
				const existingFiche = await AppDataSource.getRepository(Fiche).findOne({
					where: {
						entite: { entreprise: { siret } },
						territoire: { id: territoire.id },
					},
					relations: { entite: true },
				});
				if (existingFiche) {
					entiteId = existingFiche.entite.id;
				} else {
					const entite = await entrepriseService.getOrCreateEntrepriseAndEntite({
						siret,
						createur,
					});
					entiteId = entite.id;
				}
				break;
			}
			case 'manual': {
				const {
					prenom,
					nom,
					email,
					telephone,
				}: {
					prenom: string;
					nom: string;
					email: string;
					telephone: string;
				} = ctx.request.body;
				const particulierId = await particulierService.createParticulier({
					nom,
					prenom,
					telephone,
					email,
				});
				const { identifiers: entites } = await AppDataSource.getRepository(Entite).insert({
					territoire: { id: territoire.id },
					particulier: { id: particulierId },
					entiteType: 'PP',
				});

				entiteId = entites[0].id;
				break;
			}
		}
		try {
			await AppDataSource.getRepository(Proprietaire).insert({
				local: { id: local.id },
				entite: { id: entiteId },
			});
			const localRepository = AppDataSource.getRepository(Local);

			await localRepository.update(id, {
				...local,
				dateModification: now,
				auteurModification: createur,
				updatedAt: now,
			});

			await eventLogService.addEventLog({
				action: EventLogActionType.UPDATE,
				deveco: createur,
				entityType: EventLogEntityType.LOCAL,
				entityId: id,
			});
		} catch (_) {
			console.error('Tried adding the same entite as an owner twice, skipping', {
				local,
				entiteId,
			});
		}

		const viewLocal = await loadView(id);
		ctx.body = viewLocal;
	}
);

router.get(
	routes.getOne,
	async (
		ctx: Koa.ParameterizedContext<
			Koa.DefaultState,
			Koa.DefaultContext,
			LocalView | { error: string }
		>
	) => {
		const { id } = ctx.params;
		await eventLogService.addEventLog({
			action: EventLogActionType.VIEW,
			deveco: ctx.state.deveco as DevEco,
			entityType: EventLogEntityType.LOCAL,
			entityId: id,
		});

		const viewLocal = await loadView(id);

		ctx.body = viewLocal;
	}
);

router.post(
	routes.creer,
	async (ctx: Koa.ParameterizedContext<Koa.DefaultState, Koa.DefaultContext, LocalView>) => {
		const {
			titre,
			adresse,
			ville,
			codePostal,
			geolocation,
			localStatut,
			localTypes,
			surface,
			loyer,
			localisations,
			commentaire,
		}: {
			titre: string;
			adresse: string;
			ville: string;
			codePostal: string;
			geolocation: string;
			localStatut: LocalStatut;
			localTypes: LocalType[];
			surface: string;
			loyer: string;
			localisations: string[];
			commentaire: string;
		} = ctx.request.body;

		const createur = ctx.state.deveco as DevEco;
		const territoire = createur.territory;

		await entiteService.createIfNecessary(LocalisationEntreprise, localisations ?? [], territoire);

		const now = new Date();
		const localRepository = AppDataSource.getRepository(Local);
		const local: Local = {
			titre,
			adresse,
			ville,
			codePostal,
			geolocation,
			localStatut,
			localTypes,
			surface,
			loyer,
			localisations,
			commentaire,
			territoire,
			createur,
			dateModification: now,
			auteurModification: createur,
			createdAt: now,
			updatedAt: now,
		};

		const savedLocal = await localRepository.save(local);

		await eventLogService.addEventLog({
			action: EventLogActionType.CREATE,
			deveco: createur,
			entityType: EventLogEntityType.LOCAL,
			entityId: savedLocal.id,
		});

		ctx.body = toView(savedLocal);
	}
);

router.del(
	routes.getOne,
	async (
		ctx: Koa.ParameterizedContext<Koa.DefaultState, Koa.DefaultContext, Record<never, never>>
	) => {
		const id = ctx.params.id;
		const deveco = ctx.state.deveco as DevEco;
		const territoire = deveco.territory;

		const local = await AppDataSource.getRepository(Local).findOneOrFail({
			where: { id, territoire: { id: territoire.id } },
		});
		await AppDataSource.getRepository(LocalArchive).insert({ content: JSON.stringify(local) });
		await AppDataSource.getRepository(Proprietaire).delete({ local: { id } });
		await AppDataSource.getRepository(Local).delete({ id, territoire });

		await eventLogService.addEventLog({
			action: EventLogActionType.DELETE,
			deveco,
			entityType: EventLogEntityType.LOCAL,
			entityId: id,
		});

		ctx.body = {};
	}
);

export default router;
