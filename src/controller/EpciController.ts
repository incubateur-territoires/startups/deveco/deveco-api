import Router from 'koa-router';

import AppDataSource from '../data-source';
import { Epci } from '../entity/Epci';
import AuthMiddleware, { DevecoContext } from '../middleware/AuthMiddleware';

const routerOpts: Router.IRouterOptions = {
	prefix: '/epcis',
};

const router: Router = new Router(routerOpts);
router.use(AuthMiddleware);

router.get('/', async (ctx: DevecoContext) => {
	const epcis = await AppDataSource.getRepository(Epci).find();
	ctx.body = epcis;
});

router.get('/petr', async (ctx: DevecoContext) => {
	const deveco = ctx.state.deveco;

	if (!deveco) {
		ctx.throw('Deveco not found');
	}

	const territory = deveco.territory;

	const petrId = territory.petr?.id;

	if (!petrId) {
		ctx.body = [];
		return;
	}

	const epcis = await AppDataSource.getRepository(Epci).find({ where: { petr: { id: petrId } } });
	ctx.body = epcis;
});

router.get('/:id', async (ctx: DevecoContext) => {
	const id = ctx.params.id;
	const epci = await AppDataSource.getRepository(Epci).findOne({ where: { id } });
	ctx.body = epci;
});

export default router;
