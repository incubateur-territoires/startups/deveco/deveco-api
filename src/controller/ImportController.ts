import { StatusCodes } from 'http-status-codes';
import * as Koa from 'koa';
import Router from 'koa-router';

import { Readable } from 'stream';

import AppDataSource from '../data-source';
import { DevEco } from '../entity/DevEco';
import { Fiche } from '../entity/Fiche';
import { Entite } from '../entity/Entite';
import contactService from '../service/ContactService';
import entrepriseService from '../service/EntrepriseService';
import entiteService from '../service/EntiteService';
import devecoService from '../service/DevEcoService';
import { DevecoContext } from '../middleware/AuthMiddleware';

const ImportAuthMiddleware = async (
	importToken: string,
	ctx: Koa.ParameterizedContext<{ deveco?: DevEco }>,
	next: (ctx: Koa.ParameterizedContext<{ deveco?: DevEco }>) => Promise<unknown>
) => {
	if (!process.env.IMPORT_TOKEN || importToken !== process.env.IMPORT_TOKEN) {
		ctx.throw(StatusCodes.FORBIDDEN, `Jeton invalide : ${ctx.params.token}`);
	}

	const { email }: { email: string } = ctx.request.body as any;

	if (!email) {
		ctx.throw(
			StatusCodes.BAD_REQUEST,
			"La clef 'email' contenant l'email du Deveco créateur des fiches est obligatoire."
		);
	}

	const deveco = await devecoService.getDevEcoByAccountWithTerritory({ email });

	if (!deveco) {
		ctx.throw(StatusCodes.BAD_REQUEST, 'Aucun DevEco trouvé avec cet email');
	}

	ctx.state.deveco = deveco;
	return next(ctx);
};

const routerOpts: Router.IRouterOptions = {
	prefix: '/import',
};

const router: Router = new Router(routerOpts);

router.param('token', ImportAuthMiddleware);

type Createur = {
	nom: string;
	prenom: string;
	email: string;
	telephone: string;
	futureEnseigne: string;
	description: string;
};

type Contact = {
	siret: string;
	fonction: string;
	nom: string;
	prenom: string;
	email: string;
	telephone: string;
	dateDeNaissance: string;
};

type Qualification = {
	siret: string;
	activites: string[];
	zones: string[];
	mots: string[];
	autre?: string;
};

const sendResultAsStream = async <T>(
	ctx: DevecoContext,
	callback: (deveco: DevEco, line: T) => Promise<string>,
	deveco: DevEco,
	data: T[]
): Promise<void> => {
	ctx.status = StatusCodes.OK;
	ctx.type = 'text/plain';

	const filename = 'resultat';
	ctx.set('Content-disposition', `attachment; filename=${filename}.txt`);

	const stream = new Readable({
		read() {
			return;
		},
	});
	ctx.body = stream;

	let stop = false;
	ctx.req.on('close', () => {
		stop = true;
	});

	let i = 0;

	for (const line of data) {
		if (stop) {
			return;
		}

		try {
			const result = await callback(deveco, line);

			if (i > 0) {
				stream.push('\n');
			}

			stream.push(result);

			i += 1;
		} catch (e) {
			console.error(e);
		}
	}

	stream.push(null);
	ctx.res.end();
};

const processCreateur = async (deveco: DevEco, line: Createur) => {
	let message = 'OK';

	const territoire = deveco.territory;

	const { nom, prenom, email, telephone, futureEnseigne, description } = line;

	const entiteType = 'PP';

	try {
		const newEntite = AppDataSource.getRepository(Entite).create({
			territoire,
			createur: deveco,
			entiteType,
			particulier: {
				nom,
				prenom,
				email,
				telephone,
				description,
			},
			futureEnseigne,
			auteurModification: deveco,
		});
		const entite = await AppDataSource.getRepository(Entite).save(newEntite);

		const now = new Date();
		const fiche = AppDataSource.getRepository(Fiche).create({
			territoire,
			createur: deveco,
			entite,
			dateModification: now,
			auteurModification: deveco,
			createdAt: now,
			updatedAt: now,
		});

		await AppDataSource.getRepository(Fiche).save(fiche);
	} catch (e) {
		message = e instanceof Error ? (message = e.message) : 'Erreur inconnue';
	}

	return `${nom}-${prenom}: ${message}`;
};

const processContact = async (deveco: DevEco, line: Contact) => {
	let message = 'OK';

	const { siret, nom, prenom, telephone, email, dateDeNaissance, fonction } = line;

	let entite: Entite;
	try {
		entite = await entrepriseService.getOrCreateEntrepriseAndEntite({
			createur: deveco,
			siret,
			skipApiEntreprise: true,
		});
	} catch (e: unknown) {
		message =
			e instanceof Error
				? (message = e.message)
				: `Problème inconnu lors de la récupération/création de l'entité pour le SIRET ${siret}`;
		return `${siret}-${nom}-${prenom}: ${message}`;
	}

	try {
		await contactService.createContactFromParticulier({
			nom,
			prenom,
			email,
			telephone,
			dateDeNaissance,
			entite,
			fonction,
			deveco,
			source: 'import',
		});
	} catch (e: unknown) {
		message = e instanceof Error ? (message = e.message) : 'Erreur inconnue';
	}

	return `${siret}-${nom}-${prenom}: ${message}`;
};

const processEntreprise = async (deveco: DevEco, line: Qualification) => {
	let message = 'OK';

	const { siret: fuzzy } = line;

	try {
		const entite = await entiteService.createEntiteForSiret({
			createur: deveco,
			fuzzy,
			skipApiEntreprise: true,
		});

		await entiteService.addQualificationsToEntites([entite.id], deveco, {
			activitesReelles: line.activites ?? [],
			entrepriseLocalisations: line.zones ?? [],
			motsCles: line.mots ?? [],
			activiteAutre: line.autre,
		});
	} catch (e: unknown) {
		message = e instanceof Error ? (message = e.message) : 'Erreur inconnue';
	}

	return `${fuzzy}: ${message}`;
};

const ensureData = <T>(ctx: DevecoContext) => {
	const { data }: { data: T[] } = ctx.request.body as any;

	if (!data || !Array.isArray(data) || data.length === 0) {
		ctx.throw(StatusCodes.BAD_REQUEST, "La clef 'data' doit contenir une liste non vide");
	}

	return data;
};

router.post('/createurs/:token', async (ctx: DevecoContext) => {
	const deveco = ctx.state.deveco;
	if (!deveco) {
		return;
	}

	const data = ensureData<Createur>(ctx);
	if (!data) {
		return;
	}

	sendResultAsStream(ctx, processCreateur, deveco, data);
});

router.post('/contacts/:token', async (ctx: DevecoContext) => {
	const deveco = ctx.state.deveco;
	if (!deveco) {
		return;
	}

	const data = ensureData<Contact>(ctx);
	if (!data) {
		return;
	}

	sendResultAsStream(ctx, processContact, deveco, data);
});

router.post('/entreprises/:token', async (ctx: DevecoContext) => {
	const deveco = ctx.state.deveco;
	if (!deveco) {
		return;
	}

	const data = ensureData<Qualification>(ctx);
	if (!data) {
		return;
	}

	const activitesReelles = data.flatMap(({ activites }) => activites);
	const entrepriseLocalisations = data.flatMap(({ zones }) => zones);
	const motsCles = data.flatMap(({ mots }) => mots);
	await entiteService.createQualificationsIfNecessary({
		activitesReelles,
		entrepriseLocalisations,
		motsCles,
		deveco,
	});

	sendResultAsStream(ctx, processEntreprise, deveco, data);
});

export default router;
