import Excel from 'exceljs';

import { ServerResponse } from 'http';
import { Transform } from 'stream';

import { DevEco } from '../entity/DevEco';
import { Particulier } from '../entity/Particulier';
import { Entreprise } from '../entity/Entreprise';
import { displayEntrepriseName, displayParticulierName } from '../lib/utils';
import { Criteria as FicheCriteria } from '../service/FicheService';
import inseeService, {
	Criteria as EntrepriseCriteria,
	EntityReadStream,
	ExcelPayload,
} from '../service/InseeService';
import { fullCodeToFormeJuridique } from '../lib/formeJuridique';
import { codeToTrancheEffectifs } from '../lib/trancheEffectifs';
import { Demande } from '../entity/Demande';
import { Echange } from '../entity/Echange';
import { toEntiteView, toView, ContactView } from '../view/FicheView';
import { Fiche } from '../entity/Fiche';

type TypedColumn<T> = Partial<Excel.Column> & { key: T };

type CommonInfo = {
	nomPublic: string;
};

type EntiteInfo = {
	activitesReelles: string;
	zonesGeographiques: string;
	motsCles: string;
	commentaires: string;
};

type StructureType = Particulier | Entreprise;

type StructureInfo<T extends StructureType> = T extends Entreprise
	? {
			siret: string;
			codeNaf: string | null;
			libelleNaf: string | null;
			adresse: string | null;
	  }
	: {
			siret: undefined;
			codeNaf: undefined;
			libelleNaf: undefined;
			adresse: string;
	  };

type DemandeInfo = {
	typeDemande: string;
	echanges: string;
	state: string;
	motifCloture: string;
	dateCloture: string;
};

type WsDemandeRow<T extends StructureType> = CommonInfo & StructureInfo<T> & DemandeInfo;

type WsDemandeColumns<T extends StructureType> = TypedColumn<keyof WsDemandeRow<T>>[];

type ContactInfo = ContactView;

type WsContactRow<T extends StructureType> = CommonInfo & StructureInfo<T> & ContactInfo;

type WsContactColumns<T extends StructureType> = TypedColumn<keyof WsContactRow<T>>[];

type WsEntiteColumns = TypedColumn<keyof EntiteInfo>[];

type DemandeStructureInfo<T extends StructureType> = CommonInfo & {
	structureInfo: StructureInfo<T>;
};

type EtablissementInfo = {
	siren: string;
	sigle: string | null;
	codePostal: string | null;
	effectifs: string | null;
	dateEffectifs: string | null;
	libelleCategorieNaf: string | null;
	dateCreation: string | null;
	etatAdministratif: string | null;
	formeJuridique: string | null;
};

type CreateurInfo = {
	id?: number;
	nom: string;
	prenom: string;
	email: string | null;
	telephone: string | null;
	adresse: string | null;
	codePostal: string | null;
	ville: string | null;
	description: string | null;
};

type CreateurOrEtablissementInfo<T extends StructureType> = T extends Entreprise
	? EtablissementInfo
	: CreateurInfo;

type WsStructureRow<T extends StructureType> = CommonInfo &
	EntiteInfo &
	StructureInfo<T> &
	CreateurOrEtablissementInfo<T>;

type WsEtablissementColums = TypedColumn<keyof WsStructureRow<Entreprise>>[];

type WsCreateurColumns = TypedColumn<keyof WsStructureRow<Particulier>>[];

type Criteria<T extends StructureType> = T extends Particulier ? FicheCriteria : EntrepriseCriteria;

class ExcelService {
	private static instance: ExcelService;

	public static getInstance(): ExcelService {
		if (!ExcelService.instance) {
			ExcelService.instance = new ExcelService();
		}

		return ExcelService.instance;
	}

	wsEntrepriseColumns: WsEtablissementColums = [
		{ header: 'SIRET', key: 'siret' },
		{ header: 'SIREN', key: 'siren' },
		{ header: 'Nom', key: 'nomPublic' },
		{ header: 'Sigle', key: 'sigle' },
		{ header: 'Adresse', key: 'adresse' },
		{ header: 'Code postal', key: 'codePostal' },
		{ header: 'Effectifs', key: 'effectifs' },
		{ header: 'Date des effectifs', key: 'dateEffectifs' },
		{ header: 'Code NAF', key: 'codeNaf' },
		{ header: 'Libellé NAF', key: 'libelleNaf' },
		{ header: 'Catégorie NAF', key: 'libelleCategorieNaf' },
		{ header: 'Date création', key: 'dateCreation' },
		{ header: 'État administratif', key: 'etatAdministratif' },
		{ header: 'Forme juridique', key: 'formeJuridique' },
	];

	wsCreateurColumns: WsCreateurColumns = [
		{ header: 'Identifiant', key: 'id' },
		{ header: 'Nom', key: 'nom' },
		{ header: 'Prénom', key: 'prenom' },
		{ header: 'Email', key: 'email' },
		{ header: 'Téléphone', key: 'telephone' },
		{ header: 'Adresse', key: 'adresse' },
		{ header: 'Code postal', key: 'codePostal' },
		{ header: 'Commune', key: 'ville' },
		{ header: 'Description du projet', key: 'description' },
	];

	wsEntiteColumns: WsEntiteColumns = [
		{ header: 'Activités réelles', key: 'activitesReelles' },
		{ header: 'Zones géographiques', key: 'zonesGeographiques' },
		{ header: 'Mots-clés', key: 'motsCles' },
		{ header: 'Commentaires', key: 'commentaires' },
	];

	wsContactsColumns: WsContactColumns<StructureType> = [
		{ header: 'Siret', key: 'siret' },
		{ header: 'Dénomination', key: 'nomPublic' },
		{ header: 'Prénom', key: 'prenom' },
		{ header: 'Nom', key: 'nom' },
		{ header: 'Fonction', key: 'fonction' },
		{ header: 'Email', key: 'email' },
		{ header: 'Téléphone', key: 'telephone' },
		{ header: 'Date de naissance', key: 'dateDeNaissance' },
	];

	wsDemandesColumns: WsDemandeColumns<StructureType> = [
		{ header: 'Siret', key: 'siret' }, // empty for Créateurs
		{ header: 'Dénomination', key: 'nomPublic' },
		{ header: 'Adresse', key: 'adresse' },
		{ header: 'Code NAF', key: 'codeNaf' }, // empty for Créateurs
		{ header: 'Libellé NAF', key: 'libelleNaf' }, // empty for Créateurs
		{ header: 'Type de demande', key: 'typeDemande' },
		{ header: 'Échanges', key: 'echanges' },
		{ header: 'État', key: 'state' },
		{ header: 'Motif de clôture', key: 'motifCloture' },
		{ header: 'Date de clôture', key: 'dateCloture' },
	];

	public getAugmentedContacts<T extends StructureType>({
		entite,
		demandeStructureInfo,
	}: {
		entite: ExcelPayload['entite'];
		demandeStructureInfo: DemandeStructureInfo<T>;
	}): WsContactRow<T>[] {
		return (entite?.contacts || []).map(
			(contact: ContactView): WsContactRow<T> => ({
				nomPublic: demandeStructureInfo.nomPublic,
				...demandeStructureInfo.structureInfo,
				...contact,
			})
		);
	}

	public getAugmentedDemandes<T extends StructureType>({
		demandes,
		echanges,
		demandeStructureInfo,
	}: {
		demandes: Demande[];
		echanges: Echange[];
		demandeStructureInfo: DemandeStructureInfo<T>;
	}): WsDemandeRow<T>[] {
		return demandes.map((demande) => {
			const theme = demande.typeDemande;
			const relevantEchanges = echanges.filter((echange) => echange.themes.includes(theme));
			const summaryEchanges = relevantEchanges
				.map((echange) => echange.compteRendu)
				.join('\n\n')
				.trim();

			const demandeRow: WsDemandeRow<T> = {
				nomPublic: demandeStructureInfo.nomPublic,

				...demandeStructureInfo.structureInfo,

				typeDemande: demande.typeDemande,
				echanges: summaryEchanges,
				state: demande.dateCloture !== null ? 'Clôturé' : 'Ouvert',
				motifCloture: demande.motif,
				dateCloture: demande.dateCloture as unknown as string,
			};

			return demandeRow;
		});
	}

	public entrepriseToEtablissementInfo(structure: Entreprise): EtablissementInfo {
		return {
			siren: structure.etablissement.siren,
			sigle: structure.etablissement.sigle,
			codePostal: structure.etablissement.adresseComplete?.slice(-5) ?? null,
			effectifs: codeToTrancheEffectifs(structure.etablissement.trancheEffectifs ?? ''),
			dateEffectifs: structure.dateEffectifs as unknown as string,
			dateCreation: structure.dateCreation as unknown as string,
			formeJuridique: fullCodeToFormeJuridique(`${structure.etablissement.categorieJuridique}`),
			libelleCategorieNaf: structure.libelleCategorieNaf,
			etatAdministratif: 'A' === structure.etatAdministratif ? 'Actif' : 'Inactif',
		};
	}

	public particulierToCreateurInfo(structure: Particulier): CreateurInfo {
		return {
			id: structure.id,
			nom: structure.nom,
			prenom: structure.prenom,
			email: structure.email,
			telephone: structure.telephone,
			adresse: structure.adresse,
			codePostal: structure.codePostal,
			ville: structure.ville,
			description: structure.description,
		};
	}

	public structureToData<T extends StructureType>(structure: T): CreateurOrEtablissementInfo<T> {
		return (
			structure instanceof Particulier
				? this.particulierToCreateurInfo(structure)
				: this.entrepriseToEtablissementInfo(structure)
		) as CreateurOrEtablissementInfo<T>;
	}

	public formatStructureDataToRow<T extends StructureType>({
		criteria,
		structure,
		structureInfo,
		entite,
		fiche,
	}: {
		criteria: Criteria<T>;
		structure: T;
		structureInfo: StructureInfo<T>;
		entite?: ExcelPayload['entite'];
		fiche?: ExcelPayload['fiche'];
	}): {
		row: WsStructureRow<T>;
		demandes: WsDemandeRow<T>[];
		contacts: WsContactRow<T>[];
	} {
		const nomPublic =
			structure instanceof Particulier
				? displayParticulierName(structure)
				: displayEntrepriseName(structure);
		const demandeStructureInfo = {
			nomPublic,
			structureInfo,
		};

		const commonRow = {
			nomPublic,
		};

		const contacts: WsContactRow<T>[] = [];
		const demandes: WsDemandeRow<T>[] = [];

		let entiteRow: EntiteInfo = {
			activitesReelles: '',
			zonesGeographiques: '',
			motsCles: '',
			commentaires: '',
		};
		if (entite) {
			entiteRow = {
				activitesReelles: entite.activitesReelles ? (entite.activitesReelles || []).join(', ') : '',
				zonesGeographiques: entite.localisations ? (entite.localisations || []).join(', ') : '',
				motsCles: entite.motsCles ? (entite.motsCles || []).join(', ') : '',
				commentaires: entite.activiteAutre || '',
			};

			const augmentedContacts = this.getAugmentedContacts({ entite, demandeStructureInfo });
			contacts.push(...augmentedContacts);

			if (fiche) {
				const demandesRaw = (fiche.demandes || []).filter(
					(demande) =>
						criteria.demandes.length === 0 || criteria.demandes.includes(demande.typeDemande)
				);

				const echanges = fiche.echanges;

				const augmentedDemandes = this.getAugmentedDemandes({
					demandes: demandesRaw,
					echanges,
					demandeStructureInfo,
				});

				demandes.push(...augmentedDemandes);
			}
		}

		const data = this.structureToData(structure);
		const row: WsStructureRow<T> = {
			...commonRow,
			...entiteRow,
			...structureInfo,
			...data,
		};

		return { row, contacts, demandes };
	}

	public makeWorkbookForCreateurs({
		fiches,
		criteria,
		output,
	}: {
		fiches: Fiche[];
		criteria: FicheCriteria;
		output: ServerResponse;
	}): Promise<void> {
		return new Promise((resolve, reject) => {
			const workbook = new Excel.stream.xlsx.WorkbookWriter({
				stream: output,
			});

			const wsCreateur = workbook.addWorksheet(`Créateurs d'entreprise`);
			const createursColumns: WsCreateurColumns = [
				...this.wsCreateurColumns,
				...this.wsEntiteColumns,
			];
			wsCreateur.columns = createursColumns;

			const wsDemandes = workbook.addWorksheet('Demandes');
			wsDemandes.columns = this.wsDemandesColumns;

			const wsContacts = workbook.addWorksheet('Contacts');
			wsContacts.columns = this.wsContactsColumns;

			for (const fiche of fiches) {
				if (!fiche.entite.particulier) {
					continue;
				}

				const particulier = fiche.entite.particulier;

				const entite = toEntiteView(fiche.entite);

				const { row, contacts, demandes } = this.formatStructureDataToRow({
					criteria,
					structure: particulier,
					structureInfo: {
						siret: undefined,
						codeNaf: undefined,
						libelleNaf: undefined,
						adresse: particulier.adresse,
					},
					entite,
					fiche: toView(fiche.entite, fiche),
				});

				wsCreateur.addRow(row).commit();

				if (contacts.length > 0) {
					contacts.forEach((contact) => wsContacts.addRow(contact).commit());

					contacts.length = 0;
				}

				if (demandes.length > 0) {
					demandes.forEach((demande) => wsDemandes.addRow(demande).commit());

					demandes.length = 0;
				}
			}

			workbook
				.commit()
				.catch(() => reject('Failed to commit workbook'))
				.then(() => resolve());
		});
	}

	public flushExcel(
		deveco: DevEco,
		criteria: Criteria<Entreprise>,
		wsEtablissements: Excel.Worksheet,
		wsDemandes: Excel.Worksheet,
		wsContacts: Excel.Worksheet
	) {
		const formatter = this.formatStructureDataToRow.bind(this);
		return async function (sirets: string[]) {
			if (!sirets.length) {
				return;
			}

			const etablissements = await inseeService.siretsToPayload(deveco, sirets);

			for (const etablissement of etablissements) {
				const { entreprise, entite, fiche } = etablissement;

				const {
					row,
					demandes,
					contacts,
				}: {
					row: WsStructureRow<Entreprise>;
					demandes: WsDemandeRow<Entreprise>[];
					contacts: WsContactRow<Entreprise>[];
				} = formatter({
					criteria,
					structure: entreprise,
					structureInfo: {
						siret: entreprise.siret,
						adresse: entreprise.adresse,
						codeNaf: entreprise.codeNaf,
						libelleNaf: entreprise.libelleNaf,
					},
					entite,
					fiche,
				});

				wsEtablissements.addRow(row).commit();

				if (contacts.length > 0) {
					contacts.forEach((contact) => wsContacts.addRow(contact).commit());

					contacts.length = 0;
				}

				if (demandes.length > 0) {
					demandes.forEach((demande) => wsDemandes.addRow(demande).commit());

					demandes.length = 0;
				}
			}

			etablissements.length = 0;

			return;
		};
	}

	public async makeWorkbookForEtablissements({
		deveco,
		criteria,
		input,
		output,
	}: {
		deveco: DevEco;
		criteria: EntrepriseCriteria;
		input: EntityReadStream<{ siret: string }>;
		output: ServerResponse;
	}): Promise<void> {
		return new Promise((resolve, reject) => {
			const workbook = new Excel.stream.xlsx.WorkbookWriter({
				stream: output,
			});

			const wsEtablissements = workbook.addWorksheet('Entreprises');
			wsEtablissements.columns = [...this.wsEntrepriseColumns, ...this.wsEntiteColumns];

			const wsDemandes = workbook.addWorksheet('Demandes');
			wsDemandes.columns = this.wsDemandesColumns;

			const wsContacts = workbook.addWorksheet('Contacts');
			wsContacts.columns = this.wsContactsColumns;

			const siretsProcessor = this.flushExcel(
				deveco,
				criteria,
				wsEtablissements,
				wsDemandes,
				wsContacts
			);

			const sirets: string[] = [];

			const dataStream = new Transform({
				transform: async (data: { siret: string }, _encoding, callback) => {
					sirets.push(data.siret);

					if (sirets.length > 49) {
						const toProcess = sirets.splice(0, sirets.length);

						await siretsProcessor(toProcess);
					}

					callback(null, {});
				},
				readableObjectMode: true,
				writableObjectMode: true,
			});

			dataStream.on('error', reject);

			dataStream.on('end', async () => {
				const toProcess = sirets.splice(0, sirets.length);

				await siretsProcessor(toProcess);

				await workbook.commit();

				resolve();
			});

			dataStream.on('data', () => {
				// this is needed to "consume" the stream, otherwise it never pulls
			});

			input.pipe(dataStream);
		});
	}
}

const excelService = ExcelService.getInstance();

export default excelService;
