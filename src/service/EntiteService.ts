import { FindOptionsWhere, In, IsNull, Not } from 'typeorm';
import { QueryDeepPartialEntity } from 'typeorm/query-builder/QueryPartialEntity';
import { StatusCodes } from 'http-status-codes';

import AppDataSource from '../data-source';
import { isSiret } from '../lib/regexp';
import { DevEco } from '../entity/DevEco';
import { Entite } from '../entity/Entite';
import { Territory } from '../entity/Territory';
import { ActiviteEntreprise } from '../entity/ActiviteEntreprise';
import { LocalisationEntreprise } from '../entity/LocalisationEntreprise';
import { MotCle } from '../entity/MotCle';
import { EventLogActionType } from '../entity/EventLog';
import { dedupe, getLogEntityType } from '../lib/utils';
import entrepriseService from '../service/EntrepriseService';
import { Infos, relationsForToView } from '../service/InseeService';
import eventLogService from '../service/EventLogService';
import { DevecoContext } from '../middleware/AuthMiddleware';

export type Conditions<T> = FindOptionsWhere<T>[] | FindOptionsWhere<T>;

export type BatchResults = {
	total: number;
	successes: number;
	errors: string[];
	errorEntites?: Entite['id'][];
};

class EntiteService {
	private static instance: EntiteService;

	public static getInstance(): EntiteService {
		if (!EntiteService.instance) {
			EntiteService.instance = new EntiteService();
		}

		return EntiteService.instance;
	}

	public async createEntiteForSiret({
		createur,
		fuzzy,
		skipApiEntreprise,
	}: {
		createur: DevEco;
		fuzzy: string;
		skipApiEntreprise?: boolean;
	}): Promise<Entite> {
		if (!isSiret(fuzzy)) {
			throw new Error(`${fuzzy} is not a valid SIRET.`);
		}
		const siret = fuzzy.replaceAll(/\s/g, '');

		const entite = await entrepriseService.getOrCreateEntrepriseAndEntite({
			createur,
			siret,
			skipApiEntreprise,
		});

		return entite;
	}

	public async updateEntite(
		id: Entite['id'],
		deveco: DevEco,
		payload: Partial<Entite>,
		action: EventLogActionType
	): Promise<void> {
		const now = new Date();

		await AppDataSource.getRepository(Entite).update(id, {
			...payload,
			updatedAt: now,
			auteurModification: deveco,
		});

		const entite = await AppDataSource.getRepository(Entite).findOneByOrFail({ id });

		await eventLogService.addEventLog({
			action,
			deveco,
			entityType: getLogEntityType(entite),
			entityId: id,
		});
	}

	public dedupeQualifications(
		entite: Entite,
		{
			activitesReelles,
			entrepriseLocalisations,
			motsCles,
			activiteAutre,
		}: {
			activitesReelles: Entite['activitesReelles'];
			entrepriseLocalisations: Entite['entrepriseLocalisations'];
			motsCles: Entite['motsCles'];
			activiteAutre: Entite['activiteAutre'];
		}
	): {
		activitesReelles: Entite['activitesReelles'];
		entrepriseLocalisations: Entite['entrepriseLocalisations'];
		motsCles: Entite['motsCles'];
		activiteAutre?: Entite['activiteAutre'];
	} {
		return {
			activitesReelles: dedupe([...(entite.activitesReelles ?? []), ...(activitesReelles ?? [])]),
			entrepriseLocalisations: dedupe([
				...(entite.entrepriseLocalisations ?? []),
				...(entrepriseLocalisations ?? []),
			]),
			motsCles: dedupe([...(entite.motsCles ?? []), ...(motsCles ?? [])]),
			activiteAutre: activiteAutre
				? entite.activiteAutre
					? entite.activiteAutre + '\n' + activiteAutre
					: activiteAutre
				: entite.activiteAutre,
		};
	}

	public async updateQualificationsForEntite(
		id: Entite['id'],
		deveco: DevEco,
		payload: {
			activitesReelles: Entite['activitesReelles'];
			entrepriseLocalisations: Entite['entrepriseLocalisations'];
			motsCles: Entite['motsCles'];
			activiteAutre?: Entite['activiteAutre'];
		}
	): Promise<boolean | null> {
		const entite = await AppDataSource.getRepository(Entite).findOne({
			where: { id },
		});

		// NOTE: this can only happen if entiteId was known to the front-end and somehow is not connected to an actual entite by the time we get this request
		if (!entite) {
			return null;
		}

		const { activitesReelles, entrepriseLocalisations, motsCles } = payload;
		await this.createQualificationsIfNecessary({
			activitesReelles,
			entrepriseLocalisations,
			motsCles,
			deveco,
		});

		try {
			await this.updateEntite(id, deveco, payload, EventLogActionType.UPDATE_QUALIFICATION);

			return true;
		} catch (e) {
			console.error('Error updating Entite', e);
			return false;
		}
	}

	public async touchEntite({ id, auteur }: { id: Entite['id']; auteur: DevEco }): Promise<void> {
		await this.updateEntite(id, auteur, {}, EventLogActionType.UPDATE);
	}

	public async getOrCreateEntite(ctx: DevecoContext): Promise<Entite> {
		const deveco = ctx.state.deveco;

		if (!deveco) {
			return ctx.throw(StatusCodes.UNAUTHORIZED);
		}

		const territoireId = deveco.territory.id;

		const {
			ficheId: ficheIdAsString = '',
			siret,
		}: {
			ficheId?: string;
			siret?: string;
		} = ctx.query;

		if (ficheIdAsString && siret) {
			return ctx.throw(
				StatusCodes.BAD_REQUEST,
				'Il faut préciser seulement ficheId OU siret, mais pas les deux.'
			);
		}

		if (ficheIdAsString) {
			const ficheId = parseInt(ficheIdAsString);
			const entite = await AppDataSource.getRepository(Entite).findOne({
				where: {
					fiche: {
						id: ficheId,
					},
					territoire: {
						id: territoireId,
					},
				},
				relations: relationsForToView,
			});

			if (!entite) {
				return ctx.throw(StatusCodes.NOT_FOUND, `Fiche ${ficheId} non trouvée`);
			}

			return entite;
		}

		if (siret) {
			const entite = await entrepriseService.getOrCreateEntrepriseAndEntite({
				createur: deveco,
				siret,
				skipApiEntreprise: false,
			});

			if (entite) {
				return entite;
			}

			console.error('Could not create Entreprise and Entite', { deveco, siret });
			return ctx.throw(`Impossible de créer l'entreprise pour le siret ${siret}`);
		}

		return ctx.throw(StatusCodes.BAD_REQUEST, `Mauvaise requête : ni ficheId ni siret`);
	}

	public async createQualificationsIfNecessary({
		activitesReelles,
		entrepriseLocalisations,
		motsCles,
		deveco,
	}: {
		activitesReelles: Entite['activitesReelles'];
		entrepriseLocalisations: Entite['entrepriseLocalisations'];
		motsCles: Entite['motsCles'];
		deveco: DevEco;
	}) {
		const territoire = deveco.territory;
		await Promise.all([
			this.createIfNecessary(ActiviteEntreprise, activitesReelles ?? [], territoire),
			this.createIfNecessary(LocalisationEntreprise, entrepriseLocalisations ?? [], territoire),
			this.createIfNecessary(MotCle, motsCles ?? [], territoire),
		]);
	}

	public async addQualificationsToEntites(
		entiteIds: Entite['id'][],
		deveco: DevEco,
		{
			activitesReelles,
			entrepriseLocalisations,
			motsCles,
			activiteAutre,
		}: {
			activitesReelles: Entite['activitesReelles'];
			entrepriseLocalisations: Entite['entrepriseLocalisations'];
			motsCles: Entite['motsCles'];
			activiteAutre?: Entite['activiteAutre'];
		}
	): Promise<BatchResults> {
		const errors: string[] = [];

		await this.createQualificationsIfNecessary({
			activitesReelles,
			entrepriseLocalisations,
			motsCles,
			deveco,
		});

		const createursWithEtablissementSet = (
			await AppDataSource.getRepository(Entite).find({
				where: { id: In(entiteIds), etablissementCree: Not(IsNull()) },
				select: ['id'],
			})
		).reduce((acc, { id }) => acc.add(id), new Set());

		// Remove Créateurs with already created Etablissements from update as they're uneditable
		entiteIds = entiteIds.filter((id) => !createursWithEtablissementSet.has(id));

		for (const id of Array.from(createursWithEtablissementSet)) {
			errors.push(`Le Créateur (${id}) a déjà créé un Etablissement, il ne peut être édité.`);
		}

		// TODO refuse to batch edit Créateurs when they have created an Établissement
		const results = await AppDataSource.manager.query(
			`
				UPDATE entite
				SET
					activites_reelles = array_to_string(
						ARRAY (
							SELECT DISTINCT unnest(
								string_to_array(activites_reelles, ',') || $3
							)
						),
						','
					),
					entreprise_localisations = array_to_string(
						ARRAY (
							SELECT DISTINCT unnest(
								string_to_array(entreprise_localisations, ',') || $4
							)
						),
						','
					),
					mots_cles = array_to_string(
						ARRAY (
							SELECT DISTINCT unnest(
								string_to_array(mots_cles, ',') || $5
							)
						),
						','
					),
					activite_autre =
						CASE
							WHEN activite_autre IS NULL THEN COALESCE($2, '')
							WHEN activite_autre = '' THEN COALESCE($2, '')
							ELSE
								CASE
									WHEN $2 IS NULL THEN activite_autre
									WHEN $2 = '' THEN activite_autre
									ELSE activite_autre || '\n' || $2
								END
						END
				WHERE id = ANY ($1)
				RETURNING id
			`,
			[entiteIds, activiteAutre, activitesReelles, entrepriseLocalisations, motsCles]
		);
		const successes = results[0];

		for (const entiteId of entiteIds) {
			const entite = await AppDataSource.getRepository(Entite).findOneOrFail({
				where: { id: entiteId },
			});

			await eventLogService.addEventLog({
				action: EventLogActionType.UPDATE_QUALIFICATION,
				deveco,
				entityType: getLogEntityType(entite),
				entityId: entiteId,
			});
		}

		return {
			total: entiteIds.length,
			successes: successes.length,
			errors,
			errorEntites: entiteIds.filter(
				(id) => !successes.map((s: { id: number }) => s.id).includes(id)
			),
		};
	}

	public isSuivi(entite: Entite): boolean {
		return (
			entite.contacts.filter(
				(contact) => !!contact.particulier.email || !!contact.particulier.telephone
			).length > 0 ||
			(entite.fiche?.echanges || []).length > 0 ||
			(entite.activitesReelles || []).length > 0 ||
			(entite.entrepriseLocalisations || []).length > 0 ||
			(entite.motsCles || []).length > 0 ||
			!!entite.activiteAutre
		);
	}

	public isAccompagne(entite: Entite): boolean {
		return (entite.fiche?.demandes || []).length > 0;
	}

	public async infosForEntite(
		entiteId?: Entite['id'],
		devecoId?: DevEco['id'],
		territoryId?: Territory['id']
	): Promise<Infos> {
		if (!entiteId || !devecoId || !territoryId) {
			return {
				contacts: false,
				echanges: false,
				rappels: false,
				favori: false,
			};
		}

		return (
			await AppDataSource.manager.query(
				`
				SELECT
					(EXISTS (
						SELECT contact.id
						FROM contact
						JOIN particulier ON particulier.id = contact.particulier_id
						WHERE contact.entite_id = entite.id AND (particulier.email <> '' OR particulier.telephone <> '')
						LIMIT 1
					)) as contacts,
					(EXISTS (
						SELECT echange.id
						FROM echange
						WHERE echange.fiche_id = fiche.id
						LIMIT 1
					)) as echanges,
					(EXISTS (
						SELECT rappel.id
						FROM rappel
						WHERE rappel.fiche_id = fiche.id
						AND rappel.date_cloture IS NULL
						LIMIT 1
					)) as rappels,
					(EXISTS (
						SELECT id
						FROM etablissement_favori
						WHERE deveco_id = $2
						AND favori
						AND territory_id = $3
						AND siret = entite.entreprise_id
					)) as favori
				FROM entite
				LEFT JOIN fiche on fiche.entite_id = entite.id
				WHERE entite.id = $1
				`,
				[entiteId, devecoId, territoryId]
			)
		)[0];
	}

	public async getExistingQualifications(
		territoire: Territory
	): Promise<{ activites: string[]; zones: string[]; mots: string[] }> {
		const params = { territoireId: territoire.id };

		const allActivitesReelles = await AppDataSource.getRepository(ActiviteEntreprise)
			.createQueryBuilder('activite')
			.where(`activite.territory_id = :territoireId`, params)
			.orderBy('activite.activite', 'ASC')
			.getMany();

		const activites = allActivitesReelles.map((a) => a.activite);

		const allZones = await AppDataSource.getRepository(LocalisationEntreprise)
			.createQueryBuilder('zone')
			.where(`zone.territory_id = :territoireId`, params)
			.orderBy('zone.localisation', 'ASC')
			.getMany();

		const zones = allZones.map((zone) => zone.localisation);

		const allMots = await AppDataSource.getRepository(MotCle)
			.createQueryBuilder('mot')
			.where(`mot.territory_id = :territoireId`, params)
			.orderBy('mot.mot_cle', 'ASC')
			.getMany();

		const mots = allMots.map((mot) => mot.motCle);

		return { activites, zones, mots };
	}

	public async createIfNecessary<T extends ActiviteEntreprise | LocalisationEntreprise | MotCle>(
		classe: { new (): T },
		data: string[],
		territory: Territory
	): Promise<void> {
		data = (data ?? []).join(',').split(','); // split comma-separated values
		const fake = new classe();
		const key: string =
			fake instanceof ActiviteEntreprise
				? 'activite'
				: fake instanceof LocalisationEntreprise
				? 'localisation'
				: fake instanceof MotCle
				? 'motCle'
				: '';

		if (!key) {
			return;
		}

		const newData = dedupe(data)
			.filter(Boolean)
			.map((d) => ({ [key]: d, territory })) as QueryDeepPartialEntity<T>;

		await AppDataSource.createQueryBuilder()
			.insert()
			.into(classe)
			.values(newData)
			.orIgnore()
			.execute();

		return;
	}
}

const entiteService = EntiteService.getInstance();

export default entiteService;
