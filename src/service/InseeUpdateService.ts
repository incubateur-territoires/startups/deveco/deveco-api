import { DeepPartial, EntityTarget, FindOptionsWhere, In } from 'typeorm';
import { QueryDeepPartialEntity } from 'typeorm/query-builder/QueryPartialEntity';

import * as path from 'path';
import * as crypto from 'crypto';
import { readFileSync } from 'fs';

import territoireService from './TerritoireService';

import { SireneUniteLegale } from '../entity/SireneUniteLegale';
import AppDataSource from '../data-source';
import { SireneEtablissement } from '../entity/SireneEtablissement';
import { httpGet } from '../lib/httpRequest';
import {
	caractereEmployeur,
	CategorieEntreprise,
	DiffusionStatut,
	ecoSocSol,
	EtatAdministratif,
	EtatAdministratifEtablissement,
	Sexe,
} from '../entity/enums';
import { TerritoryEtablissementReference } from '../entity/TerritoryEtablissementRef';
import { LienSuccessionTemp } from '../entity/LienSuccessionTemp';
import { Territory } from '../entity/Territory';

export type SireneApiEtablissements = {
	header: {
		total: number;
		statut: number;
		message: string;
		debut: number;
		nombre: number;
		curseur: string;
		curseurSuivant: string;
	};
	etablissements: EtablissementApi[];
};

export type SireneApiLiensSuccession = {
	header: {
		total: number;
		statut: number;
		message: string;
		debut: number;
		nombre: number;
		curseur: string;
		curseurSuivant: string;
	};
	liensSuccession: LienSuccessionApi[];
};

export type LienSuccessionApi = {
	siretEtablissementPredecesseur: string;
	siretEtablissementSuccesseur: string;
	dateLienSuccession: string;
	transfertSiege: boolean;
	continuiteEconomique: boolean;
	dateDernierTraitementLienSuccession: string;
};

export type EtablissementApi = {
	siren: string;
	nic: string;
	siret: string;
	statutDiffusionEtablissement: DiffusionStatut;
	dateCreationEtablissement: string;
	trancheEffectifsEtablissement: string;
	anneeEffectifsEtablissement: string;
	activitePrincipaleRegistreMetiersEtablissement: string;
	dateDernierTraitementEtablissement: string;
	etablissementSiege: boolean;
	nombrePeriodesEtablissement: number;
	uniteLegale: UniteLegaleApi;
	adresseEtablissement: AdresseEtablissementApi;
	adresse2Etablissement: Adresse2EtablissementApi;
	periodesEtablissement: EtablissementPeriodeApi[];
};

export type UniteLegaleApi = {
	statutDiffusionUniteLegale: DiffusionStatut;
	dateCreationUniteLegale: string;
	sigleUniteLegale: string;
	sexeUniteLegale: Sexe;
	prenom1UniteLegale: string;
	prenom2UniteLegale: string;
	prenom3UniteLegale: string;
	prenom4UniteLegale: string;
	prenomUsuelUniteLegale: string;
	pseudonymeUniteLegale: string;
	identifiantAssociationUniteLegale: string;
	trancheEffectifsUniteLegale: string;
	anneeEffectifsUniteLegale: number;
	dateDernierTraitementUniteLegale: string;
	categorieEntreprise: CategorieEntreprise;
	anneeCategorieEntreprise: number;
	etatAdministratifUniteLegale: EtatAdministratif;
	nomUniteLegale: string;
	nomUsageUniteLegale: string;
	denominationUniteLegale: string;
	denominationUsuelle1UniteLegale: string;
	denominationUsuelle2UniteLegale: string;
	denominationUsuelle3UniteLegale: string;
	categorieJuridiqueUniteLegale: number;
	activitePrincipaleUniteLegale: string;
	nomenclatureActivitePrincipaleUniteLegale: string;
	nicSiegeUniteLegale: string;
	economieSocialeSolidaireUniteLegale: ecoSocSol;
	caractereEmployeurUniteLegale: string;
};

export type AdresseEtablissementApi = {
	complementAdresseEtablissement: string;
	numeroVoieEtablissement: string;
	indiceRepetitionEtablissement: string;
	typeVoieEtablissement: string;
	libelleVoieEtablissement: string;
	codePostalEtablissement: string;
	libelleCommuneEtablissement: string;
	libelleCommuneEtrangerEtablissement: string;
	distributionSpecialeEtablissement: string;
	codeCommuneEtablissement: string;
	codeCedexEtablissement: string;
	libelleCedexEtablissement: string;
	codePaysEtrangerEtablissement: string;
	libellePaysEtrangerEtablissement: string;
};

export type Adresse2EtablissementApi = {
	complementAdresse2Etablissement: string;
	numeroVoie2Etablissement: string;
	indiceRepetition2Etablissement: string;
	typeVoie2Etablissement: string;
	libelleVoie2Etablissement: string;
	codePostal2Etablissement: string;
	libelleCommune2Etablissement: string;
	libelleCommuneEtranger2Etablissement: string;
	distributionSpeciale2Etablissement: string;
	codeCommune2Etablissement: string;
	codeCedex2Etablissement: string;
	libelleCedex2Etablissement: string;
	codePaysEtranger2Etablissement: string;
	libellePaysEtranger2Etablissement: string;
};

export type EtablissementPeriodeApi = {
	dateFin: string;
	dateDebut: string;
	etatAdministratifEtablissement: EtatAdministratifEtablissement;
	changementEtatAdministratifEtablissement: string;
	enseigne1Etablissement: string;
	enseigne2Etablissement: string;
	enseigne3Etablissement: string;
	changementEnseigneEtablissement: string;
	denominationUsuelleEtablissement: string;
	changementDenominationUsuelleEtablissement: string;
	activitePrincipaleEtablissement: string;
	nomenclatureActivitePrincipaleEtablissement: string;
	changementActivitePrincipaleEtablissement: string;
	caractereEmployeurEtablissement: caractereEmployeur;
	changementCaractereEmployeurEtablissement: string;
};

const readFile = (p: string) =>
	readFileSync(path.join(__dirname, ...p.split('/')), {
		encoding: 'utf8',
	});

const runSqlOnSirets = (p: string) => async (sirets: string[]) => {
	const sql = readFile(p);
	return AppDataSource.manager.query(sql, [sirets]);
};

const upsertInEtablissementFromSireneEtablissements = runSqlOnSirets(
	'../sql/upsertInEtablissementFromSireneEtablissements.sql'
);
const updateEntrepriseFromSireneEtablissements = runSqlOnSirets(
	'../sql/updateEntrepriseFromSireneEtablissements.sql'
);
const upsertInTerFromSireneEtablissements = runSqlOnSirets(
	'../sql/upsertInTerFromSireneEtablissements.sql'
);

// const updateEtablissementsGeoloc = runSqlOnSirets('../sql/updateGeoloc.sql');
// const updateTERQpv = runSqlOnSirets('../sql/updateQPV.sql');

const createTemporaryTerritoryToCommunesTable = () => {
	const sql = readFile('../sql/createTemporaryTerritoryToCommunesTable.sql');
	return AppDataSource.manager.query(sql);
};

const dropTemporaryTerritoryToCommunesTable = () => {
	const sql = readFile('../sql/dropTemporaryTerritoryToCommunesTable.sql');
	return AppDataSource.manager.query(sql);
};

const upsertDedupedLienSuccession = () => {
	const sql = readFile('../sql/upsertDedupedLienSuccession.sql');
	return AppDataSource.manager.query(sql);
};

const setNDToNull = (s: string) => (s === '[ND]' ? null : s);

const cleanNDValues = (o: Record<string, any>): Record<string, any> =>
	Object.entries(o).reduce(
		(acc, [key, value]) => ({
			...acc,
			[key]: typeof value === 'string' ? setNDToNull(value) : value,
		}),
		{}
	);

class InseeUpdateService {
	private static instance: InseeUpdateService;

	public static getInstance(): InseeUpdateService {
		if (!InseeUpdateService.instance) {
			InseeUpdateService.instance = new InseeUpdateService();
		}

		return InseeUpdateService.instance;
	}

	public etablissementApiToSireneUniteLegale(
		siren: string,
		etablissementApi: EtablissementApi
	): SireneUniteLegale {
		const { sexeUniteLegale, ...restUniteLegale } = etablissementApi.uniteLegale;
		const modifiedUniteLegale = cleanNDValues(restUniteLegale);

		const sireneUniteLegale = AppDataSource.getRepository(SireneUniteLegale).create({
			siren,
			sexeUniteLegale,
			...modifiedUniteLegale,
		});
		const hash = this.hashSireneUniteLegale(sireneUniteLegale);

		return { ...sireneUniteLegale, hash };
	}

	public etablissementApiToSireneEtablissement(
		siret: string,
		etablissementApi: EtablissementApi
	): SireneEtablissement {
		const {
			siren,
			nic,
			statutDiffusionEtablissement,
			dateCreationEtablissement,
			trancheEffectifsEtablissement,
			anneeEffectifsEtablissement,
			activitePrincipaleRegistreMetiersEtablissement,
			dateDernierTraitementEtablissement,
			etablissementSiege,
			nombrePeriodesEtablissement,
			uniteLegale,
			adresseEtablissement,
			adresse2Etablissement,
			periodesEtablissement,
		} = etablissementApi;

		const lastPeriod = periodesEtablissement?.[0];

		if (lastPeriod.dateFin !== null) {
			throw new Error('Current period has a dateFin!');
		}

		const cleanedLastPeriod = cleanNDValues(lastPeriod);

		const { sexeUniteLegale, ...restUniteLegale } = uniteLegale;
		const modifiedUniteLegale = cleanNDValues(restUniteLegale);

		const cleanedAdresseEtablissement = cleanNDValues(adresseEtablissement);
		const cleanedAdresse2Etablissement = cleanNDValues(adresse2Etablissement);

		const sireneEtablissementPayload: DeepPartial<SireneEtablissement> = {
			siren,
			nic,
			siret,
			statutDiffusionEtablissement,
			dateCreationEtablissement,
			trancheEffectifsEtablissement,
			anneeEffectifsEtablissement,
			activitePrincipaleRegistreMetiersEtablissement,
			dateDernierTraitementEtablissement,
			etablissementSiege,
			nombrePeriodesEtablissement,
			...modifiedUniteLegale,
			...cleanedAdresseEtablissement,
			...cleanedAdresse2Etablissement,
			...cleanedLastPeriod,
		};

		const sireneEtablissement = AppDataSource.getRepository(SireneEtablissement).create(
			sireneEtablissementPayload
		);

		const hash = this.hashSireneEtablissement(sireneEtablissement);

		return { ...sireneEtablissement, hash };
	}

	public hashSireneEtablissement(sireneEtablissement: SireneEtablissement) {
		return crypto.createHash('md5').update(JSON.stringify(sireneEtablissement)).digest('hex');
	}

	public hashSireneUniteLegale(sireneUniteLegale: SireneUniteLegale) {
		return crypto.createHash('md5').update(JSON.stringify(sireneUniteLegale)).digest('hex');
	}

	// public async genericCheckForHash(data, { entity, key, hasher }) {}
	public async upsertSireneEtablissementOrSireneUniteLegale<
		T extends SireneUniteLegale | SireneEtablissement
	>({
		etablissements,
		classe,
		keyField,
		hasher,
		mapper,
	}: {
		etablissements: EtablissementApi[];
		classe: EntityTarget<T>;
		keyField: (T extends SireneEtablissement
			? 'siret'
			: T extends SireneUniteLegale
			? 'siren'
			: never) &
			keyof T;
		hasher: (entity: T) => string;
		mapper: (key: string, apiItem: EtablissementApi) => T;
	}): Promise<[DeepPartial<T>[], DeepPartial<T>[]]> {
		const apiMap = etablissements.reduce(
			(map, etablissement: EtablissementApi) =>
				map.set(etablissement[keyField], mapper(etablissement[keyField], etablissement)),
			<Map<string, T>>new Map()
		);

		const ids = Array.from(apiMap.keys());

		const where = {
			[keyField]: In(ids),
		} as FindOptionsWhere<T>;
		const dbItems = await AppDataSource.getRepository(classe).find({
			where,
		});

		const dbMap = dbItems.reduce(
			(map, dbItem) => map.set(`${dbItem[keyField]}`, dbItem),
			<Map<string, T>>new Map()
		);

		const [itemsToUpsert, itemsToUpdateHash] = Array.from(apiMap.entries()).reduce(
			([toUpsert, toHash]: [DeepPartial<T>[], DeepPartial<T>[]], [key, apiItem]) => {
				const dbItem = dbMap.get(key);

				const hashedNew = apiItem.hash as string;

				// if the Item is not in the DB
				// or hashes aren't the same
				// we upsert the data
				if (typeof dbItem === 'undefined' || hasher(dbItem) !== hashedNew) {
					toUpsert.push(apiItem);
					return [toUpsert, toHash];
				}

				// if the DB and the payload are the same, but the hash is not in the DB,
				// we update only the hash
				if (dbItem.hash === null) {
					toHash.push({ [keyField]: key, hash: hashedNew } as unknown as DeepPartial<T>);
					return [toUpsert, toHash];
				}

				// the Item exists, has a hash, and is the same as the new one
				// nothing to be done here
				return [toUpsert, toHash];
			},
			[[], []]
		);

		// const toUpsert = sireneUnitesLegalesToUpsert.concat(sireneUnitesLegalesToUpdateHash);
		const toUpsert = itemsToUpsert.concat(itemsToUpdateHash) as QueryDeepPartialEntity<T>;

		await AppDataSource.getRepository(classe).upsert(toUpsert, {
			conflictPaths: [keyField],
			skipUpdateIfNoValuesChanged: true,
		});

		return [itemsToUpsert, itemsToUpdateHash];
	}

	public async handleBatchSireneUpdate(etablissements: EtablissementApi[]): Promise<void> {
		// UniteLegale may not have been updated because we call SIRENE /sirets route,
		// but we'll try to update the database anyway
		// Should we need to use /sirens route instead to only get updated UniteLegale?

		try {
			await this.upsertSireneEtablissementOrSireneUniteLegale<SireneUniteLegale>({
				etablissements,
				classe: SireneUniteLegale,
				keyField: 'siren',
				hasher: (d) => this.hashSireneUniteLegale(d),
				mapper: (s, e) => this.etablissementApiToSireneUniteLegale(s, e),
			});
		} catch (e) {
			console.error('InseeUpdateService, upsertUniteLegale:', e);
			return;
		}

		let sireneEtablissementsToUpsert;
		try {
			[sireneEtablissementsToUpsert] =
				await this.upsertSireneEtablissementOrSireneUniteLegale<SireneEtablissement>({
					etablissements,
					classe: SireneEtablissement,
					keyField: 'siret',
					hasher: (d) => this.hashSireneEtablissement(d),
					mapper: (s, e) => this.etablissementApiToSireneEtablissement(s, e),
				});
		} catch (e) {
			console.error('InseeUpdateService, upsertEtablissement:', e);
			return;
		}

		const upsertedSireneEtablissementSirets = sireneEtablissementsToUpsert
			.filter(({ siret }) => !!siret)
			.map(({ siret }) => siret) as string[];

		if (!upsertedSireneEtablissementSirets.length) {
			return;
		}

		try {
			// update etablissement table
			await upsertInEtablissementFromSireneEtablissements(upsertedSireneEtablissementSirets);

			await Promise.all([
				// update entreprise table
				updateEntrepriseFromSireneEtablissements(upsertedSireneEtablissementSirets),

				// update ter table
				upsertInTerFromSireneEtablissements(upsertedSireneEtablissementSirets),
			]);

			// TODO
			// our "reliable" geocoding comes from a file that comes ~20 days after the monthly SIRENE release
			// which means that when we're getting daily SIRENE updates, there's two possibilities :
			// - the SIRET was already in the database and is simply being updated; in that case, we already have
			// correctly geocoded it when we've updated the geoloc file, so updating its coordinates would bring no benefit.
			// - the SIRET is new in the database; in that case, we will not find it in the last geoloc file we got,
			// so trying to update the coordinates would bring no benefit either
			//
			// This means that we don't need to do the following.
			//
			// Likewise, we need to handle QPVs when we update the coordinates, but doing so now would be useless.
			// await updateEtablissementsGeoloc(upsertedSireneEtablissementSirets).then(() =>
			//   // update qpvs
			//   updateTERQpv(upsertedSireneEtablissementSirets)
			// );
		} catch (e) {
			console.error('InseeUpdateService, sequentialUpdate:', e);
		}
	}

	public async handleBatchSireneSuccession(liensSuccession: LienSuccessionApi[]): Promise<void> {
		try {
			await AppDataSource.getRepository(LienSuccessionTemp).insert(liensSuccession);

			await upsertDedupedLienSuccession();

			await AppDataSource.getRepository(LienSuccessionTemp).clear();
		} catch (e: unknown) {
			console.info(e);
		}
	}

	public async updateGeolocalisation() {
		const countResult = await AppDataSource.manager.query(
			`
			SELECT count(*)
			FROM territory_etablissement_reference
			`
		);
		const count = Number(countResult[0].count);
		console.info('Will start updating geolocation for all SIRETs in TER:', count);

		for (let i = 0; i <= count; i += 500000) {
			const terSirets = (
				await AppDataSource.getRepository(TerritoryEtablissementReference).find({
					select: {
						siret: true,
					},
					take: 500000,
					skip: i,
				})
			).map(({ siret }) => siret);

			const terCount = terSirets.length;
			console.info(
				`Will start updating geolocation for loop #${i / 500000}, for ${terCount} SIRETs`
			);

			while (terSirets.length > 0) {
				const sirets = terSirets.splice(0, 1000);
				await AppDataSource.manager.query(
					`
					UPDATE "etablissement" E
					SET
						longitude = GE.x_longitude,
						latitude = GE.y_latitude
					FROM "geoloc_entreprise" GE
					WHERE GE.siret = E.siret AND GE.siret = ANY($1)
					`,
					[sirets]
				);
			}

			console.info(`Finished updating geolocation for loop #${i / 500000}, for ${terCount} SIRETs`);
		}

		console.info('Finished updating geolocation for all SIRETs in TER:', count);
	}

	public async updateDateFermeture() {
		const countResult = await AppDataSource.manager.query(
			`
			SELECT count(*)
			FROM territory_etablissement_reference
			WHERE etat_administratif = 'F'
			`
		);
		const count = Number(countResult[0].count);

		console.info('Will start updating dateFermeture for all SIRETs in TER:', count);

		for (let i = 0; i <= count; i += 500000) {
			const terSirets = (
				await AppDataSource.getRepository(TerritoryEtablissementReference).find({
					select: {
						siret: true,
					},
					where: {
						etatAdministratif: 'F',
					},
					take: 500000,
					skip: i,
				})
			).map(({ siret }) => siret);

			const terCount = terSirets.length;

			console.info(
				`Will start updating dateFermeture for loop #${i / 500000}, for ${terCount} SIRETs`
			);

			while (terSirets.length > 0) {
				const sirets = terSirets.splice(0, 1000);
				await AppDataSource.manager.query(
					`
					UPDATE "etablissement" E
					SET date_fermeture = S.date_debut
					FROM "sirene_etablissement" S
					WHERE S.siret = E.siret AND S.siret = ANY($1)
					`,
					[sirets]
				);
			}

			console.info(
				`Finished updating dateFermeture for loop #${i / 500000}, for ${terCount} SIRETs`
			);
		}

		console.info('Finished updating dateFermeture for all SIRETs in TER:', count);
	}

	public async getSireneUpdateForDates({
		start,
		end,
		cursor: nextCursor = '*',
		loop = 0,
		extraParams = [],
	}: {
		start?: string;
		end?: string;
		cursor?: string;
		loop?: number;
		extraParams?: string[];
	}): Promise<void> {
		const amount = 1000;
		const pause = 2500;

		const siretEndpoint = 'siret';

		const params: string[] = [];

		if (start || end) {
			params.push(
				encodeURIComponent(`dateDernierTraitementEtablissement:[${start ?? '*'} TO ${end ?? '*'}]`)
			);
		}

		if (extraParams?.length) {
			// TODO: handle URL encoding of params
			params.push(...extraParams.map((param) => encodeURIComponent(param)));
		}

		if (params.length === 0) {
			throw new Error('getSireneUpdateForDates: no start/end dates nor extra params passed');
		}

		const apiUrl = (cursor: string) =>
			`${process.env.INSEE_API}/${siretEndpoint}?nombre=${amount}&curseur=${encodeURIComponent(
				cursor
			)}&q=${params.join('%20AND%20')}`;

		const headers = {
			Authorization: `Bearer ${process.env.INSEE_ACCESS_TOKEN}`,
		};

		let lastTotal = 0;
		let processedEtablissements = 0;
		let previousCursor = '';
		let lastEtablissement = null;

		await createTemporaryTerritoryToCommunesTable();

		while (previousCursor !== nextCursor) {
			let timeout: Promise<void> | null = null;
			let hasEnoughTimeElapsed = true;

			// await only if a request has been made
			if (nextCursor !== '*') {
				hasEnoughTimeElapsed = false;

				timeout = new Promise<void>((resolve) =>
					setTimeout(() => {
						hasEnoughTimeElapsed = true;
						return resolve();
					}, pause)
				);
			}

			try {
				const url = apiUrl(nextCursor);
				const response = await httpGet<SireneApiEtablissements>(url, headers);

				if (!response.__success) {
					// TODO better support needed to resume on error
					throw response;
				}

				const { header, etablissements } = response.data;
				lastTotal = header.total;
				loop += 1;
				lastEtablissement = etablissements.slice(-1)[0];

				console.info(
					new Date().toISOString(),
					`About to start processing ${header.nombre} SIRETs out of ${lastTotal} (loop #${loop})`
				);

				await this.handleBatchSireneUpdate(etablissements);

				previousCursor = header.curseur;
				nextCursor = header.curseurSuivant;
				processedEtablissements += etablissements.length;
			} catch (e) {
				console.info(`Could not get SIRENE updates`, e);
				console.info(
					`Processed until ${lastTotal} (loop #${loop}), last cursor was: ${nextCursor}`
				);
				console.info(
					`Last gotten Etablissement example:
					${lastEtablissement?.siret},
					${lastEtablissement?.dateDernierTraitementEtablissement}`
				);
				previousCursor = '';
				nextCursor = '';
			}

			if (timeout && !hasEnoughTimeElapsed) {
				await new Promise<void>((res) => (timeout ? timeout.then(() => res()) : res()));
			}
		}

		console.info(
			`Stopped processing SIRENE updates, processed ${processedEtablissements} out of ${lastTotal} SIRETs`
		);

		await dropTemporaryTerritoryToCommunesTable();

		return;
	}

	public async getSireneSuccessionUpdateForDates({
		start,
		end,
		cursor: nextCursor = '*',
		loop = 0,
		extraParams = [],
	}: {
		start?: string;
		end?: string;
		cursor?: string;
		loop?: number;
		extraParams?: string[];
	}): Promise<void> {
		const amount = 1000;
		const pause = 2500;

		const siretEndpoint = 'siret/liensSuccession';

		const dateDernierTraitementLienSuccession = encodeURIComponent(
			`dateDernierTraitementLienSuccession:[${start ?? '*'} TO ${end ?? '*'}]`
		);
		const extraParamsString = extraParams?.length ? ` AND ${extraParams.join(' AND ')}` : '';
		const apiUrl = (cursor: string) =>
			`${process.env.INSEE_API}/${siretEndpoint}?nombre=${amount}&curseur=${encodeURIComponent(
				cursor
			)}&q=${dateDernierTraitementLienSuccession}${extraParamsString}`;

		const headers = {
			Authorization: `Bearer ${process.env.INSEE_ACCESS_TOKEN}`,
		};

		let lastTotal = 0;
		let processedLienSuccession = 0;
		let previousCursor = '';
		let lastLien = null;

		while (previousCursor !== nextCursor) {
			let timeout: Promise<void> | null = null;
			let hasEnoughTimeElapsed = true;

			// await only if a request has been made
			if (nextCursor !== '*') {
				hasEnoughTimeElapsed = false;

				timeout = new Promise<void>((resolve) =>
					setTimeout(() => {
						hasEnoughTimeElapsed = true;
						return resolve();
					}, pause)
				);
			}

			try {
				const url = apiUrl(nextCursor);
				const response = await httpGet<SireneApiLiensSuccession>(url, headers);

				if (!response.__success) {
					// TODO better support needed to resume on error
					throw response;
				}

				const { header, liensSuccession } = response.data;
				lastTotal = header.total;
				loop += 1;
				lastLien = liensSuccession.slice(-1)[0];

				console.info(
					new Date().toISOString(),
					`About to start processing ${header.nombre} successions liens out of ${lastTotal} (loop #${loop})`
				);

				await this.handleBatchSireneSuccession(liensSuccession);

				previousCursor = header.curseur;
				nextCursor = header.curseurSuivant;
				processedLienSuccession += liensSuccession.length;
			} catch (e) {
				console.info(`Could not get SIRENE updates`, e);
				console.info(
					`Processed until ${lastTotal} (loop #${loop}), last cursor was: ${nextCursor}`
				);
				console.info(
					`Last gotten Etablissement example:
					${lastLien?.siretEtablissementPredecesseur},
					${lastLien?.siretEtablissementSuccesseur},
					${lastLien?.dateDernierTraitementLienSuccession}`
				);
				previousCursor = '';
				nextCursor = '';
			}

			if (timeout && !hasEnoughTimeElapsed) {
				await new Promise<void>((res) => (timeout ? timeout.then(() => res()) : res()));
			}
		}

		console.info(
			`Stopped processing SIRENE updates, processed ${processedLienSuccession} out of ${lastTotal} SIRETs`
		);

		return;
	}

	public async adressesToBAN() {
		const sireneEtablissementsWithValidAdressesQuery = `
			FROM sirene_etablissement SE
			WHERE NOT ban_adresse
			AND geo_score IS NOT NULL
			AND geo_adresse IS NOT NULL
			AND geo_score > 0.6
			AND geo_adresse ILIKE '%' || code_postal_etablissement || '%'
			`;
		const countResult = await AppDataSource.manager.query(
			`
			SELECT count(*)
			${sireneEtablissementsWithValidAdressesQuery}
			`
		);
		const count = Number(countResult[0].count);

		console.info(
			'Will start updating BAN adresse in etablissement for all relevant SIRETs in sirene_etablissement:',
			count
		);

		for (let i = 0; i <= count; i += 500000) {
			const etablissementsSirets = (
				await AppDataSource.manager.query(
					`SELECT siret ${sireneEtablissementsWithValidAdressesQuery} limit 500000 offset ${i}`
				)
			).map(({ siret }: { siret: string }) => siret);

			const etablissementsCount = etablissementsSirets.length;

			console.info(
				`Will start updating BAN adresse of etablissements for loop #${
					i / 500000
				}, for ${etablissementsCount} SIRETs`
			);

			while (etablissementsSirets.length > 0) {
				const sirets = etablissementsSirets.splice(0, 1000);
				await AppDataSource.manager.query(
					`
					UPDATE sirene_etablissement SE
					SET ban_adresse = true
					WHERE SE.siret = ANY($1)
					`,
					[sirets]
				);

				await AppDataSource.manager.query(
					`
					UPDATE etablissement E
					SET adresse_complete = SE.geo_adresse
					FROM sirene_etablissement SE
					WHERE SE.siret = E.siret
					AND SE.siret = ANY($1)
					`,
					[sirets]
				);

				await AppDataSource.manager.query(
					`
					UPDATE entreprise EN
					SET adresse = E.adresse_complete
					FROM etablissement E
					WHERE E.siret = EN.siret
					AND E.siret = ANY($1)
					`,
					[sirets]
				);

				await AppDataSource.manager.query(
					`
					UPDATE territory_etablissement_reference TER
					SET adresse_complete = E.adresse_complete
					FROM etablissement E
					WHERE E.siret = TER.siret
					AND E.siret = ANY($1)
					`,
					[sirets]
				);
			}

			console.info(
				`Finished updating BAN adresse of etablissements for loop #${
					i / 500000
				}, for ${etablissementsCount} SIRETs`
			);
		}

		console.info(
			'Finished updating BAN adresse in etablissement for all relevant SIRETs in sirene_etablissement:',
			count
		);
	}

	public async updateESSTerritory(territoryId: Territory['id']) {
		return AppDataSource.manager.query(territoireService.updateESSForTerritory, [territoryId]);
	}

	public async updateESS() {
		const countResult = await AppDataSource.manager.query(
			`
			SELECT count(*)
			FROM territory_etablissement_reference
			WHERE ess = 'N'
			`
		);
		const count = Number(countResult[0].count);

		console.info('Will start updating ess for all SIRETs in TER:', count);

		for (let i = 0; i <= count; i += 500000) {
			const terSirets = (
				await AppDataSource.getRepository(TerritoryEtablissementReference).find({
					select: {
						siret: true,
					},
					where: {
						ess: false,
					},
					take: 500000,
					skip: i,
				})
			).map(({ siret }) => siret);

			const terCount = terSirets.length;

			console.info(`Will start updating ESS for loop #${i / 500000}, for ${terCount} SIRETs`);

			while (terSirets.length > 0) {
				const sirets = terSirets.splice(0, 1000);
				await AppDataSource.manager.query(territoireService.updateESSForSirets, [sirets]);
			}

			console.info(`Finished updating ESS for loop #${i / 500000}, for ${terCount} SIRETs`);
		}

		console.info('Finished updating ESS for all SIRETs in TER:', count);
	}
}

const inseeUpdateService = InseeUpdateService.getInstance();

export default inseeUpdateService;
