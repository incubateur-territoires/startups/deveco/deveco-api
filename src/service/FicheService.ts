import { FindOptionsRelations, FindOptionsWhere } from 'typeorm';
import { StatusCodes } from 'http-status-codes';

import AppDataSource from '../data-source';
import { Demande } from '../entity/Demande';
import { DevEco } from '../entity/DevEco';
import { Echange } from '../entity/Echange';
import { Fiche } from '../entity/Fiche';
import { FicheView, toView } from '../view/FicheView';
import { isSiret } from '../lib/regexp';
import entrepriseService from '../service/EntrepriseService';
import { Territory } from '../entity/Territory';
import { Entite } from '../entity/Entite';
import { DevecoContext } from '../middleware/AuthMiddleware';

export type Conditions<T> = FindOptionsWhere<T>[] | FindOptionsWhere<T>;

const relationsLight: FindOptionsRelations<Fiche> = {
	entite: {
		entreprise: true,
		particulier: {
			qpv: true,
		},
	},
	createur: { account: true },
	auteurModification: { account: true },
};

const relationsFull: FindOptionsRelations<Fiche> = {
	...relationsLight,
	demandes: true,
	echanges: {
		createur: {
			account: true,
		},
		auteurModification: {
			account: true,
		},
	},
	rappels: true,
	entite: {
		particulier: {
			qpv: true,
		},
		entreprise: {
			exercices: true,
		},
		contacts: {
			particulier: true,
		},
		proprietes: true,
		etablissementCree: {
			etablissement: true,
		},
	},
};

export type Criteria = {
	recherche: string;
	activites: string[];
	zones: string[];
	demandes: string[];
	local: number;
	territoire: number;
	mots: string[];
	cp: string;
	rue: string;
	qpvs: string[];
	suivi: string;
	ids?: number[];
};

export type Suivi = 'tous' | 'createurs' | 'etablissements';

const isDirection = (orderDirection?: string): orderDirection is 'ASC' | 'DESC' =>
	!!orderDirection && ['ASC', 'DESC'].includes(orderDirection);

class FicheService {
	private static instance: FicheService;

	public static getInstance(): FicheService {
		if (!FicheService.instance) {
			FicheService.instance = new FicheService();
		}

		return FicheService.instance;
	}

	public async loadFiche(where: Conditions<Fiche>, light?: boolean): Promise<Fiche> {
		const fiche = await AppDataSource.getRepository(Fiche).findOneOrFail({
			relations: light ? relationsLight : relationsFull,
			where,
		});

		return fiche;
	}

	public async ficheToView(where: Conditions<Fiche>, light?: boolean): Promise<FicheView> {
		const fiche = await this.loadFiche(where, light);
		return toView(fiche.entite, fiche);
	}

	public async ficheToViewWithFiche(where: FindOptionsWhere<Fiche>): Promise<[FicheView, Fiche]> {
		const fiche = await this.loadFiche(where);

		return [toView(fiche.entite, fiche), fiche];
	}

	public async search({
		criteria,
		orderBy,
		orderDirection,
		page,
		resultsPerPage,
	}: {
		criteria: Criteria;
		orderBy?: string;
		orderDirection?: string;
		page?: number;
		resultsPerPage?: number;
	}): Promise<[Entite['id'][], number]> {
		const { recherche, activites, zones, demandes, mots, local, territoire, cp, rue, qpvs, suivi } =
			criteria;

		const selects = ['SELECT entite.id'];
		const froms = [`FROM entite`];
		const defaultWheres = [
			`entite.territoire_id = ${territoire}`,
			`entite.particulier_id IS NOT NULL`,
		];
		const joins: Record<string, string> = {
			fiche: 'INNER JOIN fiche ON fiche.entite_id = entite.id',
		};

		const withs: string[] = [];
		const conditions: string[] = [];

		if (recherche) {
			// search in Particulier: Nom, Prénom and Future Enseigne
			joins.particulier = 'LEFT JOIN particulier ON entite.particulier_id = particulier.id';
			const or = [];
			or.push(`lower(particulier.nom) ILIKE '%${recherche}%'`);
			or.push(`lower(particulier.prenom) ILIKE '%${recherche}%'`);
			or.push(`entite.future_enseigne ILIKE '%${recherche}%'`);
			conditions.push('( ' + or.join(' OR ') + ' )');
		}

		if (local) {
			joins.local = `LEFT JOIN proprietaire ON proprietaire.entite_id = entite.id`;
			joins.local = `LEFT JOIN local ON proprietaire.local_id = local.id`;
			conditions.push(`local.id IS DISTINCT FROM ${local}`);
		}

		if (activites?.length) {
			conditions.push(
				`ARRAY[${activites
					.map((a) => `'${a}'`)
					.join(',')}] && string_to_array(entite.activites_reelles, ',')`
			);
		}

		if (zones?.length) {
			conditions.push(
				`ARRAY[${zones
					.map((a) => `'${a}'`)
					.join(',')}] && string_to_array(entite.entreprise_localisations, ',')`
			);
		}

		if (mots?.length) {
			conditions.push(
				`ARRAY[${mots.map((a) => `'${a}'`).join(',')}] && string_to_array(entite.mots_cles, ',')`
			);
		}

		if (demandes?.length) {
			const demandesFormatted = demandes.map((d) => `'${d.replace("'", "''")}'`).join(',');
			conditions.push(
				`EXISTS
					(SELECT demande.id
					 FROM demande
					 WHERE demande.fiche_id = fiche.id
					 AND (demande.cloture = false OR demande.cloture is null)
					 AND demande.type_demande IN (${demandesFormatted})
					)`
			);
		}

		// normalize string to remove accents for search (the data we use is normalized, it would seem)
		const normalizedRue = rue
			.replace("'", ' ')
			.normalize('NFD')
			.replace(/\p{Diacritic}/gu, '');
		if (cp && rue) {
			joins.particulier = 'LEFT JOIN particulier ON entite.particulier_id = particulier.id';
			conditions.push(
				`(particulier.adresse ILIKE '%${normalizedRue}%' AND particulier.code_postal ILIKE '%${cp}%')`
			);
		}

		if (qpvs?.length) {
			joins.particulier = 'LEFT JOIN particulier ON entite.particulier_id = particulier.id';
			conditions.push(`particulier.qpv_id IN (${qpvs.map((q) => `'${q}'`).join(',')})`);
		}

		if (suivi === 'createurs') {
			conditions.push(`entite.enseigne_id IS NULL`);
		} else if (suivi === 'etablissements') {
			conditions.push(`entite.enseigne_id IS NOT NULL`);
		}

		const pagination = [];
		if (page && resultsPerPage) {
			const skip = (page - 1) * resultsPerPage;
			pagination.push(`OFFSET ${skip}`);
			pagination.push(`LIMIT ${resultsPerPage}`);
		}

		const direction: 'ASC' | 'DESC' = isDirection(orderDirection) ? orderDirection : 'DESC';
		const nulls: 'NULLS FIRST' | 'NULLS LAST' = 'NULLS LAST';

		let orderByDirective: string;
		switch (orderBy) {
			case 'AZ': {
				joins.particulier = 'LEFT JOIN particulier ON entite.particulier_id = particulier.id';
				orderByDirective = `ORDER BY lower(particulier.nom) ${direction} ${nulls}, lower(particulier.prenom) ${direction} ${nulls}`;
				break;
			}
			case 'CO':
			default: {
				withs.push(`
					view_order(id, date_visualisation) as (
						SELECT E.id, MAX(EL.date) as date_visualisation
						FROM event_log EL
						JOIN entite E on E.id = EL.entity_id
						JOIN territory T on T.id = E.territoire_id and T.id = ${territoire}
						WHERE
							EL.action = 'VIEW'
							AND EL.entity_type = 'FICHE_PP'
						GROUP BY E.id
					)
				`);

				joins.viewOrder = `LEFT JOIN view_order ON view_order.id = entite.id`;

				orderByDirective = `ORDER BY view_order.date_visualisation ${direction} ${nulls}`;
				break;
			}
		}

		const baseQuery = [
			froms.join(' '),
			Object.values(joins).join(' '),
			`WHERE ${defaultWheres.join(' AND ')}`,
			conditions.length ? `AND ${conditions.join(' AND ')}` : '',
		];

		const countQuery = [
			withs.length ? `WITH ${withs.join(',')}` : '',
			'SELECT count(entite.id)',
			...baseQuery,
		].join(' ');

		const itemsQuery = [
			withs.length ? `WITH ${withs.join(',')}` : '',
			...selects,
			...baseQuery,
			orderByDirective,
			...pagination,
		].join(' ');

		const count = await AppDataSource.query(countQuery);

		const items = await AppDataSource.query(itemsQuery);

		return [items.map(({ id }: { id: Entite['id'] }) => id), Number(count[0].count)];
	}

	public async createDemandesIfNecessary(themes: string[], ficheId: Fiche['id']) {
		if (themes.length <= 0) {
			return;
		}

		const repository = AppDataSource.getRepository(Demande);
		const demandes = themes.map((loc) => loc.toLocaleLowerCase().replace("'", "''"));

		const result = await AppDataSource.createQueryBuilder()
			.select('demande')
			.from(Demande, 'demande')
			.where('LOWER(type_demande) IN (:...demandes)', {
				demandes,
			})
			.andWhere('fiche_id = :ficheId', { ficheId })
			.getMany();

		for (const typeDemande of demandes) {
			if (
				!result
					.map((demande) => demande.typeDemande.toLocaleLowerCase())
					.includes(typeDemande.toLocaleLowerCase())
			) {
				await repository.insert({ typeDemande, fiche: { id: ficheId } });
			}
		}
	}

	public async removeDemandesIfNecessary(ficheId: Fiche['id']) {
		const demandes = await AppDataSource.getRepository(Demande).find({
			where: { fiche: { id: ficheId } },
		});
		const types = demandes.map(({ typeDemande }) => typeDemande);

		const echangesTypes = (
			await AppDataSource.getRepository(Echange).find({ where: { fiche: { id: ficheId } } })
		)
			.map((e) => e.themes)
			.reduce((acc, themes) => [...acc, ...themes], []);

		for (const type_ of types) {
			if (!echangesTypes.includes(type_)) {
				await AppDataSource.getRepository(Demande).delete({
					typeDemande: type_,
					fiche: { id: ficheId },
				});
			}
		}
	}

	public async getOrCreateFicheForSiret({
		deveco,
		siret,
	}: {
		deveco: DevEco;
		siret: string;
	}): Promise<Fiche> {
		const fiche = await AppDataSource.getRepository(Fiche).findOne({
			where: {
				entite: {
					entreprise: {
						siret,
					},
				},
				territoire: {
					id: deveco.territory.id,
				},
			},
			relations: {
				entite: {
					entreprise: true,
					particulier: true,
				},
			},
		});

		if (fiche) {
			return fiche;
		}

		return this.createFicheForSiret({
			createur: deveco,
			fuzzy: siret,
			skipApiEntreprise: false,
		});
	}

	public async getOrCreateFicheFromRequest(ctx: DevecoContext): Promise<Fiche> {
		const deveco = ctx.state.deveco;

		if (!deveco) {
			return ctx.throw(StatusCodes.UNAUTHORIZED);
		}

		const territoireId = deveco.territory.id;

		const {
			ficheId: ficheIdAsString = '',
			siret,
		}: {
			ficheId?: string;
			siret?: string;
		} = ctx.query;

		if (ficheIdAsString && siret) {
			return ctx.throw(
				StatusCodes.BAD_REQUEST,
				'Il faut préciser seulement ficheId OU siret, mais pas les deux.'
			);
		}

		if (ficheIdAsString) {
			const ficheId = parseInt(ficheIdAsString);
			const fiche = await AppDataSource.getRepository(Fiche).findOne({
				where: {
					id: ficheId,
					territoire: {
						id: territoireId,
					},
				},
				relations: {
					entite: {
						entreprise: true,
						particulier: true,
					},
				},
			});

			if (!fiche) {
				return ctx.throw(StatusCodes.NOT_FOUND, `Fiche ${ficheId} non trouvée`);
			}

			return fiche;
		}

		if (siret) {
			const fiche = await this.getOrCreateFicheForSiret({ deveco, siret });

			if (fiche) {
				return fiche;
			}

			console.error('Could not create Fiche', { deveco, siret });
			return ctx.throw(`Impossible de créer la fiche pour le siret ${siret}`);
		}

		return ctx.throw(StatusCodes.BAD_REQUEST, `Mauvaise requête : ni ficheId ni siret`);
	}

	public async createFicheForSiret({
		createur,
		fuzzy,
		skipApiEntreprise,
	}: {
		createur: DevEco;
		fuzzy: string;
		skipApiEntreprise?: boolean;
	}): Promise<Fiche> {
		if (!isSiret(fuzzy)) {
			throw new Error(`${fuzzy} is not a valid SIRET.`);
		}
		const siret = fuzzy.replaceAll(/\s/g, '');

		const territoire = createur.territory;
		const ficheRepository = AppDataSource.getRepository(Fiche);
		const now = new Date();

		const entite = await entrepriseService.getOrCreateEntrepriseAndEntite({
			createur,
			siret,
			skipApiEntreprise,
		});
		const existing = await ficheRepository.findOne({
			where: {
				territoire: {
					id: territoire.id,
				},
				entite: {
					id: entite.id,
				},
			},
			relations: {
				entite: {
					entreprise: true,
					particulier: true,
				},
			},
		});
		if (existing) {
			return existing;
		}
		const fiche = ficheRepository.create({
			territoire,
			createur,
			entite,
			dateModification: now,
			auteurModification: createur,
			createdAt: now,
			updatedAt: now,
		});

		const { id } = await ficheRepository.save(fiche);

		const f = await AppDataSource.getRepository(Fiche).findOneOrFail({
			where: {
				id,
			},
			relations: {
				entite: {
					entreprise: true,
					particulier: true,
				},
			},
		});

		return f;
	}

	public async touchFiche({ ficheId, auteur }: { ficheId: number; auteur: DevEco }): Promise<void> {
		const now = new Date();
		await AppDataSource.getRepository(Fiche).update(ficheId, {
			updatedAt: now,
			dateModification: now, // TODO deprecated?!
			auteurModification: auteur,
		});
	}

	public async getExistingDemandes(territoire: Territory): Promise<string[]> {
		const allDemandes = await AppDataSource.getRepository(Demande)
			.createQueryBuilder('demande')
			.innerJoin('demande.fiche', 'fiche')
			.where(`fiche.territoire_id = ${territoire.id}`)
			.andWhere(`(demande.cloture = false or demande.cloture is null)`)
			.select('distinct(demande.type_demande)')
			.orderBy('demande.type_demande', 'ASC')
			.getRawMany();

		const demandes = allDemandes.map((d) => d.type_demande);

		return demandes;
	}

	public parseSuivi(suivi?: string): Suivi {
		if (!suivi) {
			return 'tous';
		}
		if (['tous', 'createurs', 'etablissements'].includes(suivi)) {
			return suivi as Suivi;
		}
		return 'tous';
	}
}

const ficheService = FicheService.getInstance();

export default ficheService;
