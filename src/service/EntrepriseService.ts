import { In, Like } from 'typeorm';

import entiteService from './EntiteService';
import eventLogService from './EventLogService';

import AppDataSource from '../data-source';
import { Entreprise, MandataireSocial } from '../entity/Entreprise';
import { Exercice } from '../entity/Exercice';
import { DevEco } from '../entity/DevEco';
import { Entite } from '../entity/Entite';
import { Territory } from '../entity/Territory';
import { Etablissement } from '../entity/Etablissement';
import contactService from '../service/ContactService';
import inseeService, { Infos, relationsForToView } from '../service/InseeService';
import apiEntrepriseService, { ApiEntrepriseExercice } from '../service/ApiEntrepriseService';
import territoireService from '../service/TerritoireService';
import { isSiret } from '../lib/regexp';
import { anneeToDateEffectifs, trancheToEffectifs } from '../lib/trancheEffectifs';
import { fullCodeToFormeJuridique } from '../lib/formeJuridique';
import { TerritoryEtablissementReference } from '../entity/TerritoryEtablissementRef';
import { EventLogActionType } from '../entity/EventLog';
import { getLogEntityType } from '../lib/utils';
import { LienSuccession } from '../entity/LienSuccession';
import { SireneUniteLegale } from '../entity/SireneUniteLegale';

export type EtablissementSimple = {
	entreprise: Entreprise;
	infos: Infos;
};

export type EtablissementsFreres = Array<EtablissementSimple>;

export type EtablissementHistory = {
	predecesseurs: Array<EtablissementSimple>;
	successeurs: Array<EtablissementSimple>;
};

class EntrepriseService {
	private static instance: EntrepriseService;

	public static getInstance(): EntrepriseService {
		if (!EntrepriseService.instance) {
			EntrepriseService.instance = new EntrepriseService();
		}

		return EntrepriseService.instance;
	}

	public async getOrCreateEntreprise(
		fuzzy: string,
		skipApiEntreprise?: boolean
	): Promise<Entreprise> {
		if (!isSiret(fuzzy)) {
			throw new Error(`${fuzzy} is not a valid SIRET.`);
		}

		const siret = fuzzy.replaceAll(/\s/g, '');

		const existing = await AppDataSource.getRepository(Entreprise).findOne({
			where: { siret },
			relations: {
				exercices: true,
				etablissement: true,
			},
		});

		if (existing) {
			if (!skipApiEntreprise && !existing.lastApiUpdate) {
				return this.updateEntrepriseWithApiEntrepriseData({ entreprise: existing });
			}
			return existing;
		}

		const etablissement = await inseeService.searchBySiret(siret);

		if (!etablissement) {
			throw new Error('Could not find SIRET');
		}

		const entreprisePayload = this.etablissementToEntreprise(etablissement);

		const entreprise = await AppDataSource.getRepository(Entreprise).save(entreprisePayload);

		if (skipApiEntreprise) {
			return { ...entreprise, etablissement };
		}

		return this.updateEntrepriseWithApiEntrepriseData({ entreprise });
	}

	public etablissementToEntreprise(etablissement: Etablissement): Entreprise {
		// TODO should we do that using the raw SQL that is used in the SIRENE update code?
		const {
			siret,
			nomPublic,
			adresseComplete,
			trancheEffectifs,
			anneeEffectifs,
			codeNaf,
			libelleNaf,
			libelleCategorieNaf,
			etatAdministratif,
			longitude,
			latitude,
			codeCommune,
			dateCreation,
			dateFermeture,
			categorieJuridique,
			enseigne,
			siegeSocial,
		} = etablissement;

		// TODO start using entreprise.etablissement["property"] instead
		return {
			etablissement,
			siret,
			longitude,
			latitude,
			dateCreation,
			dateFermeture,
			etatAdministratif,
			nom: nomPublic?.toLocaleUpperCase() ?? '',
			enseigne: enseigne?.toLocaleUpperCase() ?? null,
			adresse: adresseComplete?.toLocaleUpperCase() ?? '',
			codeCommune: codeCommune ?? '',
			codeNaf,
			libelleNaf,
			libelleCategorieNaf,
			effectifs: trancheToEffectifs(trancheEffectifs || ''),
			dateEffectifs: anneeToDateEffectifs(anneeEffectifs) ?? null,
			exercices: [],
			formeJuridique: fullCodeToFormeJuridique(`${categorieJuridique}`) ?? '',
			mandatairesSociaux: [],
			lastApiUpdate: null,
			siegeSocial,
		};
	}

	public async updateEntrepriseWithApiEntrepriseData({
		entreprise,
	}: {
		entreprise: Entreprise;
	}): Promise<Entreprise> {
		const siret = entreprise.siret;
		const siren = entreprise.etablissement.siren;

		let apiEntrepriseEntreprise = null;
		try {
			// Check if the Unité légale is an Entrepreneur Individuel, in which case we'll create the "mandataire" instead of requesting it from the API
			const uniteLegale = await AppDataSource.getRepository(SireneUniteLegale).findOneOrFail({
				where: {
					siren,
				},
				select: {
					categorieJuridiqueUniteLegale: true,
					prenom1UniteLegale: true,
					prenom2UniteLegale: true,
					prenom3UniteLegale: true,
					prenom4UniteLegale: true,
					prenomUsuelUniteLegale: true,
					nomUniteLegale: true,
					nomUsageUniteLegale: true,
				},
			});

			let mandatairePersonnePhysique: MandataireSocial | undefined = undefined;

			if (
				uniteLegale.categorieJuridiqueUniteLegale &&
				uniteLegale.categorieJuridiqueUniteLegale > 999 &&
				uniteLegale.categorieJuridiqueUniteLegale < 2000
			) {
				const nom = uniteLegale.nomUsageUniteLegale
					? `${uniteLegale.nomUsageUniteLegale}` +
					  (uniteLegale.nomUniteLegale ? ` (${uniteLegale.nomUniteLegale})` : '')
					: uniteLegale.nomUniteLegale ?? '';
				const prenoms = [
					uniteLegale.prenom1UniteLegale,
					uniteLegale.prenom2UniteLegale,
					uniteLegale.prenom3UniteLegale,
					uniteLegale.prenom4UniteLegale,
				]
					.filter(Boolean)
					.filter((p) => p !== uniteLegale.prenomUsuelUniteLegale);
				const prenom = [uniteLegale.prenomUsuelUniteLegale, ...prenoms].filter(Boolean).join(' ');
				mandatairePersonnePhysique = {
					nom,
					prenom,
					fonction: 'ENTREPRENEUR',
					type: 'personne_physique',
				};
			}

			apiEntrepriseEntreprise = await apiEntrepriseService.getEntreprise(
				siret,
				mandatairePersonnePhysique
			);

			if (!apiEntrepriseEntreprise) {
				// TODO maybe we actually update lastApiUpdate if the semantics of a null response is that API Entreprise has no info to give us, but not because any error occurred
				return entreprise;
			}
		} catch (e) {
			let message = 'Erreur inconnue';

			if (e instanceof Error) {
				message = e.message;
			}

			console.error('Error calling API Entreprise for siret', { siret, message });

			return entreprise;
		}

		let lastApiUpdate = null;
		const now = new Date();

		if (apiEntrepriseEntreprise.mandataires || apiEntrepriseEntreprise.exercices) {
			lastApiUpdate = now;

			const mandatairesIfExists = apiEntrepriseEntreprise.mandataires
				? { mandatairesSociaux: apiEntrepriseEntreprise.mandataires }
				: {};

			await AppDataSource.getRepository(Entreprise).update(entreprise.siret, {
				...mandatairesIfExists,
				lastApiUpdate,
			});

			if (apiEntrepriseEntreprise.exercices?.length) {
				try {
					await this.createExercicesForEntreprise({
						entreprise,
						exercices: apiEntrepriseEntreprise.exercices,
					});
				} catch (e) {
					let message = 'Erreur inconnue';
					if (e instanceof Error) {
						message = e.message;
					}
					console.error('Could not save exercices information for siret', {
						siret: entreprise.siret,
						message,
					});
				}
			}
		}

		// re-fetching entreprise to get updated data
		return AppDataSource.getRepository(Entreprise).findOneOrFail({
			where: { siret: entreprise.siret },
			relations: { exercices: true, etablissement: true },
		});
	}

	public async createExercicesForEntreprise({
		entreprise,
		exercices,
	}: {
		entreprise: Entreprise;
		exercices: ApiEntrepriseExercice[];
	}): Promise<Exercice[]> {
		const results = [];

		for (const ex of exercices) {
			const newExercice = AppDataSource.getRepository(Exercice).create({
				entreprise,
				ca: parseInt(ex.data.chiffre_affaires),
				dateCloture: new Date(ex.data.date_fin_exercice),
			});

			try {
				const exercice = await AppDataSource.getRepository(Exercice).save(newExercice);

				results.push(exercice);
			} catch (e: unknown) {
				const message = e instanceof Error ? e.message : 'Erreur inconnue';

				console.info('Could not create exercice:', message);
			}
		}

		return results;
	}

	public isCodeCommuneInTerritory({
		codeCommune,
		territoire,
	}: {
		codeCommune: string | null;
		territoire: Territory;
	}): boolean {
		if (!codeCommune) {
			return false;
		}

		const communesOfTerritoire = territoireService
			.getCommunesForTerritoire(territoire)
			.map((commune) => commune.id);

		const endogene = communesOfTerritoire.includes(codeCommune);
		return endogene;
	}

	private async createEntrepriseAndEntite({
		createur,
		siret,
		skipApiEntreprise,
	}: {
		createur: DevEco;
		siret: string;
		skipApiEntreprise?: boolean;
	}): Promise<Entite> {
		const territoire = createur.territory;

		// Entite not found, create it with entreprise (or not)
		// HOTFIX: on skip API entreprise pour éviter l'erreur de duplicate
		const entreprise = await entrepriseService.getOrCreateEntreprise(siret, true);

		const exogene = !this.isCodeCommuneInTerritory({
			codeCommune: entreprise.codeCommune,
			territoire,
		});

		const newEntite = AppDataSource.getRepository(Entite).create({
			territoire,
			entiteType: 'PM',
			entreprise,
			createur,
			auteurModification: createur,
			exogene,
		});
		const savedEntite = await AppDataSource.getRepository(Entite).save(newEntite);

		if (exogene) {
			await createTER(territoire.id, siret);
		}

		await contactService.createInitialContactsForEntite({ entreprise, entite: savedEntite });

		const entite = await AppDataSource.getRepository(Entite).findOneOrFail({
			where: { id: savedEntite.id },
			relations: relationsForToView,
		});

		await eventLogService.addEventLog({
			action: EventLogActionType.CREATE,
			deveco: createur,
			entityType: getLogEntityType(entite),
			entityId: entite.id,
		});

		if (!skipApiEntreprise) {
			// HOTFIX: on va chercher les infos dans API entreprise maintenant que l'entité est créée
			await entrepriseService.getOrCreateEntreprise(siret, skipApiEntreprise);
		}

		return entite;
	}

	public async getOrCreateEntrepriseAndEntite({
		createur,
		siret: fuzzy,
		skipApiEntreprise,
	}: {
		createur: DevEco;
		siret: string;
		skipApiEntreprise?: boolean;
	}): Promise<Entite> {
		if (!isSiret(fuzzy)) {
			throw new Error(`${fuzzy} is not a valid SIRET.`);
		}
		const siret = fuzzy.replaceAll(/\s/g, '');

		const territoire = createur.territory;

		const entite = await AppDataSource.getRepository(Entite).findOne({
			where: {
				entreprise: {
					siret,
				},
				territoire: {
					id: territoire.id,
				},
			},
			relations: relationsForToView,
		});

		// update entite with API entreprise if found
		if (entite) {
			if (!skipApiEntreprise && entite.entreprise && !entite.entreprise?.lastApiUpdate) {
				const updatedEntreprise = await this.updateEntrepriseWithApiEntrepriseData({
					entreprise: entite.entreprise,
				});

				await contactService.createInitialContactsForEntite({
					entreprise: updatedEntreprise,
					entite,
				});
			}

			return AppDataSource.getRepository(Entite).findOneOrFail({
				where: { id: entite.id },
				relations: relationsForToView,
			});
		}

		return this.createEntrepriseAndEntite({ createur, siret, skipApiEntreprise });
	}

	public async getEtablissementsFreres(
		entreprise: Entreprise,
		territoire: Territory,
		deveco: DevEco
	): Promise<EtablissementsFreres> {
		const siret = entreprise.siret;
		const siren = siret.slice(0, 9);

		const sirets = (
			await AppDataSource.getRepository(TerritoryEtablissementReference).find({
				where: {
					territory: { id: territoire.id },
					siret: Like(`${siren}%`),
				},
			})
		)
			.map((ter) => ter.siret)
			.filter((s) => s !== siret);

		const entitesPromise = async () => {
			const entites = await AppDataSource.getRepository(Entite).find({
				where: { entreprise: { siret: In(sirets) } },
				relations: relationsForToView,
			});

			return entites.reduce((acc, entite) => {
				if (entite.entreprise?.siret) {
					return { ...acc, [entite.entreprise.siret]: entite };
				}
				return acc;
			}, <Record<string, Entite>>{});
		};

		const [entites, freres] = await Promise.all([
			entitesPromise(),
			AppDataSource.getRepository(Etablissement).find({
				where: {
					siret: In(sirets),
				},
			}),
		]);

		const infos = (s: string) =>
			entiteService.infosForEntite(entites[s]?.id, deveco.id, territoire.id);

		const exogene = (codeCommune: string | null) =>
			!this.isCodeCommuneInTerritory({
				codeCommune,
				territoire,
			});

		const freresWithInfos = await Promise.all(
			freres.map(async (frere) => ({
				entreprise: {
					...entrepriseService.etablissementToEntreprise(frere),
					exogene: exogene(frere.codeCommune),
				},
				infos: await infos(frere.siret),
			}))
		);

		return freresWithInfos.sort((a, b) =>
			(b.entreprise.dateCreation as unknown as string).localeCompare(
				a.entreprise.dateCreation as unknown as string
			)
		);
	}

	public async getEtablissementHistory(
		entreprise: Entreprise,
		territoire: Territory,
		deveco: DevEco
	): Promise<EtablissementHistory> {
		const siret = entreprise.siret;

		const [predsSirets, succsSirets] = await Promise.all([
			AppDataSource.getRepository(LienSuccession)
				.find({
					where: {
						siretEtablissementSuccesseur: siret,
						continuiteEconomique: true,
					},
					select: ['siretEtablissementPredecesseur'],
				})
				.then((liens: LienSuccession[]) =>
					liens.map((lien) => lien.siretEtablissementPredecesseur)
				),
			AppDataSource.getRepository(LienSuccession)
				.find({
					where: {
						siretEtablissementPredecesseur: siret,
						continuiteEconomique: true,
					},
					select: ['siretEtablissementSuccesseur'],
				})
				.then((liens: LienSuccession[]) => liens.map((lien) => lien.siretEtablissementSuccesseur)),
		]);

		const entitesPromise = async () => {
			const entites = await AppDataSource.getRepository(Entite).find({
				where: { entreprise: { siret: In(predsSirets.concat(succsSirets)) } },
				relations: relationsForToView,
			});

			return entites.reduce((acc, entite) => {
				if (entite.entreprise?.siret) {
					return { ...acc, [entite.entreprise.siret]: entite };
				}
				return acc;
			}, <Record<string, Entite>>{});
		};

		const [entites, preds, succs] = await Promise.all([
			entitesPromise(),
			AppDataSource.getRepository(Etablissement).find({
				where: {
					siret: In(predsSirets),
				},
			}),
			AppDataSource.getRepository(Etablissement).find({
				where: {
					siret: In(succsSirets),
				},
			}),
		]);

		const infos = (s: string) =>
			entiteService.infosForEntite(entites[s]?.id, deveco.id, territoire.id);

		const exogene = (codeCommune: string | null) =>
			!this.isCodeCommuneInTerritory({
				codeCommune,
				territoire,
			});

		const predecesseursPromise = Promise.all(
			preds.map(async (pred) => ({
				entreprise: {
					...entrepriseService.etablissementToEntreprise(pred),
					exogene: exogene(pred.codeCommune),
				},
				infos: await infos(pred.siret),
			}))
		);

		const successeursPromise = Promise.all(
			succs.map(async (succ) => ({
				entreprise: {
					...entrepriseService.etablissementToEntreprise(succ),
					exogene: exogene(succ.codeCommune),
				},
				infos: await infos(succ.siret),
			}))
		);

		const [predecesseurs, successeurs] = await Promise.all([
			predecesseursPromise,
			successeursPromise,
		]);

		return { predecesseurs, successeurs };
	}
}

const entrepriseService = EntrepriseService.getInstance();

async function createTER(
	territoryId: Territory['id'],
	siret: Etablissement['siret']
): Promise<void> {
	await AppDataSource.manager.transaction(async (transaction) => {
		await transaction.query(
			`
		UPDATE etablissement EL
		SET
			longitude = GE.x_longitude,
			latitude = GE.y_latitude
		FROM geoloc_entreprise AS GE
		WHERE EL.siret = $1 AND GE.siret = $1;
`,
			[siret]
		);

		await transaction.query(
			`
		UPDATE etablissement EL
		SET
			date_fermeture = SE.date_debut
		FROM sirene_etablissement AS SE
		WHERE EL.siret = $1 AND SE.siret = $1 AND SE.etat_administratif_etablissement = 'F';
`,
			[siret]
		);

		await transaction.query(
			`
		${territoireService.insertIntoTER({ exogene: true })}
		FROM etablissement EL
		WHERE EL.siret = $2;
`,
			[territoryId, siret]
		);

		const sirets = [siret];
		await transaction.query(
			`
		${territoireService.updateESSForSirets}
`,
			[sirets]
		);

		await transaction.query(
			`
		${territoireService.insertQpv}
			AND E.siret = $1
`,
			[siret]
		);

		await transaction.query(
			`
		${territoireService.insertIntoTERZonage()}
		WHERE TER.territory_id = $1 AND E.siret = $2;
`,
			[territoryId, siret]
		);
	});
}

export default entrepriseService;
