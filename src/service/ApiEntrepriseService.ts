import _fetch from 'cross-fetch';

import { MandataireSocial } from '../entity/Entreprise';
import { httpGet } from '../lib/httpRequest';
import { isSiren } from '../lib/regexp';

type ApiMandataireSocial = {
	data: {
		numero_identification: string;
		lieu_naissance: string | null;
		pays_naissance: string | null;
		code_pays_naissance: string | null;
		nationalite: string | null;
		code_nationalite: string | null;
		code_greffe: string | null;
		libelle_greffe: string | null;
	} & MandataireSocial;
	links: Record<any, any>;
	meta: Record<any, any>;
};

// type ApiEntrepriseSiegeSocial = {
//   siege_social: boolean;
//   siret: string;
//   naf: string;
//   libelle_naf: string;
//   date_mise_a_jour: number;
//   tranche_effectif_salarie_etablissement: {
//     de: string;
//     a: string;
//     code: string;
//     date_reference: number;
//     intitule: string;
//   };
//   date_creation_etablissement: number;
//   region_implantation: {
//     code: string;
//     value: string;
//   };
//   commune_implantation: {
//     code: string;
//     value: string;
//   };
//   pays_implantation: {
//     code: string;
//     value: string;
//   };
//   diffusable_commercialement: boolean;
//   enseigne: string;
//   adresse: {
//     l1: string;
//     l2: string;
//     l3: string;
//     l4: string;
//     l5: string;
//     l6: string;
//     l7: string;
//     numero_voie: string;
//     type_voie: string;
//     nom_voie: string;
//     complement_adresse: string;
//     code_postal: string;
//     localite: string;
//     code_insee_localite: string;
//     cedex: string;
//   };
//   etat_administratif: {
//     value: string;
//     date_fermeture: number;
//   };
// };

// export type ApiEntrepriseEntreprise = {
//   data: {
//     siren: string;
//     siret_siege_social: string;
//     type: string;
//     personne_morale_attributs: {
//       raison_sociale: string;
//       sigle: string;
//     };
//     personne_physique_attributs: {
//       pseudonyme: string;
//       prenom_usuel: string;
//       prenom_1: string;
//       prenom_2: string;
//       prenom_3: string;
//       prenom_4: string;
//       nom_usage: string;
//       nom_naissance: string;
//       sexe: string;
//     };
//     categorie_entreprise: string;
//     status_diffusion: string;
//     diffusable_commercialement: boolean;
//     forme_juridique: {
//       code: string;
//       libelle: string;
//     };
//     activite_principale: {
//       code: string;
//       libelle: string;
//       nomenclature: string;
//     };
//     tranche_effectif_salarie: {
//       code: string;
//       intitule: string;
//       date_reference: string;
//       de: number;
//       a: number;
//     };
//     etat_administratif: string;
//     economie_sociale_et_solidaire: boolean;
//     date_creation: number;
//     date_cessation: number;
//   };
// };
//
// export type ApiEntrepriseEtablissement = {
//   etablissement: {
//     tranche_effectif_salarie_etablissement: {
//       de: number;
//       a: number;
//       code: string;
//       date_reference: string;
//       intitule: string;
//     };
//   };
// };
//
// export type ApiEntrepriseEffectifs = {
//   siret: string;
//   annee: string;
//   mois: string;
//   effectifs_mensuels: string;
// };

export type ApiEntrepriseExercice = {
	data: {
		chiffre_affaires: string;
		date_fin_exercice: number;
	};
};

const ANCT_SIRET = '13002603200016';

class ApiEntrepriseService {
	private static instance: ApiEntrepriseService;
	private apiEntrepriseEndPoint: string;
	private apiEntrepriseToken: string;
	private isTest: boolean;

	private constructor() {
		const isTest = (process.env.NODE_ENV ?? 'prod') === 'test';
		this.isTest = isTest;
		this.apiEntrepriseEndPoint = 'https://entreprise.api.gouv.fr';
		this.apiEntrepriseToken = process.env.API_ENTREPRISE_TOKEN || '';
	}

	public static getInstance(): ApiEntrepriseService {
		if (!ApiEntrepriseService.instance) {
			ApiEntrepriseService.instance = new ApiEntrepriseService();
		}

		return ApiEntrepriseService.instance;
	}

	// & ApiEntrepriseEntreprise {
	public async getEntreprise(
		siret: string,
		mandatairePersonnePhysique?: MandataireSocial
	): Promise<
		| ({
				mandataires: MandataireSocial[] | null;
		  } & {
				exercices: ApiEntrepriseExercice[] | null;
		  })
		| null
	> {
		if (this.isTest) {
			return {
				mandataires: null,
				exercices: null,
			};
		}

		const siren = siret.substring(0, 9);

		if (!isSiren(siren)) {
			throw new Error(`${siren} is not a valid SIREN.`);
		}

		const mandataires = mandatairePersonnePhysique
			? [mandatairePersonnePhysique]
			: await this.getMandatairesForSiren(siren);

		const exercices = await this.getExercicesForSiret(siret);

		return {
			// ...apiEntreprise.data,
			mandataires,
			exercices,
		};
	}

	public async getMandatairesForSiren(siren: string): Promise<MandataireSocial[] | null> {
		const urlApiEntrepriseExercices = `${this.apiEntrepriseEndPoint}/v3/infogreffe/rcs/unites_legales/${siren}/mandataires_sociaux?context=deveco&recipient=${ANCT_SIRET}&object=deveco`;
		const headers = {
			Authorization: `Bearer ${this.apiEntrepriseToken}`,
		};

		try {
			const response = await httpGet<{ data: ApiMandataireSocial[] }>(
				urlApiEntrepriseExercices,
				headers
			);

			if (response.__success) {
				return response.data.data.map(({ data }) => data);
			}

			throw response;
		} catch (e) {
			console.info(`Could not retrieve mandataires for entreprise with SIREN ${siren}`);
		}

		return null;
	}

	public async getExercicesForSiret(siret: string): Promise<ApiEntrepriseExercice[] | null> {
		const urlApiEntrepriseExercices = `${this.apiEntrepriseEndPoint}/v3/dgfip/etablissements/${siret}/chiffres_affaires?context=deveco&recipient=${ANCT_SIRET}&object=deveco`;
		const headers = {
			Authorization: `Bearer ${this.apiEntrepriseToken}`,
		};

		try {
			const response = await httpGet<{ data: ApiEntrepriseExercice[] }>(
				urlApiEntrepriseExercices,
				headers
			);

			// TODO assert that the received data is indeed of the expected type
			if (response.__success) {
				return response.data.data;
			}

			throw response;
		} catch (e) {
			console.info(`Could not retrieve exercices for entreprise with SIRET ${siret}`);
		}

		return null;
	}
}

const apiEntrepriseService = ApiEntrepriseService.getInstance();

export default apiEntrepriseService;
