import { FindOptionsWhere, In } from 'typeorm';

import AppDataSource from '../data-source';
import { Commune } from '../entity/Commune';

export type Conditions<T> = FindOptionsWhere<T>[] | FindOptionsWhere<T>;

export const PLM_CODES: string[] = [
	'75056', // Paris
	'69123', // Lyon
	'13055', // Marseille
];

class CommuneService {
	private static instance: CommuneService;

	public static getInstance(): CommuneService {
		if (!CommuneService.instance) {
			CommuneService.instance = new CommuneService();
		}

		return CommuneService.instance;
	}

	public async expandCommunesForMetropoles(codes: string[]): Promise<string[]> {
		if (codes.length < 0) {
			return [];
		}

		const hasMetropole = codes.some((code) => PLM_CODES.includes(code));
		if (!hasMetropole) {
			return codes;
		}

		const arrondissements = await AppDataSource.getRepository(Commune).find({
			where: {
				// ARM = Arrondissement - TODO switch to Enums for all the commune types
				typecom: 'ARM',
				comParent: { id: In([...codes]) },
			},
			relations: { comParent: true },
		});

		return [...codes, ...arrondissements.map((commune) => commune.id)];
	}
}

const communeService = CommuneService.getInstance();

export default communeService;
