import { FindOptionsRelations } from 'typeorm';

import * as path from 'path';
import { readFileSync } from 'fs';

import { Commune } from '../entity/Commune';
import { Territory, TerritoryTypeInsee } from '../entity/Territory';
import { Zonage } from '../entity/Zonage';
import AppDataSource from '../data-source';
import { Epci } from '../entity/Epci';
import { Metropole } from '../entity/Metropole';
import { Petr } from '../entity/Petr';
import { Departement } from '../entity/Departement';
import { Region } from '../entity/Region';
import { Perimetre } from '../entity/Perimetre';
import { TerritoryImportStatus } from '../entity/TerritoryImportStatus';

export type GeographyTypeForPerimetre = 'epci' | 'commune' | 'departement';

export type GeographyType =
	| GeographyTypeForPerimetre
	| 'metropole'
	| 'petr'
	| 'region'
	| 'perimetre';

export const isGeographyTypeForPerimetre = (s: unknown): s is GeographyTypeForPerimetre =>
	!!s && typeof s === 'string' && ['commune', 'epci', 'departement'].includes(s);

export type GeographyTarget =
	| Epci
	| Commune
	| Metropole
	| Petr
	| Departement
	| Region
	| Perimetre
	| null;

export const defaultTerritoryRelations: FindOptionsRelations<Territory> = {
	territoryImportStatus: true,
	commune: true,
	epci: true,
	metropole: true,
	petr: true,
	departement: true,
	region: true,
	perimetre: {
		communes: true,
		epcis: true,
		departements: true,
	},
};

class TerritoireService {
	private static instance: TerritoireService;

	public static getInstance(): TerritoireService {
		if (!TerritoireService.instance) {
			TerritoireService.instance = new TerritoireService();
		}

		return TerritoireService.instance;
	}

	public async fetchCommunesForTerritoire(territoire: Territory): Promise<Commune[]> {
		const relations: FindOptionsRelations<Territory> = {};

		switch (territoire.territoryType) {
			case 'COM':
			case 'ARM':
				relations.commune = true;
				break;
			case 'departement':
				relations.departement = { communes: true };
				break;
			case 'region':
				relations.region = { communes: true };
				break;
			case 'metropole':
				relations.metropole = { communes: true };
				break;
			case 'petr':
				relations.petr = { epcis: { communes: true } };
				break;
			case 'CA':
			case 'EPT':
			case 'ME':
			case 'CC':
			case 'CU':
				relations.epci = { communes: true };
				break;
			default:
		}

		if (territoire.perimetre) {
			relations.perimetre = {};

			switch (territoire.perimetre.level) {
				case 'commune':
					relations.perimetre.communes = true;
					break;
				case 'epci':
					relations.perimetre.epcis = { communes: true };
					break;
				case 'departement':
					relations.perimetre.departements = { communes: true };
					break;
			}
		}

		const territoireWithCommunes = await AppDataSource.getRepository(Territory).findOneOrFail({
			where: { id: territoire.id },
			relations,
		});

		return this.getCommunesForTerritoire(territoireWithCommunes);
	}

	public getCommunesForTerritoire(territoire: Territory): Commune[] {
		if (territoire.commune) {
			return [territoire.commune];
		}

		if (territoire.epci) {
			return territoire.epci.communes;
		}

		if (territoire.departement) {
			return territoire.departement.communes;
		}

		if (territoire.region) {
			return territoire.region.communes;
		}

		if (territoire.metropole) {
			return territoire.metropole.communes;
		}

		if (territoire.petr) {
			return territoire.petr.epcis.flatMap((epci) => epci.communes);
		}

		if (territoire.perimetre) {
			if (territoire.perimetre.communes) {
				return territoire.perimetre.communes;
			}

			if (territoire.perimetre.epcis) {
				return territoire.perimetre.epcis.flatMap((epci) => epci.communes);
			}

			if (territoire.perimetre.departements) {
				return territoire.perimetre.departements.flatMap((departement) => departement.communes);
			}
		}

		return [];
	}

	public async getZonagesForTerritoire(territoire: Territory) {
		return AppDataSource.manager.query(
			`
				SELECT
				Z.id,
				Z.nom
				FROM
					zonage Z
				WHERE
					Z.territoire_id = $1
			`,
			[territoire.id]
		);
	}

	public async deleteZonageForTerritoire(territoire: Territory, zonageId: Zonage['id']) {
		return AppDataSource.getRepository(Zonage).delete({
			id: zonageId,
			territoire: {
				id: territoire.id,
			},
		});
	}

	public getGeographyType(territoire: Territory): GeographyType {
		if (territoire.commune) {
			return 'commune';
		}

		if (territoire.epci) {
			return 'epci';
		}

		if (territoire.departement) {
			return 'departement';
		}

		if (territoire.region) {
			return 'region';
		}

		if (territoire.metropole) {
			return 'metropole';
		}

		if (territoire.petr) {
			return 'petr';
		}

		if (territoire.perimetre) {
			return 'perimetre';
		}

		throw Error(`Could not determine geography type for territory #${territoire.id}`);
	}

	public isImportInProgress(tis: TerritoryImportStatus) {
		return !tis.done;
	}

	public updateGeoLocationForTerritoryQuery(
		geographyType: GeographyType,
		perimetreLevel?: GeographyTypeForPerimetre
	) {
		return `
			UPDATE etablissement EL
			SET
			(longitude, latitude) = (
				SELECT
				GE.x_longitude longitude,
				GE.y_latitude latitude
				FROM
				geoloc_entreprise AS GE
				WHERE
				GE.siret = EL.siret
			),
			date_fermeture = (
				SELECT
				SE.date_debut date_fermeture
				FROM
				sirene_etablissement AS SE
				WHERE
				SE.siret = EL.siret
				AND SE.etat_administratif_etablissement = 'F'
			)
			WHERE ${this.communeConditionFromGeographyType(geographyType, perimetreLevel)};
		`;
	}

	public populateTerritoryEtablissementReferenceQuery(
		geographyType: GeographyType,
		perimetreLevel?: GeographyTypeForPerimetre
	) {
		return `
		${this.insertIntoTER({ exogene: false })}
		FROM
			etablissement EL
		WHERE
			${this.communeConditionFromGeographyType(geographyType, perimetreLevel)}
		ON CONFLICT
		DO NOTHING;
	`;
	}

	public insertQpv = `
		UPDATE territory_etablissement_reference as TER
		SET
			qpv_id = QPV.gid
		FROM
			etablissement as E
		INNER JOIN qp_metropoleoutremer_wgs84_epsg4326 as QPV ON ST_Contains (geom, ST_POINT (E.longitude, E.latitude))
		WHERE
			E.siret = TER.siret
	`;

	public updateTerWithQpv = `
${this.insertQpv}
  AND TER.territory_id = $1
`;

	public async createTerritoryForTerritoryTargets({
		name,
		geographyType,
		geographyTargets,
	}: {
		name: string;
		geographyType: GeographyType;
		geographyTargets: GeographyTarget[];
	}) {
		let territoryTypeInsee: TerritoryTypeInsee;

		let geographyTarget;

		if (geographyTargets.length > 1) {
			if (!isGeographyTypeForPerimetre(geographyType)) {
				throw new Error(`Impossible de choisir ce type pour un périmètre : ${geographyType}`);
			}

			territoryTypeInsee = `groupement de ${geographyType}`;

			geographyTarget = AppDataSource.getRepository(Perimetre).create({
				level: geographyType,
				// TODO: find out why it works without 's' ('commune' vs 'communes')
				[`${geographyType}s`]: geographyTargets,
			});

			geographyTarget = await AppDataSource.getRepository(Perimetre).save(geographyTarget);

			geographyType = 'perimetre';
		} else {
			geographyTarget = geographyTargets[0];

			if (!geographyTarget) {
				throw new Error('');
			}

			switch (geographyType) {
				case 'epci':
					territoryTypeInsee = (geographyTarget as Epci).nature;
					break;
				case 'commune':
					territoryTypeInsee = (geographyTarget as Commune).typecom;
					break;
				case 'metropole':
					territoryTypeInsee = 'metropole';
					break;
				case 'petr':
					territoryTypeInsee = 'petr';
					break;
				case 'departement':
					territoryTypeInsee = 'departement';
					break;
				case 'region':
					territoryTypeInsee = 'region';
					break;
				default:
					throw new Error(`Mauvais type : ${geographyType}`);
			}
		}

		const newTerritory = AppDataSource.getRepository(Territory).create({
			name,
			territoryType: territoryTypeInsee,
			[geographyType]: geographyTarget,
		});

		const territoire = await AppDataSource.getRepository(Territory).save(newTerritory);

		if (!territoire) {
			return null;
		}

		const territoireId = territoire.id;

		await AppDataSource.getRepository(TerritoryImportStatus).insert({
			territoire,
		});

		const newTerritoire = await AppDataSource.getRepository(Territory).findOneOrFail({
			where: {
				id: territoireId,
			},
			relations: defaultTerritoryRelations,
		});

		return newTerritoire;
	}

	public async getNextTerritoireToInit(): Promise<Territory['id'] | null> {
		const inProgressTerritories = await AppDataSource.getRepository(TerritoryImportStatus).find({
			where: {
				done: false,
				started: true,
			},
		});

		if (inProgressTerritories.length > 0) {
			return null;
		}

		const nextTerritory = await AppDataSource.getRepository(TerritoryImportStatus).findOne({
			where: {
				started: false,
				done: false,
			},
			order: {
				createdAt: {
					direction: 'ASC',
					nulls: 'LAST',
				},
			},
			select: {
				territoire: {
					id: true,
				},
			},
			relations: {
				territoire: true,
			},
		});

		return nextTerritory?.territoire?.id ?? null;
	}

	public async doImportStepsForTerritory(territoireId: Territory['id']): Promise<void> {
		const territoire = await AppDataSource.getRepository(Territory).findOne({
			where: {
				id: territoireId,
			},
			relations: defaultTerritoryRelations,
		});

		if (!territoire) {
			console.error(`Could not find territory #${territoireId}`);

			return;
		}

		const geographyType = territoireService.getGeographyType(territoire);

		const importStatus = await AppDataSource.getRepository(TerritoryImportStatus).findOne({
			where: {
				territoire: {
					id: territoireId,
				},
			},
		});

		if (!importStatus) {
			console.error(`Could not get import status territory #${territoireId}`);

			return;
		}

		// update geolocation, copy Etablissements to TerritoryEtablissementReference and update QPVs
		// we fire and forget the process as it's too long to wait for its end
		// Note: we don't want to block the response, but the various queries need to be done in sequence
		Promise.resolve().then(async () => {
			if (territoire.territoryImportStatus.started) {
				console.info(`[import] Import already started for Territory #${territoireId}, skipping.`);
			} else {
				console.info(`[import] Import starting for Territory #${territoireId}`);
				await AppDataSource.getRepository(TerritoryImportStatus).update(
					{
						territoire: {
							id: territoireId,
						},
					},
					{
						started: true,
					}
				);
			}

			if (territoire.territoryImportStatus.geolocationDone) {
				console.info(`[import] Geolocation already done for Territory #${territoireId}, skipping.`);
			} else {
				console.info(`[import] Geolocation in progress for Territory #${territoireId}`);

				await AppDataSource.manager.query(
					this.updateGeoLocationForTerritoryQuery(geographyType, territoire.perimetre?.level),
					[territoireId]
				);

				await AppDataSource.getRepository(TerritoryImportStatus).update(
					{
						territoire: {
							id: territoireId,
						},
					},
					{
						geolocationDone: true,
					}
				);

				console.info(`[import] Geolocation done for Territory #${territoireId}`);
			}

			if (territoire.territoryImportStatus.terDone) {
				console.info(
					`[import] Populate TER already done for Territory #${territoireId}, skipping.`
				);
			} else {
				console.info(`[import] Populate TER in progress for Territory #${territoireId}`);

				await AppDataSource.manager.query(
					this.populateTerritoryEtablissementReferenceQuery(
						geographyType,
						territoire.perimetre?.level
					),
					[territoireId]
				);

				await AppDataSource.getRepository(TerritoryImportStatus).update(
					{
						territoire: {
							id: territoireId,
						},
					},
					{
						terDone: true,
					}
				);

				console.info(`[import] Populate TER done for Territory #${territoireId}`);
			}

			if (territoire.territoryImportStatus.essDone) {
				console.info(
					`[import] Update TER with ESS already done for Territory #${territoireId}, skipping.`
				);
			} else {
				console.info(`[import] Update TER with ESS in progress for Territory #${territoireId}`);

				await AppDataSource.manager.query(this.updateESSForTerritory, [territoireId]);

				await AppDataSource.getRepository(TerritoryImportStatus).update(
					{
						territoire: {
							id: territoireId,
						},
					},
					{
						essDone: true,
					}
				);

				console.info(`[import] Update TER with ESS done for Territory #${territoireId}`);
			}

			if (territoire.territoryImportStatus.qpvDone) {
				console.info(
					`[import] Update TER with QPV already done for Territory #${territoireId}, skipping.`
				);
			} else {
				console.info(`[import] Update TER with QPV in progress for Territory #${territoireId}`);

				await AppDataSource.manager.query(this.updateTerWithQpv, [territoireId]);

				await AppDataSource.getRepository(TerritoryImportStatus).update(
					{
						territoire: {
							id: territoireId,
						},
					},
					{
						qpvDone: true,
					}
				);

				console.info(`[import] Update TER with QPV done for Territory #${territoireId}`);
			}

			if (territoire.territoryImportStatus.statsDone) {
				console.info(
					`[import] Insert Stats already done for Territory #${territoireId}, skipping.`
				);
			} else {
				console.info(`[import] Insert Stats in progress for Territory #${territoireId}`);

				const communes = await territoireService.fetchCommunesForTerritoire(territoire);
				await this.insertStatsForCommunes(communes);

				await AppDataSource.getRepository(TerritoryImportStatus).update(
					{
						territoire: {
							id: territoireId,
						},
					},
					{
						statsDone: true,
					}
				);

				console.info(`[import] Insert Stats done for Territory #${territoireId}`);
			}

			await AppDataSource.getRepository(TerritoryImportStatus).update(
				{
					territoire: {
						id: territoireId,
					},
				},
				{
					done: true,
				}
			);

			console.info(`[import] All import tasks done for Territory #${territoireId}`);
		});
	}

	public insertIntoTER({ exogene }: { exogene: boolean }): string {
		return `
			INSERT INTO
			territory_etablissement_reference (
				id,
				siret,
				territory_id,
				nom_recherche,
				etat_administratif,
				code_naf,
				libelle_naf,
				date_creation,
				categorie_juridique_courante,
				tranche_effectifs,
				code_commune,
				adresse_complete,
				exogene
			)
			SELECT
			CONCAT($1::INT, '-', EL.siret) as id,
			EL.siret as siret,
			$1 as territory_id,
			EL.nom_recherche as nom_recherche,
			EL.etat_administratif as etat_administratif,
			EL.code_naf as code_naf,
			EL.libelle_naf as libelle_naf,
			EL.date_creation as date_creation,
			CASE
				WHEN substring(CAST(EL.categorie_juridique AS VARCHAR), 1, 2) IN ( '00', '21', '22', '27', '65', '71', '72', '73', '74', '81', '83', '84', '85', '91') THEN false
				ELSE true
			END as categorie_juridique_courante,
			EL.tranche_effectifs as tranche_effectifs,
			EL.code_commune as code_commune,
			EL.adresse_complete as adresse_complete,
			${exogene ? 'true' : 'false'} as exogene
		`;
	}

	public insertIntoTERZonage(): string {
		return `
			INSERT INTO
			territory_etablissement_reference_zonages_zonage(territory_etablissement_reference_id, zonage_id)
			SELECT
			TER.id, Z.id
			FROM
			territory_etablissement_reference TER
			JOIN
			etablissement E ON TER.siret = E.siret AND E.longitude IS NOT NULL AND E.latitude IS NOT NULL
			JOIN
			zonage Z ON ST_CONTAINS(Z.geometry, ST_SetSRID(ST_Point(E.longitude, E.latitude), 4326)) AND Z.territoire_id = TER.territory_id
		`;
	}

	public updateESSForSirets = `
		UPDATE "territory_etablissement_reference" TER
		SET ess = ('O' = COALESCE(U.eco_soc_sol_unite_legale, 'N'))
		FROM "sirene_unite_legale" U
		WHERE U.siren = SUBSTRING(TER.siret, 0, 10) AND TER.siret = ANY($1)
	`;

	public updateESSForTerritory = `
		UPDATE "territory_etablissement_reference" TER
		SET ess = ('O' = COALESCE(U.eco_soc_sol_unite_legale, 'N'))
		FROM "sirene_unite_legale" U
		WHERE U.siren = SUBSTRING(TER.siret, 0, 10) AND TER.territory_id = $1
	`;

	public communeConditionFromGeographyType(
		geographyType: GeographyType,
		perimetreLevel?: Perimetre['level']
	) {
		if (geographyType === 'commune') {
			return `EL.code_commune = (SELECT commune_id FROM territory WHERE id = $1)`;
		}

		let communeCondition: string;

		switch (geographyType) {
			case 'epci':
				communeCondition = `C.insee_epci = T.epci_id`;
				break;

			case 'petr':
				communeCondition = `C.insee_epci IN (
					SELECT insee_epci
					FROM epci
					WHERE epci.code_petr = T.petr_id
				)`;
				break;

			case 'departement':
				communeCondition = `C.ctcd = T.departement_id`;
				break;

			case 'region':
				communeCondition = `C.insee_reg = T.region_id`;
				break;

			case 'metropole':
				communeCondition = `C.com_parent = T.metropole_id`;
				break;

			case 'perimetre':
				switch (perimetreLevel) {
					case 'commune':
						communeCondition = `C.insee_com IN (
							SELECT "communeInseeCom"
							FROM perimetre_communes_commune PCC
							WHERE PCC."perimetreId" = T.perimetre_id
						)`;
						break;

					case 'epci':
						communeCondition = `C.insee_epci IN (
							SELECT "epciInseeEpci"
							FROM perimetre_epcis_epci PEE
							WHERE PEE."perimetreId" = T.perimetre_id
						)`;
						break;

					case 'departement':
						communeCondition = `C.ctcd IN (
							SELECT "departementCtcd"
							FROM perimetre_departements_departement PDD
							WHERE PDD."perimetreId" = T.perimetre_id
						)`;
						break;

					default:
						throw new Error('Cannot have a perimetre without its level');
				}
		}

		return `
			EL.code_commune IN (
				SELECT
				insee_com
				FROM
				commune as C
				INNER JOIN territory T ON ${communeCondition}
				WHERE
				T.id = $1
			)
		`;
	}

	public formatGeographyLibelle(territoire: Territory) {
		const type = this.getGeographyType(territoire);

		switch (type) {
			case 'commune':
				return `${territoire.commune.libCom} (${territoire.commune.inseeDep})`;

			case 'metropole':
				return territoire.metropole.libCom;

			case 'epci':
				return territoire.epci.libEpci;

			case 'petr':
				return territoire.petr.libPetr;

			case 'departement':
				return territoire.departement.nom;

			case 'region':
				return territoire.region.nom;

			case 'perimetre': {
				type PerimetreGeographyType = 'communes' | 'epcis' | 'departements';

				let perimetreType: PerimetreGeographyType;
				let perimetreTypeLibelle: string;

				if (territoire.perimetre.communes?.length > 0) {
					perimetreType = 'communes';
					perimetreTypeLibelle = 'commune';
				} else if (territoire.perimetre.epcis?.length > 0) {
					perimetreType = 'epcis';
					perimetreTypeLibelle = 'EPCI';
				} else if (territoire.perimetre.departements?.length > 0) {
					perimetreType = 'departements';
					perimetreTypeLibelle = 'département';
				} else {
					throw new Error(`Unknown type of perimetre`);
				}

				const nombre = territoire.perimetre[perimetreType].length;

				return `${nombre} ${perimetreTypeLibelle}${nombre > 1 ? 's' : ''}`;
			}
		}
	}

	public async insertStats() {
		const p = '../sql/insertEtablissementStats.sql';
		const sql = readFileSync(path.join(__dirname, ...p.split('/')), {
			encoding: 'utf8',
		});

		return AppDataSource.manager.query(sql);
	}

	public async insertStatsForCommunes(communes: Array<Commune>) {
		const p = '../sql/insertEtablissementStatsForCommunes.sql';
		const sql = readFileSync(path.join(__dirname, ...p.split('/')), {
			encoding: 'utf8',
		});

		const codes = communes.map((c) => c.id);

		return AppDataSource.manager.query(sql, [codes]);
	}
}

const territoireService = TerritoireService.getInstance();

export default territoireService;
