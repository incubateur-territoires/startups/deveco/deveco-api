import entiteService from './EntiteService';

import AppDataSource from '../data-source';
import { DevEco } from '../entity/DevEco';
import { Echange } from '../entity/Echange';
import { Entite } from '../entity/Entite';
import { Fiche } from '../entity/Fiche';
import { Qpv } from '../entity/Qpv';
import { isSiret } from '../lib/regexp';
import entrepriseService from '../service/EntrepriseService';
import ficheService from '../service/FicheService';
import { Rappel } from '../entity/Rappel';
import { Demande } from '../entity/Demande';
import { Contact } from '../entity/Contact';

const TRANSFORMATION_ECHANGE_TITRE = 'Créateur transformé en établissement';
const TRANSFORMATION_ECHANGE_TYPE = 'transformation';

class CreateurService {
	private static instance: CreateurService;

	public static getInstance(): CreateurService {
		if (!CreateurService.instance) {
			CreateurService.instance = new CreateurService();
		}

		return CreateurService.instance;
	}

	public async createFicheFor({
		createur,
		particulierData,
		echangeData,
		activitesReelles,
		localisations,
		mots,
		autre,
	}: {
		createur: DevEco;
		particulierData: {
			nom: string;
			prenom: string;
			email: string;
			telephone: string;
			adresse: string;
			ville: string;
			codePostal: string;
			geolocation: string;
			description: string;
			futureEnseigne: string;
		};
		echangeData: {
			compteRendu: string;
			date: Date;
			type: string;
			themes: string[];
			titre: string;
		};
		activitesReelles: string[];
		localisations: string[];
		mots: string[];
		autre: string;
	}): Promise<Fiche['id']> {
		const now = new Date();

		const {
			nom,
			prenom,
			email,
			telephone,
			adresse,
			ville,
			codePostal,
			geolocation,
			description,
			futureEnseigne,
		} = particulierData;
		const { compteRendu, date: dateEchange, type: typeEchange, themes, titre } = echangeData;
		const echange: Echange = {
			createur,
			titre,
			typeEchange,
			dateEchange,
			compteRendu,
			themes: themes || [],
			createdAt: now,
			updatedAt: null, // TODO maybe don't do that and instead compare createdAt to updatedAt on the front-end?
		};

		const territoire = createur.territory;

		let qpvRef: { qpv: { id: Qpv['id'] } } | Record<string, never> = {};

		if (geolocation) {
			const [x, y] = geolocation?.replace('(', '').replace(')', '').split(',');
			const qpv = await AppDataSource.getRepository(Qpv)
				.createQueryBuilder()
				.where('ST_Contains (geom, ST_POINT (:x, :y))', { x, y })
				.getOne();
			qpvRef = qpv ? { qpv: { id: qpv.id } } : {};
		}

		const newEntite = AppDataSource.getRepository(Entite).create({
			territoire,
			createur,
			entiteType: 'PP',
			particulier: {
				nom,
				prenom,
				email,
				telephone,
				adresse,
				ville,
				codePostal,
				geolocation,
				description,
				...qpvRef,
			},
			futureEnseigne,
			activitesReelles,
			entrepriseLocalisations: localisations,
			motsCles: mots,
			activiteAutre: autre,
			auteurModification: createur,
			createdAt: now,
			updatedAt: now,
		});

		const entite = await AppDataSource.getRepository(Entite).save(newEntite);

		const demandes = (themes || []).map((demande: string) => ({
			typeDemande: demande,
		}));

		const fiche = AppDataSource.getRepository(Fiche).create({
			territoire,
			createur,
			entite,
			demandes,
			echanges: [echange],
			dateModification: now,
			auteurModification: createur,
			createdAt: now,
			updatedAt: now,
		});

		await entiteService.createQualificationsIfNecessary({
			activitesReelles,
			entrepriseLocalisations: localisations,
			motsCles: mots,
			deveco: createur,
		});

		const { id } = await AppDataSource.getRepository(Fiche).save(fiche);

		return id;
	}

	public async linkEntreprise({
		id,
		deveco,
		siret: fuzzy,
	}: {
		id: number;
		deveco: DevEco;
		siret: string;
	}): Promise<Fiche> {
		if (!isSiret(fuzzy)) {
			throw new Error(`${fuzzy} is not a valid SIRET.`);
		}

		const territoire = deveco.territory;

		const siret = fuzzy.replaceAll(/\s/g, '');

		const ficheCreateur = await AppDataSource.getRepository(Fiche).findOneOrFail({
			where: {
				id,
				territoire: { id: territoire.id },
			},
			relations: {
				entite: {
					particulier: true,
				},
			},
		});

		const entiteEtablissement = await entrepriseService.getOrCreateEntrepriseAndEntite({
			siret,
			createur: deveco,
		});

		// Get or create a Fiche for the Établissement Entite
		const ficheEtablissement = await ficheService.getOrCreateFicheForSiret({ deveco, siret });

		// Create a Contact to link the Créateur to the Établissement Entite
		const createurContact = AppDataSource.getRepository(Contact).create({
			particulier: { id: ficheCreateur.entite.particulier?.id },
			fonction: 'Créateur',
			entite: { id: entiteEtablissement.id },
		});

		// Get all Créateur Entite's contacts
		const contacts = await AppDataSource.getRepository(Contact).find({
			where: { entite: { id: ficheCreateur.entite.id } },
		});

		// Add all the Créateur Entite Étiquettes to the Établissement Entite
		const oldActiviteAutre = [
			ficheCreateur.entite.futureEnseigne,
			ficheCreateur.entite.activiteAutre,
		]
			.filter((s?: string) => Boolean(s?.trim()))
			.join('\n');

		await Promise.all([
			// Link the Créateur to the Établissement's SIRET
			AppDataSource.getRepository(Entite).update(ficheCreateur.entite.id, {
				etablissementCree: { siret },
			}),

			// Link the Établissement Entite to the Créateur Entite
			AppDataSource.getRepository(Entite).update(entiteEtablissement.id, {
				createurLie: { id: ficheCreateur.entite.particulier?.id },
			}),

			// Add an Échange to document the switch from Créateur to Établissement
			AppDataSource.getRepository(Echange).insert({
				createur: deveco,
				fiche: { id: ficheEtablissement.id },
				titre: TRANSFORMATION_ECHANGE_TITRE,
				typeEchange: TRANSFORMATION_ECHANGE_TYPE,
				compteRendu: '',
				themes: [],
				dateEchange: new Date(),
			}),

			// Move all Échanges to the Établissement's Fiche
			AppDataSource.getRepository(Echange).update(
				{ fiche: { id: ficheCreateur.id } },
				{ fiche: { id: ficheEtablissement.id } }
			),

			// Move all Demandes to the Établissement's Fiche
			AppDataSource.getRepository(Demande).update(
				{ fiche: { id: ficheCreateur.id } },
				{ fiche: { id: ficheEtablissement.id } }
			),

			// Move all Rappels to the Établissement's Fiche
			AppDataSource.getRepository(Rappel).update(
				{ fiche: { id: ficheCreateur.id } },
				{ fiche: { id: ficheEtablissement.id } }
			),

			// Add all Créateur Entite's Contacts to the Établissement Entite, along with the new one
			AppDataSource.getRepository(Contact).insert([
				...contacts.map((c) => {
					const { id, ...rest } = c;
					return { ...rest, entite: { id: entiteEtablissement.id } };
				}),
				createurContact,
			]),

			entiteService.addQualificationsToEntites([entiteEtablissement.id], deveco, {
				activitesReelles: ficheCreateur.entite.activitesReelles,
				entrepriseLocalisations: ficheCreateur.entite.entrepriseLocalisations,
				motsCles: ficheCreateur.entite.motsCles,
				activiteAutre: oldActiviteAutre,
			}),
		]);

		return ficheCreateur;
	}

	public async unlinkEntreprise({ id, deveco }: { id: number; deveco: DevEco }): Promise<Fiche> {
		const territoire = deveco.territory;

		const ficheCreateur = await AppDataSource.getRepository(Fiche).findOneOrFail({
			where: {
				id,
				territoire: { id: territoire.id },
			},
			relations: {
				entite: {
					particulier: true,
					etablissementCree: true,
				},
			},
		});

		const etablissementCree = ficheCreateur.entite.etablissementCree;

		if (etablissementCree) {
			const ficheEtablissement = await ficheService.getOrCreateFicheForSiret({
				deveco,
				siret: etablissementCree.siret,
			});

			await Promise.all([
				// Unlink the Établissement from the Créateur's Entité
				AppDataSource.getRepository(Entite).update(ficheCreateur.entite.id, {
					etablissementCree: { siret: undefined },
				}),

				// Unlink the Créateur from the Établissement's Entité
				AppDataSource.getRepository(Entite).update(
					{
						entreprise: { siret: etablissementCree.siret },
					},
					{
						createurLie: { id: undefined },
					}
				),

				// Remove Créateur's Contact from Établissement Entite
				AppDataSource.getRepository(Contact).delete({
					particulier: { id: ficheCreateur.entite.particulier?.id },
				}),

				// Delete the Échange documenting the switch from Créateur to Établissement
				AppDataSource.getRepository(Echange).delete({
					fiche: { id: ficheEtablissement.id },
					titre: TRANSFORMATION_ECHANGE_TITRE,
					typeEchange: TRANSFORMATION_ECHANGE_TYPE,
				}),

				// Move all Échanges back to the Créateur's Fiche
				AppDataSource.getRepository(Echange).update(
					{ fiche: { id: ficheEtablissement.id } },
					{ fiche: { id: ficheCreateur.id } }
				),

				// Move all Demandes back to the Créateur's Fiche
				AppDataSource.getRepository(Demande).update(
					{ fiche: { id: ficheEtablissement.id } },
					{ fiche: { id: ficheCreateur.id } }
				),

				// Move all Rappels back to the Créateur's Fiche
				AppDataSource.getRepository(Rappel).update(
					{ fiche: { id: ficheEtablissement.id } },
					{ fiche: { id: ficheCreateur.id } }
				),
			]);
		}

		return ficheCreateur;
	}
}

const createurService = CreateurService.getInstance();

export default createurService;
