import { FindOptionsWhere, IsNull } from 'typeorm';
import { addDays, setHours } from 'date-fns';

import devecoService from './DevEcoService';

import AppDataSource from '../data-source';
import { Rappel } from '../entity/Rappel';
import { isSmtp, send } from '../lib/emailing';
import { displayFicheName, wait } from '../lib/utils';
import { EntiteData } from '../service/StatsService';

export type Conditions<T> = FindOptionsWhere<T>[] | FindOptionsWhere<T>;

class RappelService {
	private static instance: RappelService;

	public static getInstance(): RappelService {
		if (!RappelService.instance) {
			RappelService.instance = new RappelService();
		}

		return RappelService.instance;
	}

	private static getRappelsBetween(inTwoDays: Date): Promise<Rappel[]> {
		return AppDataSource.getRepository(Rappel).find({
			where: {
				dateCloture: IsNull(),
				dateRappel: inTwoDays,
			},
			order: { dateRappel: 'ASC' },
			relations: {
				fiche: {
					entite: {
						entreprise: true,
						particulier: true,
					},
				},
				createur: true,
			},
		});
	}

	private static formatRappel(rappel: Rappel): EntiteData | null {
		// TODO this should not happen but it is modelled with that possibility
		if (!rappel.fiche) {
			return null;
		}

		const nom = displayFicheName(rappel.fiche.entite);

		const [year, month, day] = (rappel.dateRappel as unknown as string).split('-'); // TypeORM returns "Date" as string but TypeScript disagrees
		const date = `${day}/${month}/${year}`;

		return {
			nom,
			date,
			contenu: rappel.titre,
			siret: rappel.fiche.entite.entreprise?.siret,
			ficheId: rappel.fiche.id,
		};
	}

	private static sendRappelsDataToDeveco(appUrl: string, rappelsData: EntiteData[]) {
		return async (email: string) => {
			const template = 'rappelsReminder' as const;
			const mailOptions = {
				options: {
					to: email,
					subject: 'Rappels Dévéco',
				},
				template,
				params: {
					url: {
						appUrl,
						page: '/actions',
					},
					rappelsData,
				},
			};
			send(mailOptions).catch((emailError) => {
				console.error(emailError);
			});
		};
	}

	public async sendRemindersForRappels() {
		if (!isSmtp()) {
			console.info('No SMTP config found');
			return;
		}

		const appUrl = process.env.APP_URL || '';

		const today = new Date();
		const inTwoDays = setHours(addDays(today, 2), 12);

		const rappels = await RappelService.getRappelsBetween(inTwoDays);
		if (!rappels.length) {
			console.info('No rappels found to send reminders for.');

			return;
		}

		const devecos = await devecoService.getAllActive();
		if (!devecos.length) {
			console.info('No devecos found to send reminders to.');

			return;
		}

		for (const deveco of devecos) {
			const devecoRappels = rappels.filter((rappel) => rappel.createur?.id === deveco.id);
			if (!devecoRappels.length) {
				continue;
			}

			console.info(
				`Sending daily rappels reminder to deveco ${deveco.account.email}. ${devecoRappels.length} found.`
			);

			const rappelsData: EntiteData[] = devecoRappels.reduce((acc: EntiteData[], r: Rappel) => {
				const formatted = RappelService.formatRappel(r);
				if (!formatted) {
					return acc;
				}

				return [...acc, formatted];
			}, []);

			const mailSender = RappelService.sendRappelsDataToDeveco(appUrl, rappelsData);

			if (process.env.CAPTURE_RAPPELS_REMINDERS) {
				console.info('Captured reminders to', process.env.CAPTURE_RAPPELS_REMINDERS);

				mailSender(process.env.CAPTURE_RAPPELS_REMINDERS);

				const delay = 500;
				await wait(delay);
			} else {
				console.info('Sending reminder to deveco', deveco.account.email);

				mailSender(deveco.account.email);
			}
		}
	}
}

const rappelService = RappelService.getInstance();

export default rappelService;
