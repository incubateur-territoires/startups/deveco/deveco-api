import AppDataSource from '../data-source';
import { DevEco } from '../entity/DevEco';
import { EventLog, EventLogActionType, EventLogEntityType } from '../entity/EventLog';

export type EventLogParam = {
	action: EventLogActionType;
	deveco: DevEco;
	entityId?: number;
	entityType: EventLogEntityType;
};

class EventLogService {
	private static instance: EventLogService;

	public static getInstance(): EventLogService {
		if (!EventLogService.instance) {
			EventLogService.instance = new EventLogService();
		}

		return EventLogService.instance;
	}

	public async addEventLog(param: EventLogParam): Promise<void> {
		await AppDataSource.getRepository(EventLog).insert({ ...param });
	}
}

const eventLogService = EventLogService.getInstance();

export default eventLogService;
