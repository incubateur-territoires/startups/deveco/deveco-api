import AppDataSource from '../data-source';
import { Particulier } from '../entity/Particulier';

class ParticulierService {
	private static instance: ParticulierService;

	public static getInstance(): ParticulierService {
		if (!ParticulierService.instance) {
			ParticulierService.instance = new ParticulierService();
		}

		return ParticulierService.instance;
	}

	public async createParticulier(payload: Partial<Particulier>): Promise<Particulier['id']> {
		const { identifiers: particuliers } = await AppDataSource.getRepository(Particulier).insert(
			payload
		);

		return particuliers[0].id;
	}
}

const particulierService = ParticulierService.getInstance();

export default particulierService;
