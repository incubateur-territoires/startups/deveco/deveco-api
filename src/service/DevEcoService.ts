import { FindOptionsRelations } from 'typeorm';

import AppDataSource from '../data-source';
import { Account } from '../entity/Account';
import { Territory } from '../entity/Territory';
import { DevEco } from '../entity/DevEco';
import { isSmtp, send } from '../lib/emailing';

class DevEcoService {
	private static instance: DevEcoService;

	public static getInstance(): DevEcoService {
		if (!DevEcoService.instance) {
			DevEcoService.instance = new DevEcoService();
		}

		return DevEcoService.instance;
	}

	public async getDevEcoByAccountWithTerritory(account: {
		id?: Account['id'];
		email?: Account['email'];
	}): Promise<DevEco> {
		const accountCondition = account.id ? { id: account.id } : { email: account.email };

		const deveco = await AppDataSource.getRepository(DevEco).findOneOrFail({
			where: {
				account: accountCondition,
			},
			relations: {
				territory: {
					perimetre: true,
				},
			},
		});

		const relations: FindOptionsRelations<Territory> = {
			territoryImportStatus: true,
		};

		switch (deveco.territory.territoryType) {
			case 'COM':
			case 'ARM':
				relations.commune = true;
				break;
			case 'departement':
				relations.departement = { communes: true };
				break;
			case 'region':
				relations.region = { communes: true };
				break;
			case 'metropole':
				relations.metropole = { communes: true };
				break;
			case 'petr':
				relations.petr = { epcis: { communes: true } };
				break;
			case 'CA':
			case 'EPT':
			case 'ME':
			case 'CC':
			case 'CU':
				relations.epci = { communes: true };
				break;
			default:
		}

		if (deveco.territory.perimetre) {
			relations.perimetre = {};

			switch (deveco.territory.perimetre.level) {
				case 'commune':
					relations.perimetre.communes = true;
					break;
				case 'epci':
					relations.perimetre.epcis = { communes: true };
					break;
				case 'departement':
					relations.perimetre.departements = { communes: true };
					break;
			}
		}

		const territory = await AppDataSource.getRepository(Territory).findOneOrFail({
			where: { id: deveco.territory.id },
			relations,
		});

		return {
			...deveco,
			territory,
		};
	}

	public getAllActive(): Promise<DevEco[]> {
		return AppDataSource.getRepository(DevEco).find({
			where: {
				account: {
					enabled: true,
				},
			},
			relations: { account: true },
		});
	}

	public getAllByTerritoryId(id: number): Promise<DevEco[]> {
		return AppDataSource.getRepository(DevEco).find({
			where: { territory: { id } },
			relations: { account: true },
		});
	}

	public async sendWelcomeEmailToDeveco(deveco: DevEco): Promise<void> {
		const appUrl = process.env.APP_URL || '';

		const account = deveco.account;

		const subjectHeader = account.prenom ? `${account.prenom}, votre` : 'Votre';

		if (isSmtp()) {
			try {
				await send({
					options: {
						to: account.email,
						subject: `${subjectHeader} accès à Deveco est créé !`,
					},
					template: 'welcome',
					params: {
						url: {
							accessKey: account.accessKey ?? '',
							appUrl,
						},
						account,
					},
				});
			} catch (emailError) {
				console.error(emailError);
			}
		} else {
			console.info('No SMTP config found');
		}

		await AppDataSource.getRepository(DevEco).update(deveco.id, {
			welcomeEmailSent: true,
		});
	}

	public async sendWelcomeEmails(): Promise<void> {
		// send welcome email
		const devecos = await AppDataSource.getRepository(DevEco).find({
			where: {
				welcomeEmailSent: false,
				territory: {
					territoryImportStatus: {
						done: true,
					},
				},
			},
			relations: {
				account: true,
			},
		});

		if (devecos.length === 0) {
			return;
		}

		console.info("Sending welcome email to finished importing Territory's Devecos.");

		for (const deveco of devecos) {
			await this.sendWelcomeEmailToDeveco(deveco);
		}

		console.info("Finished sending welcome email to finished importing Territory's Devecos.");
	}
}

const devecoService = DevEcoService.getInstance();

export default devecoService;
