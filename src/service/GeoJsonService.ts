import { EtablissementColumns } from '../entity/Etablissement';

export type EtablissementGeoJsonData = typeof propsMap & typeof extraPropsMap;

type propsMapKey = 's' | 'n' | 'e' | 'lo' | 'la';

export const propsMap: Record<propsMapKey, EtablissementColumns> = {
	s: 'siret',
	n: 'nom_public',
	e: 'enseigne',
	lo: 'longitude',
	la: 'latitude',
};

type extraPropsMapKey = 'dc' | 'df' | 'ce';

export const extraPropsMap: Record<extraPropsMapKey, EtablissementColumns> = {
	dc: 'date_creation',
	df: 'date_fermeture',
	ce: 'date_creation_entreprise',
};

export const toFeat = (etablissement: EtablissementGeoJsonData) =>
	JSON.stringify(
		{
			type: 'Feature',
			properties: {
				nom: etablissement.e ? `${etablissement.e} (${etablissement.n})` : etablissement.n,
				siret: etablissement.s,
				...(etablissement.dc ? { creation: etablissement.dc } : {}),
				...(etablissement.df ? { fermeture: etablissement.df } : {}),
				...(etablissement.ce ? { creationEntreprise: etablissement.ce } : {}),
			},
			geometry:
				etablissement.la && etablissement.lo
					? {
							type: 'Point',
							coordinates: [etablissement.lo, etablissement.la],
					  }
					: null,
		},
		null,
		0
	);
