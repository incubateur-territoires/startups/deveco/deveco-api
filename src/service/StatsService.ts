import { subDays, setHours } from 'date-fns';
import { FindManyOptions, MoreThanOrEqual } from 'typeorm';

import AppDataSource from '../data-source';
import { EventLog } from '../entity/EventLog';
import { DevEco } from '../entity/DevEco';
import { Territory } from '../entity/Territory';
import { isSmtp, send } from '../lib/emailing';
import { Rappel } from '../entity/Rappel';
import { Entite } from '../entity/Entite';
import { Contact } from '../entity/Contact';
import { Echange } from '../entity/Echange';
import { displayFicheName, wait } from '../lib/utils';
import { Account } from '../entity/Account';

export type Stats = {
	consultations: StatsData;
	echanges: StatsData;
	contacts: StatsData;
	rappels: StatsData;
	qualifications: StatsData;
	etablissements: StatsData;
	createurs: StatsData;
	territoire: string;
};

export type StatsData = {
	period: number;
	periodList: EntiteData[];
	total: number;
};

export type EntiteData = {
	nom: string;
	date: string;
	contenu: string;
	siret?: string;
	ficheId?: number;
};

class StatsService {
	private static instance: StatsService;

	public static getInstance(): StatsService {
		if (!StatsService.instance) {
			StatsService.instance = new StatsService();
		}

		return StatsService.instance;
	}

	private static formatEntiteSimple(entite: Entite): EntiteData | null {
		const isEtablissement = !!entite.entreprise;

		const nom = displayFicheName(entite);

		return {
			nom,
			date: '',
			contenu: '',
			siret: isEtablissement ? entite.entreprise?.siret : undefined,
			ficheId: isEtablissement ? undefined : entite.fiche?.id,
		};
	}

	public static formatAndFilter =
		<T>(formatter: (arg: T) => EntiteData | null) =>
		(data: T[]) =>
			data.reduce((acc: EntiteData[], item: T) => {
				const formatted = formatter(item);

				if (formatted) {
					acc.push(formatted);
				}

				return acc;
			}, []);

	private async computeStatsForTerritoire(territoire: Territory): Promise<Stats | null> {
		const today = new Date();
		const twoWeeksAgo = setHours(subDays(today, 14), 12);
		const time = twoWeeksAgo;

		//

		const consultationsQuery = AppDataSource.createQueryBuilder()
			.select('event_log')
			.from(EventLog, 'event_log')
			.innerJoin(DevEco, 'deveco', 'deveco.id = event_log.deveco_id')
			.where(`event_log.action = 'VIEW'`)
			.andWhere(`deveco.territory_id = :territoireId`, { territoireId: territoire.id });
		const allConsultations = await consultationsQuery.getCount();
		const periodConsultations = await consultationsQuery
			.andWhere('event_log.date > :time', { time })
			.getMany();
		const consultations = {
			total: allConsultations,
			period: periodConsultations.length,
			periodList: [],
		};

		//

		const rappelsQuery = AppDataSource.createQueryBuilder()
			.select('rappel')
			.from(Rappel, 'rappel')
			.innerJoin(DevEco, 'deveco', 'deveco.id = rappel.createur_id')
			.where(`deveco.territory_id = :territoireId`, { territoireId: territoire.id });
		const allRappels = await rappelsQuery.getCount();
		const periodRappels = await rappelsQuery
			.andWhere('rappel.date_rappel > :time', { time })
			.getMany();
		const rappels = {
			total: allRappels,
			period: periodRappels.length,
			periodList: [],
		};

		//

		const entitesQuery = AppDataSource.createQueryBuilder()
			.select('entite')
			.from(Entite, 'entite')
			.where(`entite.territoire_id = :territoireId`, { territoireId: territoire.id });

		const etablissementsQuery = entitesQuery.clone().andWhere(`entite_type = 'PM'`);
		const allEtablissements = await etablissementsQuery.getCount();
		const periodEtablissements = await etablissementsQuery
			.andWhere('entite.created_at > :time', { time })
			.getMany();
		const etablissements = {
			total: allEtablissements,
			period: periodEtablissements.length,
			periodList: [],
		};

		const createursQuery = entitesQuery.clone().andWhere(`entite_type = 'PP'`);
		const allCreateurs = await createursQuery.getCount();
		const periodCreateurs = await createursQuery
			.andWhere('entite.created_at > :time', { time })
			.getMany();
		const createurs = {
			total: allCreateurs,
			period: periodCreateurs.length,
			periodList: [],
		};

		//

		const contactsQuery = AppDataSource.createQueryBuilder()
			.select('contact')
			.from(Contact, 'contact')
			.innerJoin(Entite, 'entite', 'entite.id = contact.entite_id')
			.where(`entite.territoire_id = :territoireId`, { territoireId: territoire.id });
		const allContacts = await contactsQuery.getCount();
		const periodContacts = await contactsQuery
			.andWhere('contact.created_at > :time', { time })
			.getMany();

		const contacts = {
			total: allContacts,
			period: periodContacts.length,
			periodList: [],
		};

		//

		const echangesOptions = (date: Date): FindManyOptions<Echange> => ({
			where: {
				fiche: {
					territoire: {
						id: territoire.id,
					},
				},
				createdAt: MoreThanOrEqual(date),
			},
			relations: {
				fiche: {
					entite: {
						fiche: true,
						entreprise: true,
						particulier: true,
					},
				},
			},
		});

		const allEchangesCount = await AppDataSource.getRepository(Echange).count(
			echangesOptions(new Date(0))
		);
		const periodEchanges = await AppDataSource.getRepository(Echange).find(echangesOptions(time));

		const entitesEchanges = Array.from(
			periodEchanges
				.reduce((acc: Map<Entite['id'], Entite>, echange: Echange) => {
					const entite = echange.fiche?.entite;
					if (entite) {
						acc.set(entite.id, entite);
					}

					return acc;
				}, new Map())
				.values()
		);

		const echanges = {
			total: allEchangesCount,
			period: periodEchanges.length,
			periodList: StatsService.formatAndFilter(StatsService.formatEntiteSimple)(entitesEchanges),
		};

		//

		const qualificationsQuery = AppDataSource.createQueryBuilder()
			.select('event_log')
			.from(EventLog, 'event_log')
			.innerJoin(DevEco, 'deveco', 'deveco.id = event_log.deveco_id')
			.where(`event_log.action = 'UPDATE_QUALIFICATION'`)
			.andWhere(`deveco.territory_id = :territoireId`, { territoireId: territoire.id });
		const allQualifications = await qualificationsQuery.getCount();
		const periodQualifications = await qualificationsQuery
			.andWhere('event_log.date > :time', { time })
			.getMany();

		const entitesQualifications = new Map();
		for (const eventLog of periodQualifications) {
			if (eventLog.entityType === 'LOCAL') {
				continue;
			}

			const entiteId = eventLog.entityId;
			const entite = await AppDataSource.getRepository(Entite).findOne({
				where: {
					id: entiteId,
				},
				relations: {
					fiche: true,
					entreprise: true,
					particulier: true,
				},
			});

			if (!entite) {
				continue;
			}

			entitesQualifications.set(entite.id, entite);
		}

		const entiteQualifications = StatsService.formatAndFilter(StatsService.formatEntiteSimple)(
			Array.from(entitesQualifications.values())
		);
		const qualifications = {
			total: allQualifications,
			period: periodQualifications.length,
			periodList: entiteQualifications,
		};

		const totalActivites =
			consultations.period +
			echanges.period +
			contacts.period +
			rappels.period +
			qualifications.period +
			etablissements.period +
			createurs.period;

		return totalActivites > 0
			? {
					consultations,
					echanges,
					contacts,
					rappels,
					qualifications,
					etablissements,
					createurs,
					territoire: territoire.name,
			  }
			: null;
	}

	private static sendStatsToDeveco(appUrl: string, stats: Stats) {
		const subject = `Mon tableau de bord Deveco${' '}: les deux semaines précédentes de ${
			stats.territoire
		}`;

		return async (account: Account) => {
			const template = 'statsRecap' as const;
			const mailOptions = {
				options: {
					to: account.email,
					subject,
				},
				template,
				params: {
					url: {
						appUrl,
					},
					account,
					stats,
				},
			};

			send(mailOptions).catch((emailError) => {
				console.error(emailError);
			});
		};
	}

	public async sendTerritoryActivity() {
		if (!isSmtp()) {
			console.info('No SMTP config found');
			return;
		}

		const appUrl = process.env.APP_URL || '';

		const territoires = await AppDataSource.getRepository(Territory).find();

		// for each territoire
		for (const territoire of territoires) {
			// 1) compute stats
			const stats = await this.computeStatsForTerritoire(territoire);

			if (!stats) {
				continue;
			}

			// 2) get devecos
			const devecos = await AppDataSource.getRepository(DevEco).find({
				where: {
					territory: {
						id: territoire.id,
					},
				},
				relations: {
					account: true,
				},
			});

			// 3) send mails
			const mailSender = StatsService.sendStatsToDeveco(appUrl, stats);
			for (const deveco of devecos) {
				if (process.env.CAPTURE_TERRITORY_ACTIVITY) {
					console.info('Captured reminders to', process.env.CAPTURE_TERRITORY_ACTIVITY);
					const account = { email: process.env.CAPTURE_TERRITORY_ACTIVITY } as Account;
					mailSender(account);
					const delay = 500;
					await wait(delay);
				} else {
					console.info('Sending reminder to deveco', deveco.account.email);
					mailSender(deveco.account);
				}
			}
		}
	}
}

const statsService = StatsService.getInstance();

export default statsService;
