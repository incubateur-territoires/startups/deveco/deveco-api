import _fetch from 'cross-fetch';
import { StatusCodes } from 'http-status-codes';
import { FindOptionsRelations, In } from 'typeorm';
import { QueryRunner } from 'typeorm';

import { ReadStream } from 'fs';

import AppDataSource from '../data-source';
import { DevecoContext } from '../middleware/AuthMiddleware';
import { Etablissement } from '../entity/Etablissement';
import { DevEco } from '../entity/DevEco';
import { Entite } from '../entity/Entite';
import { Entreprise } from '../entity/Entreprise';
import { Territory } from '../entity/Territory';
import entiteService from '../service/EntiteService';
import entrepriseService, {
	EtablissementHistory,
	EtablissementsFreres,
} from '../service/EntrepriseService';
import { TerritoryEtablissementReference } from '../entity/TerritoryEtablissementRef';
import { Qpv } from '../entity/Qpv';
import { isSiret } from '../lib/regexp';
import {
	toEntiteView,
	EntiteView,
	toView,
	FicheView,
	EchangeView,
	ContactView,
} from '../view/FicheView';
import { EtablissementGeoJsonData, extraPropsMap, propsMap } from '../service/GeoJsonService';
import { replaceBanVoieToInseeVoie } from '../lib/normalisationAdresse';
import { SireneEtablissement } from '../entity/SireneEtablissement';
import { Demande } from '../entity/Demande';
import { displayIdentity } from '../lib/utils';
import { SireneUniteLegale } from '../entity/SireneUniteLegale';
import { anneeToDateEffectifs, trancheToEffectifs } from '../lib/trancheEffectifs';

export type EntityReadStream<T> = ReadStream & {
	on(event: 'data', listener: (chunk: T) => void): EntityReadStream<T>;
};

export type EffectifsGroupe = {
	effectifs: string | null;
	dateEffectifs: Date | null;
};

export type Payload = {
	entreprise: Entreprise &
		EffectifsGroupe & {
			exogene: boolean;
		} & {
			zonages?: string[];
		} & {
			ess?: boolean;
		};
	entite?: EntiteView;
	fiche?: FicheView;
	infos: Infos;
	sireneUpdate?: Date | null;
	freres?: EtablissementsFreres;
	history?: EtablissementHistory;
};

export type ExcelPayload = {
	entreprise: Entreprise & { qpvs?: Qpv[] };
	entite?: {
		contacts?: ContactView[];
		activitesReelles?: string[];
		localisations?: string[];
		motsCles?: string[];
		activiteAutre?: string;
	};
	fiche?: {
		echanges: EchangeView[];
		demandes?: Demande[];
	};
};

export type Infos = {
	contacts: boolean;
	echanges: boolean;
	rappels: boolean;
	favori: boolean;
};

export type Criteria = {
	recherche: string;
	etatType: string;
	codesNaf: string[];
	codesCommunes: string[];
	epcis: string[];
	tranchesEffectifs: string[];
	formes: string;
	territoire: number;
	cp: string;
	rue: string;
	suivi: Suivi;
	activites: string[];
	zones: string[];
	mots: string[];
	demandes: string[];
	territorialite: Territorialite;
	qpvs: string[];
	zrrs: string[];
	ess: string;
	zonages: string[];
};

const isDirection = (orderDirection?: string): orderDirection is 'ASC' | 'DESC' =>
	!!orderDirection && ['ASC', 'DESC'].includes(orderDirection);

export type Suivi = 'tous' | 'portefeuille' | 'favoris' | 'accompagnes';
export type Territorialite = 'tous' | 'endogene' | 'exogene';

export type EtablissementSiretWithInfos = {
	siret: string;
	exogene: boolean;
} & Infos;

export const relationsForToView: FindOptionsRelations<Entite> = {
	entreprise: {
		etablissement: true,
		exercices: true,
	},
	particulier: true,
	contacts: {
		particulier: true,
	},
	fiche: {
		demandes: true,
		echanges: {
			createur: {
				account: true,
			},
			auteurModification: {
				account: true,
			},
		},
		rappels: true,
	},
	etablissementCree: {
		etablissement: true,
	},
	createurLie: {
		entite: {
			fiche: true,
		},
	},
};

export const relationsForList: FindOptionsRelations<Entite> = {
	entreprise: true,
	particulier: true,
	contacts: true,
	fiche: {
		demandes: true,
		echanges: true,
	},
};

// this is the source of truth for whether to pre-filter on Entite when listing TER/Entreprises
// because Activites, Zones, Mots and Demandes can only exist in relation to Entites so it's much
// faster to not look through all in that case.
const filterOnEntite = ({ suivi, activites, zones, mots, demandes }: Criteria) =>
	suivi === 'portefeuille' ||
	suivi === 'accompagnes' ||
	activites.length > 0 ||
	zones.length > 0 ||
	mots.length > 0 ||
	demandes.length > 0;

const filterOnFavoris = ({ suivi }: Criteria) => suivi === 'favoris';

class InseeService {
	private static instance: InseeService;

	public static getInstance(): InseeService {
		if (!InseeService.instance) {
			InseeService.instance = new InseeService();
		}

		return InseeService.instance;
	}

	public siretsToPayload = async (deveco: DevEco, sirets: string[]): Promise<ExcelPayload[]> => {
		const etablissements = await AppDataSource.getRepository(Etablissement).find({
			where: { siret: In(sirets) },
		});

		// we need to sort to respect the original sirets order, because the In() does not
		etablissements.sort((e1, e2) => sirets.indexOf(e1.siret) - sirets.indexOf(e2.siret));

		const entites = await AppDataSource.getRepository(Entite).find({
			where: {
				entreprise: { siret: In(sirets.map((s) => s.padStart(14, '0'))) },
				territoire: { id: deveco.territory.id },
			},
			relations: {
				entreprise: true,
				fiche: {
					demandes: true,
					echanges: {
						auteurModification: {
							account: true,
						},
					},
				},
				contacts: {
					particulier: true,
				},
			},
		});

		const siretToEntite: Record<string, ExcelPayload['entite']> = {};
		for (const entite of entites) {
			siretToEntite[entite.entreprise?.siret ?? ''] = {
				contacts: (entite.contacts || []).map(({ fonction, particulier, id }) => ({
					fonction,
					...particulier,
					id,
				})),
				activitesReelles: entite.activitesReelles,
				localisations: entite.entrepriseLocalisations || [],
				motsCles: entite.motsCles,
				activiteAutre: entite.activiteAutre,
			};
		}

		const siretToFiche: Record<string, ExcelPayload['fiche']> = {};
		for (const entite of entites) {
			if (entite.fiche) {
				siretToFiche[entite.entreprise?.siret ?? ''] = {
					echanges: entite.fiche.echanges?.map((e) => ({
						...e,
						auteurModif: displayIdentity(e.auteurModification?.account),
					})),
					demandes: entite.fiche.demandes,
				};
			}
		}

		return etablissements.map(
			(etablissement: Etablissement): ExcelPayload => ({
				entreprise: {
					...entrepriseService.etablissementToEntreprise(etablissement),
				},
				entite: siretToEntite[etablissement.siret],
				fiche: siretToFiche[etablissement.siret],
			})
		);
	};

	private async queryEtablissementsList(
		sirets: string[],
		territoire: number,
		deveco: number
	): Promise<EtablissementSiretWithInfos[]> {
		const siretsValues = sirets.map((s) => `'${s}'`).join(',');

		const etablissements: EtablissementSiretWithInfos[] = await AppDataSource.getRepository(
			Etablissement
		)
			.createQueryBuilder('etablissement')
			.select('etablissement.siret', 'siret')
			.addSelect(
				`EXISTS
					(SELECT *
						FROM entite
						LEFT JOIN fiche on fiche.entite_id = entite.id
						WHERE entite.territoire_id = ${territoire}
						AND entite.entreprise_id = etablissement.siret
						AND (
							EXISTS (
								SELECT contact.id
								FROM contact
								JOIN particulier ON particulier.id = contact.particulier_id
								WHERE contact.entite_id = entite.id AND (particulier.email <> '' OR particulier.telephone <> '')
								LIMIT 1
							)
						)
					)`,
				'contacts'
			)
			.addSelect(
				`EXISTS
					(SELECT *
						FROM entite
						LEFT JOIN fiche on fiche.entite_id = entite.id
						LEFT JOIN demande on demande.fiche_id = fiche.id
						WHERE entite.territoire_id = ${territoire}
						AND entite.entreprise_id = etablissement.siret
						AND (
							EXISTS (
								SELECT echange.id
								FROM echange
								WHERE echange.fiche_id = fiche.id
								LIMIT 1
							)
						)
					)`,
				'echanges'
			)
			.addSelect(
				`EXISTS
					(SELECT *
						FROM entite
						LEFT JOIN fiche on fiche.entite_id = entite.id
						LEFT JOIN demande on demande.fiche_id = fiche.id
						WHERE entite.territoire_id = ${territoire}
						AND entite.entreprise_id = etablissement.siret
						AND (
							EXISTS (
								SELECT rappel.id
								FROM rappel
								WHERE rappel.fiche_id = fiche.id
								AND rappel.date_cloture IS NULL
								LIMIT 1
							)
						)
					)`,
				'rappels'
			)
			.addSelect(
				`
				(SELECT exogene
						FROM territory_etablissement_reference TER
						WHERE TER.siret = etablissement.siret
						AND TER.territory_id = ${territoire}
						LIMIT 1
				)`,
				'exogene'
			)
			.addSelect(
				`(SELECT COALESCE(favori, false)
						FROM etablissement_favori
						WHERE etablissement_favori.deveco_id = ${deveco}
						AND etablissement_favori.territory_id = ${territoire}
						AND etablissement_favori.siret = etablissement.siret
						LIMIT 1
				)`,
				'favori'
			)
			.where(`etablissement.siret IN (${siretsValues})`)
			.getRawMany();

		return etablissements;
	}

	public siretsToListPayload = async (deveco: DevEco, sirets: string[]): Promise<Payload[]> => {
		const [etablissements, etablissementsInfos]: [Etablissement[], EtablissementSiretWithInfos[]] =
			await Promise.all([
				AppDataSource.getRepository(Etablissement).find({
					where: { siret: In(sirets) },
				}),
				this.queryEtablissementsList(sirets, deveco.territory.id, deveco.id),
			]);

		const siretToInfos: Map<string, EtablissementSiretWithInfos> = etablissementsInfos.reduce(
			(acc, { siret, ...infos }) => acc.set(siret, infos),
			new Map()
		);

		// we need to sort to respect the original sirets order, because the In() does not
		etablissements.sort((e1, e2) => sirets.indexOf(e1.siret) - sirets.indexOf(e2.siret));

		return etablissements.map((etablissement: Etablissement): Payload => {
			const infos = siretToInfos.get(etablissement.siret);
			const effectifsGroupe = {
				effectifsGroupe: null,
				dateEffectifsGroupe: null,
			};

			return {
				entreprise: {
					...entrepriseService.etablissementToEntreprise(etablissement),
					// TODO: create specialized types for front payloads
					etablissement: undefined as unknown as Etablissement,
					exogene: !!infos?.exogene,
					...effectifsGroupe,
				},
				infos: {
					contacts: !!infos?.contacts,
					echanges: !!infos?.echanges,
					rappels: !!infos?.rappels,
					favori: !!infos?.favori,
				},
			};
		});
	};

	public async searchBySiret(search: string): Promise<null | Etablissement> {
		const siret = search.replaceAll(/\s/g, '');
		if (!isSiret(siret)) {
			// TODO return actual error about invalid SIRET
			return null;
		}

		return await AppDataSource.getRepository(Etablissement).findOne({
			where: {
				siret,
			},
		});
	}

	private makeTerQuery({
		criteria,

		pagination,
		selects,
		conditions,
		joins,
		withs,

		orderByDirective,
	}: {
		criteria: Criteria;

		pagination: string[];
		selects: string[];
		conditions: string[];
		joins: Record<string, string>;
		withs: string[];

		orderByDirective: string;
	}): [string, string] {
		const { territoire } = criteria;

		const defaultWheres = [`TER.territory_id = ${territoire}`];

		const baseQuery = [
			`FROM territory_etablissement_reference AS TER`,
			`LEFT JOIN entite ON entite.entreprise_id = TER.siret AND entite.territoire_id = TER.territory_id`,
			Object.values(joins).join(' '),
			`WHERE ${defaultWheres.join(' AND ')}`,
			conditions.length ? `AND ${conditions.join(' AND ')}` : '',
		];

		const countQuery = [
			withs.length ? `WITH ${withs.join(',')}` : '',
			'SELECT count(TER.id)',
			...baseQuery,
		].join(' ');
		const itemsQuery = [
			withs.length ? `WITH ${withs.join(',')}` : '',
			...selects,
			...baseQuery,
			orderByDirective,
			...pagination,
		].join(' ');

		return [countQuery, itemsQuery];
	}

	private makeFavoriQuery({
		criteria,

		pagination,
		selects,
		conditions,
		joins,
		withs,

		orderByDirective,

		deveco,
	}: {
		criteria: Criteria;

		selects: string[];
		pagination: string[];
		conditions: string[];
		joins: Record<string, string>;
		withs: string[];

		orderByDirective: string;

		deveco: DevEco;
	}): [string, string] {
		const { activites, zones, mots, demandes } = criteria;

		const from = `FROM etablissement_favori FAV`;
		const defaultWheres = [`FAV.deveco_id = ${deveco.id}`, 'FAV.favori'];

		if (activites?.length) {
			const activitesValues = activites.map((a) => `'${a.replaceAll("'", "''")}'`).join(',');
			conditions.push(
				`ARRAY[${activitesValues}] && string_to_array(entite.activites_reelles, ',')`
			);
		}

		if (zones?.length) {
			const zonesValues = zones.map((z) => `'${z.replaceAll("'", "''")}'`).join(',');
			conditions.push(
				`ARRAY[${zonesValues}] && string_to_array(entite.entreprise_localisations, ',')`
			);
		}

		if (mots?.length) {
			const motsValues = mots.map((m) => `'${m.replaceAll("'", "''")}'`).join(',');
			conditions.push(`ARRAY[${motsValues}] && string_to_array(entite.mots_cles, ',')`);
		}

		if (demandes?.length) {
			joins.fiche = `LEFT JOIN fiche ON fiche.entite_id = entite.id`;

			const demandesValues = demandes.map((d) => `'${d.replaceAll("'", "''")}'`).join(',');
			conditions.push(
				`EXISTS
					(SELECT demande.id
						FROM demande
						WHERE demande.fiche_id = fiche.id
						AND (demande.cloture = false OR demande.cloture is null)
						AND demande.type_demande IN (${demandesValues})
					)`
			);
		}

		const baseQuery = [
			from,
			`INNER JOIN territory_etablissement_reference TER ON TER.siret = FAV.siret AND TER.territory_id = FAV.territory_id`,
			`LEFT JOIN entite ON TER.siret = entite.entreprise_id`,
			Object.values(joins).join(' '),
			`WHERE ${defaultWheres.join(' AND ')}`,
			conditions.length ? `AND ${conditions.join(' AND ')}` : '',
		];

		const countQuery = [
			withs.length ? `WITH ${withs.join(',')}` : '',
			'SELECT count(FAV.id)',
			...baseQuery,
		].join(' ');

		const itemsQuery = [
			withs.length ? `WITH ${withs.join(',')}` : '',
			...selects,
			...baseQuery,
			orderByDirective,
			...pagination,
		].join(' ');

		return [countQuery, itemsQuery];
	}

	private makeSuiviQuery({
		criteria,

		pagination,
		selects,
		conditions,
		joins,
		withs,

		orderByDirective,
	}: {
		criteria: Criteria;

		selects: string[];
		pagination: string[];
		conditions: string[];
		joins: Record<string, string>;
		withs: string[];

		orderByDirective: string;
	}): [string, string] {
		const { territoire, suivi, activites, zones, mots, demandes } = criteria;

		const from = `FROM entite`;
		const defaultWheres = [`entite.territoire_id = ${territoire}`];

		joins.ter = `INNER JOIN territory_etablissement_reference TER ON entite.entreprise_id = TER.siret AND entite.territoire_id = TER.territory_id`;

		if (suivi === 'portefeuille') {
			joins.fiche = 'LEFT JOIN fiche ON entite.id = fiche.entite_id';
			const or = [
				`(entite.activites_reelles IS NOT NULL AND entite.activites_reelles <> '')`,
				`(entite.entreprise_localisations IS NOT NULL AND entite.entreprise_localisations <> '')`,
				`(entite.mots_cles IS NOT NULL AND entite.mots_cles <> '')`,
				`(entite.activite_autre IS NOT NULL AND entite.activite_autre <> '')`,
				`EXISTS (
					SELECT rappel.id
					FROM rappel
					WHERE rappel.fiche_id = fiche.id
					LIMIT 1
				)`,
				`EXISTS (
					SELECT echange.id
					FROM echange
					WHERE echange.fiche_id = fiche.id
					LIMIT 1
				)`,
				`EXISTS (
					SELECT contact.id
					FROM contact
					JOIN particulier ON particulier.id = contact.particulier_id
					WHERE contact.entite_id = entite.id AND (particulier.email <> '' OR particulier.telephone <> '')
					LIMIT 1
				)`,
			];
			conditions.push('( ' + or.join(' OR ') + ' )');
		}

		if (suivi === 'accompagnes') {
			conditions.push(`entite.createur_id IS NOT NULL`);
		}

		if (activites?.length) {
			const activitesValues = activites.map((a) => `'${a.replaceAll("'", "''")}'`).join(',');
			conditions.push(
				`ARRAY[${activitesValues}] && string_to_array(entite.activites_reelles, ',')`
			);
		}

		if (zones?.length) {
			const zonesValues = zones.map((z) => `'${z.replaceAll("'", "''")}'`).join(',');
			conditions.push(
				`ARRAY[${zonesValues}] && string_to_array(entite.entreprise_localisations, ',')`
			);
		}

		if (mots?.length) {
			const motsValues = mots.map((m) => `'${m.replaceAll("'", "''")}'`).join(',');
			conditions.push(`ARRAY[${motsValues}] && string_to_array(entite.mots_cles, ',')`);
		}

		if (demandes?.length) {
			joins.fiche = `LEFT JOIN fiche ON fiche.entite_id = entite.id`;

			const demandesValues = demandes.map((d) => `'${d.replaceAll("'", "''")}'`).join(',');
			conditions.push(
				`EXISTS
					(SELECT demande.id
						FROM demande
						WHERE demande.fiche_id = fiche.id
						AND (demande.cloture = false OR demande.cloture is null)
						AND demande.type_demande IN (${demandesValues})
					)`
			);
		}

		const baseQuery = [
			from,
			Object.values(joins).join(' '),
			`WHERE ${defaultWheres.join(' AND ')}`,
			conditions.length ? `AND ${conditions.join(' AND ')}` : '',
		];

		const countQuery = [
			withs.length ? `WITH ${withs.join(',')}` : '',
			'SELECT count(entite.id)',
			...baseQuery,
		].join(' ');

		const itemsQuery = [
			withs.length ? `WITH ${withs.join(',')}` : '',
			...selects,
			...baseQuery,
			orderByDirective,
			...pagination,
		].join(' ');

		return [countQuery, itemsQuery];
	}

	private makeQueryForCriteria({
		criteria,

		pagination,
		selects,
		conditions,
		joins,
		withs,

		orderByDirective,

		deveco,
	}: {
		criteria: Criteria;

		selects: string[];
		pagination: string[];
		conditions: string[];
		joins: Record<string, string>;
		withs: string[];

		orderByDirective: string;

		deveco: DevEco;
	}): [string, string] {
		if (filterOnFavoris(criteria)) {
			return this.makeFavoriQuery({
				criteria,

				pagination,
				selects,
				conditions: conditions.slice(),
				joins,
				withs: withs.slice(),

				orderByDirective,

				deveco,
			});
		}

		if (filterOnEntite(criteria)) {
			return this.makeSuiviQuery({
				criteria,

				pagination,
				selects,
				conditions,
				joins,
				withs,

				orderByDirective,
			});
		}

		return this.makeTerQuery({
			criteria,

			pagination,
			selects,
			conditions: conditions.slice(),
			joins,
			withs: withs.slice(),

			orderByDirective,
		});
	}

	public async search({
		criteria,
		page,
		resultsPerPage,
		countOnly,
		orderBy,
		orderDirection,
		queryRunner,
		deveco,
	}: {
		criteria: Criteria;
		page?: number;
		resultsPerPage?: number;
		countOnly?: boolean;
		orderBy?: string;
		orderDirection?: string;
		queryRunner: QueryRunner;
		deveco: DevEco;
	}): Promise<EntityReadStream<{ siret: string }>>;
	public async search({
		criteria,
		page,
		resultsPerPage,
		countOnly,
		orderBy,
		orderDirection,
		deveco,
	}: {
		criteria: Criteria;
		page?: number;
		resultsPerPage?: number;
		countOnly?: boolean;
		orderBy?: string;
		orderDirection?: string;
		deveco: DevEco;
	}): Promise<[string[], number]>;
	public async search({
		criteria,
		page,
		resultsPerPage,
		countOnly,
		orderBy,
		orderDirection,
		queryRunner,
		deveco,
	}: {
		criteria: Criteria;
		page?: number;
		resultsPerPage?: number;
		countOnly?: boolean;
		orderBy?: string;
		orderDirection?: string;
		queryRunner?: QueryRunner;
		deveco: DevEco;
	}): Promise<[string[], number] | EntityReadStream<{ siret: string }>> {
		const {
			recherche,
			etatType,
			codesNaf,
			codesCommunes,
			epcis,
			tranchesEffectifs,
			formes,
			territoire,
			cp,
			rue,
			territorialite,
			qpvs,
			zrrs,
			ess,
			zonages,
		} = criteria;

		const selects = ['SELECT TER.siret'];
		const conditions: string[] = [];
		const joins: Record<string, string> = {};
		const withs: string[] = [];

		if (recherche) {
			const or = [];
			or.push(`TER.nom_recherche ilike '%${recherche}%'`);
			or.push(`TER.tsv @@to_tsquery('french', '''${recherche.replaceAll("''", ' ').trim()}''')`);
			or.push(`TER.siret ILIKE '%${recherche}%'`);
			withs.push(`
				existing_particulier (entite_id) AS
					(
						SELECT
							contact.entite_id
						FROM
							particulier
							INNER JOIN contact ON particulier.id = contact.particulier_id
							INNER JOIN entite ON entite.id = contact.entite_id
						WHERE
							particulier.nom ILIKE '%${recherche}%'
							AND entite.territoire_id = ${territoire}
					)
			`);
			joins.existingParticulier =
				'LEFT JOIN existing_particulier ON existing_particulier.entite_id = entite.id';
			or.push(`existing_particulier.entite_id is not null`);
			conditions.push('( ' + or.join(' OR ') + ' )');
		}

		switch (etatType) {
			case 'F':
				conditions.push(`TER.etat_administratif = 'F'`);
				break;
			case 'A':
				conditions.push(`TER.etat_administratif = 'A'`);
				break;
			case 'T':
			default:
				// no filter
				break;
		}

		if (codesNaf?.length) {
			conditions.push(`TER.code_naf IN (${codesNaf.map((c) => `'${c}'`).join(',')})`);
		}

		if (codesCommunes?.length) {
			conditions.push(`TER.code_commune IN (${codesCommunes.map((c) => `'${c}'`).join(',')})`);
		}

		if (epcis?.length) {
			conditions.push(
				`TER.code_commune IN (SELECT insee_com FROM commune WHERE insee_epci IN (${epcis
					.map((c) => `'${c}'`)
					.join(',')}))`
			);
		}

		if (tranchesEffectifs?.length) {
			conditions.push(
				`TER.tranche_effectifs IN (${tranchesEffectifs.map((t) => `'${t}'`).join(',')})`
			);
		}

		if (formes === 'courantes') {
			conditions.push(`TER.categorie_juridique_courante`);
		}

		if (territorialite === 'exogene') {
			conditions.push('TER.exogene');
		} else if (territorialite === 'endogene') {
			conditions.push('NOT TER.exogene');
		}

		if (cp && rue) {
			// normalize string to remove accents for search (the data we use is normalized, it would seem)
			const rueNormalized = replaceBanVoieToInseeVoie(rue)
				.replace("'", "''")
				.normalize('NFD')
				.replace(/\p{Diacritic}/gu, '');

			// we try to support both BAN-style addresses and raw INSEE addresses
			conditions.push(
				[
					'(',
					`TER.adresse_complete ILIKE '%${rue.replace("'", '’')}%${cp}%'`, // BAN format
					'OR',
					`TER.adresse_complete ILIKE '%${rueNormalized}%${cp}%'`, // INSEE format
					')',
				].join(' ')
			);
		}

		if (qpvs?.length) {
			conditions.push(`TER.qpv_id IN (${qpvs.map((q) => `'${q}'`).join(',')})`);
		}

		if (zrrs?.length) {
			const zrrValues = zrrs
				.filter((zrr) => {
					switch (zrr) {
						case 'C':
						case 'P':
						case 'N':
							return true;
					}
				})
				.map((zrr) => `'${zrr}'`)
				.join(',');

			if (zrrValues.length > 0) {
				conditions.push(`TER.zrr IN (${zrrValues})`);
			}
		}

		if (ess) {
			if (ess === 'oui') {
				conditions.push(`TER.ess`);
			} else if (ess === 'non') {
				conditions.push(`NOT TER.ess`);
			}
		}

		if (zonages?.length) {
			const zonagesValues = zonages.map((z) => `${z}`).join(',');

			joins.zonages = `INNER JOIN territory_etablissement_reference_zonages_zonage ON TER.id = territory_etablissement_reference_zonages_zonage.territory_etablissement_reference_id`;
			joins.zones = `INNER JOIN zonage ON zonage.id = territory_etablissement_reference_zonages_zonage.zonage_id`;
			conditions.push(`zonage.id IN (${zonagesValues})`);
		}

		const pagination = [];
		if (page && resultsPerPage) {
			const skip = (page - 1) * resultsPerPage;
			pagination.push(`OFFSET ${skip}`);
			pagination.push(`LIMIT ${resultsPerPage}`);
		}

		const direction: 'ASC' | 'DESC' = isDirection(orderDirection) ? orderDirection : 'DESC';

		const nulls: 'NULLS FIRST' | 'NULLS LAST' = 'NULLS LAST';

		let orderByDirective: string;
		switch (orderBy) {
			// TODO find a good sorting criterion; currently we have none in TER, afaict
			// case 'AZ': {
			//   orderByDirective = `ORDER BY ??? ${direction} ${nulls}`;
			//   break;
			// }
			case 'DC': {
				orderByDirective = `ORDER BY TER.date_creation ${direction} ${nulls}, TER.siret`;
				break;
			}
			case 'CO':
			default: {
				withs.push(`
					view_order(id, date_visualisation) as (
						SELECT E.id, MAX(EL.date) as date_visualisation
						FROM event_log EL
						JOIN entite E on E.id = EL.entity_id and E.territoire_id = ${territoire}
						WHERE
							EL.action = 'VIEW'
							AND EL.entity_type = 'FICHE_PM'
						GROUP BY E.id
					)
				`);

				joins.viewOrder = `LEFT JOIN view_order ON view_order.id = entite.id`;

				orderByDirective = `ORDER BY
					view_order.date_visualisation ${direction} ${nulls},
					TER.date_creation ${direction} ${nulls},
					TER.siret
				`;

				break;
			}
		}

		const [countQuery, itemsQuery] = this.makeQueryForCriteria({
			criteria,

			pagination,
			selects,
			conditions,
			joins,
			withs,

			orderByDirective,

			deveco,
		});

		if (queryRunner) {
			return queryRunner.stream(itemsQuery);
		}

		if (countOnly) {
			const count = await AppDataSource.query(countQuery);
			return [[], Number(count[0].count)];
		}

		const [count, items] = await Promise.all([
			AppDataSource.query(countQuery),
			AppDataSource.query(itemsQuery),
		]);

		return [items.map(({ siret }: { siret: string }) => siret), Number(count[0].count)];
	}

	public parseSuivi(suivi?: string): Suivi {
		if (!suivi) {
			return 'tous';
		}
		if (['tous', 'portefeuille', 'favoris', 'accompagnes'].includes(suivi)) {
			return suivi as Suivi;
		}
		return 'tous';
	}

	public parseTerritorialite(territorialite?: string): Territorialite {
		if (!territorialite) {
			return 'tous';
		}
		if (['tous', 'endogene', 'exogene'].includes(territorialite)) {
			return territorialite as Territorialite;
		}
		return 'tous';
	}

	public async augmentedEntrepriseFromEntite({
		ctx,
		deveco,
		entite,
		withFreres,
		withHistory,
	}: {
		ctx: DevecoContext;
		deveco: DevEco;
		entite: Entite;
		withFreres?: boolean;
		withHistory?: boolean;
	}): Promise<Payload> {
		const territoire = deveco.territory;

		const entreprise = entite.entreprise;

		if (!entreprise) {
			return ctx.throw(
				StatusCodes.BAD_REQUEST,
				`Cannot serialize augmented entreprise for Entite for a Personne Physique`,
				entite
			);
		}

		const siret = entreprise.siret;
		const siren = entreprise.etablissement.siren;

		const [zonagesRaw, infos, sireneEtablissement, sireneUniteLegale, freres, history]: [
			Record<string, string> | undefined,
			Infos,
			SireneEtablissement,
			SireneUniteLegale,
			EtablissementsFreres,
			EtablissementHistory
		] = await Promise.all([
			AppDataSource.createQueryBuilder()
				.select(`COALESCE(qpv.nomQp, '')`, 'qpv')
				.addSelect(
					`case ter.zrr when 'C' then 'ZRR' when 'P' then 'ZRR (partielle)' else '' end`,
					'zrr'
				)
				.from(TerritoryEtablissementReference, 'ter')
				.leftJoin(Qpv, 'qpv', 'ter.qpv_id = qpv.gid')
				.where('ter.siret = :siret', { siret })
				.getRawOne(),

			entiteService.infosForEntite(entite?.id, deveco.id, deveco.territory.id),

			AppDataSource.getRepository(SireneEtablissement).findOneOrFail({
				where: {
					siret,
				},
				select: {
					siret: true,
					dateDernierTraitementEtablissement: true,
				},
			}),

			AppDataSource.getRepository(SireneUniteLegale).findOneOrFail({
				where: {
					siren,
				},
				select: {
					siren: true,
					economieSocialeSolidaireUniteLegale: true,
					trancheEffectifsUniteLegale: true,
					anneeEffectifsUniteLegale: true,
				},
			}),

			withFreres
				? entrepriseService.getEtablissementsFreres(entreprise, territoire, deveco)
				: Promise.resolve([]),

			withHistory
				? entrepriseService.getEtablissementHistory(entreprise, territoire, deveco)
				: Promise.resolve({ predecesseurs: [], successeurs: [] }),
		]);

		const { dateDernierTraitementEtablissement: sireneUpdate } = sireneEtablissement;

		const { economieSocialeSolidaireUniteLegale } = sireneUniteLegale;

		const zonages: string[] = zonagesRaw ? Object.values(zonagesRaw).filter(Boolean) : [];

		const effectifsGroupe = {
			effectifsGroupe: trancheToEffectifs(sireneUniteLegale.trancheEffectifsUniteLegale || ''),
			dateEffectifsGroupe:
				anneeToDateEffectifs(
					sireneUniteLegale.anneeEffectifsUniteLegale
						? `${sireneUniteLegale.anneeEffectifsUniteLegale}`
						: ''
				) ?? null,
		};

		return {
			entreprise: {
				...entreprise,
				exogene: !entrepriseService.isCodeCommuneInTerritory({
					codeCommune: entreprise.etablissement?.codeCommune || '',
					territoire,
				}),
				zonages,
				ess: economieSocialeSolidaireUniteLegale === 'O',
				...effectifsGroupe,
			},
			...(entite ? { entite: toEntiteView(entite) } : {}),
			...(entite?.fiche ? { fiche: toView(entite, entite.fiche) } : {}),
			infos,
			sireneUpdate,
			freres,
			history,
		};
	}

	public createEtablissementStream(
		queryRunner: QueryRunner,
		territoire: Territory,
		requestedKeys: Array<keyof typeof propsMap | keyof typeof extraPropsMap>,
		sirets?: string[]
	): Promise<EntityReadStream<EtablissementGeoJsonData>> {
		const selectKeys = Object.entries({ ...propsMap, ...extraPropsMap })
			.filter(([k]) => requestedKeys.includes(k as any))
			.map(([k, v]) => {
				return `E.${v} as ${k}`;
			})
			.join(', ');

		let query = `
			SELECT ${selectKeys}
			FROM territory_etablissement_reference TER
			INNER JOIN etablissement E ON E.siret = TER.siret
			WHERE TER.territory_id = $1
		`;

		if (sirets?.length) {
			query += `
			AND TER.siret IN (
				${sirets.map((siret) => `'${siret}'`).join(',')}
			)
		`;
		}

		return queryRunner.stream(query, [territoire.id]) as Promise<
			EntityReadStream<EtablissementGeoJsonData>
		>;
	}
}

const inseeService = InseeService.getInstance();

export default inseeService;
