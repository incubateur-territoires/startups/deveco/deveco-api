import AppDataSource from '../data-source';
import { Contact } from '../entity/Contact';
import { DevEco } from '../entity/DevEco';
import { Fiche } from '../entity/Fiche';
import { Particulier } from '../entity/Particulier';
import { Entreprise } from '../entity/Entreprise';
import { Entite } from '../entity/Entite';
import particulierService from '../service/ParticulierService';
import ficheService from '../service/FicheService';
import entiteService from '../service/EntiteService';

class ContactService {
	private static instance: ContactService;

	public static getInstance(): ContactService {
		if (!ContactService.instance) {
			ContactService.instance = new ContactService();
		}

		return ContactService.instance;
	}

	public async createContact({
		particulierId,
		fiche,
		fonction,
		deveco,
	}: {
		particulierId: Particulier['id'];
		fiche: Fiche;
		fonction: string;
		deveco: DevEco;
	}): Promise<Contact> {
		const contact = AppDataSource.getRepository(Contact).create({
			fonction,
			particulier: { id: particulierId },
			entite: fiche.entite,
		});
		const newContact = await AppDataSource.getRepository(Contact).save(contact);

		await ficheService.touchFiche({
			ficheId: fiche.id,
			auteur: deveco,
		});

		return newContact;
	}

	public async createContactFromParticulier({
		nom,
		prenom,
		email,
		telephone,
		dateDeNaissance,
		fonction,
		deveco,
		entite,
		source,
	}: {
		nom: string;
		prenom: string;
		email: string;
		telephone: string;
		dateDeNaissance: string;
		fonction: string;
		deveco: DevEco;
		entite: Entite;
		source: Contact['source'];
	}): Promise<Contact> {
		const particulierId = await particulierService.createParticulier({
			nom,
			prenom,
			email,
			telephone,
			dateDeNaissance: dateDeNaissance as unknown as Date,
		});
		const contact = AppDataSource.getRepository(Contact).create({
			fonction,
			particulier: { id: particulierId },
			entite,
			source,
		});
		const newContact = await AppDataSource.getRepository(Contact).save(contact);

		await entiteService.touchEntite({
			id: entite.id,
			auteur: deveco,
		});

		return newContact;
	}

	public async createInitialContactsForEntite({
		entreprise,
		entite,
	}: {
		entreprise: Entreprise;
		entite: Entite;
	}): Promise<void> {
		for (const ms of entreprise.mandatairesSociaux) {
			if (ms.type !== 'personne_physique') {
				continue;
			}

			const newParticulier = AppDataSource.getRepository(Particulier).create({
				nom: ms.nom ?? '',
				prenom: ms.prenom ?? '',
				dateDeNaissance: ms.date_naissance ? new Date(`${ms.date_naissance} 12:00:00`) : null, // the timestamp is in seconds, we need it in milliseconds
			});
			const particulier = await AppDataSource.getRepository(Particulier).save(newParticulier);
			const newContact = AppDataSource.getRepository(Contact).create({
				particulier,
				fonction: ms.fonction,
				entite,
				source: 'API',
			});
			await AppDataSource.getRepository(Contact).save(newContact);
		}
	}
}

const contactService = ContactService.getInstance();

export default contactService;
