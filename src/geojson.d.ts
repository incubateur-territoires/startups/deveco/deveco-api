declare module 'geojson' {
	export function parse(data: T[], options: Record<string, any>): string;
}
