import { Demande } from '../entity/Demande';
import { Echange } from '../entity/Echange';
import { Entite } from '../entity/Entite';
import { Entreprise } from '../entity/Entreprise';
import { Fiche } from '../entity/Fiche';
import { Particulier } from '../entity/Particulier';
import { Qpv } from '../entity/Qpv';
import { Rappel } from '../entity/Rappel';
import { Territory } from '../entity/Territory';
import { displayFirstName, displayIdentity } from '../lib/utils';

export type EchangeView = Echange & { auteurModif?: string };

export interface EntiteView {
	entiteId?: number;

	territoire: Territory;

	contacts?: ContactView[];

	entiteType: string;

	activitesReelles?: string[];

	localisations?: string[];

	motsCles?: string[];

	activiteAutre?: string;

	entreprise?: Entreprise;

	particulier?: Particulier;

	futureEnseigne?: string;

	nbProprietes?: number[];

	zonagesPrioritaires?: string[];

	etablissementCree?: Entreprise;

	createurLie?: {
		particulier: Particulier;
		ficheId: Fiche['id'];
	};
}

export type FicheView = EntiteView & {
	id?: number;

	auteurModification: string;

	echanges: EchangeView[];

	rappels?: Rappel[];

	demandes?: Demande[];

	dateModification?: Date | null;

	sireneUpdate?: string;
};

export type ContactView = {
	fonction?: string;
	nom: string;
	prenom: string;
	email?: string;
	telephone?: string;
	dateDeNaissance?: Date | null;
};

export function toEntiteView(entite: Entite, zonagesPrioritaires?: Qpv[]): EntiteView {
	const createurLie =
		entite?.createurLie && entite?.createurLie?.entite?.fiche?.id
			? {
					particulier: entite.createurLie,
					ficheId: entite.createurLie.entite.fiche.id,
			  }
			: undefined;

	return {
		entiteId: entite.id,
		territoire: entite.territoire,
		contacts: (entite.contacts || []).map(({ fonction, particulier, id }) => ({
			fonction,
			...particulier,
			id,
		})),
		entiteType: entite.entiteType,
		activitesReelles: entite.activitesReelles,
		localisations: entite.entrepriseLocalisations || [],
		motsCles: entite.motsCles,
		activiteAutre: entite.activiteAutre,
		entreprise: entite.entreprise
			? { ...entite.entreprise, nom: entite.entreprise?.nom?.toLocaleUpperCase() }
			: undefined,
		particulier: entite.particulier,
		futureEnseigne: entite.futureEnseigne,
		nbProprietes: entite.proprietes?.map((e) => e.id || 0),
		zonagesPrioritaires: zonagesPrioritaires?.map(({ nomQp }) => 'QPV ' + nomQp),
		etablissementCree: entite.etablissementCree,
		createurLie,
	};
}

// TODO refactoring: use Entite as the entry point instead of Fiche
export function toView(entite: Entite, fiche: Fiche, zonagesPrioritaires?: Qpv[]): FicheView {
	return {
		...toEntiteView(entite, zonagesPrioritaires),
		id: fiche.id,
		auteurModification: displayIdentity(fiche.auteurModification?.account),
		dateModification: fiche.dateModification,
		echanges: fiche.echanges?.map((e) => ({
			...e,
			auteur: displayFirstName(e.createur?.account),
			auteurModif: displayIdentity(e.auteurModification?.account),
		})),
		rappels: fiche.rappels,
		demandes: fiche.demandes,
		sireneUpdate: process.env.SIRENE_UPDATE,
	};
}
