import { FindOptionsRelations } from 'typeorm';

import AppDataSource from '../data-source';
import { Entite } from '../entity/Entite';
import { Local } from '../entity/Local';
import { Proprietaire } from '../entity/Proprietaire';
import { displayIdentity } from '../lib/utils';

type LocalView = Local & {
	proprietaires: (Proprietaire & { entite: Entite & { nbProprietes: number[] } })[];
	nomAuteurModification: string;
};

function toView(local: Local): LocalView {
	const updatedProprietaires = (local.proprietaires ?? []).map((proprietaire) => {
		const nbProprietes = (proprietaire.entite.proprietes ?? []).map((e) => e.id || 0);
		return {
			...proprietaire,
			entite: { ...proprietaire.entite, entiteId: proprietaire.entite.id, nbProprietes },
		};
	});

	const viewLocal = {
		...local,
		proprietaires: updatedProprietaires,
		nomAuteurModification: displayIdentity(local.auteurModification?.account),
	};

	return viewLocal;
}

async function loadView(id: number): Promise<LocalView> {
	const relations: FindOptionsRelations<Local> = {
		auteurModification: { account: true },
		proprietaires: {
			entite: { particulier: true, entreprise: true, fiche: true, proprietes: true },
		},
	};
	const local = await AppDataSource.getRepository(Local).findOneOrFail({
		where: { id },
		relations,
	});

	return toView(local);
}

export { LocalView, loadView, toView };
