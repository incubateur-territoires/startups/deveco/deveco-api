import * as cron from 'node-cron';
import { format, getDay, getWeek, addDays } from 'date-fns';

import rappelService from './service/RappelService';
import statsService from './service/StatsService';
import devEcoService from './service/DevEcoService';
import inseeUpdateService from './service/InseeUpdateService';
import territoireService from './service/TerritoireService';

function triggerOnlyOnSomeDays(date: Date): boolean {
	const dayNumber = getDay(date);
	const weekNumber = getWeek(date, {
		locale: {
			code: 'fr-FR',
		},
		weekStartsOn: 1,
		firstWeekContainsDate: 4,
	});

	// if we're on a fourth week (weeks 4, 8, 12, 16, 20, 24, ..., 48, 52)
	if (weekNumber % 4 === 0) {
		// true on Tuesdays only
		return dayNumber === 2;
	}

	// if we're on a second week (weeks 2, 6, 10, 14, ..., 46, 50)
	if ((weekNumber + 2) % 4 === 0) {
		// true on Mondays only
		return dayNumber === 1;
	}

	return false;
}

export function launchCron() {
	if (process.env.NODE_ENV === 'test') {
		return;
	}

	console.info('[Cron] starting cronjobs...');

	// compute Rappels reminders every day at 22:00 by default
	const rappelsReminderSchedule = process.env.RAPPELS_REMINDER_SCHEDULE
		? `${process.env.RAPPELS_REMINDER_SCHEDULE}`
		: '0 22 * * *';
	cron.schedule(rappelsReminderSchedule, () => {
		console.info('[Cron] Rappels - Start');
		rappelService.sendRemindersForRappels();
		console.info('[Cron] Rappels - Stop');
	});

	// send activity logs to each Territory
	const territoryActivitySchedule = process.env.TERRITORY_ACTIVITY_SCHEDULE
		? `${process.env.TERRITORY_ACTIVITY_SCHEDULE}`
		: '0 5 * * *';

	cron.schedule(territoryActivitySchedule, () => {
		const now = new Date();

		if (!triggerOnlyOnSomeDays(now)) {
			return;
		}

		console.info('[Cron] Territory activity - Start');

		statsService.sendTerritoryActivity();

		console.info('[Cron] Territory activity - Stop');
	});

	// refresh territory stats every day at 4
	const territoryStatsSchedule = process.env.TERRITORY_STATS_SCHEDULE
		? `${process.env.TERRITORY_STATS_SCHEDULE}`
		: '0 4 * * 6';

	cron.schedule(territoryStatsSchedule, async () => {
		console.info('[Cron] Territory stats - Start');

		await territoireService.insertStats();

		console.info('[Cron] Territory stats - Stop');
	});

	// send welcome emails to DevEcos if their Territory has finished importing every 5 minutes
	const territoryDevecoWelcomeEmailSchedule = process.env.TERRITORY_DEVECO_WELCOME_EMAIL_SCHEDULE
		? `${process.env.TERRITORY_DEVECO_WELCOME_EMAIL_SCHEDULE}`
		: '*/5 * * * *';

	cron.schedule(territoryDevecoWelcomeEmailSchedule, async () => {
		console.info('[Cron] Welcome emails - Start');

		await devEcoService.sendWelcomeEmails();

		console.info('[Cron] Welcome emails - Stop');
	});

	// update SIRENE data
	const SIRENE_API_DATE_FORMAT = 'yyyy-MM-dd';

	const today = new Date();

	const sireneDailySchedule = process.env.SIRENE_UPDATE_DAILY_SCHEDULE
		? `${process.env.SIRENE_UPDATE_DAILY_SCHEDULE}`
		: '0 3 * * *';

	cron.schedule(sireneDailySchedule, async () => {
		console.info('[Cron] Sirene update - Daily - Start');

		const start = format(addDays(today, -2), SIRENE_API_DATE_FORMAT);
		const end = format(addDays(today, -1), SIRENE_API_DATE_FORMAT);

		inseeUpdateService.getSireneUpdateForDates({
			start,
			end,
		});

		console.info('[Cron] Sirene update - Daily - Stop');
	});

	const sireneWeeklySchedule = process.env.SIRENE_UPDATE_WEEKLY_SCHEDULE
		? `${process.env.SIRENE_UPDATE_WEEKLY_SCHEDULE}`
		: '0 4 * * 6';

	cron.schedule(sireneWeeklySchedule, async () => {
		console.info('[Cron] Sirene update - Weekly - Start');

		const start = format(addDays(today, -9), SIRENE_API_DATE_FORMAT);
		const end = format(addDays(today, -1), SIRENE_API_DATE_FORMAT);

		inseeUpdateService.getSireneUpdateForDates({
			start,
			end,
		});

		console.info('[Cron] Sirene update - Weekly - Stop');
	});

	const sireneMonthlySchedule = process.env.SIRENE_UPDATE_MONTHLY_SCHEDULE
		? `${process.env.SIRENE_UPDATE_MONTHLY_SCHEDULE}`
		: '0 4 * * 0';

	cron.schedule(sireneMonthlySchedule, async () => {
		console.info('[Cron] Sirene update - Monthly - Start');

		const start = format(addDays(today, -31), SIRENE_API_DATE_FORMAT);
		const end = format(addDays(today, -1), SIRENE_API_DATE_FORMAT);

		inseeUpdateService.getSireneUpdateForDates({
			start,
			end,
		});

		console.info('[Cron] Sirene update - Monthly - Stop');
	});

	const territoryInitDataSchedule = process.env.TERRITORY_INIT_DATA_SCHEDULE
		? `${process.env.TERRITORY_INIT_DATA_SCHEDULE}`
		: '*/1 * * * *';

	cron.schedule(territoryInitDataSchedule, async () => {
		console.info('[Cron] Next Territory import - Start');

		const nextTerritoireId = await territoireService.getNextTerritoireToInit();

		if (nextTerritoireId) {
			console.info(`[Cron] Next Territory import - Found Territory #${nextTerritoireId}`);
			await territoireService.doImportStepsForTerritory(nextTerritoireId);
		}

		console.info('[Cron] Next Territory import - Stop');
	});
}
