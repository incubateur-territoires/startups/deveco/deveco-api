import type { SignOptions } from 'jsonwebtoken';
import jwt from 'jsonwebtoken';

import AppDataSource from '../data-source';
import { Account } from '../entity/Account';
import { Password } from '../entity/Password';
import { Territory } from '../entity/Territory';
import devecoService from '../service/DevEcoService';
import territoireService from '../service/TerritoireService';

export type JwtPayload = {
	id: number;
	email: string;
	accountType: string;
	territoire?: {
		id: Territory['id'];
		name: Territory['name'];
		territoryType: Territory['territoryType'];
	};
	nom?: string;
	prenom?: string;
	hasPassword?: boolean;
	showTutorial?: boolean;
};

export async function createJwt(
	account: Account,
	showTutorial?: boolean
): Promise<
	{
		token: string;
	} & JwtPayload
> {
	const { id, email, accountType, nom, prenom } = account;

	let welcomeEmailSent = false;
	let territoire: Territory | null = null;
	if (accountType === 'deveco') {
		const deveco = await devecoService.getDevEcoByAccountWithTerritory(account);
		territoire = deveco.territory;
		welcomeEmailSent = deveco.welcomeEmailSent;
	}

	const hasPassword =
		null !==
		(await AppDataSource.getRepository(Password).findOne({
			where: { account: { id: account.id } },
		}));

	const signOptions: SignOptions = {
		algorithm: 'HS256',
		expiresIn: '30d',
		subject: '' + id,
	};

	const jwtPayload = {
		id,
		email,
		accountType,
		welcomeEmailSent,
		territoire: territoire
			? {
					id: territoire.id,
					name: territoire.name,
					territoryType: territoire.territoryType,
					importInProgress: territoireService.isImportInProgress(territoire.territoryImportStatus),
			  }
			: undefined,
		nom,
		prenom,
		hasPassword,
		showTutorial,
	};

	const key = process.env.JWT_KEY || '';

	const token = jwt.sign(jwtPayload, key, signOptions);
	return { token, ...jwtPayload };
}

export function verifyJwt(token: unknown) {
	const key = process.env.JWT_KEY || '';

	if (typeof token !== 'string') {
		return null;
	}

	try {
		return jwt.verify(token, key, { complete: false });
	} catch (error) {
		console.info('Error when verifying token', { token, error });
		return null;
	}
}
