import { Account } from '../../entity/Account';
import { EntiteData, Stats } from '../../service/StatsService';
import { displayIdentity, plural } from '../utils';

const devecoContactEmail = process.env.SMTP_FROM;

type UrlLogin = {
	appUrl: string;
	accessKey: string;
	redirectUrl?: string;
};

type UrlPage = {
	appUrl: string;
	page?: string;
};

const createLoginLink = ({ appUrl, accessKey, redirectUrl }: UrlLogin) =>
	`${appUrl}${accessKey ? `/auth/jwt/${accessKey}` : '/connexion'}${
		redirectUrl ? `?url=${redirectUrl}` : ''
	}`;

const createPageLink = ({ appUrl, page }: UrlPage) => `${appUrl}${page ?? '/connexion'}`;

const createAccessButton = (url: UrlLogin) =>
	`
    <p>Pour accéder à Deveco, veuillez cliquer sur le lien ci-dessous&nbsp;:</p>
    <p style="padding-left: 20%">
      <a
      rel="nofollow"
      href="${createLoginLink(url)}"
      style="
          background-color: #000091;
          font-size: 14px;
          font-family: Marianne;
          font-weight: bold;
          text-decoration: none;
          padding: 8px 10px;
          color: #ffffff;
          display: inline-block;
        "
      >
      <span>Accédez à Deveco</span>
      </a>
    </p>
  `;

const createMesActionsButton = (url: UrlPage) =>
	`
    <p>Pour retrouver toutes vos actions sur Deveco, veuillez cliquer sur le lien ci-dessous&nbsp;:</p>
    <p style="padding-left: 20%">
      <a
      rel="nofollow"
      href="${createPageLink(url)}"
      style="
          background-color: #000091;
          font-size: 14px;
          font-family: Marianne;
          font-weight: bold;
          text-decoration: none;
          padding: 8px 10px;
          color: #ffffff;
          display: inline-block;
        "
      >
      <span>Mes rappels</span>
      </a>
    </p>
  `;

const formatEntiteData =
	({ appUrl }: UrlPage) =>
	({ nom, contenu, date, siret, ficheId }: EntiteData): string => {
		let path;

		if (siret) {
			path = `etablissements/${siret}`;
		} else if (ficheId) {
			path = `createurs/${ficheId}`;
		}

		let link = `${nom}`;
		if (path) {
			const url = `${appUrl}/${path}`;
			link = `<a href="${url}">${nom}</a>`;
		}

		date = date ? `${date}&nbsp;: ` : '';

		contenu = contenu ? ` - ${contenu}` : '';

		return `${date}${link}${contenu}`;
	};

const showRappels = (url: UrlPage) => {
	const formatter = formatEntiteData(url);

	return (rappelsData: EntiteData[]): string =>
		`<p>
            Voici les actions Deveco à réaliser dans deux jours&nbsp;:<br>
            <ul>
                    ${rappelsData
											.map((r) => formatter(r))
											.map((formatted) => `<li>${formatted}</li>`)
											.join('')}
            </ul>
    </p>`;
};

const documentationUrl = '/documentation/';
const createHelpLink = ({ appUrl }: UrlPage): string =>
	`<p>
L'équipe Deveco met à votre disposition <a rel="nofollow" href="${appUrl}${documentationUrl}"><span>un guide d'utilisation</span></a>.
	</p>`;

const greeting = (account: Account): string =>
	`<p>
		Bonjour ${displayIdentity(account)},
	</p>`;

const greetingAnonyme = (): string =>
	`<p>
		Bonjour,
	</p>`;

const felicitations = () =>
	`
	<p>
		Félicitations, vous avez désormais un accès gratuit et illimité à Deveco
	</p>
	<br>
	­<p>
		Deveco est un outil numérique dédié aux chargés du développement économique des collectivités. Il est porté par l’Agence Nationale de la Cohésion des Territoires (ANCT).
	</p>
	<p>
		Suite à votre demande, votre accès sécurisé est ouvert.
	</p>
	`;

const remerciements = () =>
	`<p>
		Merci d’utiliser Deveco, la solution numérique pour les développeurs économiques.
	</p>`;

const contact = (url: string | undefined) =>
	`<p>
		Et bien sûr, si l'information recherchée n'apparaît pas,
		n'hésitez pas à nous écrire&nbsp;: <a href="mailto:${url}">${url}</a>.
	</p>`;

const footer = () =>
	`<p>
		L'équipe Deveco.
	</p>`;

export const welcome = ({ url, account }: { url: UrlLogin; account: Account }): string =>
	`
    ${greeting(account)}
    ${felicitations()}
    ${createAccessButton(url)}
    <br>
    <p>
      Pas besoin de mot de passe, votre adresse mail suffit.
    </p>
    ${createHelpLink(url)}
    ${contact(devecoContactEmail)}
    ${footer()}
	`;

export const loginRequest = ({ url, account }: { url: UrlLogin; account: Account }): string =>
	`
    ${greeting(account)}
    ${createAccessButton(url)}
    ${remerciements()}
    ${createHelpLink(url)}
    ${contact(devecoContactEmail)}
    ${footer()}
	`;

export const exportWeeklyActivity = (): string =>
	`
	${greetingAnonyme()}
	<p>
		Vous trouvez dans le PJ de ce mail un compte-rendu sur l'activité hebdomadaire des beta-testeurs deveco.
	</p>
	${contact(devecoContactEmail)}
	${footer()}
	`;

export const rappelsReminder = ({
	url,
	rappelsData,
}: {
	url: UrlPage;
	rappelsData: EntiteData[];
}): string =>
	`
	${greetingAnonyme()}
	${showRappels(url)(rappelsData)}
	${createMesActionsButton(url)}
	${remerciements()}
	${createHelpLink(url)}
	${contact(devecoContactEmail)}
	${footer()}
	`;

const showStats = (url: UrlPage) => {
	const formatter = formatEntiteData(url);

	return (stats: Stats): string =>
		`
			<p>Nous sommes heureux de vous partager quelque éléments de votre tableau de bord.</p>

			<p>Voici les éléments clés des deux dernières semaines dans le Deveco de votre collectivité (${
				stats.territoire
			}).</p>

			<h2>Établissements</h2>
			<p><strong>${stats.etablissements.period ? `+${stats.etablissements.period}` : 'Aucun'} ${plural(
			stats.etablissements.period,
			'établissement'
		)}</strong> dans le portefeuille sur les deux dernières semaines.
			<br>
			${stats.etablissements.total} depuis le début.
			</p>

			<h2>Créateurs d'entreprise</h2>
			<p><strong>${stats.createurs.period ? `+${stats.createurs.period}` : 'Aucun'} ${plural(
			stats.createurs.period,
			'créateur'
		)}</strong> dans le portefeuille sur les deux dernières semaines.
			<br>
			${stats.createurs.total} depuis le début.
			</p>

			<h2>Consultations</h2>
			<p><strong>${stats.consultations.period ? `+${stats.consultations.period}` : 'Aucun'} ${plural(
			stats.consultations.period,
			'action'
		)} de consultation</strong> sur les deux dernières semaines.
			<br>
			${stats.consultations.total} depuis le début.
			</p>

			<h2>Échanges</h2>
			<p><strong>${stats.echanges.period ? `+${stats.echanges.period}` : 'Aucun'} ${plural(
			stats.echanges.period,
			'échange'
		)}</strong> sur les deux dernières semaines.</p>
			${
				stats.echanges.period
					? `<p>Établissements et créateurs concernés&nbsp;:<p>
			<ul>
				${stats.echanges.periodList
					.map((r) => formatter(r))
					.map((formatted) => `<li>${formatted}</li>`)
					.join('')}
			</ul>`
					: ''
			}

			<h2>Qualifications</h2>
			<p><strong>${stats.qualifications.period ? `+${stats.qualifications.period}` : 'Aucune'} ${plural(
			stats.qualifications.period,
			'action'
		)} de qualification</strong> sur les deux dernières semaines.
			<br>
			${stats.qualifications.total} depuis le début.
			</p>
			${
				stats.qualifications.period
					? `<p>Établissements et créateurs concernés&nbsp;:<p>
			<ul>
				${stats.qualifications.periodList
					.map((r) => formatter(r))
					.map((formatted) => `<li>${formatted}</li>`)
					.join('')}
			</ul>`
					: ''
			}

			<h2>Contacts</h2>
			<p><strong>${stats.contacts.period ? `+${stats.contacts.period}` : 'Aucun'} ${plural(
			stats.contacts.period,
			'contact'
		)} ${plural(stats.contacts.period, 'créé')}</strong> sur les deux dernières semaines.
			<br>
			${stats.contacts.total} depuis le début.
			</p>

			<h2>Rappels</h2>
			<p><strong>${stats.rappels.period ? `+${stats.rappels.period}` : 'Aucun'} ${plural(
			stats.rappels.period,
			'rappel'
		)} ${plural(stats.rappels.period, 'créé')}</strong> sur les deux dernières semaines.
			<br>
			${stats.rappels.total} depuis le début.
			</p>
	`;
};

export const statsRecap = ({
	url,
	account,
	stats,
}: {
	url: UrlPage;
	account: Account;
	stats: Stats;
}) =>
	`
	${greeting(account)}
	${showStats(url)(stats)}
	${remerciements()}
	${createHelpLink(url)}
	${contact(devecoContactEmail)}
	${footer()}
	`;

export const templates = {
	welcome,
	loginRequest,
	exportWeeklyActivity,
	rappelsReminder,
	statsRecap,
};

export default templates;
