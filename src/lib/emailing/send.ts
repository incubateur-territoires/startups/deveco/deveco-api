import nodemailer from 'nodemailer';
import type Mail from 'nodemailer/lib/mailer';
import type SMTPTransport from 'nodemailer/lib/smtp-transport';

const smtpConfig: SMTPTransport.Options = {
	host: process.env.SMTP_HOST,
	ignoreTLS: false,
	port: Number(process.env.SMTP_PORT),
	requireTLS: true,
	secure: false,
	auth: {
		pass: process.env.SMTP_PASS,
		user: process.env.SMTP_USER,
	},
};

console.info('Using SMTP config: ', { smtpConfig });

export function send({
	to,
	subject,
	text,
	html,
	bcc,
	attachments = [],
}: Mail.Options): Promise<SMTPTransport.SentMessageInfo> {
	const transporter = nodemailer.createTransport(smtpConfig);
	const mailOptions: Mail.Options = {
		bcc,
		from: process.env.SMTP_FROM,
		replyTo: process.env.SMTP_REPLYTO,
		html,
		subject,
		text,
		to,
		attachments,
	};
	return transporter.sendMail(mailOptions);
}
