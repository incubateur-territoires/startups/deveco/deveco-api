import { Branded } from './utils';

export type Siret = Branded<string, 'siret'>;
export type Siren = Branded<string, 'siren'>;

const regExpSiren = '^[0-9]{9}$';
const regExpSiret = '^[0-9]{14}$';

export function isSiret(fuzzy: string): fuzzy is Siret {
	const search = fuzzy.replaceAll(/\s/g, '');
	if (search.match(regExpSiret)) {
		return true;
	}
	return false;
}

export function isSiren(fuzzy: string): fuzzy is Siren {
	const search = fuzzy.replaceAll(/\s/g, '');
	if (search.match(regExpSiren)) {
		return true;
	}
	return false;
}
