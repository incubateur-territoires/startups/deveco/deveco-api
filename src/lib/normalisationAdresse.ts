import { keysOf } from './utils';

export const banVoieToInseeVoie = {
	Avenue: 'AV',
	Boulevard: 'BD',
	Carrefour: 'CAR',
	Chemin: 'CHE',
	Chaussée: 'CHS',
	Cité: 'CITE',
	Corniche: 'COR',
	Cours: 'CRS',
	Domaine: 'DOM',
	Descente: 'DSC',
	Ecart: 'ECA',
	Esplanade: 'ESP',
	Faubourg: 'FG',
	'Grande Rue': 'GR',
	Hameau: 'HAM',
	Halle: 'HLE',
	Impasse: 'IMP',
	'Lieu-dit': 'LD',
	Lotissement: 'LOT',
	Marché: 'MAR',
	Montée: 'MTE',
	Passage: 'PAS',
	Place: 'PL',
	Plaine: 'PLN',
	Plateau: 'PLT',
	Promenade: 'PRO',
	Parvis: 'PRV',
	Quartier: 'QUA',
	Quai: 'QUAI',
	Résidence: 'RES',
	Ruelle: 'RLE',
	Rocade: 'ROC',
	'Rond-point': 'RPT',
	Route: 'RTE',
	Rue: 'RUE',
	Sente: 'SEN',
	Sentier: 'SEN',
	Square: 'SQ',
	'Terre-plein': 'TPL',
	Traverse: 'TRA',
	Villa: 'VLA',
	Village: 'VLGE',
};

const banToInseeKeys = keysOf(banVoieToInseeVoie);

export const replaceBanVoieToInseeVoie = (rue: string) => {
	for (const banVoie of banToInseeKeys) {
		if (rue.match(banVoie)) {
			return rue.replace(banVoie, banVoieToInseeVoie[banVoie]);
		}
	}

	return rue;
};
