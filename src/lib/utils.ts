import { ObjectLiteral, SelectQueryBuilder } from 'typeorm';

import { Account } from '../entity/Account';
import { Contact } from '../entity/Contact';
import { Entite } from '../entity/Entite';
import { Entreprise } from '../entity/Entreprise';
import { EventLogEntityType } from '../entity/EventLog';
import { Particulier } from '../entity/Particulier';

const monthNames = [
	'Janvier',
	'Février',
	'Mars',
	'Avril',
	'May',
	'Juin',
	'Juillet',
	'Aout',
	'Septembre',
	'Octobre',
	'Novembre',
	'Décembre',
];

export function displayIdentity(account: Account | undefined): string {
	if (!account) {
		return '';
	}
	const { email, prenom, nom } = account;
	if (prenom && nom) {
		return `${prenom} ${nom}`;
	}

	if (prenom) {
		return prenom;
	}

	if (nom) {
		return nom;
	}

	return email;
}

export const abbreviateName = (nom: string): string => `${nom.slice(0, 1)}.`;

// for now, ignore space-separated names, since it does not handle particles well (Kelvin De La Soul, Olivia van der Wilde, etc.)
export const abbreviateCompoundNames = (nom: string): string =>
	nom.includes('-') ? nom.split('-').map(abbreviateName).join('-') : abbreviateName(nom);

export function displayFirstName(account: Account | undefined): string {
	if (!account) {
		return '';
	}

	const { prenom, nom } = account;
	if (prenom && nom) {
		const noms = abbreviateCompoundNames(nom);
		return `${prenom} ${noms}`;
	}

	if (prenom) {
		return prenom;
	}

	return '-';
}

interface EntiteLike {
	entreprise?: Entreprise;
	particulier?: Particulier;
}

export function displayEntrepriseName(entreprise: Entreprise) {
	const enseigne = entreprise.enseigne ?? '';
	const nom = entreprise.nom ?? '';
	const nomEntreprise = enseigne ? (enseigne === nom ? enseigne : `${enseigne} (${nom})`) : nom;

	return nomEntreprise;
}

export function displayParticulierName(particulier: Particulier) {
	const nomParticulier = `${particulier.nom} ${particulier.prenom}`;

	return nomParticulier;
}

export function displayContact(contact: Contact) {
	return [
		contact.particulier.prenom,
		contact.particulier.nom,
		contact.fonction ? `(${contact.fonction})` : null,
	]
		.filter(Boolean)
		.join(' ');
}

export function displayFicheName(entite: EntiteLike): string {
	if (entite.entreprise) {
		return displayEntrepriseName(entite.entreprise);
	}

	if (entite.particulier) {
		return displayParticulierName(entite.particulier);
	}

	return '-';
}

export function getLogEntityType(entite: Entite): EventLogEntityType {
	return entite.particulier?.id
		? EventLogEntityType.PERSONNE_PHYSIQUE
		: EventLogEntityType.PERSONNE_MORALE;
}

export function getNWeeksAgo(today: Date, weeks: number) {
	const newDate = new Date(today.getTime() - 7 * 24 * 60 * 60 * 1000 * weeks);
	newDate.setHours(0, 0, 0, 0);
	return newDate;
}

export function getMonthAgo(today: Date) {
	const newDate = new Date(today.getFullYear(), today.getMonth() - 1, today.getDate());
	newDate.setHours(0, 0, 0, 0);
	return newDate;
}

const days = (amount: number) => 1000 * 60 * 60 * 24 * amount;

export function getPeriodStart(periode: string | undefined): Date {
	const now = new Date();

	switch (periode) {
		case 'mois':
			return new Date(now.getTime() - days(30));
		case 'semaine':
			return new Date(now.getTime() - days(7));
		case 'tout':
		default:
			return new Date(0);
	}
}

export function formatJSDateToPostgresDate(d: Date) {
	const year = d.getFullYear();
	const month = d.getMonth() + 1 < 10 ? '0' + (d.getMonth() + 1) : d.getMonth() + 1;
	const day = d.getDate() < 10 ? '0' + d.getDate() : d.getDate();
	const hours = d.getHours() < 10 ? '0' + d.getHours() : d.getHours();
	const minutes = d.getMinutes() < 10 ? '0' + d.getMinutes() : d.getMinutes();
	const seconds = d.getSeconds() < 10 ? '0' + d.getSeconds() : d.getSeconds();
	const milliseconds = d.getMilliseconds() < 10 ? '0' + d.getMilliseconds() : d.getMilliseconds();
	return `${year}-${month}-${day} ${hours}:${minutes}:${seconds}.${milliseconds}`;
}

export function formatJSDateToHumainFriendly(d: Date) {
	const year = d.getFullYear();
	const month = monthNames[d.getMonth()];
	const day = d.getDate();
	return `${day} ${month} ${year}`;
}

export function wait(ms: number): Promise<void> {
	return new Promise((resolve) => setTimeout(resolve, ms));
}

export function dedupe<T extends string | number>(list: T[]): T[] {
	return list.filter((v, i) => list.indexOf(v) === i);
}

export type Branded<K, T> = K & { __brand: T };

// usage:
// `plural(1, 'thing') => 'thing'`
// `plural(4, 'thing') => 'things'`
// `plural(4, 'thief', 'thieves') => 'thieves'`
export const plural = (num: number, singular: string, plur?: string) =>
	`${num > 1 ? plur ?? singular + 's' : singular}`;

export const toPercent = (num: number, total?: number) =>
	Number(num / (total ?? 100)).toLocaleString('fr-FR', {
		style: 'percent',
		minimumFractionDigits: 0,
		maximumFractionDigits: 0,
	});

export const keysOf = <T extends Record<string, unknown>>(arg: T) =>
	Object.keys(arg) as (keyof T)[];

export const getSqlAndApply = <T extends ObjectLiteral>(
	query: SelectQueryBuilder<T>,
	label?: string
): Promise<any[]> => {
	if (label) {
		console.time(label);
	}
	const sql = query.getQueryAndParameters();
	console.log(sql);

	if (label) {
		return query.getRawMany().then((result) => {
			console.timeEnd(label);
			return result;
		});
	}

	return query.getRawMany();
};
