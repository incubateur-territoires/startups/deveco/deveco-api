import { healthRoutePrefix } from '../controller/HealthController';
import { DevecoContext } from '../middleware/AuthMiddleware';

const getTimestamp = (date: Date) => `${date.toISOString().replace('Z', '')}`;

export const getMemUsed = () => {
	const mem = process.memoryUsage().heapUsed / 1024 / 1024;
	const memPrint = Math.round(mem).toString().padStart(3, ' ');

	return `Mem(${memPrint}MB)`;
};

const logger =
	(dir: string) =>
	(ctx: DevecoContext, ...info: any[]) => {
		if (ctx.req.url === healthRoutePrefix) {
			return;
		}

		const timestamp = getTimestamp(new Date());

		const reqId = ctx.state.reqId;

		const memUsed = getMemUsed();

		const deveco = ctx.state?.deveco;

		const devecoLog = deveco ? `[T(${deveco.territory?.id}) D(${deveco.id})]` : '';

		const status = dir === '=>' ? ctx.res.statusCode : '...';

		const log = `${ctx.req.method} ${status} ${ctx.req.url}`;

		console.info(`${memUsed} ${timestamp} ${reqId} ${dir} ${log} ${info.join(' ')} ${devecoLog}`);
	};

const loggerIn = logger('<=');
const loggerOut = logger('=>');
const loggerMid = logger('==');
export { loggerMid as logger };

let reqCount = 0;
const MAX_COUNT = 10000;
const ZERO_PAD = MAX_COUNT.toString().length - 1;

const timestampLogger = async (ctx: DevecoContext, next: () => Promise<unknown>) => {
	const start = new Date();

	const reqId = (reqCount++ % MAX_COUNT).toString().padStart(ZERO_PAD, '0');

	ctx.state.reqId = reqId;

	const done = ((ctx: DevecoContext) => {
		ctx.res.removeListener('finish', done);
		ctx.res.removeListener('close', done);

		const time = Date.now() - start.getTime();

		loggerOut(ctx, `${time}ms`);
	}).bind(this, ctx);

	ctx.res.once('finish', done);
	ctx.res.once('close', done);

	loggerIn(ctx);

	await next();
};

export default timestampLogger;
