// type Code =
//   | 'NN'
//   | '00'
//   | '01'
//   | '02'
//   | '03'
//   | '11'
//   | '12'
//   | '21'
//   | '22'
//   | '31'
//   | '32'
//   | '41'
//   | '42'
//   | '51'
//   | '52'
//   | '53';
//
type Label =
	| 'Unité non employeuse'
	| 'Pas de salarié(e)'
	| '1 ou 2 salarié(e)s'
	| '3 à 5 salarié(e)s'
	| '6 à 9 salarié(e)s'
	| '10 à 19 salarié(e)s'
	| '20 à 49 salarié(e)s'
	| '50 à 99 salarié(e)s'
	| '100 à 199 salarié(e)s'
	| '200 à 249 salarié(e)s'
	| '250 à 499 salarié(e)s'
	| '500 à 999 salarié(e)s'
	| '1 000 à 1 999 salarié(e)s'
	| '2 000 à 4 999 salarié(e)s'
	| '5 000 à 9 999 salarié(e)s'
	| '10 000 salarié(e)s et plus';

const map: Record<string, Label> = {
	NN: 'Unité non employeuse',
	'00': 'Pas de salarié(e)',
	'01': '1 ou 2 salarié(e)s',
	'02': '3 à 5 salarié(e)s',
	'03': '6 à 9 salarié(e)s',
	'11': '10 à 19 salarié(e)s',
	'12': '20 à 49 salarié(e)s',
	'21': '50 à 99 salarié(e)s',
	'22': '100 à 199 salarié(e)s',
	'31': '200 à 249 salarié(e)s',
	'32': '250 à 499 salarié(e)s',
	'41': '500 à 999 salarié(e)s',
	'42': '1 000 à 1 999 salarié(e)s',
	'51': '2 000 à 4 999 salarié(e)s',
	'52': '5 000 à 9 999 salarié(e)s',
	'53': '10 000 salarié(e)s et plus',
};

export const codeToTrancheEffectifs = (code: string) => map[code] ?? undefined;

export function trancheToEffectifs(tranche_effectifs: string): string {
	/*
	 * NN	Unités non employeuses (pas de salarié au cours de l'année de référence et pas d'effectif au 31/12). Cette tranche peut contenir quelques effectifs inconnus
	 * 00	0 salarié
	 * 01	1 ou 2 salariés
	 * 02	3 à 5 salariés
	 * 03	6 à 9 salariés
	 * 11	10 à 19 salariés
	 * 12	20 à 49 salariés
	 * 21	50 à 99 salariés
	 * 22	100 à 199 salariés
	 * 31	200 à 249 salariés
	 * 32	250 à 499 salariés
	 * 41	500 à 999 salariés
	 * 42	1 000 à 1 999 salariés
	 * 51	2 000 à 4 999 salariés
	 * 52	5 000 à 9 999 salariés
	 * 53	10 000 salariés et plus
	 */
	switch (tranche_effectifs) {
		case '00':
			return '0';
		case '01':
			return '1 ou 2';
		case '02':
			return '3 à 5';
		case '03':
			return '6 à 9';
		case '11':
			return '10 à 19';
		case '12':
			return '20 à 49';
		case '21':
			return '50 à 99';
		case '22':
			return '100 à 199';
		case '31':
			return '200 à 249';
		case '32':
			return '250 à 499';
		case '41':
			return '500 à 999';
		case '42':
			return '1 000 à 1 999';
		case '51':
			return '2 000 à 4 999';
		case '52':
			return '5 000 à 9 999';
		case '53':
			return '10 000 et plus';
		case 'NN':
		default:
			return '?';
	}
}

export function anneeToDateEffectifs(annee_effectifs: string): Date | undefined {
	const annee = parseInt(annee_effectifs);
	if (annee > 2000 && annee < 2050) {
		return new Date(annee, 11, 25, 12, 0, 0, 0); // 25 so that it is always a day in the current month regardless of the timezone. Hackish, yeah.
	}
	return;
}
