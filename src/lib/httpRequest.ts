import fetch from 'cross-fetch';

type ErrorResponse = { __success: false; error: 'KO' };
type DataResponse<Data> = { __success: true; data: Data };

export async function httpGet<Data>(
	url: string,
	headers?: Record<string, string>
): Promise<DataResponse<Data> | ErrorResponse> {
	if (!headers) {
		headers = {
			Accept: 'application/json',
		};
	}

	const response = await fetch(url, {
		method: 'GET',
		headers,
	});

	if (!response.ok) {
		console.info({ response });
		return {
			__success: false,
			error: 'KO',
		};
	}

	const res = await response.json();

	return {
		__success: true,
		data: res,
	};
}
