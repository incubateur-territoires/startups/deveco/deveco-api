import { StatusCodes } from 'http-status-codes';

import { DevecoContext } from '../middleware/AuthMiddleware';
import { createJwt } from '../lib/getJwt';
import { Account } from '../entity/Account';

export const cookieName = 'jwt';

export const respondWithAccountCookie = async (
	{
		account,
		ctx,
		redirectUrl,
	}: {
		account: Account;
		ctx: DevecoContext;
		redirectUrl?: string;
	},
	showTutorial = false
): Promise<void> => {
	const jwtPayload = await createJwt(account, showTutorial);

	ctx.status = StatusCodes.OK;
	ctx.body = { ...jwtPayload, redirectUrl };
	ctx.cookies.set(cookieName, jwtPayload.token, {
		path: '/',
		httpOnly: true,
		secure: process.env.NODE_ENV === 'production',
		sameSite: 'strict',
	});
	ctx.headers['cache-control'] = 'private';
};

export const respondWithDeletedCookie = async ({ ctx }: { ctx: DevecoContext }): Promise<void> => {
	ctx.status = StatusCodes.OK;
	ctx.body = {};
	ctx.cookies.set(cookieName, 'deleted', {
		path: '/',
		httpOnly: true,
		secure: process.env.NODE_ENV === 'production',
		sameSite: 'strict',
		expires: new Date(0),
	});
	ctx.headers['cache-control'] = 'private';
};
