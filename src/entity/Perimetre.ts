import { Column, Entity, ManyToMany, JoinTable, PrimaryGeneratedColumn } from 'typeorm';

import { Commune } from './Commune';
import { Departement } from './Departement';
import { Epci } from './Epci';

import { GeographyTypeForPerimetre } from '../service/TerritoireService';

@Entity()
export class Perimetre {
	@PrimaryGeneratedColumn()
	id: number;

	@Column({ type: 'varchar', nullable: false })
	level: GeographyTypeForPerimetre;

	@ManyToMany(() => Commune)
	@JoinTable()
	communes: Commune[];

	@ManyToMany(() => Epci)
	@JoinTable()
	epcis: Epci[];

	@ManyToMany(() => Departement)
	@JoinTable()
	departements: Departement[];
}
