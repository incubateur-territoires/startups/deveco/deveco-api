import {
	Entity,
	PrimaryGeneratedColumn,
	Column,
	JoinColumn,
	ManyToOne,
	CreateDateColumn,
	UpdateDateColumn,
	Index,
} from 'typeorm';

import { Fiche } from './Fiche';

@Entity({ name: 'demande' })
export class Demande {
	@PrimaryGeneratedColumn()
	id: number;

	@ManyToOne(() => Fiche, (fiche) => fiche.demandes, { onDelete: 'CASCADE' })
	@JoinColumn({ name: 'fiche_id' })
	@Index()
	fiche: Fiche;

	// foncier, immobilier, economique, fiscalite, accompagnement, rh, autre
	@Column({ name: 'type_demande' })
	typeDemande: string;

	@Column({ name: 'cloture', nullable: true })
	cloture: boolean;

	@Column({ name: 'motif', nullable: true })
	motif: string;

	@Column({ name: 'date_cloture', type: 'date', nullable: true })
	dateCloture: Date;

	@CreateDateColumn({ name: 'created_at' })
	createdAt: Date;

	@UpdateDateColumn({ name: 'updated_at' })
	updatedAt: Date;
}
