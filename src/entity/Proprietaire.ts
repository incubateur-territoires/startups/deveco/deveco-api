import {
	Entity,
	PrimaryGeneratedColumn,
	JoinColumn,
	ManyToOne,
	CreateDateColumn,
	UpdateDateColumn,
	Unique,
} from 'typeorm';

import { DevEco } from './DevEco';
import { Entite } from './Entite';
import { Local } from './Local';

@Entity({ name: 'proprietaire' })
@Unique('UNIQ_proprietaire_local_entite', ['local', 'entite'])
export class Proprietaire {
	@PrimaryGeneratedColumn()
	id?: number;

	@ManyToOne(() => Local)
	@JoinColumn({ name: 'local_id' })
	local: Local;

	@ManyToOne(() => Entite)
	@JoinColumn({ name: 'entite_id' })
	entite: Entite;

	@ManyToOne(() => DevEco)
	@JoinColumn({ name: 'auteur_modification_id' })
	auteurModification: DevEco;

	@ManyToOne(() => DevEco)
	@JoinColumn({ name: 'deveco_id' })
	createur: DevEco;

	@CreateDateColumn({ name: 'created_at' })
	createdAt: Date;

	@UpdateDateColumn({ name: 'updated_at' })
	updatedAt: Date;
}
