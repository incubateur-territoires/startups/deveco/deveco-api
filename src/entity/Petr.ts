import { Entity, Column, PrimaryColumn, OneToMany } from 'typeorm';

import { Epci } from './Epci';

@Entity({ name: 'petr' })
export class Petr {
	@PrimaryColumn({ name: 'code_petr' })
	id: string;

	@Column({ name: 'lib_petr' })
	libPetr: string;

	@OneToMany(() => Epci, (epci) => epci.petr)
	epcis: Epci[];
}
