import { Entity, PrimaryGeneratedColumn, Column, OneToOne, Index } from 'typeorm';

import { Etablissement } from './Etablissement';

// Payload API Insee liensSuccession
// {
// 	"siretEtablissementPredecesseur": "80529641500013",
// 	"siretEtablissementSuccesseur": "80529641500021",
// 	"dateLienSuccession": "2015-07-08",
// 	"transfertSiege": false,
// 	"continuiteEconomique": true,
// 	"dateDernierTraitementLienSuccession": "2015-10-14T15:17:05"
//   }

@Entity({ name: 'lien_succession_temp' })
export class LienSuccessionTemp {
	@PrimaryGeneratedColumn()
	id: number;

	@Column({ name: 'siret_predecesseur', type: 'varchar', length: 14, nullable: true })
	@OneToOne(() => Etablissement)
	@Index()
	siretEtablissementPredecesseur: Etablissement['siret'];

	@Column({ name: 'siret_successeur', type: 'varchar', length: 14, nullable: true })
	@OneToOne(() => Etablissement)
	@Index()
	siretEtablissementSuccesseur: Etablissement['siret'];

	@Column({ name: 'date_succession', type: 'timestamptz' })
	dateLienSuccession: Date;

	@Column({ name: 'date_traitement', type: 'timestamptz' })
	dateDernierTraitementLienSuccession: Date;

	@Column({ name: 'transfert_siege' })
	transfertSiege: boolean;

	@Column({ name: 'continuite_economique' })
	continuiteEconomique: boolean;
}
