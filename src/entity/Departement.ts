import { Entity, Column, PrimaryColumn, OneToMany } from 'typeorm';

import { Commune } from './Commune';

@Entity({ name: 'departement' })
export class Departement {
	@PrimaryColumn({ name: 'ctcd' })
	id: string;

	@Column({ name: 'nom' })
	nom: string;

	@OneToMany(() => Commune, (commune) => commune.departement)
	communes: Commune[];
}
