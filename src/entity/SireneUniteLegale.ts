import { Entity, Column, PrimaryColumn } from 'typeorm';

import {
	CategorieEntreprise,
	DiffusionStatut,
	ecoSocSol,
	EtatAdministratif,
	Sexe,
	societeMissionUniteLegale,
} from './enums';

@Entity({ name: 'sirene_unite_legale' })
export class SireneUniteLegale {
	@PrimaryColumn({ length: 9 })
	siren: string;

	/*
	 * statutDiffusionUniteLegale: Statut de diffusion de l’unité légale.
	 * Seules les unités légales diffusibles sont accessibles à tout public (statutDiffusionUniteLegale=O)
	 */
	@Column({
		type: 'enum',
		enum: DiffusionStatut,
		default: DiffusionStatut.N,
		name: 'statut_diffusion_unite_legale',
	})
	statutDiffusionUniteLegale: DiffusionStatut;

	/* unitePurgeeUniteLegale: Cette variable indique si l'unité légale a été purgée */
	@Column({ nullable: true, name: 'unite_purgee_unite_legale' })
	unitePurgeeUniteLegale: boolean;

	@Column({ type: 'date', nullable: true, name: 'date_creation_unite_legale' })
	dateCreationUniteLegale: Date | null;

	/*
  sigleUniteLegale : Un sigle est une forme réduite de la raison sociale ou de la dénomination d'une personne morale ou d'un organisme public.

  Il est habituellement constitué des initiales de certains des mots de la dénomination. Afin d'en faciliter la prononciation,
  il arrive qu'on retienne les deux ou trois premières lettres de certains mots : il s'agit alors, au sens strict, d'un acronyme;
  mais l'usage a étendu à ce cas l'utilisation du terme sigle.
  Cette variable est à null pour les personnes physiques.
  Elle peut être non renseignée pour les personnes morales.
  */
	@Column({ type: 'varchar', nullable: true, name: 'sigle_unite_legale' })
	sigleUniteLegale: string | null;

	/*
  sexeUniteLegale: Caractère féminin ou masculin de la personne physique.
  Cette variable est à null pour les personnes morales.
  La variable sexeUniteLegale est également non renseignée pour quelques personnes physiques.
  */
	@Column({ type: 'enum', enum: Sexe, nullable: true, name: 'sexe_unite_legale' })
	sexeUniteLegale: Sexe | null;

	/*
  prenom1UniteLegale:  Premier prénom déclaré pour une personne physique.
  Les variables prenom1UniteLegale à prenom4UniteLegale sont les prénoms déclarés pour une personne physique.
  Ces variables sont à null pour les personnes morales.
  Toute personne physique sera identifiée au minimum par son nom de naissance et son premier prénom.
  La variable prenom1UniteLegale peut être à null pour des personnes physiques (unités purgées).
  Les prenom1UniteLegale à prenom4UniteLegale peuvent contenir des *, qui ne sont pas significatifs.
  */

	@Column({ type: 'varchar', length: 20, nullable: true, name: 'prenom1_unite_legale' })
	prenom1UniteLegale: string | null;

	@Column({ type: 'varchar', length: 20, nullable: true, name: 'prenom2_unite_legale' })
	prenom2UniteLegale: string | null;

	@Column({ type: 'varchar', length: 20, nullable: true, name: 'prenom3_unite_legale' })
	prenom3UniteLegale: string | null;

	@Column({ type: 'varchar', length: 20, nullable: true, name: 'prenom4_unite_legale' })
	prenom4UniteLegale: string | null;

	/*
  prenomUsuelUniteLegale: Prénom usuel de la personne physique.
  Le prénom usuel est le prénom par lequel une personne choisit de se faire appeler dans la vie courante, parmi l'ensemble de ceux
  qui lui ont été donnés à sa naissance et qui sont inscrits à l'état civil.
  Il correspond généralement au prenom1UniteLegale.
  Cette variable est à null pour les personnes morales.
  */
	@Column({ type: 'varchar', length: 20, nullable: true, name: 'prenom_usuel_unite_legale' })
	prenomUsuelUniteLegale: string | null;

	/*
  pseudonymeUniteLegale: Pseudonyme de la personne physique.
  Cette variable correspond au nom qu’une personne utilise pour se désigner dans l’exercice de son activité,
  généralement littéraire ou artistique.
  Le pseudonyme est protégé, comme le nom de famille, contre l’usurpation venant d’un tiers.
  */
	@Column({ type: 'varchar', length: 100, nullable: true, name: 'pseudonyme_unite_legale' })
	pseudonymeUniteLegale: string | null;

	/*
  identifiantAssociationUniteLegale:  Numéro au Répertoire National des Associations.
  Lors de sa déclaration en préfecture, l’association reçoit automatiquement un numéro d’inscription au RNA.
  Elle doit en outre demander son immatriculation au répertoire Sirene lorsqu'elle souhaite demander des
  subventions auprès de l'État ou des collectivités territoriales, lorsqu'elle emploie des salariés ou lorsqu'elle
  exerce des activités qui conduisent au paiement de la TVA ou de l'impôt sur les sociétés.
  Le RNA est le fichier national, géré par le ministère de l'Intérieur, qui recense l'ensemble des informations
  sur les associations.
  */
	@Column({
		type: 'varchar',
		length: 10,
		nullable: true,
		name: 'identifiant_association_unite_legale',
	})
	identifiantAssociationUniteLegale: string | null;

	/*
  trancheEffectifsUniteLegale: Tranche d’effectif salarié de l’unité légale.
  Il s’agit d’une variable statistique, millésimée au 31/12 d’une année donnée (voir variable
  anneeEffectifsUniteLegale).
  Liste de codes
  NN : Unité non employeuse (pas de salarié au cours de l'année de référence et pas d'effectif au 31/12)
  00 : 0 salarié (n'ayant pas d'effectif au 31/12 mais ayant employé des salariés au cours de l'année de
  référence)
  01 : 1 ou 2 salariés
  02 : 3 à 5 salariés
  03 : 6 à 9 salariés
  11 : 10 à 19 salariés
  12 : 20 à 49 salariés
  21 : 50 à 99 salariés
  22 : 100 à 199 salariés
  31 : 200 à 249 salariés
  32 : 250 à 499 salariés
  41 : 500 à 999 salariés
  42 : 1 000 à 1 999 salariés
  51 : 2 000 à 4 999 salariés
  52 : 5 000 à 9 999 salariés
  53 : 10 000 salariés et plus
  */
	@Column({ type: 'varchar', length: 2, nullable: true, name: 'tranche_effectifs_unite_legale' })
	trancheEffectifsUniteLegale: string | null;

	/*
  anneeEffectifsUniteLegale: Année de validité de la tranche d’effectif salarié de l’unité légale
  Seule la dernière année de mise à jour de l’effectif salarié de l’unité légale est donnée si celle-ci est
  inférieure ou égale à l’année d’interrogation-3. Ainsi une interrogation en 2018 ne renverra la dernière année
  de mise à jour de l’effectif que si cette année est supérieure ou égale à 2015.
  */
	@Column({ nullable: true, name: 'annee_effectifs_unite_legale' })
	anneeEffectifsUniteLegale?: number;

	/*
  dateDernierTraitementUniteLegale: Date du dernier traitement de l’unité légale dans le répertoire Sirene
  Cette date peut concerner des mises à jour de données du répertoire Sirene qui ne sont pas diffusées.
  Cette variable peut être à null.
  */
	@Column({ type: 'timestamp', nullable: true, name: 'date_dernier_traitement_unite_legale' })
	dateDernierTraitementUniteLegale: Date | null;

	/*
  nombrePeriodesUniteLegale: Nombre de périodes de l’unité légale.
  Cette variable donne le nombre de périodes [dateDebut,dateFin] de l’unité légale. Chaque période
  correspond à l’intervalle de temps pendant lequel la totalité des variables historisées de l’unité légale n’ont
  pas été modifiées.
  Les dates de ces périodes sont des dates d’effet (et non des dates de traitement).
  */
	@Column({ nullable: true, name: 'nombre_periodes_unite_legale' })
	nombrePeriodesUniteLegale?: number;

	/*
  categorieEntreprise: Catégorie à laquelle appartient l’entreprise
  */
	@Column({ type: 'enum', enum: CategorieEntreprise, nullable: true, name: 'categorie_entreprise' })
	categorieEntreprise: CategorieEntreprise | null;

	/*
  anneeCategorieEntreprise: Année de validité de la catégorie d’entreprise.
  Cette variable désigne l’année de validité correspondant à la catégorie d'entreprise diffusée.
  */
	@Column({ nullable: true, name: 'annee_categorie_entreprise' })
	anneeCategorieEntreprise?: number;

	/*
  dateDebut: Date de début d'une période d'historique d'une unité légale
  Date de début de la période au cours de laquelle toutes les variables historisées de l'entreprise restent inchangées.
  La date 1900-01-01 signifie : date non déterminée. dateDebut peut-être vide uniquement pour les unités
  purgées (cf. variable unitePurgeeUniteLegale).
  La date de début de la période la plus ancienne ne correspond pas obligatoirement à la date de création de
  l'entreprise, certaines variables historisées pouvant posséder des dates de début soit au 1900-01-01, soit
  antérieures à la date de création.
  */
	@Column({ type: 'date', nullable: true, name: 'date_debut' })
	dateDebut: Date | null;

	/*

  etatAdministratifUniteLegale: État administratif de l’unité légale.
  Le passage à l’état « Cessée » découle de la prise en compte d’une déclaration de cessation administrative.
  Pour les personnes morales, cela correspond au dépôt de la déclaration de disparition de la personne morale.
  Pour les personnes physiques, cela correspond soit à la prise en compte de la déclaration de cessation
  d’activité déposée par l’exploitant soit au décès de l’exploitant conformément à la réglementation.
  En dehors de ces cas, l’unité légale est toujours à l’état administratif « Active ».
  Pour les personnes morales, la cessation administrative est, en théorie, définitive, l’état administratif "Cessée" est irréversible.
  Cependant, il existe actuellement dans la base un certain nombre d’unités légales personnes morales avec
  un historique d'état présentant un état cessé entre deux périodes à l’état actif.
  Pour les personnes physiques, dans le cas où l’exploitant déclare la cessation de son activité, puis la
  reprend quelque temps plus tard, cet état est réversible. Il est donc normal d'avoir des périodes successives
  d'état actif puis cessé pour les personnes physiques. En règle générale, la première période d’historique
  d’une unité légale correspond à un etatAdministratifUniteLegale égal à « Active ».
  Toutefois, l'état administratif peut être à null (première date de début de l'état postérieure à la première date
  de début d'une autre variable historisée).
  */
	@Column({
		type: 'enum',
		enum: EtatAdministratif,
		nullable: true,
		name: 'etat_administratif_unite_legale',
	})
	etatAdministratifUniteLegale: EtatAdministratif | null;

	/*
  nomUniteLegale:Nom de naissance de la personne physique
  Cette variable indique le libellé le nom de naissance pour une personne physique.
  Cette variable est à null pour les personnes morales. Le répertoire Sirene gère des caractères majuscules
  non accentués et les seuls caractères spéciaux tiret (-) et apostrophe.
  Le nom peut être à null (cas des unités purgées, première date de début du nom postérieure à la première
  date de début d'une autre variable historisée).
  */
	@Column({ type: 'varchar', length: 100, nullable: true, name: 'nom_unite_legale' })
	nomUniteLegale: string | null;

	/*
  nomUsageUniteLegale: Nom d’usage de la personne physique.
  Le nom d’usage est celui que la personne physique a choisi d’utiliser.
  Cette variable est à null pour les personnes morales. Elle peut être également à null pour les personnes physiques.
  Le répertoire Sirene gère des caractères majuscules non accentués et les seuls caractères spéciaux tiret (-) et apostrophe.
  Cette variable est historisée.
  */
	@Column({ type: 'varchar', length: 100, nullable: true, name: 'nom_usage_unite_legale' })
	nomUsageUniteLegale: string | null;

	/*
  denominationUniteLegale: Dénomination de l’unité légale.
  Cette variable désigne la raison sociale pour les personnes morales. Il s'agit du nom sous lequel est
  déclarée l'unité légale.
  Cette variable est à null pour les personnes physiques.
  Le répertoire Sirene gère des caractères majuscules non accentués avec caractères spéciaux (- & + @ ! ? *
  ° . % : € #).
  La dénomination peut parfois contenir la mention de la forme de la société (SA, SAS, SARL, etc.)
  */
	@Column({ type: 'varchar', length: 120, nullable: true, name: 'denomination_unite_legale' })
	denominationUniteLegale: string | null;

	/*
  denominationUsuelle1UniteLegale: Dénomination usuelle de l’unité légale.
  Cette variable désigne le nom (ou les noms) sous lequel l'entreprise est connue du grand public. Cet
  élément d'identification de l'entreprise (sur trois champs : denominationUsuelle1UniteLegale,
  denominationUsuelle2UniteLegale et denominationUsuelle3UniteLegale) a été enregistré au niveau unité
  légale avant l'application de la norme d'échanges CFE de 2008.
  À partir de la norme 2008, la dénomination usuelle est enregistrée au niveau de l'établissement sur un seul
  champ : denominationUsuelleEtablissement.
  Variables historisées avec une seule indicatrice de changement pour les trois variables.
  */
	@Column({
		type: 'varchar',
		length: 70,
		nullable: true,
		name: 'denomination_usuelle1_unite_legale',
	})
	denominationUsuelle1UniteLegale: string | null;

	/*
  denominationUsuelle2UniteLegale: Dénomination usuelle de l’unité légale – deuxième champ.
  */
	@Column({
		type: 'varchar',
		length: 70,
		nullable: true,
		name: 'denomination_usuelle2_unite_legale',
	})
	denominationUsuelle2UniteLegale: string | null;

	/*
  denominationUsuelle3UniteLegale: Dénomination usuelle de l’unité légale – troisième champ*/
	@Column({
		type: 'varchar',
		length: 70,
		nullable: true,
		name: 'denomination_usuelle3_unite_legale',
	})
	denominationUsuelle3UniteLegale: string | null;

	/*
   categorieJuridiqueUniteLegale: Catégorie juridique de l’unité légale.
   La catégorie juridique est un attribut des unités légales.
   Cette variable est à 1000 pour les personnes physiques.
   Lors de son dépôt de demande de création, le déclarant indique la forme juridique de l’unité légale qu’il crée,
   qui est ensuite traduite en code. Ce code est modifiable, à la marge, au cours de la vie de l’unité légale (pour
   les personnes morales) en fonction des déclarations de l’exploitant. Pour chaque unité légale, il existe à un
   instant donné un seul code Catégorie juridique. Il est attribué selon la nomenclature en vigueur.

   Description détaillée des codes : https://www.insee.fr/fr/information/2028129
  */
	@Column({ nullable: true, name: 'categorie_juridique_unite_legale' })
	categorieJuridiqueUniteLegale?: number;

	/*
  activitePrincipaleUniteLegale: Activité principale de l’unité légale.
  Lors de son inscription au répertoire, l’Insee attribue à toute unité légale un code dit « APE » sur la base de
  la description de l’activité principale faite par le déclarant. Ce code est modifiable au cours de la vie de l’unité
  légale en fonction des déclarations de l’exploitant.
  Pour chaque unité légale, il existe à un instant donné un seul code « APE». Il est attribué selon la
  nomenclature en vigueur. La nomenclature en vigueur est la Naf Rév2 et ce depuis le 1er Janvier 2008.
  Chaque code comporte 2 chiffres, un point, 2 chiffres et une lettre. Toutes les unités légales actives au
  01/01/2008 ont eu leur code APE recodé dans la nouvelle nomenclature, ainsi de très nombreuses
  entreprises ont une période débutant à cette date.
  Au moment de la déclaration de l’entreprise, il peut arriver que l’Insee ne soit pas en mesure d’attribuer le
  bon code APE : la modalité 00.00Z peut alors être affectée provisoirement.

  *Lien vers la Nomenclature d'Activités Française*
  https://insee.fr/fr/information/2406147
  */
	@Column({ type: 'varchar', length: 6, nullable: true, name: 'activite_principale_unite_legale' })
	activitePrincipaleUniteLegale: string | null;

	/*
  nomenclatureActivitePrincipaleUniteLegale: Nomenclature d’activité de la variable activitePrincipaleUniteLegale.
  Cette variable indique la nomenclature d’activité correspondant à la variable activitePrincipaleUniteLegale
  (cf. activitePrincipaleUniteLegale).

  Liste de codes
    NAFRev2
    NAFRev1
    NAF1993
    NAP
  */
	@Column({
		type: 'varchar',
		length: 8,
		nullable: true,
		name: 'nomenclature_activite_principale_unite_legale',
	})
	nomenclatureActivitePrincipaleUniteLegale: string | null;

	/*
  nicSiegeUniteLegale: Numéro interne de classement (Nic) de l’unité légale.
  Le siège d’une unité légale est le lieu où sont centralisées l’administration et la direction effective de l’unité légale.
  À un instant donné, chaque unité légale a un seul siège. Mais, au cours de la vie de l’unité légale, le siège
  peut être différent. Chaque siège est identifié par un numéro Nic (Numéro Interne de Classement de
  l'établissement) qui respecte les règles d’attribution des numéros d’établissement.
  Le Nic est composé de quatre chiffres et d'un cinquième qui permet de contrôler la validité du numéro Siret
  (concaténation du numéro Siren et du Nic).
  */
	@Column({ type: 'varchar', length: 5, nullable: true, name: 'nic_siege_unite_legale' })
	nicSiegeUniteLegale: string | null;

	/*
  economieSocialeSolidaireUniteLegale: Appartenance au champ de l’économie sociale et solidaire.
  Cette variable indique si l'entreprise appartient au champ de l’économie sociale et solidaire.
  La loi n° 2014-856 du 31 juillet 2014 définit officiellement le périmètre de l’économie sociale et solidaire
  (ESS). Celle-ci comprend les quatre familles traditionnelles en raison de leur régime juridique (associations,
  fondations, coopératives et mutuelles) et inclut une nouvelle catégorie, les entreprises de l’ESS, adhérant
  aux mêmes principes :
  • poursuivre un but social autre que le seul partage des bénéfices ;
  • un caractère lucratif encadré (notamment des bénéfices majoritairement consacrés au maintien et
  au développement de l’activité) ;
  • une gouvernance démocratique et participative.
  Cette variable est historisée, renseignée pour environ 1 million d'entreprises, sinon null.
  */
	@Column({
		type: 'enum',
		enum: ecoSocSol,
		nullable: true,
		name: 'eco_soc_sol_unite_legale',
	})
	economieSocialeSolidaireUniteLegale: ecoSocSol | null;

	@Column({
		type: 'enum',
		enum: societeMissionUniteLegale,
		nullable: true,
		name: 'societe_mission_unite_legale',
	})
	societeMissionUniteLegale: societeMissionUniteLegale | null;

	/*
  caractereEmployeurUniteLegale: Caractère employeur de l’unité légale.
  Lors de sa formalité de création, le déclarant indique si l’unité légale aura ou non des employés. Par la suite,
  le déclarant peut également faire des déclarations de prise d’emploi et de fin d’emploi. La prise en compte
  d’une déclaration de prise d’emploi bascule immédiatement l’unité légale en « Employeuse ». Inversement,
  lorsqu’une déclaration de fin d’emploi est traitée, l’unité légale devient « Non employeuse ».
  Le caractère employeur est O si au moins l'un des établissements actifs de l'unité légale emploie des
  salariés.
  Cette variable est historisée.
  */
	@Column({ type: 'varchar', length: 6, nullable: true, name: 'caractere_employeur_unite_legale' })
	caractereEmployeurUniteLegale: string | null;

	@Column({ type: 'varchar', length: 32, nullable: true })
	hash: string | null;
}
