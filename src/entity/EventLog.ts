import {
	Entity,
	PrimaryGeneratedColumn,
	Index,
	JoinColumn,
	ManyToOne,
	CreateDateColumn,
	Column,
} from 'typeorm';

import { DevEco } from './DevEco';

export enum EventLogActionType {
	CREATE = 'CREATE',
	DELETE = 'DELETE',
	UPDATE = 'UPDATE',
	VIEW = 'VIEW',
	UPDATE_QUALIFICATION = 'UPDATE_QUALIFICATION',
	CREATE_ENTREPRISE = 'CREATE_ENTREPRISE',
	UNLINK_ENTREPRISE = 'UNLINK_ENTREPRISE',
	CONTACT_QUALIFICATION = 'CONTACT_QUALIFICATION',
}

export enum EventLogEntityType {
	PERSONNE_PHYSIQUE = 'FICHE_PP',
	PERSONNE_MORALE = 'FICHE_PM',
	LOCAL = 'LOCAL',
}

@Entity({ name: 'event_log' })
export class EventLog {
	@PrimaryGeneratedColumn()
	id: number;

	@ManyToOne(() => DevEco, (deveco) => deveco.eventLogs, { onDelete: 'CASCADE' })
	@JoinColumn({ name: 'deveco_id' })
	@Index()
	deveco: DevEco;

	@CreateDateColumn({ name: 'date' })
	date: Date;

	@Column({ name: 'entity_type' })
	@Index()
	entityType: EventLogEntityType;

	@Column({ name: 'entity_id' })
	@Index()
	entityId: number;

	@Column({ name: 'action' })
	@Index()
	action: EventLogActionType;
}
