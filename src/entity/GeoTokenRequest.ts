import {
	Entity,
	Column,
	PrimaryGeneratedColumn,
	ManyToOne,
	CreateDateColumn,
	JoinColumn,
} from 'typeorm';

import { GeoToken } from './GeoToken';

@Entity({ name: 'geo_token_request' })
export class GeoTokenRequest {
	@PrimaryGeneratedColumn()
	id: number;

	@ManyToOne(() => GeoToken)
	@JoinColumn({ name: 'geo_token_id' })
	geoToken?: GeoToken;

	@Column({ name: 'url' })
	url: string;

	@Column({ type: 'varchar', name: 'user_agent', nullable: true })
	userAgent: string | null;

	@CreateDateColumn({ name: 'created_at' })
	createdAt: Date;
}
