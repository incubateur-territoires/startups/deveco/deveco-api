import {
	Entity,
	JoinColumn,
	PrimaryGeneratedColumn,
	CreateDateColumn,
	UpdateDateColumn,
	Unique,
	ManyToOne,
	Column,
	Index,
} from 'typeorm';

import { DevEco } from './DevEco';
import { Etablissement } from './Etablissement';
import { Territory } from './Territory';
import { TerritoryEtablissementReference } from './TerritoryEtablissementRef';

@Entity({ name: 'etablissement_favori' })
@Unique('UNIQ_deveco_etablissement_favori', ['deveco', 'ter'])
export class EtablissementFavori {
	@PrimaryGeneratedColumn()
	id?: number;

	@ManyToOne(() => DevEco, (deveco) => deveco.id)
	@Index()
	@JoinColumn({ name: 'deveco_id' })
	deveco?: DevEco;

	@ManyToOne(() => TerritoryEtablissementReference)
	@JoinColumn([
		{ name: 'territory_id', referencedColumnName: 'territory' },
		{ name: 'siret', referencedColumnName: 'siret' },
	])
	ter: TerritoryEtablissementReference;

	@ManyToOne(() => Territory, (territory) => territory.id)
	@JoinColumn({ name: 'territory_id' })
	territory: Territory;

	@ManyToOne(() => Etablissement, (etablissement) => etablissement.siret)
	@JoinColumn({ name: 'siret' })
	siret: Etablissement;

	@Column()
	favori: boolean;

	@CreateDateColumn({ name: 'created_at' })
	createdAt: Date;

	@UpdateDateColumn({ name: 'updated_at' })
	updatedAt: Date;
}
