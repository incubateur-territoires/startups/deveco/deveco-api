import { Entity, Column, PrimaryColumn, OneToMany } from 'typeorm';

import { Commune } from './Commune';

@Entity({ name: 'region' })
export class Region {
	@PrimaryColumn({ name: 'insee_reg' })
	id: string;

	@Column({ name: 'nom' })
	nom: string;

	@OneToMany(() => Commune, (commune) => commune.region)
	communes: Commune[];
}
