import {
	Entity,
	PrimaryGeneratedColumn,
	Index,
	JoinColumn,
	ManyToOne,
	Column,
	Unique,
} from 'typeorm';

import { Territory } from './Territory';

@Entity({ name: 'localisation_entreprise' })
@Unique('UNIQ_localisation_entreprise_for_territoire', ['territory', 'localisation'])
export class LocalisationEntreprise {
	@PrimaryGeneratedColumn()
	id: number;

	@ManyToOne(() => Territory)
	@JoinColumn({ name: 'territory_id' })
	@Index()
	territory: Territory;

	@Column()
	localisation: string;
}
