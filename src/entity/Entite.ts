import {
	Entity,
	PrimaryGeneratedColumn,
	JoinColumn,
	ManyToOne,
	Column,
	OneToOne,
	OneToMany,
	CreateDateColumn,
	UpdateDateColumn,
	Unique,
	Index,
} from 'typeorm';

import { Contact } from './Contact';
import { DevEco } from './DevEco';
import { Entreprise } from './Entreprise';
import { Fiche } from './Fiche';
import { Particulier } from './Particulier';
import { Proprietaire } from './Proprietaire';
import { Territory } from './Territory';

export type EntiteType = 'PP' | 'PM';

@Entity({ name: 'entite' })
@Unique('UNIQ_entite_territoire_entreprise', ['territoire', 'entreprise'])
export class Entite {
	@PrimaryGeneratedColumn()
	id: number;

	@OneToOne(() => Fiche, (fiche) => fiche.entite)
	fiche?: Fiche;

	@OneToMany(() => Proprietaire, (proprietaire) => proprietaire.entite)
	proprietes?: Proprietaire[];

	@ManyToOne(() => Territory)
	@JoinColumn({ name: 'territoire_id' })
	@Index()
	territoire: Territory;

	@ManyToOne(() => DevEco)
	@JoinColumn({ name: 'deveco_id' })
	createur: DevEco;

	@ManyToOne(() => DevEco)
	@JoinColumn({ name: 'auteur_modification_id' })
	auteurModification: DevEco;

	@OneToMany(() => Contact, (contact) => contact.entite, { cascade: true })
	contacts: Contact[];

	@Column({ name: 'entite_type' })
	@Index()
	entiteType: EntiteType;

	@Column('simple-array', { name: 'activites_reelles', nullable: true })
	activitesReelles?: string[];

	@Column('simple-array', { name: 'entreprise_localisations', nullable: true })
	entrepriseLocalisations?: string[];

	@Column('simple-array', { name: 'mots_cles', nullable: true })
	motsCles?: string[];

	@Column({ name: 'activite_autre', nullable: true })
	activiteAutre?: string;

	@ManyToOne(() => Entreprise, { onDelete: 'NO ACTION', onUpdate: 'NO ACTION' })
	@JoinColumn({ name: 'entreprise_id' })
	@Index()
	entreprise?: Entreprise;

	@OneToOne(() => Particulier, (particulier) => particulier.entite, { cascade: true })
	@JoinColumn({ name: 'particulier_id' })
	@Index()
	particulier?: Particulier;

	@CreateDateColumn({ name: 'created_at' })
	createdAt: Date;

	@UpdateDateColumn({ name: 'updated_at' })
	updatedAt: Date;

	@Column({ name: 'future_enseigne', nullable: true })
	futureEnseigne?: string;

	@ManyToOne(() => Entreprise, { onDelete: 'NO ACTION', onUpdate: 'NO ACTION' })
	@JoinColumn({ name: 'enseigne_id' })
	@Index()
	etablissementCree?: Entreprise;

	@ManyToOne(() => Particulier, { onDelete: 'NO ACTION', onUpdate: 'NO ACTION' })
	@JoinColumn({ name: 'createur_id' })
	@Index()
	createurLie?: Particulier;

	@Column({ default: false })
	exogene: boolean;
}
