import {
	Entity,
	PrimaryGeneratedColumn,
	Index,
	JoinColumn,
	ManyToOne,
	Column,
	Unique,
} from 'typeorm';

import { Territory } from './Territory';

@Entity({ name: 'activite_entreprise' })
@Unique('UNIQ_activite_entreprise_for_territoire', ['territory', 'activite'])
export class ActiviteEntreprise {
	@PrimaryGeneratedColumn()
	id: number;

	@ManyToOne(() => Territory)
	@JoinColumn({ name: 'territory_id' })
	@Index()
	territory: Territory;

	@Column()
	activite: string;
}
