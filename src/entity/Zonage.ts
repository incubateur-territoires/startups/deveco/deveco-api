import {
	Entity,
	Column,
	Index,
	PrimaryGeneratedColumn,
	ManyToOne,
	JoinColumn,
	Unique,
} from 'typeorm';

import { Territory } from './Territory';

@Unique('uniq_zonage_territory', ['nom', 'territoire'])
@Entity({ name: 'zonage' })
export class Zonage {
	@PrimaryGeneratedColumn()
	id: number;

	@ManyToOne(() => Territory)
	@JoinColumn({ name: 'territoire_id' })
	@Index()
	territoire: Territory;

	@Column({ name: 'nom', length: 254, nullable: true })
	nom: string;

	@Index('geometry', { spatial: true })
	@Column({
		type: 'geometry',
		srid: 4326,
		spatialFeatureType: 'MultiPolygon',
		name: 'geometry',
		nullable: true,
	})
	geometry: string;

	@Column({
		type: 'geometry',
		srid: 3949,
		spatialFeatureType: 'MultiPolygon',
		name: 'geometry_source',
		nullable: true,
	})
	geometrySource: string;
}
