import {
	Entity,
	PrimaryGeneratedColumn,
	Column,
	ManyToOne,
	JoinColumn,
	OneToOne,
	Index,
	CreateDateColumn,
} from 'typeorm';

import { Entite } from './Entite';
import { Particulier } from './Particulier';

@Entity({ name: 'contact' })
export class Contact {
	@PrimaryGeneratedColumn()
	id?: number;

	@ManyToOne(() => Entite, (entite) => entite.contacts, { onDelete: 'CASCADE' })
	@JoinColumn({ name: 'entite_id' })
	@Index()
	entite: Entite;

	@Column({ name: 'fonction', nullable: true })
	fonction: string;

	@Column({ name: 'source', type: 'varchar', nullable: true })
	source: 'API' | 'import' | null;

	// TODO if this changes to support one Particulier to many Contacts,
	// ContactController.delete should be updated accordingly
	@OneToOne(() => Particulier)
	@JoinColumn({ name: 'particulier_id' })
	@Index()
	particulier: Particulier;

	@CreateDateColumn({ name: 'created_at' })
	createdAt: Date;
}
