import { Entity, Column, PrimaryColumn, Index } from 'typeorm';

import { caractereEmployeur, DiffusionStatut, EtatAdministratifEtablissement } from './enums';

@Entity({ name: 'sirene_etablissement' })
export class SireneEtablissement {
	//48 fields

	@Index()
	@Column({ type: 'varchar', length: 9 })
	siren: string;

	/*
  nic: Numéro interne de classement de l'établissement.
  Le numéro interne de classement permet de distinguer les établissements d'une même entreprise. Il est
  composé de 5 chiffres. Associé au Siren, il forme le Siret de l'établissement. Il est composé de quatre
  chiffres et d'un cinquième qui permet de contrôler la validité du numéro Siret.
  Le Nic est attribué une seule fois au sein de l'entreprise. Si l'établissement ferme, son Nic ne peut être
  réattribué à un nouvel établissement.
  */
	@Column({ type: 'varchar', length: 5 })
	nic: string;

	/*
  siret: Numéro Siret.
  Le numéro Siret est le numéro unique d’identification attribué à chaque établissement par l’Insee. Ce numéro
  est un simple numéro d’ordre, composé de 14 chiffres non significatifs : les neuf premiers correspondent au
  numéro Siren de l’entreprise dont l’établissement dépend et les cinq derniers à un numéro interne de classement (Nic).
  Une entreprise est constituée d’autant d’établissements qu’il y a de lieux différents où elle exerce son
  activité. L’établissement est fermé quand l’activité cesse dans l’établissement concerné ou lorsque
  l’établissement change d’adresse.
  */
	@PrimaryColumn({ length: 14 })
	siret: string;

	/*
  statutDiffusionUniteLegale: Statut de diffusion de l’unité légale.
  Seules les unités légales diffusibles sont accessibles à tout public (statutDiffusionUniteLegale=O)
  */
	@Column({
		type: 'enum',
		enum: DiffusionStatut,
		default: DiffusionStatut.N,
		name: 'statut_diffusion_etablissement',
	})
	statutDiffusionEtablissement: DiffusionStatut;

	/*
  dateCreationEtablissement: Date de création de l’établissement
  La date de création correspond à la date qui figure dans les statuts de l’entreprise déposés au CFE
  compétent.
  Pour les établissements des unités purgées (unitePurgeeUniteLegale=true) : si la date de création est au
  01/01/1900 dans Sirene, la date est forcée à null.
  Dans tous les autres cas, la date de création n'est jamais à null. Si elle est non renseignée, elle sera au
  01/01/1900.
  La date de création ne correspond pas obligatoirement à dateDebut de la première période de
  l'établissement, certaines variables historisées pouvant posséder des dates de début soit au 1900-01-01,
  soit antérieures à la date de création.
  */
	@Column({ type: 'date', nullable: true, name: 'date_creation_etablissement' })
	dateCreationEtablissement: Date | null;

	/*
  trancheEffectifsEtablissement: Tranche d’effectif salarié de l’établissement.
  Il s’agit d’une variable statistique, millésimée au 31/12 d’une année donnée (voir variable
  anneeEffectifsEtablissement).
  Liste de codes
  NN : Etablissement non employeur (pas de salarié au cours de l'année de référence et pas d'effectif au
  31/12)
  00 : 0 salarié (n'ayant pas d'effectif au 31/12 mais ayant employé des salariés au cours de l'année de
  référence)
  01 : 1 ou 2 salariés
  02 : 3 à 5 salariés
  03 : 6 à 9 salariés
  11 : 10 à 19 salariés
  12 : 20 à 49 salariés
  21 : 50 à 99 salariés
  22 : 100 à 199 salariés
  31 : 200 à 249 salariés
  32 : 250 à 499 salariés
  41 : 500 à 999 salariés
  42 : 1 000 à 1 999 salariés
  51 : 2 000 à 4 999 salariés
  52 : 5 000 à 9 999 salariés
  53 : 10 000 salariés et plus
  */

	@Column({ type: 'varchar', length: 2, nullable: true, name: 'tranche_effectifs_etablissement' })
	trancheEffectifsEtablissement: string | null;

	@Column({ nullable: true, length: 9, name: 'annee_effectifs_etablissement' })
	anneeEffectifsEtablissement: string;

	/*
  activitePrincipaleRegistreMetiersEtablissement:  Activité exercée par l’artisan inscrit au registre des métiers
  Cette variable désigne le code de l'activité exercée par l'artisan inscrit au registre des métiers selon la
  Nomenclature d'Activités Française de l'Artisanat (NAFA).
  Liste de codes
  *Lien vers la Nomenclature d'Activités Française de l'Artisanat (NAFA)*
  https://www.entreprises.gouv.fr/secteurs-professionnels/artisanat/la-nafa
  PS: https://www.entreprises.gouv.fr/files/files/directions_services/secteurs-professionnels/artisanat/nafa/nomenclature_nafa_rev2.pdf
  */
	@Column({ nullable: true, length: 6, name: 'activite_principale_registre_metiers_etablissement' })
	activitePrincipaleRegistreMetiersEtablissement?: string;

	/*
  dateDernierTraitementEtablissement: Date du dernier traitement de l’établissement dans le répertoire Sirene.
  Cette date peut concerner des mises à jour de données du répertoire Sirene qui ne sont pas diffusées par
  API Sirene.
  Cette variable peut être à null.
  */
	@Column({ type: 'timestamp', nullable: true, name: 'date_dernier_traitement_etablissement' })
	dateDernierTraitementEtablissement: Date | null;

	/*
  etablissementSiege: Qualité de siège ou non de l’établissement.
  C’est une variable booléenne qui indique si l'établissement est le siège ou non de l'unité légale. Variable calculée toujours renseignée.
  */
	@Column({ name: 'etablissement_siege' })
	etablissementSiege: boolean;

	/*
  nombrePeriodesEtablissement: Nombre de périodes de l’établissement.
  Cette variable donne le nombre de périodes [dateDebut,dateFin] de l’établissement. Chaque période
  correspond à l’intervalle de temps pendant lequel la totalité des variables historisées de l’établissement n’ont
  pas été modifiées.
  Les dates de ces périodes sont des dates d’effet (et non des dates de traitement)
  */
	@Column({ nullable: true, name: 'nombre_periodes_etablissement' })
	nombrePeriodesEtablissement?: number;

	/*
  complementAdresseEtablissement: Complément d’adresse.
  Cette variable est un élément constitutif de l’adresse.
  C'est une variable facultative qui précise l'adresse avec :
    • une indication d'étage, d'appartement, de porte, de N° de boîte à lettres,
    • la désignation d'un bâtiment, d'un escalier, d'une entrée, d'un bloc,
    • le nom d'une résidence, d'un ensemble…
  */
	@Column({ nullable: true, length: 100, name: 'complement_adresse_etablissement' })
	complementAdresseEtablissement?: string;

	/*
  numeroVoieEtablissement: Numéro de voie.
  C'est un élément constitutif de l’adresse.
  Cette variable est facultative.
  Si plusieurs numéros de voie sont indiqués (5-7, 5 à 7…), l'information complète (5-7) ou (5 à 7) figure en
  complément d'adresse et le premier des numéros (5 dans l'exemple) est porté dans la variable numeroVoieEtablissement.
  */
	@Column({ nullable: true, type: 'varchar', length: 10, name: 'numero_voie_etablissement' })
	numeroVoieEtablissement?: string | null;

	/*
  indiceRepetitionEtablissement: Indice de répétition dans la voie.
  Cette variable un élément constitutif de l’adresse.
  Cette variable est facultative.
  Elle doit être associée à la variable numeroVoieEtablissement.
  */
	@Column({ nullable: true, type: 'varchar', length: 10, name: 'indice_repetition_etablissement' })
	indiceRepetitionEtablissement: string | null;

	/*
  typeVoieEtablissement: Type de voie.
  C'est un élément constitutif de l’adresse.
  Liste de codes
    *Liste des types de voie normalisés*
    AIRE : Aire
    ALL : Allée
    AV : Avenue
    BASE : Base
    BD : Boulevard
    CAMI : Cami
    CAR : Carrefour
    CHE : Chemin
    CHEM : Cheminement
    CHS : Chaussée
    CITE : Cité
    CLOS : Clos
    COIN : Coin
    COR : Corniche
    COTE : Cote
    COUR : Cour
    CRS : Cours
    DOM : Domaine
    DSC : Descente
    ECA : Ecart
    ESP : Esplanade
    FG : Faubourg
    GARE : Gare
    GR : Grande Rue
    HAM : Hameau
    HLE : Halle
    ILOT : Ilôt
    IMP : Impasse
    LD : Lieu dit
    LOT : Lotissement
    MAR : Marché
    MTE : Montée
    PARC : Parc
    PAS : Passage
    PL : Place
    PLAN : Plan
    PLN : Plaine
    PLT : Plateau
    PONT : Pont
    PORT : Port
    PRO : Promenade
    PRV : Parvis
    QUA : Quartier
    QUAI : Quai
    RES : Résidence
    RLE : Ruelle
    ROC : Rocade
    RPT : Rond Point
    RTE : Route
    RUE : Rue
    SEN : Sente - Sentier
    SQ : Square
    TOUR : Tour
    TPL : Terre-plein
    TRA : Traverse
    VLA : Villa
    VLGE : Village
    VOIE : Voie
    ZA : Zone artisanale
    ZAC : Zone d'aménagement concerté
    ZAD : Zone d'aménagement différé
    ZI : Zone industrielle
    ZONE : Zone
    " " : sans objet ou autre
  */
	@Column({ nullable: true, type: 'varchar', length: 4, name: 'type_voie_etablissement' })
	typeVoieEtablissement: string | null;

	/*
  libelleVoieEtablissement: Libellé de voie.
  Cette variable indique le libellé de voie de la commune de localisation de l'établissement.
  C’est un élément constitutif de l’adresse.
  Cette variable est facultative : elle n'est pas toujours renseignée, en particulier dans les petites communes.
  */
	@Column({ nullable: true, length: 100, type: 'varchar', name: 'libelle_voie_etablissement' })
	libelleVoieEtablissement: string | null;

	/*
  codePostalEtablissement: Code postal.
  Cette variable désigne le code postal de l’adresse de l’établissement.
  Pour les adresses à l’étranger (codeCommuneEtablissement commence par 99), cette variable est à null
  */
	@Column({ nullable: true, length: 5, type: 'varchar', name: 'code_postal_etablissement' })
	codePostalEtablissement: string | null;

	/*
  libelleCommuneEtablissement: Libellé de la commune.
  Cette variable indique le libellé de la commune de localisation de l'établissement si celui-ci n’est pas à
  l’étranger. C’est un élément constitutif de l’adresse.
  */
	@Column({ nullable: true, length: 100, type: 'varchar', name: 'libelle_commune_etablissement' })
	libelleCommuneEtablissement: string | null;

	/*
  libelleCommuneEtrangerEtablissement: Libellé de la commune pour un établissement situé à l’étranger.
  Cette variable est un élément constitutif de l’adresse pour les établissements situés sur le territoire étranger
  (la variable codeCommuneEtablissement est vide)
  */
	@Column({
		nullable: true,
		length: 100,
		type: 'varchar',
		name: 'libelle_commune_etranger_etablissement',
	})
	libelleCommuneEtrangerEtablissement: string | null;

	/*
  distributionSpecialeEtablissement: Distribution spéciale de l’établissement.
  La distribution spéciale reprend les éléments particuliers qui accompagnent une adresse de distribution
  spéciale. C’est un élément constitutif de l’adresse.
  Cette variable est facultative.
  *Exemples de début de modalités attendues*
    BP : Boîte postale
    TSA : Tri par service à l'arrivée
    LP : Local postal
    RP : Référence postale
    SP : Secteur postal
    CP : Case postale
    CE : Case entreprise
    CS : Course spéciale
    POSTE RESTANTE
    CEDEX : Courrier d'entreprise à distribution exceptionnelle
    CIDEX : Courrier individuel à distribution exceptionnelle
    CASE, NIVEAU : Mots utilisés pour la distribution interne du courrier à LA DEFENSE
    CASIER : Utilisé pour le centre commercial des Quatre Temps
    SILIC, SENIA, MAREE, FLEURS… : Utilisés sur le site de Rungis
  */
	@Column({
		nullable: true,
		length: 100,
		type: 'varchar',
		name: 'distribution_speciale_etablissement',
	})
	distributionSpecialeEtablissement: string | null;

	/*
  codeCommuneEtablissement: Code commune de l’établissement
  Cette variable désigne le code de la commune de localisation de l'établissement, hors adresse à l'étranger.
  Le code commune correspond au code commune existant à la date de la mise à disposition : toute
  modification du code officiel géographique est répercutée sur la totalité des établissements (même ceux
  fermés) correspondant à ce code commune.
  Pour les établissements localisés à l'étranger, la variable codeCommuneEtablissement est à null.
  Liste de codes
  *Lien vers le code officiel géographique*
  https://www.insee.fr/fr/information/2028028
  */
	@Column({ nullable: true, length: 5, type: 'varchar', name: 'code_commune_etablissement' })
	codeCommuneEtablissement: string | null;

	/*
  codeCedexEtablissement: Code cedex
  Cette variable est un élément constitutif de l’adresse.
  Elle est facultative.
  */
	@Column({ nullable: true, length: 9, type: 'varchar', name: 'code_cedex_etablissement' })
	codeCedexEtablissement: string | null;

	/*
  libelleCedexEtablissement: Libellé du code cedex
  Cette variable indique le libellé correspondant au code cedex de l'établissement si celui-ci est non null. Ce
  libellé est le libellé utilisé dans la ligne 6 d’adresse pour l’acheminement postal.
  */
	@Column({ nullable: true, length: 100, type: 'varchar', name: 'libelle_cedex_etablissement' })
	libelleCedexEtablissement: string | null;

	/*
  codePaysEtrangerEtablissement: Code pays pour un établissement situé à l’étranger.
  Cette variable désigne le code du pays de localisation de l'établissement pour les adresses à l'étranger.
  La variable codePaysEtrangerEtablissement commence toujours par 99 si elle est renseignée. Les 3
  caractères suivants sont le code du pays étranger.
  Liste de codes
  *Lien vers le code officiel géographique*
  https://www.insee.fr/fr/information/2028273
  */

	@Column({ nullable: true, length: 5, type: 'varchar', name: 'code_pays_etranger_etablissement' })
	codePaysEtrangerEtablissement: string | null;

	/*
  libellePaysEtrangerEtablissement:  Libellé du pays pour un établissement situé à l’étranger.
  Cette variable indique le libellé du pays de localisation de l'établissement si celui-ci est à l’étranger.
  C’est un élément constitutif de l’adresse.
  */
	@Column({
		nullable: true,
		length: 100,
		type: 'varchar',
		name: 'libelle_pays_etranger_etablissement',
	})
	libellePaysEtrangerEtablissement: string | null;

	/*
  complementAdresse2Etablissement: Complément d’adresse secondaire.
  Dans le cas où l’établissement dispose d’une entrée secondaire, cette variable est un élément constitutif de
  l’adresse secondaire.
  C'est une variable facultative qui précise l'adresse secondaire avec :
    • une indication d'étage, d'appartement, de porte, de N° de boîte à lettres,
    • la désignation d'un bâtiment, d'un escalier, d'une entrée, d'un bloc,
    • le nom d'une résidence, d'un ensemble…
  */
	@Column({
		nullable: true,
		length: 38,
		type: 'varchar',
		name: 'complement_adresse2_etablissement',
	})
	complementAdresse2Etablissement: string | null;

	/*
  numeroVoie2Etablissement: Numéro de la voie de l’adresse secondaire.
  C'est un élément constitutif de l’adresse secondaire.
  Cette variable est facultative.
  Si plusieurs numéros de voie sont indiqués (5-7, 5 à 7…), l'information complète (5-7) ou (5 à 7) figure en
  complément d'adresse secondaire et le premier des numéros (5 dans l'exemple) est porté dans la variable
  numeroVoie2Etablissement.
  */
	@Column({ nullable: true, length: 5, type: 'varchar', name: 'numero_voie2_etablissement' })
	numeroVoie2Etablissement: string | null;

	/*
  indiceRepetition2Etablissement: Indice de répétition dans la voie pour l’adresse secondaire.
  Cette variable un élément constitutif de l’adresse secondaire. Cette variable est facultative.
  Elle doit être associée à la variable numeroVoie2Etablissement.

  */
	@Column({ nullable: true, length: 5, type: 'varchar', name: 'indice_repetition2_etablissement' })
	indiceRepetition2Etablissement: string | null;

	/*
  typeVoie2Etablissement: Type de voie de l’adresse secondaire.
  Cette variable est un élément constitutif de l’adresse secondaire.
  Liste de codes
    *Liste des types de voie normalisés*
    AIRE : Aire
    ALL : Allée
    AV : Avenue
    BASE : Base
    BD : Boulevard
    CAMI : Cami
    CAR : Carrefour
    CHE : Chemin
    CHEM : Cheminement
    CHS : Chaussée
    CITE : Cité
    CLOS : Clos
    COIN : Coin
    COR : Corniche
    COTE : Cote
    COUR : Cour
    CRS : Cours
    DOM : Domaine
    DSC : Descente
    ECA : Ecart
    ESP : Esplanade
    FG : Faubourg
    GARE : Gare
    GR : Grande Rue
    HAM : Hameau
    HLE : Halle
    ILOT : Ilôt
    IMP : Impasse
    LD : Lieu dit
    LOT : Lotissement
    MAR : Marché
    MTE : Montée
    PARC : Parc
    PAS : Passage
    PL : Place
    PLAN : Plan
    PLN : Plaine
    PLT : Plateau
    PONT : Pont
    PORT : Port
    PRO : Promenade
    PRV : Parvis
    QUA : Quartier
    QUAI : Quai
    RES : Résidence
    RLE : Ruelle
    ROC : Rocade
    RPT : Rond Point
    RTE : Route
    RUE : Rue
    SEN : Sente - Sentier
    SQ : Square
    TOUR : Tour
    TPL : Terre-plein
    TRA : Traverse
    VLA : Villa
    VLGE : Village
    VOIE : Voie
    ZA : Zone artisanale
    ZAC : Zone d'aménagement concerté
    ZAD : Zone d'aménagement différé
    ZI : Zone industrielle
    ZONE : Zone
    " " : sans objet ou autre
  */
	@Column({ nullable: true, length: 4, type: 'varchar', name: 'type_voie2_etablissement' })
	typeVoie2Etablissement: string | null;

	/*
  libelleVoie2Etablissement: Libellé de voie de l’adresse secondaire.
  Cette variable indique le libellé de la voie de l’adresse secondaire de l'établissement.
  C’est un élément constitutif de l’adresse secondaire.
  Cette variable est facultative : elle n'est pas toujours renseignée, en particulier dans les petites communes.
  */
	@Column({ nullable: true, length: 100, type: 'varchar', name: 'libelle_voie2_etablissement' })
	libelleVoie2Etablissement: string | null;

	/*
  codePostal2Etablissement: Code postal de l’adresse secondaire
  Dans le cas où l’établissement dispose d’une entrée secondaire, cette variable désigne le code postal de
  l’adresse secondaire de l’établissement.
  Pour les adresses à l’étranger (codeCommune2Etablissement commence par 99), cette variable est à null.
  */
	@Column({ nullable: true, length: 5, type: 'varchar', name: 'code_postal2_etablissement' })
	codePostal2Etablissement: string | null;

	/*
  libelleCommune2Etablissement: Libellé de la commune de l’adresse secondaire.
  Cette variable indique le libellé de la commune de l’adresse secondaire de l'établissement si celui-ci n’est
  pas à l’étranger. C’est un élément constitutif de l’adresse.
  */
	@Column({ nullable: true, length: 100, type: 'varchar', name: 'libelle_commune2_etablissement' })
	libelleCommune2Etablissement: string | null;

	/*
  libelleCommuneEtranger2Etablissement: Libellé de la commune de l’adresse secondaire pour un établissement situé à l’étranger.
  Dans le cas où l’établissement dispose d’une entrée secondaire, cette variable est un élément constitutif de
  l’adresse secondaire pour les établissements situés sur le territoire étranger (la variable codeCommune2Etablissement est vide).
  */
	@Column({
		nullable: true,
		length: 100,
		type: 'varchar',
		name: 'libelle_commune_etranger2_etablissement',
	})
	libelleCommuneEtranger2Etablissement: string | null;

	/*
  distributionSpeciale2Etablissement: Distribution spéciale de l’adresse secondaire de l’établissement
  Dans le cas où l’établissement dispose d’une entrée secondaire, la distribution spéciale reprend les
  éléments particuliers qui accompagnent l’adresse secondaire de distribution spéciale. C’est un élément
  constitutif de l’adresse secondaire.
  Cette variable est facultative

  *Exemples de début de modalités attendues*
    BP : Boîte postale
    TSA : Tri par service à l'arrivée
    LP : Local postal
    RP : Référence postale
    SP : Secteur postal
    CP : Case postale
    CE : Case entreprise
    CS : Course spéciale
    POSTE RESTANTE
    CEDEX : Courrier d'entreprise à distribution exceptionnelle
    CIDEX : Courrier individuel à distribution exceptionnelle
    CASE, NIVEAU : Mots utilisés pour la distribution interne du courrier à LA DEFENSE
    CASIER : Utilisé pour le centre commercial des Quatre Temps
    SILIC, SENIA, MAREE, FLEURS… : Utilisés sur le site de Rungis
  */
	@Column({
		nullable: true,
		length: 26,
		type: 'varchar',
		name: 'distribution_speciale2_etablissement',
	})
	distributionSpeciale2Etablissement: string | null;

	/*
  codeCommune2Etablissement: Code commune de l’adresse secondaire
  */
	@Column({ nullable: true, length: 5, type: 'varchar', name: 'code_commune2_etablissement' })
	codeCommune2Etablissement: string | null;

	/*
  codeCedex2Etablissement: Code cedex de l’adresse secondaire
  */
	@Column({ nullable: true, length: 9, type: 'varchar', name: 'code_cedex2_etablissement' })
	codeCedex2Etablissement: string | null;

	/*
  libelleCedex2Etablissement: Libellé du code cedex de l’adresse secondaire
  */
	@Column({ nullable: true, length: 100, type: 'varchar', name: 'libelle_cedex2_etablissement' })
	libelleCedex2Etablissement: string | null;

	/*
  codePaysEtranger2Etablissement: Code pays de l’adresse secondaire pour un établissement situé à l’étranger
  */
	@Column({ nullable: true, length: 5, type: 'varchar', name: 'code_pays_etranger2_etablissement' })
	codePaysEtranger2Etablissement: string | null;

	/*
  libellePaysEtranger2Etablissement: Libellé du pays de l’adresse secondaire pour un établissement situé à l’étranger
  */
	@Column({
		nullable: true,
		length: 100,
		type: 'varchar',
		name: 'libelle_pays_etranger2_etablissement',
	})
	libellePaysEtranger2Etablissement: string | null;

	/*
  dateDebut: Date de début d'une période d'historique d'un établissement.
  Date de début de la période au cours de laquelle toutes les variables historisées de l'établissement restent inchangées.
  La date 1900-01-01 signifie : date non déterminée.
  dateDebut peut-être vide uniquement pour les établissements des unités purgées (cf. variable
  unitePurgeeUniteLegale dans le descriptif des variables du fichier StockUniteLegale).
  La date de début de la période la plus ancienne ne correspond pas obligatoirement à la date de création de
  l'établissement, certaines variables historisées pouvant posséder des dates de début soit au 1900-01-01 soit
  antérieures à la date de création.
  */
	@Column({ type: 'date', nullable: true, name: 'date_debut' })
	dateDebut: Date | null;

	/*
  etatAdministratifEtablissement: État administratif de l’établissement.
  Lors de son inscription au répertoire, un établissement est, sauf exception, à l’état « Actif ».
  Le passage à l’état « Fermé » découle de la prise en compte d’une déclaration de fermeture.
  À noter qu’un établissement fermé peut être rouvert.
  En règle générale, la première période d’historique d’un établissement correspond à un
  etatAdministratifUniteLegale égal à « Actif ». Toutefois, l'état administratif peut être à null (première date de
  début de l'état postérieure à la première date de début d'une autre variable historisée).
  */
	@Column({
		type: 'enum',
		enum: EtatAdministratifEtablissement,
		default: EtatAdministratifEtablissement.ACTIF,
		name: 'etat_administratif_etablissement',
	})
	etatAdministratifEtablissement: EtatAdministratifEtablissement;

	/*
  enseigne1Etablissement: Première ligne d’enseigne de l’établissement

  Les trois variables enseigne1Etablissement, enseigne2Etablissement et enseigne3Etablissement
  contiennent la ou les enseignes de l'établissement.
  L'enseigne identifie l'emplacement ou le local dans lequel est exercée l'activité. Un établissement peut
  posséder une enseigne, plusieurs enseignes ou aucune.
  L'analyse des enseignes et de son découpage en trois variables dans Sirene montre deux cas possibles :
  soit les 3 champs concernent 3 enseignes bien distinctes, soit ces trois champs correspondent au
  découpage de l'enseigne qui est déclarée dans la liasse (sur un seul champ) avec une continuité des trois
  champs.
  *Exemples*
      SIRET=53053581400178
      "enseigne1Etablissement": "LES SERRURIERS DES YVELINES LES VITRIERS DES YVELI",
      "enseigne2Etablissement": "NES LES CHAUFFAGISTES DES YVELINES LES PLATRIERS D",
      "enseigne3Etablissement": "ES YVELINES LES ELECTRICIENS DES YVELINES.…"
  */
	@Column({ nullable: true, length: 100, type: 'varchar', name: 'enseigne1_etablissement' })
	enseigne1Etablissement: string | null;

	/*
  enseigne2Etablissement:  Deuxième ligne d’enseigne de l’établissement
  */
	@Column({ nullable: true, length: 50, type: 'varchar', name: 'enseigne2_etablissement' })
	enseigne2Etablissement: string | null;

	/*
  enseigne3Etablissement: Troisième ligne d’enseigne de l’établissement
  */
	@Column({ nullable: true, length: 50, type: 'varchar', name: 'enseigne3_etablissement' })
	enseigne3Etablissement: string | null;

	/*
  denominationUsuelleEtablissement:  Dénomination usuelle de l’établissement
  Cette variable désigne le nom sous lequel l'établissement est connu du grand public.
  Cet élément d'identification de l'établissement a été enregistré au niveau établissement depuis l'application
  de la norme d'échanges CFE de 2008. Avant la norme 2008, la dénomination usuelle était enregistrée au
  niveau de l'unité légale sur trois champs (cf. variables denominationUsuelle1UniteLegale à
  denominationUsuelle3UniteLegale dans le descriptif des variables du fichier StockUniteLegale).
  Cette variable est historisée.
  */
	@Column({
		nullable: true,
		length: 500,
		type: 'varchar',
		name: 'denomination_usuelle_etablissement',
	})
	denominationUsuelleEtablissement: string | null;

	/*
  activitePrincipaleEtablissement:  Activité principale de l'établissement pendant la période
  Liste de codes
    La variable nomenclatureActivitePrincipaleEtablissement indique à quelle nomenclature d'activité appartient
    le code.
    *Lien vers la Nomenclature d'Activités Française*
    https://insee.fr/fr/information/2406147
  */
	@Column({ nullable: true, length: 6, type: 'varchar', name: 'activite_principale_etablissement' })
	activitePrincipaleEtablissement: string | null;

	/*
  nomenclatureActivitePrincipaleEtablissement: Nomenclature d’activité de la variable activitePrincipaleEtablissement
  Liste de codes
    NAFRev2
    NAFRev1
    NAF1993
    NAP
  */
	@Column({
		nullable: true,
		length: 8,
		type: 'varchar',
		name: 'nomenclature_activite_principale_etablissement',
	})
	nomenclatureActivitePrincipaleEtablissement: string | null;

	/*
  caractereEmployeurEtablissement:  Caractère employeur de l’établissement
  Lors de sa formalité d’ouverture, le déclarant indique si l’établissement aura ou non des employés. Par la
  suite, le déclarant peut également faire des déclarations de prise d’emploi et de fin d’emploi. La prise en
  compte d’une déclaration de prise d’emploi bascule immédiatement l’établissement en « employeur ».
  Inversement, lorsqu’une déclaration de fin d’emploi est traitée, l’établissement devient « non employeur ».
  Pour chaque établissement, il existe à un instant donné un seul code « employeur ».
  Cette variable est historisée pour les établissements qui étaient ouverts en 2005 et pour ceux ouverts
  ultérieurement.
  */
	@Column({
		nullable: true,
		type: 'enum',
		enum: caractereEmployeur,
		name: 'caractere_employeur_etablissement',
	})
	caractereEmployeurEtablissement: caractereEmployeur | null;

	@Column({ type: 'float', nullable: true })
	longitude?: number;

	@Column({ type: 'float', nullable: true })
	latitude?: number;

	@Column({ type: 'float', nullable: true })
	geo_score?: number;

	@Column({ type: 'varchar', nullable: true })
	geo_type?: string;

	@Column({ type: 'varchar', nullable: true })
	geo_adresse?: string;

	@Column({ type: 'varchar', nullable: true })
	geo_id?: string;

	@Column({ type: 'varchar', nullable: true })
	geo_ligne?: string;

	@Column({ type: 'varchar', nullable: true })
	geo_l4?: string;

	@Column({ type: 'varchar', nullable: true })
	geo_l5?: string;

	@Column({ type: 'varchar', length: 32, nullable: true })
	hash: string | null;

	@Column({ type: 'boolean', name: 'ban_adresse', default: false, nullable: true })
	banAdresse: boolean;
}
