import { Entity, PrimaryGeneratedColumn, CreateDateColumn, JoinColumn, ManyToOne } from 'typeorm';

import { Account } from './Account';

@Entity()
export class Login {
	@PrimaryGeneratedColumn()
	id: number;

	@ManyToOne(() => Account)
	@JoinColumn({ name: 'account_id' })
	account: Account;

	@CreateDateColumn({ name: 'created_at' })
	createdAt: Date;
}
