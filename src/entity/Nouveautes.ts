import { Entity, PrimaryGeneratedColumn, Column, CreateDateColumn } from 'typeorm';

@Entity({ name: 'nouveautes' })
export class Nouveautes {
	@PrimaryGeneratedColumn()
	id: number;

	@Column()
	amount: number;

	@CreateDateColumn({ name: 'created_at' })
	createdAt: Date;
}
