import {
	Entity,
	PrimaryGeneratedColumn,
	Index,
	JoinColumn,
	ManyToOne,
	Column,
	Unique,
} from 'typeorm';

import { Territory } from './Territory';

@Entity({ name: 'mot_cle' })
@Unique('UNIQ_mot_cle_for_territoire', ['territory', 'motCle'])
export class MotCle {
	@PrimaryGeneratedColumn()
	id: number;

	@ManyToOne(() => Territory)
	@JoinColumn({ name: 'territory_id' })
	@Index()
	territory: Territory;

	@Column({ name: 'mot_cle' })
	motCle: string;
}
