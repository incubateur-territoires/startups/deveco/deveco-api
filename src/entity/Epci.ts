import { Entity, Column, PrimaryColumn, OneToMany, Index, ManyToOne, JoinColumn } from 'typeorm';

import { Commune } from './Commune';
import { Petr } from './Petr';

export type TypeEpci = 'CA' | 'EPT' | 'ME' | 'CC' | 'CU';

@Entity({ name: 'epci' })
export class Epci {
	@PrimaryColumn({ name: 'insee_epci' })
	id: string;

	@Column({ name: 'lib_epci' })
	libEpci: string;

	@Column({ name: 'nature' })
	nature: TypeEpci;

	@ManyToOne(() => Petr)
	@JoinColumn({ name: 'code_petr' })
	@Index()
	petr: Petr;

	@OneToMany(() => Commune, (commune) => commune.epci)
	communes: Commune[];
}
