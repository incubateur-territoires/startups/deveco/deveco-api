import {
	Entity,
	PrimaryGeneratedColumn,
	Column,
	OneToOne,
	JoinColumn,
	CreateDateColumn,
	UpdateDateColumn,
	Unique,
} from 'typeorm';

import { Territory } from './Territory';

@Entity()
@Unique('uniq_territory', ['territoire'])
export class TerritoryImportStatus {
	@PrimaryGeneratedColumn()
	id: number;

	@OneToOne(() => Territory, { nullable: false })
	@JoinColumn({ name: 'territoire_id' })
	territoire: Territory;

	@Column({ name: 'started', default: false })
	started: boolean;

	@Column({ name: 'done', default: false })
	done: boolean;

	@Column({ name: 'geolocation', default: false })
	geolocationDone: boolean;

	@Column({ name: 'ter', default: false })
	terDone: boolean;

	@Column({ name: 'ess', default: false })
	essDone: boolean;

	@Column({ name: 'qpv', default: false })
	qpvDone: boolean;

	@Column({ name: 'stats', default: false })
	statsDone: boolean;

	@CreateDateColumn({ name: 'created_at' })
	createdAt: Date;

	@UpdateDateColumn({ name: 'updated_at' })
	updatedAt: Date;
}
