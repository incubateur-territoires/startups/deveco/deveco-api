import { Entity, Column, Index, PrimaryColumn, ManyToOne, JoinColumn } from 'typeorm';

import { Epci } from './Epci';
import { Metropole } from './Metropole';
import { Departement } from './Departement';
import { Region } from './Region';

export type ZRR = 'C' | 'P' | 'N';

export type TypeCommune = 'COM' | 'ARM';

@Entity({ name: 'commune' })
export class Commune {
	@Column({ name: 'typecom' })
	typecom: TypeCommune;

	@PrimaryColumn({ name: 'insee_com' })
	id: string;

	@Column({ name: 'insee_dep' })
	inseeDep: string;

	@ManyToOne(() => Region, { nullable: false })
	@JoinColumn({ name: 'insee_reg' })
	@Index()
	region: Region;

	@ManyToOne(() => Departement, { nullable: false })
	@JoinColumn({ name: 'ctcd' })
	@Index()
	departement: Departement;

	@Column({ name: 'insee_arr' })
	inseeArr: string;

	@Column({ name: 'tncc' })
	tncc: string;

	@Column({ name: 'ncc' })
	ncc: string;

	@Column({ name: 'nccenr' })
	nccenr: string;

	@Column({ name: 'lib_com' })
	@Index('IDX_commune_lib_com')
	libCom: string;

	@Column({ name: 'insee_can' })
	inseeCan: string;

	@ManyToOne(() => Metropole, { createForeignKeyConstraints: false })
	@JoinColumn({ name: 'com_parent' })
	comParent?: Metropole;

	@ManyToOne(() => Epci)
	@JoinColumn({ name: 'insee_epci' })
	epci?: Epci;

	@Column({ name: 'zrr', default: 'N' })
	zrr: ZRR;
}
