import { Entity, PrimaryGeneratedColumn, Column, Unique } from 'typeorm';

@Entity({ name: 'stats' })
@Unique('UNIQ_stats_for_commune_for_day', ['codeCommune', 'date'])
export class Stats {
	@PrimaryGeneratedColumn()
	id: number;

	@Column({ name: 'code_commune', type: 'varchar', nullable: false })
	codeCommune: string;

	@Column({ type: 'date', nullable: false })
	date: Date;

	@Column({ type: 'jsonb', nullable: false })
	content: string;
}
