import {
	Entity,
	PrimaryGeneratedColumn,
	JoinColumn,
	ManyToOne,
	CreateDateColumn,
	Column,
} from 'typeorm';

import { DevEco } from './DevEco';

export type SearchContextEntity = 'createurs' | 'etablissements';
type SearchContextExport = '' | 'export-';
export type SearchContext = `${SearchContextExport}${SearchContextEntity}`;

@Entity({ name: 'search_request' })
export class SearchRequest {
	@PrimaryGeneratedColumn()
	id: number;

	@ManyToOne(() => DevEco, (deveco) => deveco.searches)
	@JoinColumn({ name: 'deveco_id' })
	deveco: DevEco;

	@Column()
	query: string;

	@Column()
	type: SearchContext;

	@CreateDateColumn({ name: 'created_at' })
	createdAt: Date;
}
