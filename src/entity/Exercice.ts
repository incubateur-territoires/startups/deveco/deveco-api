import { Entity, PrimaryGeneratedColumn, JoinColumn, ManyToOne, Column, Unique } from 'typeorm';

import { Entreprise } from './Entreprise';

@Entity({ name: 'exercice' })
@Unique('UNIQ_exercice_entreprise_date_cloture', ['dateCloture', 'entreprise'])
export class Exercice {
	@PrimaryGeneratedColumn()
	id?: number;

	@ManyToOne(() => Entreprise)
	@JoinColumn({ name: 'entreprise_id' })
	entreprise: Entreprise;

	@Column({ type: 'bigint' })
	ca: number;

	@Column({ name: 'date_cloture', type: 'date' })
	dateCloture: Date;
}
