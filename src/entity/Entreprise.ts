import { Entity, Column, PrimaryColumn, OneToMany, OneToOne, JoinColumn } from 'typeorm';

import { Etablissement } from './Etablissement';
import { Exercice } from './Exercice';

export type MandataireSocial = {
	nom: string;
	prenom: string;
	fonction: string;
	date_naissance?: string;
	date_naissance_timestamp?: number;
	dirigeant?: boolean;
	raison_sociale?: string;
	identifiant?: string;
	type: string;
};

@Entity({ name: 'entreprise' })
export class Entreprise {
	@PrimaryColumn({ name: 'siret', unique: true, generated: false })
	siret: string;

	@OneToOne(() => Etablissement, (etablissement) => etablissement.siret, { cascade: true })
	@JoinColumn({ name: 'siret' })
	etablissement: Etablissement;

	// deprecated
	@Column({ name: 'longitude', type: 'float', nullable: true })
	longitude: number;

	// deprecated
	@Column({ name: 'latitude', type: 'float', nullable: true })
	latitude: number;

	// deprecated
	@Column({ type: 'date', name: 'date_creation', nullable: true })
	dateCreation: Date | null;

	// deprecated
	@Column({ type: 'date', name: 'date_fermeture', nullable: true })
	dateFermeture: Date | null;

	@Column({ type: 'varchar', name: 'nom', nullable: true })
	nom: string;

	@Column({ type: 'varchar', nullable: true, name: 'enseigne' })
	enseigne: string | null;

	// deprecated
	@Column({ type: 'varchar', nullable: true, name: 'code_naf' })
	codeNaf: string | null;

	// deprecated
	@Column({ type: 'varchar', nullable: true, name: 'libelle_naf' })
	libelleNaf: string | null;

	// deprecated
	@Column({ type: 'varchar', nullable: true, name: 'libelle_categorie_naf' })
	libelleCategorieNaf: string | null;

	@Column({ type: 'varchar', name: 'adresse', nullable: true })
	adresse: string | null;

	// deprecated
	@Column({ type: 'varchar', name: 'code_commune', nullable: true })
	codeCommune: string | null;

	// deprecated
	@Column({ type: 'varchar', name: 'etat_administratif', nullable: true })
	etatAdministratif: string | null;

	@Column({ type: 'varchar', name: 'forme_juridique', nullable: true })
	formeJuridique: string | null;

	@Column({ type: 'varchar', nullable: true })
	effectifs: string | null;

	@Column({ name: 'date_effectifs', type: 'date', nullable: true })
	dateEffectifs: Date | null;

	@OneToMany(() => Exercice, (exercice) => exercice.entreprise)
	exercices: Exercice[];

	@Column({
		name: 'mandataires_sociaux',
		type: 'jsonb',
		array: false,
		default: () => "'[]'",
		nullable: true,
	})
	mandatairesSociaux: Array<MandataireSocial>;

	@Column({ type: 'timestamptz', name: 'last_api_update', nullable: true })
	lastApiUpdate: Date | null;

	@Column({ type: 'boolean', name: 'siege_social', default: false })
	siegeSocial: boolean;
}
