import {
	Column,
	Entity,
	Index,
	JoinColumn,
	JoinTable,
	ManyToMany,
	ManyToOne,
	PrimaryColumn,
	Unique,
} from 'typeorm';

import { ZRR } from './Commune';
import { Qpv } from './Qpv';
import { Territory } from './Territory';
import { Zonage } from './Zonage';

@Entity({ name: 'territory_etablissement_reference' })
@Unique('UNIQ_siret_for_territory', ['territory', 'siret'])
export class TerritoryEtablissementReference {
	// TODO write in code that it's generated from T.id-SIRET instead
	@PrimaryColumn({ name: 'id', primaryKeyConstraintName: 'PK_territory_etablissement_reference' })
	id: string;

	@ManyToOne(() => Territory, { nullable: false })
	@JoinColumn({ name: 'territory_id', foreignKeyConstraintName: 'FK_ter_eta_ref_territory_id' })
	@Index()
	territory: Territory;

	@Column({ length: 14 })
	@Index()
	siret: string;

	@Index('index_nom_trig', { synchronize: false })
	@Column({ name: 'nom_recherche' })
	nomRecherche: string;

	@Column({ name: 'etat_administratif' })
	@Index()
	etatAdministratif: string;

	@Column({ name: 'libelle_naf', nullable: true })
	libelleNaf: string;

	@Column({ name: 'code_naf', nullable: true })
	@Index()
	codeNaf: string;

	@Column({ name: 'categorie_juridique_courante' })
	@Index()
	categorieJuridique: boolean;

	@Column({ name: 'tranche_effectifs', nullable: true })
	@Index()
	trancheEffectifs: string;

	@Column({ name: 'code_commune' })
	@Index()
	codeCommune: string;

	@Index('idx_ter_eta_ref_tsv', { synchronize: false })
	@Column({
		type: 'tsvector',
		generatedType: 'STORED',
		asExpression: "to_tsvector('french', nom_recherche)",
	})
	tsv: string[];

	@Column({ type: 'date', name: 'date_creation', nullable: true })
	@Index()
	dateCreation: Date | null;

	@Column({ type: 'varchar', nullable: true, name: 'adresse_complete' })
	adresseComplete: string | null;

	@Column({ default: false })
	@Index()
	exogene: boolean;

	@ManyToOne(() => Qpv)
	@Index()
	@JoinColumn({ name: 'qpv_id' })
	qpv: Qpv;

	@Index()
	@Column({ name: 'zrr', default: 'N' })
	zrr: ZRR;

	@Index()
	@Column({ name: 'ess', default: false })
	ess: boolean;

	@ManyToMany(() => Zonage)
	@JoinTable({
		name: 'territory_etablissement_reference_zonages_zonage',
		joinColumn: {
			name: 'territory_etablissement_reference_id',
			referencedColumnName: 'id',
		},
		inverseJoinColumn: {
			name: 'zonage_id',
			referencedColumnName: 'id',
		},
	})
	zonages: Zonage[];
}
