import { Entity, Column, PrimaryColumn, OneToMany } from 'typeorm';

import { Commune } from './Commune';

@Entity({ name: 'metropole' })
export class Metropole {
	@PrimaryColumn({ name: 'insee_com' })
	id: string;

	@Column({ name: 'lib_com' })
	libCom: string;

	@OneToMany(() => Commune, (commune) => commune.comParent)
	communes: Commune[];
}
