import {
	Entity,
	PrimaryGeneratedColumn,
	Column,
	OneToOne,
	JoinColumn,
	CreateDateColumn,
	UpdateDateColumn,
	Unique,
} from 'typeorm';

import { Commune } from './Commune';
import { Perimetre } from './Perimetre';
import { Departement } from './Departement';
import { Epci } from './Epci';
import { Metropole } from './Metropole';
import { Petr } from './Petr';
import { Region } from './Region';
import { TerritoryImportStatus } from './TerritoryImportStatus';

import { GeographyTypeForPerimetre } from '../service/TerritoireService';

export type TerritoryTypeInsee =
	| Commune['typecom']
	| Epci['nature']
	| 'departement'
	| 'region'
	| 'metropole'
	| 'petr'
	| `groupement de ${GeographyTypeForPerimetre}`;

@Entity()
@Unique('uniq_territory_name', ['name'])
@Unique('uniq_territory_epci_id', ['epci', 'name'])
@Unique('uniq_territory_metropole_id', ['metropole', 'name'])
@Unique('uniq_territory_petr_id', ['petr', 'name'])
@Unique('uniq_territory_departement_id', ['departement', 'name'])
@Unique('uniq_territory_region_id', ['region', 'name'])
@Unique('uniq_territory_commune_id', ['commune', 'name'])
export class Territory {
	@PrimaryGeneratedColumn()
	id: number;

	@Column({ name: 'territory_type' })
	territoryType: TerritoryTypeInsee;

	@Column({})
	name: string;

	@OneToOne(() => Epci, { createForeignKeyConstraints: false, nullable: true })
	@JoinColumn({ name: 'epci_id' })
	epci: Epci;

	@OneToOne(() => Metropole, { createForeignKeyConstraints: false, nullable: true })
	@JoinColumn({ name: 'metropole_id' })
	metropole: Metropole;

	@OneToOne(() => Petr, { createForeignKeyConstraints: false, nullable: true })
	@JoinColumn({ name: 'petr_id' })
	petr: Petr;

	@OneToOne(() => Departement, { createForeignKeyConstraints: false, nullable: true })
	@JoinColumn({ name: 'departement_id' })
	departement: Departement;

	@OneToOne(() => Region, { createForeignKeyConstraints: false, nullable: true })
	@JoinColumn({ name: 'region_id' })
	region: Region;

	@OneToOne(() => Commune, { createForeignKeyConstraints: false, nullable: true })
	@JoinColumn({ name: 'commune_id' })
	commune: Commune;

	@OneToOne(() => Perimetre, { createForeignKeyConstraints: false, nullable: true })
	@JoinColumn({ name: 'perimetre_id' })
	perimetre: Perimetre;

	@OneToOne(
		() => TerritoryImportStatus,
		(territoryImportStatus) => territoryImportStatus.territoire
	)
	territoryImportStatus: TerritoryImportStatus;

	@CreateDateColumn({ name: 'created_at' })
	createdAt: Date;

	@UpdateDateColumn({ name: 'updated_at' })
	updatedAt: Date;
}
