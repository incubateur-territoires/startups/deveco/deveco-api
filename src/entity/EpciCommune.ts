import { Entity, Column, PrimaryColumn } from 'typeorm';

@Entity({ name: 'epci_commune' })
export class EpciCommune {
	@PrimaryColumn({ name: 'insee_com' })
	insee_com: string;

	@Column({ name: 'insee_epci' })
	insee_epci: string;

	@Column({ name: 'insee_dep' })
	insee_dep: string;

	@Column({ name: 'insee_reg' })
	insee_reg: string;
}
