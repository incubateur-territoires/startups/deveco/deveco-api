import { Entity, PrimaryGeneratedColumn, Column, CreateDateColumn } from 'typeorm';

@Entity()
export class LocalArchive {
	@PrimaryGeneratedColumn()
	id: number;

	@Column('jsonb')
	content: string;

	@CreateDateColumn({ name: 'created_at' })
	createdAt: Date;
}
