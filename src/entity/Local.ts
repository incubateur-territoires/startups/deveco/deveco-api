import {
	Entity,
	PrimaryGeneratedColumn,
	JoinColumn,
	ManyToOne,
	Column,
	CreateDateColumn,
	UpdateDateColumn,
	Unique,
	OneToMany,
} from 'typeorm';

import { DevEco } from './DevEco';
import { Proprietaire } from './Proprietaire';
import { Territory } from './Territory';

export type LocalStatut = 'occupe' | 'vacant' | 'sans-objet';

export type LocalType =
	| 'commerce'
	| 'bureaux'
	| 'ateliersartisanaux'
	| 'batimentsindustriels'
	| 'entrepot'
	| 'terrain';

@Entity({ name: 'local' })
@Unique('titre+adresse', ['titre', 'adresse'])
export class Local {
	@PrimaryGeneratedColumn()
	id?: number;

	@Column({ name: 'titre' })
	titre: string;

	@Column({ name: 'adresse' })
	adresse: string;

	@Column({ name: 'ville', nullable: true })
	ville: string;

	@Column({ name: 'code_postal', nullable: true })
	codePostal: string;

	@Column({ name: 'geolocation', nullable: true })
	geolocation: string;

	@Column('simple-array', { name: 'local_types' })
	localTypes: LocalType[];

	@Column({ name: 'local_statut' })
	localStatut: LocalStatut;

	@Column({ name: 'surface', nullable: true })
	surface: string;

	@Column({ name: 'loyer', nullable: true })
	loyer: string;

	@Column('simple-array', { name: 'localisations', nullable: true })
	localisations: string[];

	@Column({ name: 'commentaire', nullable: true })
	commentaire: string;

	@OneToMany(() => Proprietaire, (proprietaire) => proprietaire.local)
	proprietaires?: Proprietaire[];

	@ManyToOne(() => Territory)
	@JoinColumn({ name: 'territoire_id' })
	territoire: Territory;

	@ManyToOne(() => DevEco)
	@JoinColumn({ name: 'auteur_modification_id' })
	auteurModification: DevEco;

	@Column({ name: 'date_modification', type: 'timestamptz' })
	dateModification: Date;

	@ManyToOne(() => DevEco)
	@JoinColumn({ name: 'deveco_id' })
	createur: DevEco;

	@CreateDateColumn({ name: 'created_at' })
	createdAt: Date;

	@UpdateDateColumn({ name: 'updated_at' })
	updatedAt: Date;
}
