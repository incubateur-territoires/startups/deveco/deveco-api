export enum DiffusionStatut {
	O = 'O', // unité légale faisant partie de la diffusion publique
	N = 'N', // personne physique ayant demandé à être exclue de la diffusion publique
	P = 'P', // diffusion partielle
}

export enum Sexe {
	FEMININ = 'F',
	MASCULIN = 'M',
	ND = '[ND]',
}

export enum CategorieEntreprise {
	PME = 'PME', //  petite ou moyenne entreprise
	ETI = 'ETI', // entreprise de taille intermédiaire
	GE = 'GE', //grande entreprise
}

export enum EtatAdministratif {
	ACTIVE = 'A',
	CESSEE = 'C',
}

export enum EtatAdministratifEtablissement {
	ACTIF = 'A',
	FERME = 'F',
}

export enum ecoSocSol {
	O = 'O', // l'entreprise appartient au champ de l’économie sociale et solidaire
	N = 'N', // l'entreprise n'appartient pas au champ de l’économie sociale et solidaire
}

export enum societeMissionUniteLegale {
	O = 'O', // l'entreprise appartient au champ des sociétés à mission
	N = 'N', // l'entreprise n'appartient pas au champ des sociétés à mission
}

export enum caractereEmployeur {
	O = 'O', // unité légale employeuse
	N = 'N', // unité légale non employeuse
}
