import {
	Entity,
	PrimaryGeneratedColumn,
	JoinColumn,
	ManyToOne,
	Column,
	OneToOne,
	OneToMany,
	CreateDateColumn,
	UpdateDateColumn,
	Unique,
	Index,
} from 'typeorm';

import { Demande } from './Demande';
import { Entite } from './Entite';
import { DevEco } from './DevEco';
import { Echange } from './Echange';
import { Rappel } from './Rappel';
import { Territory } from './Territory';

@Entity({ name: 'fiche' })
@Unique('UNIQ_fiche_territoire_entite', ['territoire', 'entite'])
export class Fiche {
	@PrimaryGeneratedColumn()
	id: number;

	@OneToOne(() => Entite, (entite) => entite.fiche)
	@JoinColumn({ name: 'entite_id' })
	@Index()
	entite: Entite;

	@ManyToOne(() => Territory)
	@JoinColumn({ name: 'territoire_id' })
	@Index()
	territoire: Territory;

	@ManyToOne(() => DevEco)
	@JoinColumn({ name: 'auteur_modification_id' })
	auteurModification?: DevEco;

	@ManyToOne(() => DevEco)
	@JoinColumn({ name: 'deveco_id' })
	createur: DevEco;

	@OneToMany(() => Echange, (echange) => echange.fiche, { cascade: true })
	echanges: Echange[];

	@OneToMany(() => Rappel, (rappel) => rappel.fiche, { cascade: true })
	rappels?: Rappel[];

	@OneToMany(() => Demande, (demande) => demande.fiche, { cascade: true })
	demandes?: Demande[];

	// deprecated?
	@Column({ name: 'date_modification', type: 'timestamptz' })
	dateModification: Date;

	@CreateDateColumn({ name: 'created_at' })
	createdAt: Date;

	@UpdateDateColumn({ name: 'updated_at' })
	updatedAt: Date | null;
}
