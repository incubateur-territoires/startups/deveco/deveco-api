import { Entity, Column, ManyToOne, JoinColumn, PrimaryGeneratedColumn } from 'typeorm';

import { DevEco } from './DevEco';

@Entity({ name: 'geo_token' })
export class GeoToken {
	@PrimaryGeneratedColumn()
	id: number;

	@Column()
	token: string;

	@ManyToOne(() => DevEco)
	@JoinColumn({ name: 'deveco_id' })
	deveco: DevEco;

	@Column({ default: true })
	active: boolean;
}
