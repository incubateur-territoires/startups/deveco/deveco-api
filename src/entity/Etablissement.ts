import { Entity, Column, PrimaryColumn, Index } from 'typeorm';

export type EtablissementColumns =
	| 'siret'
	| 'enseigne'
	| 'longitude'
	| 'latitude'
	| 'nom_public'
	| 'nom_recherche'
	| 'date_creation'
	| 'date_creation_entreprise'
	| 'adresse_complete'
	| 'date_fermeture';

export const mappings: Partial<Record<keyof Etablissement, EtablissementColumns>> = {
	siret: 'siret',
	enseigne: 'enseigne',
	longitude: 'longitude',
	latitude: 'latitude',
	nomPublic: 'nom_public',
	nomRecherche: 'nom_recherche',
	dateCreation: 'date_creation',
	dateCreationEntreprise: 'date_creation_entreprise',
	dateFermeture: 'date_fermeture',
	adresseComplete: 'adresse_complete',
} as const;

@Entity({ name: 'etablissement' })
export class Etablissement {
	@Column({ type: 'varchar', length: 9 })
	siren: string;

	@PrimaryColumn({ length: 14, name: 'siret', unique: true, generated: false })
	siret: string;

	@Column({ type: 'varchar', nullable: true, name: mappings['nomPublic'] })
	nomPublic: string | null;

	@Column({ type: 'varchar', nullable: true, name: mappings['nomRecherche'] })
	nomRecherche: string | null;

	@Column({ type: 'varchar', nullable: true, name: 'enseigne' })
	enseigne: string | null;

	@Column({ type: 'varchar', nullable: true, name: mappings['adresseComplete'] })
	adresseComplete: string | null;

	// TODO ça ne devrait pas être nul, mais c'est le cas pour 6 établissements en prod (tous à Arras)
	@Column({ type: 'varchar', nullable: true, name: 'code_commune' })
	@Index()
	codeCommune: string | null;

	@Column({ name: 'longitude', type: 'float', nullable: true })
	longitude: number;

	@Column({ name: 'latitude', type: 'float', nullable: true })
	latitude: number;

	@Column({ type: 'varchar', length: 2, nullable: true, name: 'tranche_effectifs' })
	trancheEffectifs: string | null;

	@Column({ nullable: true, length: 9, name: 'annee_effectifs' })
	anneeEffectifs: string;

	@Column({ type: 'varchar', nullable: true, name: 'sigle' })
	sigle: string | null;

	@Column({ type: 'varchar', nullable: true, name: 'code_naf' })
	codeNaf: string | null;

	@Column({ type: 'varchar', nullable: true, name: 'libelle_naf' })
	libelleNaf: string | null;

	@Column({ type: 'varchar', nullable: true, name: 'libelle_categorie_naf' })
	libelleCategorieNaf: string | null;

	@Column({ type: 'date', name: mappings['dateCreation'], nullable: true })
	dateCreation: Date | null;

	@Column({ type: 'date', name: mappings['dateCreationEntreprise'], nullable: true })
	dateCreationEntreprise: Date | null;

	@Column({ type: 'date', name: mappings['dateFermeture'], nullable: true })
	dateFermeture: Date | null;

	@Column({
		name: 'etat_administratif',
	})
	etatAdministratif: string;

	@Column({ name: 'categorie_juridique' })
	categorieJuridique: string;

	// personne_physique ou personne_morale
	@Column({ type: 'varchar', nullable: true, name: 'type_etablissement' })
	typeEtablissement: string | null;

	@Column({ type: 'boolean', name: 'siege_social', default: false })
	siegeSocial: boolean;
}
