import { Entity, Column, PrimaryColumn } from 'typeorm';

@Entity({ name: 'geoloc_entreprise' })
export class GeolocEntreprise {
	@PrimaryColumn({})
	siret: string;

	@Column({ type: 'double precision', nullable: true })
	x: number;

	@Column({ type: 'double precision', nullable: true })
	y: number;

	@Column({ name: 'qualite_xy', nullable: true })
	qualiteXY: string;

	@Column({ nullable: true })
	epsg: string;

	@Column({ name: 'plg_qp', nullable: true })
	plgQp: string;

	@Column({ name: 'plg_iris', nullable: true })
	plgIris: string;

	@Column({ name: 'plg_zus', nullable: true })
	plgZus: string;

	@Column({ name: 'plg_zfu', nullable: true })
	plgZfu: string;

	@Column({ name: 'plg_qva', nullable: true })
	plgQva: string;

	@Column({ name: 'plg_code_commune', nullable: true })
	plgCodeCommune: string;

	@Column({ name: 'distance_precision', nullable: true })
	distancePrecision: string;

	@Column({ name: 'qualite_qp', nullable: true })
	qualiteQp: string;

	@Column({ name: 'qualite_iris', nullable: true })
	qualiteIris: string;

	@Column({ name: 'qualite_zus', nullable: true })
	qualiteZus: string;

	@Column({ name: 'qualite_zfu', nullable: true })
	qualiteZfu: string;

	@Column({ name: 'qualite_qva', nullable: true })
	qualiteQva: string;

	@Column({ name: 'x_longitude', type: 'double precision', nullable: true })
	xLongitude: number;

	@Column({ name: 'y_latitude', type: 'double precision', nullable: true })
	yLatitude: number;
}
