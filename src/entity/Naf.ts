import { Entity, Column, PrimaryColumn } from 'typeorm';

@Entity({ name: 'naf' })
export class Naf {
	@Column({ name: 'id_1' })
	id1: string;

	@Column({ name: 'label_1' })
	label1: string;

	@Column({ name: 'id_2' })
	id2: string;

	@Column({ name: 'label_2' })
	label2: string;

	@Column({ name: 'id_3' })
	id3: string;

	@Column({ name: 'label_3' })
	label3: string;

	@Column({ name: 'id_4' })
	id4: string;

	@Column({ name: 'label_4' })
	label4: string;

	@PrimaryColumn({ name: 'id_5' })
	id5: string;

	@Column({ name: 'label_5' })
	label5: string;
}
