import 'dotenv/config';
import 'reflect-metadata';
import { DataSource } from 'typeorm';

const source = process.env.IS_BUILT ? 'dist' : 'src';
const ext = process.env.IS_BUILT ? 'js' : 'ts';

const AppDataSource = new DataSource({
	type: 'postgres',
	host: process.env.POSTGRES_HOST || 'localhost',
	port: process.env.POSTGRES_PORT ? parseInt(process.env.POSTGRES_PORT) : 5432,
	username: process.env.POSTGRES_USERNAME || process.env.POSTGRES_USER || 'deveco',
	password: process.env.POSTGRES_PASSWORD || 'deveco',
	database: process.env.POSTGRES_DB || 'deveco',
	entities: [`${source}/entity/**/*.${ext}`],
	migrationsTableName: 'migration_table',
	migrations: [`${source}/migration/**/*.${ext}`],
	extra: {
		...(process.env.TYPEORM_DRIVER_EXTRA ? JSON.parse(process.env.TYPEORM_DRIVER_EXTRA) : {}),
		poolSize: 20,
	},
	synchronize: false,
	logging: false,
});

// to initialize initial connection with the database, register all entities
// and "synchronize" database schema, call "initialize()" method of a newly created database
// once in your application bootstrap
export default AppDataSource;
