#!/bin/bash

set -o allexport
source ../.env
set +o allexport

export PGPASSWORD=$POSTGRES_PASSWORD

pg_dump -h $POSTGRES_HOST -p $POSTGRES_PORT -U $POSTGRES_USER -d $POSTGRES_DB --inserts > seed/init.sql