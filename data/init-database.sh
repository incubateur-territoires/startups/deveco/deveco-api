#!/bin/bash

set -o allexport
source ../.env
set +o allexport

export PGPASSWORD=$POSTGRES_PASSWORD

psql -h $POSTGRES_HOST -p $POSTGRES_PORT -U $POSTGRES_USER -d $POSTGRES_DB -c 'create extension "uuid-ossp";'

psql -h $POSTGRES_HOST -p $POSTGRES_PORT -U $POSTGRES_USER -d $POSTGRES_DB -c 'create extension "pg_trgm";'
